'use strict';
/* global request */

var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var CurrentSite = require('dw/system/Site').current;
var Transaction = require('dw/system/Transaction');
var StringUtils = require('dw/util/StringUtils');
var collections = require('*/cartridge/scripts/util/collections');

var SETTINGS = {
    objectType: 'AbandonedCart',
    trackAbandonedGuestCart: CurrentSite.getCustomPreferenceValue('trackAbandonedGuestCart') || false,
    trackAbandonedCustomerCart: CurrentSite.getCustomPreferenceValue('trackAbandonedCustomerCart') || false
};

/**
 * Adds or updates AbandonedCart custom object from dw.order.Basket.
 * @param {dw.order.Basket} basket - the current basket object
 */
function upsert(basket) {
    // early quit if basket is null
    if (!basket) {
        return;
    }

    // early quit when guest cart is disabled and basket is a guest basket
    if (!SETTINGS.trackAbandonedGuestCart && basket.customer && !basket.customer.authenticated) {
        return;
    }

    var basketId = basket.UUID;
    var customerLocale = request.locale;
    var isCustomerAuthenticated = (basket.customer && basket.customer.authenticated && basket.customer.profile);

    if (!customerLocale) {
        return;
    }

    var customerEmail = basket.customerEmail ||
        (isCustomerAuthenticated ? basket.customer.profile.email : null);

    // early quit when customerEmail is empty
    if (!customerEmail) {
        return;
    }

    var customerNo = basket.customerNo ||
        (isCustomerAuthenticated ? basket.customer.profile.customerNo : null);

    var customerName = basket.customerName ||
        (isCustomerAuthenticated ? StringUtils.format('{0} {1}', basket.customer.profile.firstName, basket.customer.profile.lastName) : null);

    if (!customerName) {
        var shippingAddress = basket.defaultShipment && basket.defaultShipment.shippingAddress ? basket.defaultShipment.shippingAddress : null;
        if (shippingAddress && shippingAddress.fullName) {
            customerName = shippingAddress.fullName;
        }

        if (!customerName) {
            customerName = basket.billingAddress ? basket.billingAddress.fullName : null;
        }

        // early quit when customerName is empty
        if (!customerName) {
            return;
        }
    }

    var cartLineItems = [];
    var productLineItems = basket.productLineItems;
    if (!productLineItems || productLineItems.empty || !productLineItems.length) {
        return;
    }

    collections.forEach(productLineItems, function (pli) {
        var product = pli.product;

        if (!product) {
            return;
        }

        var productId = product.ID;
        var productName = product.name;
        var productQuantity = pli.quantity.value;
        var image = product.getImage('medium');
        var productImage = image ? image.absURL.toString() : '';

        cartLineItems.push({
            productId: productId,
            productName: productName,
            productQuantity: productQuantity,
            productImage: productImage,
            productBasePrice: pli.basePrice.value,
            productGrossPrice: pli.grossPrice.value
        });
    });

    var abandonedCartObject = CustomObjectMgr.getCustomObject(SETTINGS.objectType, basketId);

    if (cartLineItems.length) {
        Transaction.wrap(function () {
            if (!abandonedCartObject) {
                abandonedCartObject = CustomObjectMgr.createCustomObject(SETTINGS.objectType, basketId);
            }

            abandonedCartObject.custom.customerNo = customerNo;
            abandonedCartObject.custom.customerEmail = customerEmail;
            abandonedCartObject.custom.customerName = customerName;
            abandonedCartObject.custom.customerLocale = customerLocale;
            abandonedCartObject.custom.currencyCode = basket.currencyCode;
            abandonedCartObject.custom.adjustedTotalPrice = basket.getAdjustedMerchandizeTotalPrice().value;
            abandonedCartObject.custom.lineItems = JSON.stringify(cartLineItems, null, 3);
        });
    } else if (!abandonedCartObject) {
        Transaction.wrap(function () {
            CustomObjectMgr.remove(abandonedCartObject);
        });
    }
}

/**
 * Removes AbandonedCart custom object when dw.order.Order is created from dw.order.Basket.
 * @param {string} basketUUID - the abandoned cart uuid
 */
function remove(basketUUID) {
    // early quit if basketUUID is null
    if (!basketUUID) {
        return;
    }

    // early quit when both disabled
    if (!SETTINGS.trackAbandonedCustomerCart && !SETTINGS.trackAbandonedGuestCart) {
        return;
    }

    var abandonedCartObject = CustomObjectMgr.getCustomObject(SETTINGS.objectType, basketUUID);

    if (abandonedCartObject) {
        Transaction.wrap(function () {
            CustomObjectMgr.remove(abandonedCartObject);
        });
    }
}

/**
 * Process Current Basket to upsert abandoned cart custom object
 */
function processBasket() {
    // early quit when both tracking are disabled
    if (!SETTINGS.trackAbandonedCustomerCart && !SETTINGS.trackAbandonedGuestCart) {
        return;
    }

    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        return;
    }

    upsert(currentBasket);
}

module.exports = {
    remove: remove,
    processBasket: processBasket
};
