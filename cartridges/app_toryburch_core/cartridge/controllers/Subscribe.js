'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.replace('ShowModalForm', server.middleware.https, csrfProtection.validateRequest, csrfProtection.generateToken, function (req, res, next) {
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var Resource = require('dw/web/Resource');

    var formImage = req.querystring.formImage;
    var formImageAltText = req.querystring.formImageAltText;
    var formButtonDisplayName = req.querystring.formButtonDisplayName;

    var template = 'subscribe/subscribe.isml';
    var subscribeForm = server.forms.getForm('subscribe');
    subscribeForm.hideAcceptTermsConditions = true;
    subscribeForm.hideOptinlist = true;

    var context = {
        headerText: Resource.msg('quickview.subscribe.form.header', 'homepage', null),
        closeButtonText: Resource.msg('link.quickview.close', 'product', null),
        template: template,
        subscribeForm: subscribeForm,
        formImage: formImage,
        formImageAltText: formImageAltText,
        formButtonDisplayName: formButtonDisplayName
    };

    res.setViewData(context);

    var viewData = res.getViewData();
    var renderedTemplate = renderTemplateHelper.getRenderedHtml(viewData, viewData.template);

    res.json({
        renderedTemplate: renderedTemplate
    });


    next();
});

module.exports = server.exports();
