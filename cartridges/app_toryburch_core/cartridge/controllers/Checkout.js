'use strict';

var server = require('server');

server.extend(module.superModule);

server.append('Begin',
    function (req, res, next) {
        var addressHelper = require('*/cartridge/scripts/helpers/addressHelpers');

        var cityData = addressHelper.getCities(req.locale.id);

        res.setViewData({
            cityData: cityData
        });

        next();
    }
);

module.exports = server.exports();
