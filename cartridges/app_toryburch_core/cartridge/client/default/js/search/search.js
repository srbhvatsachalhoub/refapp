'use strict';

var base = require('brand_core/search/search');
var urlHelper = require('brand_core/helpers/urlHelper');
var commonHelpers = require('app_brand_core/cartridge/scripts/helpers/commonHelpers');

/**
 * Returns products count to be shown in search result
 * @return {number} productsCount - ProductsCount
 */
function getProductsCount() {
    var searchResultProductsCount = $('.js-refinement-container').data('searchResultProductsCount');
    if (searchResultProductsCount && searchResultProductsCount.productsCount) {
        var width = $(window).width();
        var sz = searchResultProductsCount.productsCount.desktop;

        if (commonHelpers.isTabletView(width, window.RA_BREAKPOINTS)) {
            sz = searchResultProductsCount.productsCount.tablet;
        }
        if (commonHelpers.isMobileView(width, window.RA_BREAKPOINTS)) {
            sz = searchResultProductsCount.productsCount.mobile;
        }
        return sz;
    }
    return null;
}

/**
 * Performs an ajax request with supplied refinement url
 * and updates DOM with incoming response.
 *
 * @param {string} url - Refinement url
 * @param {string} type - Type of the request; refinement, sort, etc.
 * @param {boolean} disablePushState - If true, new refine url won't be pushed to history
 */
function applyFilter(url, type, disablePushState) {
    var $doc = $(document);
    var event = 'search:' + type;
    window.isContentLoading = true;

    $doc.trigger(event + ':start', url);
    $.ajax({
        method: 'GET',
        url: url,
        data: {
            page: $('.grid-footer').data('page-number'),
            selectedUrl: url
        },
        success: function (response) {
            var res = {
                response: response,
                url: url,
                type: type,
                disablePushState: disablePushState
            };
            $doc.trigger(event + ':success', res);
            $doc.trigger('search:success', res);
            window.isContentLoading = false;
        },
        error: function () {
            window.isContentLoading = false;
            $doc.trigger(event + ':error');
        }
    });
}

/**
 * Returns push state parameters
 *
 * @param {Object} result - Result of the apply filter method that is triggered by success event
 * @param {string} url - New url that will be pushed to history
 */
function pushState(result, url) {
    if (!result.disablePushState && window.location.href !== url) {
        history.pushState({
            refineUrl: decodeURIComponent(result.url),
            type: result.type
        }, '', decodeURIComponent(url));
    }
}

/**
 * @description init lazy load
 */
function initLazyLoad() {
    window.isContentLoading = false;

    $(document).on('scroll', function () {
        var targetElement = $('.js-search-results');
        var triggerButton = $('.show-more button');
        if (triggerButton.length) { // If false, downloaded all products
            var triggerPosition = (targetElement.innerHeight() + targetElement.offset().top) / 1.5;
            var userPosition = window.innerHeight + window.scrollY;

            if (!window.isContentLoading && (userPosition > triggerPosition)) {
                triggerButton.trigger('click');
            }
        }
    });
}

base.showMore = function () {
    // Show more products
    var sz = getProductsCount();

    // Init lazy load
    initLazyLoad();

    $(document)
        .on('click', '.show-more button', function (e) {
            e.stopPropagation();
            e.preventDefault();

            // Get paging url from the value and call it
            var url = $(this).data('url');
            if (sz) {
                // update url if sz is defined (config from BM)
                url = urlHelper.updateViewMoreUrl(url, sz);
            }
            applyFilter(url, 'showMore');
        })
        .on('search:showMore:success', function (e, result) {
            // Get start and size parameters from paging url
            var start = urlHelper.getParameter('start', result.url);
            var size = urlHelper.getParameter('sz', result.url);

            // Set size parameters to current url, as sum of start and size
            var url = urlHelper.setParameter('sz', (+size + +start));

            // Update url on address bar
            pushState(result, url);

            // Update DOM
            $('.grid-footer').replaceWith(result.response);
        });
};

module.exports = base;
