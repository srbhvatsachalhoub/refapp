import InputSelect from './InputSelect';

/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const select2 = require('select2');
const emitter = eventMgr.getEmitter('customSelect');

/*
* Usage:
* Set data attribute to select tag: data-cmp="customSelect" to initialize component;
* Set class name "select" select tag
* Set data-placeholder="Placeholder text" to select tag and add empty option first
* Set data attribute to option tag: data-option-label="Sold Out Online" to pass label for the option;
*/
export default class CustomSelect extends InputSelect {
    get configDefault() {
        return {
            minimumResultsForSearch: Infinity,
            width: '100%',
            dir: window.RA_DIRECTION,
            templateResult: this.formatOption,
            initOnDevice: ['medium', 'large', 'extra-large']
        };
    }

    init() {
        super.init();
        this.initEvents();
    }

    initEvents() {
        this.$el.select2(this.config);
        emitter.emit('inited');
    }

    update() {
        emitter.emit('updated');
    }

    formatOption(state) {
        var $text;

        if (state.element && state.element.dataset && state.element.dataset.optionLabel) {
            $text = $(' <span class="select2-results__option-text">' +
                            '<span class="select2-results__option-text-main">' + state.text + '</span>' +
                            '<span class="select2-results__option-label">' + state.element.dataset.optionLabel + '</span>' +
                      ' </span>'
            );

            return $text;
        }

        return state.text;
    }
}
