'use strict';

module.exports = function () {
    var $emailDescription = $('.js-email-description');

    $('.js-newsletter-email').on('input', function () {
        if (!$emailDescription.hasClass('d-none')) {
            return;
        }

        if ($('.js-newsletter-email:valid').length) {
            $emailDescription.removeClass('d-none');
        }
    });
};
