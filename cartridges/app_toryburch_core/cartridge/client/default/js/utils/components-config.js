'use strict';

/** The references object of all modules needed for components initialization */
var references = {
    /** Components */
    customSelect: require('../components/forms/CustomSelect').default
};

// eslint-disable-next-line
/**
 * The components initialization configuration object
 *
 * @example New "Page" configuration
 *  var configuration = {
 *      //...
 *      newpage : {
 *          enabled : true,
 *          options : {},
 *          components : {
 *              pagination : {
 *                  enabled : false,
 *                  options : {}
 *              }
 *          }
 *      }
 *  }
 */
var configuration = {
    global: {
        components: {
            customSelect: {}
        }
    }
};

module.exports = {
    configuration: configuration,
    references: references
};
