'use strict';

var layout = require('../layout').init();

var $cache = {
    $body: $('body'),
    $html: $('html')
};

/**
* init events
* @param {jQuery} $element - $element
*/
function initEvents() {
    $(document).on('show.bs.dropdown', '.js-dropdown-parent', function (event) {
        var $this = $(this);
        var config = $this.data('jsonConfig');

        if (config && config.dropdownAsModalOnMobile && layout.isMobileView()) {
            $(event.relatedTarget).attr('data-toggle', '');
            $cache.$html.addClass('modal-open-state');
            $cache.$body.addClass('modal-open');
        }
    });

    $(document).on('hide.bs.dropdown', '.js-dropdown-parent', function () {
        var $this = $(this);
        var config = $this.data('jsonConfig');

        if (config && config.dropdownAsModalOnMobile && layout.isMobileView()) {
            $cache.$html.removeClass('modal-open-state');
            $cache.$body.removeClass('modal-open');
        }
    });

    $(document).on('click', '.js-dropdown-modal-close', function () {
        var $this = $(this);

        $this.closest('.js-dropdown-parent').find('.js-dropdown-toggle').attr('data-toggle', 'dropdown').dropdown('toggle');
        $this.closest('.js-dropdown-menu').removeClass('show');
        $cache.$html.removeClass('modal-open-state');
        $cache.$body.removeClass('modal-open');
    });

    $(document).on('click', function (e) {
        var $dropdownMenu = $('.js-dropdown-menu');
        if (!$dropdownMenu.is(e.target) && $dropdownMenu.has(e.target).length === 0) {
            $dropdownMenu.removeClass('show');
        }
    });

    $(document).on('click', '.js-dropdown-toggle', function () {
        if ($(this).attr('aria-expanded') === 'false') {
            $('.js-dropdown-menu').removeClass('show');
        }
    });

    $(document).on('click.bs.dropdown.data-api', '.js-refinement-container', function (e) {
        e.stopPropagation();
    });
}

/**
 * init component
 */
function init() {
    initEvents();
}

module.exports = {
    init: init
};
