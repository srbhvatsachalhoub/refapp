'use strict';

var formHelpers = require('./formErrors');
var util = require('brand_core/util');
var notificationHelper = require('brand_core/helpers/notificationHelper')('.shipping-error');

var baseShipping = require('base/checkout/shipping');

baseShipping.selectSingleShipAddress = function () {
    $('.single-shipping .addressSelector').on('change', function () {
        var form = $(this).parents('form')[0];
        var selectedOption = $('option:selected', this);
        var attrs = selectedOption.data();
        var shipmentUUID = selectedOption[0].value;
        var originalUUID = $('input[name=shipmentUUID]', form).val();
        var element;

        Object.keys(attrs).forEach(function (attr) {
            element = attr === 'countryCode' ? 'country' : attr;
            $('[name$=' + element + ']', form).val(attrs[attr]);

            if (element === 'phone') {
                $('input[data-target$=_phone]', form).attr('data-raw-value', attrs[attr]);
            }

            if (element === 'city') {
                var $citySelect = $('.js-selectbox-city', form);
                var optionValue = attrs[attr];

                // update custom select
                $citySelect.val(optionValue);
                $citySelect.trigger('change');

                $('[name$=cityCode]', form).val(optionValue);
            }

            if (element === 'title') {
                if (attrs[attr]) {
                    $('[name$=title_titleList][value=' + attrs[attr] + ']', form).prop('checked', true);
                } else {
                    $('[name$=title_titleList]', form).prop('checked', false);
                }
            }
        });

        $('[name$=stateCode]', form).trigger('change');
        $('[name$=phone]', form).trigger('input');

        if (shipmentUUID === 'new') {
            $(form).attr('data-address-mode', 'new');
        } else if (shipmentUUID === originalUUID) {
            $(form).attr('data-address-mode', 'shipment');
        } else if (shipmentUUID.indexOf('ab_') === 0) {
            $(form).attr('data-address-mode', 'customer');
        } else {
            $(form).attr('data-address-mode', 'edit');
        }
    });
};


/**
 * Handle response from the server for valid or invalid form fields.
 * @param {Object} defer - the deferred object which will resolve on success or reject.
 * @param {Object} data - the response data with the invalid form fields or
 *  valid model data.
 */
baseShipping.methods.shippingFormResponse = function (defer, data) {
    var isMultiShip = $('#checkout-main').hasClass('multi-ship');
    var formSelector = isMultiShip
        ? '.multi-shipping .active form'
        : '.single-shipping form';

    // highlight fields with errors
    if (data.error) {
        if (data.fieldErrors.length) {
            data.fieldErrors.forEach(function (error) {
                if (Object.keys(error).length) {
                    formHelpers.loadFormErrors(formSelector, error);
                }
            });
            defer.reject(data);
        }

        if (data.serverErrors && data.serverErrors.length) {
            $.each(data.serverErrors, function (index, element) {
                notificationHelper.error(element);
            });

            defer.reject(data);
        }

        if (data.cartError) {
            window.location.href = data.redirectUrl;
            defer.reject();
        }
    } else {
        // Populate the Address Summary

        $('body').trigger('checkout:updateCheckoutView', {
            order: data.order,
            customer: data.customer
        });

        $('body').trigger('gtm:enhancedEcommercePushPageDataLayer', {
            pageDataLayerEnhancedEcommerce: data.pageDataLayerEnhancedEcommerce
        });
        defer.resolve(data);
        if (!data.disableScollingToPayment) {
            util.scrollBrowser(null, $('.payment-form'));
        } else {
            util.scrollBrowser(0, null, 0);
        }
    }
};

module.exports = baseShipping;
