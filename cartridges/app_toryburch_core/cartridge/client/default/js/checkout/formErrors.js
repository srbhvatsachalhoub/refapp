'use strict';

var scrollAnimate = require('base/components/scrollAnimate');

/**
 * Display error messages and highlight form fields with errors.
 * @param {string} parentSelector - the form which contains the fields
 * @param {Object} fieldErrors - the fields with errors
 */
function loadFormErrors(parentSelector, fieldErrors) { // eslint-disable-line
    // Display error messages and highlight form fields with errors.
    $.each(fieldErrors, function (attr) {
        var $el;

        if (attr === 'dwfrm_shipping_shippingAddress_addressFields_phone') {
            $el = $('.js-phone');
        } else {
            $el = $('*[name=' + attr + ']', parentSelector);
        }

        var $feedback = $el.siblings('.invalid-feedback');
        if ($feedback.length === 0) {
            $feedback = $el.closest('.form-group').find('.invalid-feedback');
        }
        $el.addClass('is-invalid');
        $feedback.html(fieldErrors[attr]);
        $el.closest('.form-group').addClass('is-invalid');
    });
    // Animate to top of form that has errors
    scrollAnimate($(parentSelector));
}

/**
 * Clear the form errors.
 * @param {string} parentSelector - the parent form selector.
 */
function clearPreviousErrors(parentSelector) {
    $(parentSelector).find('.is-invalid').removeClass('is-invalid');
    $(parentSelector).find('.bs-invalid').removeClass('bs-invalid');
    $('.error-message').hide();
}

module.exports = {
    loadFormErrors: loadFormErrors,
    clearPreviousErrors: clearPreviousErrors
};
