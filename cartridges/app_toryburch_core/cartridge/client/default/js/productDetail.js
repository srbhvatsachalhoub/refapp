'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./product/detail'));
    processInclude(require('brand_core/product/wishlist'));
    processInclude(require('plugin_applepay/product/detail'));
    processInclude(require('brand_core/product/backInStock'));
    processInclude(require('./product/handlePostCartAdd'));
});
