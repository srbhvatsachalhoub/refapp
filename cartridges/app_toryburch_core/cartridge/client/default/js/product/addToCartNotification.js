'use strict';

var autoClose;
var autoCloseWaitBefore = 8000; // 8 secs

/**
 * Closes product notification and backdrop elements
 */
function closeProductNotification() {
    var $addedProdNotification = $('.js-added-product-notification');
    var $addedProdNotificationBackdrop = $('.js-added-product-notification-backdrop');
    $addedProdNotification.slideUp('fast').fadeOut('fast');
    $addedProdNotificationBackdrop.fadeOut('fast');
}
/**
 * Prepares html string of an image
 * @param {Object} image - Image object
 * @return {string} Html string of image
 */
function prepareImageHtmlStr(image) {
    return '<img src="' + image.url + '" alt"' + image.alt + '" title="' + image.title + '" />';
}

/**
 * Updates the Mini-Cart, shows new added notification after the customer has pressed the "Add to Cart" button
 * @param {Event} e - event object
 * @param {string} response - ajax response from clicking the add to cart button
 */
function handlePostCartAdd(e, response) {
    // Clear auto close timeout
    clearTimeout(autoClose);

    // Update item count in cart
    $('.minicart').trigger('count:update', response);

    // Assign html containers to variables
    var $addedProdNotification = $('.js-added-product-notification');
    var $addedProdNotificationBackdrop = $('.js-added-product-notification-backdrop');
    var $addedProdNotificationDescription = $('.js-miniCart-item-description');
    var $addedProdNotificationMultipleProducts = $('.js-miniCart-item-multiple-products');
    var $addedProdNotificationMultipleProductImages = $('.js-miniCart-item-multiple-product-images').empty();
    var $addedProdNotificationMultipleProductsText = $('.js-miniCart-item-multiple-products-text').empty();
    var $addedProdNotificationImg = $('.js-miniCart-item-image');
    var $addedProdNotificationImgLink = $('.js-miniCart-item-image a').empty();
    var $addedProdNotificationName = $('.js-miniCart-item-name a').empty();
    var $addedProdNotificationBrand = $('.js-miniCart-item-brand').empty();
    var $addedProdNotificationOptions = $('.js-miniCart-item-details').empty();
    var $addedProdNotificationPrice = $('.js-miniCart-item-price').empty();
    var $addedProdNotificationListPrice = $('.js-miniCart-item-list-price').empty();
    var $addedProdNotificationSubtotal = $('.js-miniCart-subtotal').empty();
    var $addedProdNotificationLink = $('.js-miniCart-link');
    var qtyResourceStr = $addedProdNotificationOptions.data('resource-qty');
    var linkResourceStr = $addedProdNotificationLink.data('resource-link');
    var multipleImagesHtmlArr = [];
    var image;

    // Clear product options before assigning them with response
    var optionsStr = '';

    // check if product is out of stock
    if (!response.error && response.addedProducts && response.addedProducts.length) {
        var addedProducts = response.addedProducts.map(function (addedProductObj) {
            return addedProductObj.pid;
        });

        // Check response for clicked product to use product detail data in new added product notification
        for (var i = 0; i < response.cart.items.length; i++) {
            var item = response.cart.items[i];

            if (addedProducts.indexOf(item.id) > -1) {
                if (addedProducts.length === 1) {
                    // Create product image dynamically with response data
                    if (Object.keys(item.images.small).length) {
                        image = item.images.small[0];
                        $addedProdNotificationImgLink
                            .attr('href', window.RA_URL['Product-Show'].replace('PRODUCT_ID', item.id))
                            .html(prepareImageHtmlStr(image));
                    } else {
                        $addedProdNotificationImg.empty();
                    }

                    // Set product name with response
                    $addedProdNotificationName
                        .html(item.productName)
                        .attr('href', window.RA_URL['Product-Show'].replace('PRODUCT_ID', item.id));

                    if ($addedProdNotificationBrand && $addedProdNotificationBrand.length > 0) {
                        $addedProdNotificationBrand
                            .html(item.brand);
                    }

                    // Loop trough product details like size, volume etc. Add them to new added product notification
                    $addedProdNotificationOptions.hide();
                    if (item.variationAttributes) {
                        for (var j = 0; j < item.variationAttributes.length; j++) {
                            var variationAttribute = item.variationAttributes[j];
                            var attrName = $addedProdNotificationOptions.data('resource-' + variationAttribute.attributeId);
                            optionsStr += '<span class="miniCart-attribute">' + attrName + ': ' + variationAttribute.displayValue + '</span>';
                        }
                    }

                    if (qtyResourceStr) {
                        optionsStr += '<span class="miniCart-attribute">' + qtyResourceStr.replace('{0}', item.instantQuantity) + '</span>';
                    }
                    $addedProdNotificationOptions.html(optionsStr).show();

                    // Add prod price to new added product notification
                    if (item.instantPriceTotal && item.instantPriceTotal.list) {
                        $addedProdNotificationListPrice.html(item.instantPriceTotal.list.formatted);
                    }
                    if (item.instantPriceTotal && item.instantPriceTotal.sales) {
                        $addedProdNotificationPrice.html(item.instantPriceTotal.sales.formatted);
                    }

                    $addedProdNotificationLink.text(linkResourceStr.replace('{0}', item.quantity));

                    break;
                } else if (Object.keys(item.images.small).length) {
                    // Create multiple product images dynamically
                    image = item.images.small[0];
                    multipleImagesHtmlArr.push(prepareImageHtmlStr(image));
                }
            }
        }

        if (multipleImagesHtmlArr.length) {
            // Insert images into DOM
            $addedProdNotificationMultipleProductImages
                .html(multipleImagesHtmlArr.join('<i class="sc-icon-plus"></i>'));

            // Insert "x items selected" text into DOM
            $addedProdNotificationMultipleProductsText
                .html($addedProdNotificationMultipleProductsText
                    .data('text').replace('{0}', multipleImagesHtmlArr.length));

            $addedProdNotification.addClass('miniCart-multiple-products');
            $addedProdNotificationMultipleProducts.removeClass('d-none');
            $addedProdNotificationImg.addClass('d-none');
            $addedProdNotificationDescription.addClass('d-none');
        } else {
            $addedProdNotification.removeClass('miniCart-multiple-products');
            $addedProdNotificationMultipleProducts.addClass('d-none');
            $addedProdNotificationImg.removeClass('d-none');
            $addedProdNotificationDescription.removeClass('d-none');
        }

        // Add item subtotal
        $addedProdNotificationSubtotal.html(response.cart.totals.subTotal);

        // Show new added product notification backdrop
        $addedProdNotificationBackdrop.fadeIn('fast');

        // Show new added product notification
        $addedProdNotification.slideDown('fast');
    }

    // Auto-close new added product notification & backdrop after 4sec
    autoClose = setTimeout(closeProductNotification, autoCloseWaitBefore);
}

module.exports = function () {
    var $body = $('body');
    var $addedProdNotification = $('.js-added-product-notification');

    $body.on('product:afterAddToCart', handlePostCartAdd);

    // Close new added product notification & backdrop clicking outside
    $body.on('click', function (e) {
        if (!$(e.target).is('.js-added-product-notification, .js-added-product-notification *')) {
            closeProductNotification();
        }
    });

    // Close new added product notification & backdrop with close button
    $body.on('click', '.js-added-product-close', closeProductNotification);

    // Close new added product notification & backdrop clicking backdrop
    $body.on('click', '.js-added-product-notification-backdrop', closeProductNotification);

    // Close new added product notification hovering minicart icon
    $('.js-minicart-trigger').on('mouseenter', closeProductNotification);

    // Do not close new added product notification while mouse is on it
    $addedProdNotification.on('mouseenter', function () {
        clearTimeout(autoClose);
    });

    // Auto-close new added product notification & backdrop after 4sec
    $addedProdNotification.on('mouseleave', function () {
        autoClose = setTimeout(closeProductNotification, autoCloseWaitBefore);
    });
};
