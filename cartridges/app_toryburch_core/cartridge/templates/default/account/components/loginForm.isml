<isinclude template="/components/modules" sf-toolkit="off" />

<div class="login-form-container">
    <div class="account-form login-form">
        <h3 class="account-subtitle text-uppercase text-center">${Resource.msg('link.header.signin.module.subtitle', 'login', null)}</h3>
        <h2 class="login-sign-in account-title text-uppercase text-center">
            ${Resource.msg('link.header.signin.module', 'login', null)}
        </h2>

        <isif condition="${pdict.errorMessage}">
            <div class="alert alert-danger alert-dismissible valid-cart-error fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                ${pdict.errorMessage}
            </div>
        </isif>

        <form action="${pdict.actionUrl}" class="sign-in-form js-login-form" method="POST" name="login-form">
            <div class="account-form-body">
                <div class="form-group required">
                    <input  id="login-form-email"
                            class="form-control"
                            name="loginEmail"
                            value="${pdict.userName}"
                            pattern="^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$|^[0-9\+]{10,15}$"
                            required
                            aria-required="true"
                            aria-describedby="form-email-error"
                            placeholder="${Resource.msg('label.input.login.emailOrMobile', 'login', null)}"
                            data-missing-error="<isprint value="${Resource.msg('error.message.required.email', 'forms', null)}" encoding="htmldoublequote" />"
                            data-pattern-mismatch="<isprint value="${Resource.msg('error.message.parse.email', 'forms', null)}" encoding="htmldoublequote" />"
                    />
                    <label class="form-control-label" for="login-form-email">
                        <span>${Resource.msg('label.input.login.emailOrMobile', 'login', null)}</span>
                    </label>
                    <div class="invalid-feedback" id="form-email-error"></div>
                </div>

                <div class="form-group required position-relative">
                    <input  type="password"
                            id="login-form-password"
                            class="form-control js-input-password"
                            name="loginPassword"
                            required
                            aria-required="true"
                            aria-describedby="form-password-error"
                            minlength="8"
                            placeholder="${Resource.msg('label.input.login.password', 'login', null)}"
                            data-missing-error="<isprint value="${Resource.msg('error.message.required.password', 'forms', null)}" encoding="htmldoublequote" />"
                            data-range-error="<isprint value="${Resource.msg('error.message.range.password', 'forms', null)}" encoding="htmldoublequote" />"
                    />
                    <label class="form-control-label" for="login-form-password">
                        <span>${Resource.msg('label.input.login.password', 'login', null)}</span>
                    </label>
                    <div class="invalid-feedback" id="form-password-error"></div>
                </div>

                <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}" />
            </div>

            <div class="login-option-menu">
                <div class="form-group pull-left remember-me">
                    <input type="checkbox" class="form-control-checkbox" id="rememberMe" name="loginRememberMe" value="true"
                        <isif condition="${pdict.rememberMe}">checked</isif> >
                    <label class="form-control-label" for="rememberMe">
                        ${Resource.msg('label.checkbox.login.rememberme', 'login', null)}
                    </label>
                </div>
                <div class="forgot-password">
                    <a class="d-none" href="${URLUtils.url('Account-PasswordReset')}"
                        title="${Resource.msg('link.login.forgotpassword', 'login', null)}">
                        ${Resource.msg('link.login.forgotpassword', 'login', null)}
                    </a>

                    <a class="modal-reset-password-btn d-none"
                        title="${Resource.msg('link.login.forgotpassword', 'login', null)}"
                        data-remove-class-d-none=".js-reset-password-modal, .js-reset-password-form-container"
                        data-add-class-d-none=".js-login-modal, .js-reset-password-result"
                        href="${URLUtils.url('Account-PasswordResetDialogForm')}">
                        ${Resource.msg('link.login.forgotpassword', 'login', null)}
                    </a>

                    <a id="password-reset" class="page-reset-password-btn"
                        title="${Resource.msg('link.login.forgotpassword', 'login', null)}" data-toggle="modal"
                        href="${URLUtils.url('Account-PasswordResetDialogForm')}"
                        data-target="#requestPasswordResetModal"
                        data-remove-class-d-none=".js-reset-password-form-container"
                        data-add-class-d-none=".js-reset-password-result">
                        ${Resource.msg('link.login.forgotpassword', 'login', null)}
                    </a>
                </div>
            </div>

            <button type="submit" class="btn btn-block btn-primary sign-in-btn">
                ${Resource.msg('button.text.loginform', 'login', null)}
            </button>
        </form>
    </div>
    <div class="account-form create-account">
        <div class="create-account-wrapper">
            <iscontentasset aid="create-account-info"/>
            <a class="btn btn-block btn-primary modal-register-btn"
                href="${URLUtils.url('Login-Show',
                    'action', 'register', 'rurl', pdict.rurl
                )}">
                ${Resource.msg('link.create.an.account', 'login', null)}
            </a>
        </div>
    </div>
</div>
