<isinclude template="/components/modules" sf-toolkit="off" />

<form
    action="${pdict.createAccountUrl}"
    class="registration registration-form"
    method="POST"
    name="${pdict.profileForm.htmlName}">

    <div class="row">
        <div class="col-12">
            <isslot id="register-above-form-m" description="Register page above form" context="global" />
        </div>
        <div class="col-12 registration-form-title">
            <isselectbox    selectbox_formfield="${pdict.profileForm.title.titleList}"
                            selectbox_selected="${pdict.profileForm.title.titleList.selectedOption}"
                            selectbox_hidelabel="${true}"
                            selectbox_preselectvalue="${Resource.msg('label.profile.title', 'forms', null)}"
            />
        </div>

        <div class="col-12">
            <isinputtext    inputtext_formfield="${pdict.profileForm.customer.firstname}"
                            inputtext_autocomplete="given-name"
                            inputtext_missingerror="${Resource.msg('error.message.required.firstname', 'forms', null)}"
                            inputtext_rangeerror="${Resource.msg('error.message.range.firstname', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.firstname', 'forms', null)}"
                            inputtext_placeholder="${pdict.profileForm.customer.firstname}"
                            inputtext_labelafter="${true}"
            />
        </div>

        <div class="col-12">
            <isinputtext    inputtext_formfield="${pdict.profileForm.customer.lastname}"
                            inputtext_autocomplete="family-name"
                            inputtext_missingerror="${Resource.msg('error.message.required.lastname', 'forms', null)}"
                            inputtext_rangeerror="${Resource.msg('error.message.range.lastname', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.lastname', 'forms', null)}"
                            inputtext_placeholder="${pdict.profileForm.customer.lastname}"
                            inputtext_labelafter="${true}"
            />
        </div>

        <div class="col-12">
            <isinputtext    inputtext_formfield="${pdict.profileForm.customer.email}"
                            inputtext_type="email"
                            inputtext_autocomplete="email"
                            inputtext_placeholder="email@domain.com"
                            inputtext_missingerror="${Resource.msg('error.message.required.email', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.email', 'forms', null)}"
                            inputtext_labelafter="${true}"
            />
        </div>

        <div class="col-12">
            <isinputpassword    inputpassword_formfield="${pdict.profileForm.login.password}"
                                inputpassword_class="js-validate-password"
                                inputpassword_constraints="${pdict.passwordConstraints}"
                                inputpassword_placeholder="${Resource.msgf('label.input.password.placeholder', 'forms', null, pdict.passwordConstraints.minLength)}"
                                inputpassword_missingerror="${Resource.msg('error.message.required.password', 'forms', null)}"
                                inputpassword_rangeerror="${Resource.msg('error.message.range.password', 'forms', null)}"
                                inputtext_labelafter="${true}"
            />
        </div>

        <div class="col-12">
            <isinputpassword    inputpassword_formfield="${pdict.profileForm.login.passwordconfirm}"
                                inputpassword_mismatchid="${'id_' + pdict.profileForm.login.password.htmlName}"
                                inputpassword_missingerror="${Resource.msg('error.message.required.passwordconfirm', 'forms', null)}"
            />
        </div>

        <div class="col-12">
            <isinputphone   inputphone_multiregion="${true}"
                            inputphone_formfield="${pdict.profileForm.customer.phoneMobile}"
                            inputphone_value="${pdict.profileForm.customer.phoneMobile.htmlValue}"
                            inputphone_missingerror="${Resource.msg('error.message.required.phonemobile', 'forms', null)}"
                            inputphone_parseerror="${Resource.msg('error.message.parse.phonemobile', 'forms', null)}"
                            inputphone_rangeerror="${Resource.msg('error.message.range.phonemobile', 'forms', null)}"
                            inputphone_hidelabel="${true}"
            />
        </div>

        <div class="col-12 registration-form-birthday">
            <isselectdate   selectdate_formfield="${pdict.profileForm.customer.birthday}"
                            selectdate_value="${pdict.profileForm.customer.birthday.value}"
                            selectdate_label="${Resource.msg('label.register.birthday', 'forms', null)}"
                            optinlist_hidelabel="${true}"
            />
        </div>

        <div class="col-12 registration-form-subscride">
            <label class="form-control-label hidden-md-down">&nbsp;</label>
            <div class="form-group
                ${pdict.profileForm.acceptTermsConditions.mandatory === true ? 'required' : ''}">
                <input type="checkbox" id="acceptTermsConditions"
                    <isprint value="${pdict.profileForm.acceptTermsConditions.attributes}" encoding="off" />>
                <label class="form-control-label text-transform-none subscribe-confirmation-label" for="acceptTermsConditions">
                    <iscontentasset aid="register-agree-terms-conditions" />
                </label>
                <div class="invalid-feedback"></div>
            </div>
        </div>

        <div class="col-12 registration-form-subscride">
            <ischeckboxnewsletter checkboxnewsletter_formfield="${pdict.profileForm.customer.addtoemaillist}" />
        </div>

        <div class="col-12 registration-form-accountpreferences">
            <isoptinlist    optinlist_formfield="${pdict.profileForm.accountpreferences.optinlist}"
                            optinlist_checkall="${true}"
                            optinlist_separatorclass="${'accountpreferences-item'}" />
        </div>
        <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}"/>
    </div>

    <div class="row mb-5 justify-content-center registration-form-action">
        <div class="col-12 text-center">
            <button type="submit" class="btn btn-primary">
                ${Resource.msg('button.createaccount.registration', 'registration', null)}
            </button>
        </div>
    </div>
</form>
