'use strict';
var server = require('server');

module.exports = function (object, shipment) {
    if (!shipment) {
        return;
    }
    var shippingAddressFieldsForm = server.forms.getForm('shipping').shippingAddress.addressFields;

    Object.defineProperty(object, 'shippingAddress', {
        enumerable: true,
        value: {
            address1: shippingAddressFieldsForm.address1.value,
            address2: shippingAddressFieldsForm.address2.value,
            firstName: shippingAddressFieldsForm.firstName.value,
            lastName: shippingAddressFieldsForm.lastName.value,
            postalCode: shippingAddressFieldsForm.postalCode.value,
            phone: shippingAddressFieldsForm.phone.value,
            city: shippingAddressFieldsForm.city.value
        }
    });
};
