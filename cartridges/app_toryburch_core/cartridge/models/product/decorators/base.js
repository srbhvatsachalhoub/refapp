'use strict';

var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');

module.exports = function (object, apiProduct, type) {
    var productNameDisplayMaxChars = require('dw/system/Site').current.getCustomPreferenceValue('productNameDisplayMaxChars');

    Object.defineProperty(object, 'uuid', {
        enumerable: true,
        value: apiProduct.UUID
    });

    Object.defineProperty(object, 'id', {
        enumerable: true,
        value: apiProduct.ID
    });

    Object.defineProperty(object, 'productName', {
        enumerable: true,
        value: productHelpers.getCorrectProductName(apiProduct)
    });

    Object.defineProperty(object, 'productType', {
        enumerable: true,
        value: type
    });

    Object.defineProperty(object, 'brand', {
        enumerable: true,
        value: 'akeneo_brand' in apiProduct.custom &&
        apiProduct.custom.akeneo_brand &&
        apiProduct.custom.akeneo_brand.value ?
            apiProduct.custom.akeneo_brand.displayValue : apiProduct.brand
    });

    Object.defineProperty(object, 'manufacturerName', {
        enumerable: true,
        value: apiProduct.manufacturerName
    });

    Object.defineProperty(object, 'manufacturerSKU', {
        enumerable: true,
        value: apiProduct.manufacturerSKU
    });

    Object.defineProperty(object, 'onlineFrom', {
        enumerable: true,
        value: apiProduct.onlineFrom
    });
    var online = require('*/cartridge/models/product/decorators/online');
    online(object, apiProduct);

    Object.defineProperty(object, 'assignedToSite', {
        enumerable: true,
        value: apiProduct.isAssignedToSiteCatalog()
    });

    Object.defineProperty(object, 'secondaryName', {
        enumerable: true,
        value: productNameDisplayMaxChars && apiProduct.custom.secondaryName ? apiProduct.custom.secondaryName.substring(0, productNameDisplayMaxChars) : apiProduct.custom.secondaryName
    });
};
