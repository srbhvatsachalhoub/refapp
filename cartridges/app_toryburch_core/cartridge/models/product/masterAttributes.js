'use strict';

var description = require('app_storefront_base/cartridge/models/product/decorators/description');

/**
 * @description Decorate product with master product attributes
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 */
function MasterAttributes(apiProduct) {
    var result = {};

    if (apiProduct.isVariant()) {
        description(result, apiProduct.masterProduct);

        var keys = Object.keys(result);

        keys.forEach(function (key) {
            this[key] = result[key];
        }, this);
    }
}

module.exports = MasterAttributes;
