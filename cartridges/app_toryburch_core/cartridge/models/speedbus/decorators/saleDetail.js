var Constants = require('*/cartridge/scripts/helpers/constants');
/**
 * Builds orderModel view model
 * @param {Object} orderModel orderModel
 * @param {dw.order.Order} lineItemCtnr lineItemCtnr of Order
 * @param {string} quantitySign Quantity Sign
 */
module.exports = function (orderModel, lineItemCtnr, quantitySign) {
    var saleDetail = [];
    var collections = require('*/cartridge/scripts/util/collections');
    var taxRate;
    collections.forEach(lineItemCtnr.getAllProductLineItems(), function (pli) {
        var excludeCustomProratedPricesFromRetailPrice = 0;
        var proratedPrices = pli.proratedPriceAdjustmentPrices;
        var proratedPricesItr = !proratedPrices.empty ? proratedPrices.keySet().iterator() : null;
        while (proratedPricesItr && proratedPricesItr.hasNext()) {
            var adjustment = proratedPricesItr.next();
            if (adjustment.promotionID === Constants.paymentServiceFeeKey) {
                var Money = require('dw/value/Money');
                var feePrice = new Money(Number(proratedPrices.get(adjustment)), lineItemCtnr.currencyCode);
                excludeCustomProratedPricesFromRetailPrice += feePrice.divide(pli.quantityValue).value;
            }
        }
        var unitRetailPrice = pli.getAdjustedPrice(true).value > 0 ? pli.getAdjustedPrice(true).divide(pli.quantityValue).value - excludeCustomProratedPricesFromRetailPrice : 0; // price for item after adjustments

        var unitErpPrice = pli.getBasePrice().value; // price for one item from ERP.
        // financial model is funny
        // we send divided unitRetailPrice based on quantity, vat amount will not be divided :)
        var baseUnitPrice = pli.price.divide(pli.quantityValue).value;
        var discountAmount = 0;
        if (unitRetailPrice === 0) {
            discountAmount = unitErpPrice;
        } else if (unitRetailPrice < baseUnitPrice) {
            discountAmount = unitErpPrice - unitRetailPrice;
        }
        var vatAmount = pli.getTaxRate() > 0 ? Number((((unitRetailPrice - (unitRetailPrice / (1.00 + pli.getTaxRate()))) * pli.quantityValue)).toFixed(2)) : 0;
        var saleItem = {
            Discount_Amount: Number((discountAmount * pli.quantityValue).toFixed(2)), // PLI + Order Level Discount.
            Item: pli.productID,
            ItemComments: pli.productName || '',
            // Promotion_ID: promotions.join(','), allows max 20 characters
            Quantity: pli.quantityValue,
            Quantity_Sign: quantitySign,
            // Not Adjusted Price. Retail Price
            Unit_Retail: Number(unitRetailPrice.toFixed(2)),
            Unit_ERP_Price: Number(unitErpPrice.toFixed(2)),
            VAT_Amount: vatAmount
        };

        if (!taxRate) {
            taxRate = pli.getTaxRate();
        }

        saleDetail.push(saleItem);
    });
    if (['SALE', 'RETURN'].indexOf(orderModel.Transaction_Type) > -1) {
        // add shipping cost & payment service fee
        var priceAdjustment = lineItemCtnr.getPriceAdjustmentByPromotionID(Constants.paymentServiceFeeKey);
        if (priceAdjustment) {
            var VpnOfPaymentMethod = priceAdjustment.custom.speedBusSkuID;
            var serviceFee = priceAdjustment.custom.serviceFee;
            if (!VpnOfPaymentMethod || !serviceFee) {
                collections.forEach(lineItemCtnr.getPaymentInstruments(), function (paymentInstrument) {
                    var paymentMethod = require('dw/order/PaymentMgr').getPaymentMethod(paymentInstrument.paymentMethod);
                    if (paymentMethod && paymentMethod.custom.serviceFee && paymentMethod.custom.serviceFee > 0 && paymentMethod.custom.speedBusVpn) {
                        VpnOfPaymentMethod = paymentMethod.custom.speedBusSkuID;
                        if (!serviceFee) {
                            serviceFee = paymentMethod.custom.serviceFee;
                        }
                    }
                });
            }
            if (VpnOfPaymentMethod) {
                var deliveryFee = {
                    Discount_Amount: 0,
                    ItemComments: priceAdjustment.getLineItemText() || '',
                    Item: VpnOfPaymentMethod,
                    Quantity: 1,
                    Quantity_Sign: quantitySign,
                    Unit_Retail: Number(priceAdjustment.price.value.toFixed(2)),
                    Unit_ERP_Price: serviceFee,
                    VAT_Amount: taxRate
                };
                if (taxRate) {
                    var deliverFeeVatAmount = taxRate > 0 && priceAdjustment.price.value > 0 ?
                        Number((((priceAdjustment.price.value - (priceAdjustment.price.value / (1.00 + taxRate))))).toFixed(2)) : 0;
                    deliveryFee.VAT_Amount = deliverFeeVatAmount;
                }
                saleDetail.push(deliveryFee);
            }
        }

        var shipment = lineItemCtnr.getDefaultShipment();
        if (shipment && shipment.shippingMethod.custom.speedBusSkuID) {
            var discAmount = lineItemCtnr.getAdjustedShippingTotalPrice().value === 0 ? lineItemCtnr.getShippingTotalPrice().value : (lineItemCtnr.getShippingTotalPrice().value - lineItemCtnr.getAdjustedShippingTotalPrice().value);

            saleDetail.push({
                Discount_Amount: Number(discAmount.toFixed(2)),
                Item: shipment.shippingMethod.custom.speedBusSkuID,
                ItemComments: shipment.shippingMethod.displayName || '',
                Quantity: 1,
                Quantity_Sign: quantitySign,
                Unit_Retail: Number(lineItemCtnr.getAdjustedShippingTotalPrice().value.toFixed(2)),
                Unit_ERP_Price: Number(lineItemCtnr.getShippingTotalPrice().value.toFixed(2)), // not adjusted shipping price.
                VAT_Amount: Number(lineItemCtnr.getAdjustedShippingTotalTax().value.toFixed(2))
            });
        }
    }

    Object.defineProperty(orderModel, 'SaleDetail', {
        enumerable: true,
        value: saleDetail
    });
};
