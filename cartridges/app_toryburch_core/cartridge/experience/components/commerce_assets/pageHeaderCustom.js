'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for accordionAsset component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.pageTitle = content.pageTitle || '';
    model.pageSubtitle = content.pageSubtitle || '';

    return new Template('experience/components/commerce_assets/pageHeaderCustom').render(model).text;
};
