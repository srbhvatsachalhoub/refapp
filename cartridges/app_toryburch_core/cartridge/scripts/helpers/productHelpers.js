'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var ATTRIBUTE_NAME_COLOR = 'color';

var baseProductHelper = module.superModule;
var originalExports = Object.keys(module.superModule);

originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});

/**
 * Get a specific ProductVariationAttribute from a variationModel.productVariationAttributes collection
 * @param  {dw.util.Collection} productVariationAttributes - attribute value
 * @param  {string} attrId - attribute ID
 * @return {dw.catalog.ProductVariationAttribute} Represents a product variation attribute
 */
function getProductVariationAttribute(productVariationAttributes, attrId) {
    var dwAttr = collections.find(productVariationAttributes, function (item) {
        return item.attributeID === attrId || item.ID === attrId;
    });
    return dwAttr;
}

/**
 * Sets the initial attribute value into the params.variables to have selected attribute value
 * for now, only color
 * first color variation valiuye is being set, if no color is set for the request
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @param {Object} params - the query string params
 * @returns {Object} - the default selected params
 */
function setInitialAttrValues(apiProduct, params) {
    if (!apiProduct.master) {
        return params;
    }
    var variationModel = apiProduct.variationModel;
    if (!variationModel) {
        return params;
    }

    var variables = params.variables || {};
    var productVariationAttributes = variationModel.productVariationAttributes;
    if (!productVariationAttributes) {
        return params;
    }

    var productColorVariationAttribute = getProductVariationAttribute(productVariationAttributes, ATTRIBUTE_NAME_COLOR);
    if (productColorVariationAttribute && !variables[ATTRIBUTE_NAME_COLOR] && !variables[productColorVariationAttribute.ID]) {
        if (productColorVariationAttribute) {
            var productColorVariationAttributeValues = variationModel.getAllValues(productColorVariationAttribute);
            if (productColorVariationAttributeValues && productColorVariationAttributeValues.length) {
                var orderableColorValue = collections.find(productColorVariationAttributeValues, function (value) {
                    return variationModel.hasOrderableVariants(productColorVariationAttribute, value);
                });
                if (orderableColorValue) {
                    variables[ATTRIBUTE_NAME_COLOR] = {
                        id: apiProduct.ID,
                        value: orderableColorValue.value
                    };

                    params.variables = variables; // eslint-disable-line no-param-reassign
                }
            }
        }
    }

    return params;
}

/**
 * If a product is master auto select first variation
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Object with selected parameters
 */
function normalizeSelectedAttributes(apiProduct, params) {
    if (!apiProduct.master) {
        return params.variables;
    }

    var variables = params.variables || {};
    if (apiProduct.variationModel) {
        collections.forEach(apiProduct.variationModel.productVariationAttributes, function (attribute) {
            var allValues = apiProduct.variationModel.getAllValues(attribute);

            if (attribute.ID === 'color') {
                variables[attribute.ID] = {
                    id: apiProduct.ID,
                    value: allValues.get(0).ID
                };
            }
        });
    }

    return Object.keys(variables) ? variables : null;
}

/**
 * Get information for model creation
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Config object
 */
function getConfig(apiProduct, params) {
    var variationModel = null;

    var options = baseProductHelper.getConfig(apiProduct, params);
    var product = apiProduct;

    if (params.pview === 'tile' && (product.isVariant() || product.isMaster())) {
        // Preselect first variation
        var variables = normalizeSelectedAttributes(apiProduct, params);
        variationModel = baseProductHelper.getVariationModel(apiProduct, variables);

        options.variationModel = variationModel;
        options.apiProduct = product;
        options.productType = baseProductHelper.getProductType(options.apiProduct);
    }

    return options;
}

/**
 * Get the product tile context object
 * @param {Object} productTileParams - the JSON productTileParams
 * @return {Object} the product tile context object
 */
function getProductTileContext(productTileParams) {
    var URLUtils = require('dw/web/URLUtils');
    var context = baseProductHelper.getProductTileContext(productTileParams);

    if (context.product) {
        context.addToCartUrl = URLUtils.url('Cart-AddProduct');
    }

    if (context.product.productType === 'variant') {
        var masterProductID = require('dw/catalog/ProductMgr').getProduct(context.product.id).getMasterProduct().getID();
        var color = context.product.variationAttributes[0].selectedValue.id;
        var colorParameter = '?dwvar_' + masterProductID + '_color=' + color;
        context.urls.product = URLUtils.url('Product-Show', 'pid', masterProductID).relative().toString() + colorParameter;
        context.urls.quickView = URLUtils.url('Product-ShowQuickView', 'pid', masterProductID).relative().toString() + colorParameter;
    }

    return context;
}

/**
 * @description This function return current product name
 * @param {string} apiProduct object product
 * @returns {string} product name
 */
function getCorrectProductName(apiProduct) {
    var stringUtils = require('*/cartridge/scripts/util/string');
    var preferences = require('*/cartridge/scripts/util/preferences');

    var product = apiProduct.isVariant() ? apiProduct.getMasterProduct() : apiProduct;
    var productName = product.name;

    var productNameMaxChars = preferences.get('productNameDisplayMaxChars', productName.length);

    productName = productName.substring(0, productNameMaxChars);

    var formattedProductName = stringUtils.capitalize(productName);

    return formattedProductName;
}

module.exports.setInitialAttrValues = setInitialAttrValues;
module.exports.getProductTileContext = getProductTileContext;
module.exports.getConfig = getConfig;
module.exports.getCorrectProductName = getCorrectProductName;
