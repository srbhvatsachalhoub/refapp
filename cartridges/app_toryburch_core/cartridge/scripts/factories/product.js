'use strict';

var base = module.superModule;

var ProductMgr = require('dw/catalog/ProductMgr');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var productTile = require('*/cartridge/models/product/productTile');

module.exports = {
    get: function (params) {
        var productId = params.pid;
        var product = {};
        var apiProduct = ProductMgr.getProduct(productId);
        if (apiProduct === null) {
            return product;
        }
        var options = null;

        switch (params.pview) {
            case 'tile':
                options = productHelper.getConfig(apiProduct, params);
                product = productTile(product, options.apiProduct, options.productType, options);
                break;
            default:
                product = base.get.call(this, params);
                break;
        }

        return product;
    }
};
