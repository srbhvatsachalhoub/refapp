'use strict';

var site = require('dw/system/Site').getCurrent();

/**
 * Gets site custom preference value
 * @param {string} key - site preference ID
 * @param {Object} [defaultValue] - default value
 * @returns {Object} custom site preference value
 */
function get(key, defaultValue) {
    var result = site.getCustomPreferenceValue(key);

    return result === null ? defaultValue : result;
}

module.exports = {
    get: get
};
