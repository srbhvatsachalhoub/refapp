'use strict';

var base = module.superModule;
var baseCopyCustomerAddressToShipment = base.copyCustomerAddressToShipment;

var BasketMgr = require('dw/order/BasketMgr');
var Transaction = require('dw/system/Transaction');


/**
 * Copies a CustomerAddress to a Shipment as its Shipping Address
 * @param {dw.customer.CustomerAddress} address - The customer address
 * @param {dw.order.Shipment} [shipmentOrNull] - The target shipment
 */
function copyCustomerAddressToShipment(address, shipmentOrNull) {
    baseCopyCustomerAddressToShipment(address, shipmentOrNull);

    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;
    var shippingAddress = shipment.shippingAddress;

    Transaction.wrap(function () {
        if (address.title) {
            shippingAddress.setTitle(address.title);
        }
    });
}

base.copyCustomerAddressToShipment = copyCustomerAddressToShipment;

module.exports = base;
