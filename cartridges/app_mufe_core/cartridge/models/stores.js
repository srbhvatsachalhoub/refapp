'use strict';

var base = module.superModule;

var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * Creates an array of objects containing the coordinates of the store's returned by the search
 * @param {dw.util.Set} storesObject - a set of <dw.catalog.Store> objects
 * @returns {Array} an array of coordinates objects with store info
 */
function createGeoLocationObject(storesObject) {
    var context;
    var templateStoreInfoWindow = 'storeLocator/storeInfoWindow';
    var templateStoreInfoList = 'storeLocator/storeInfoList';
    return Object.keys(storesObject).map(function (key) {
        var store = storesObject[key];
        context = { store: store };
        return {
            name: store.name,
            latitude: store.latitude,
            longitude: store.longitude,
            infoWindowHtml: renderTemplateHelper.getRenderedHtml(context, templateStoreInfoWindow),
            infoListHtml: renderTemplateHelper.getRenderedHtml(context, templateStoreInfoList),
            isRetailer: store.isRetailer,
            isAirport: store.isAirport,
            isSpa: store.isSpa,
            isCafe: store.isCafe
        };
    });
}

base.createGeoLocationObject = createGeoLocationObject;
module.exports = base;
