'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');
var ImageModel = require('*/cartridge/models/product/productImages');
var HashMap = require('dw/util/HashMap');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * @param {string} hex - attribute hex color values array
 * @returns {number} sortedAttrColorValues - converted hex to decimal value
 */
function hexToDec(hex) {
    var result = 0;
    var digitValue = 0;
    // eslint-disable-next-line no-param-reassign
    hex = hex.toLowerCase();
    for (var i = 0; i < hex.length; i++) {
        digitValue = '0123456789abcdefgh'.indexOf(hex[i]);
        // eslint-disable-next-line no-mixed-operators
        result = result * 16 + digitValue;
    }
    return result;
}

/**
 * @param {Array} attrColorValues - attribute color values array
 * @returns {Array} sortedAttrColorValues - sorted attribute color values array
 */
function sortVariationAttributeColorValues(attrColorValues) {
    if (!attrColorValues || !attrColorValues.length) {
        return attrColorValues;
    }

    return attrColorValues.sort(function (a, b) {
        return hexToDec(a.shadeNumber) - hexToDec(b.shadeNumber);
    });
}

/**
 * Determines whether a product attribute has image swatches.  Currently, the only attribute that
 *     does is Color.
 * @param {string} dwAttributeId - Id of the attribute to check
 * @returns {boolean} flag that specifies if the current attribute should be displayed as a swatch
 */
function isSwatchable(dwAttributeId) {
    var imageableAttrs = ['color'];
    return imageableAttrs.indexOf(dwAttributeId) > -1;
}
/**
 * Indicate BackInStockEnablement
 * @param {dw.catalog.ProductVariationModel} variationModel - A product's variation model
 * @returns {boolean} status of back instock
 */
function isBackInStockEnabled(variationModel) {
    var isEnabled = (variationModel.selectedVariant && variationModel.selectedVariant.custom.enableBackInStockNotification) ||
        (variationModel.master && variationModel.master.custom.enableBackInStockNotification);
    if (!isEnabled && variationModel.master && variationModel.master.custom.enableBackInStockNotification) {
        var categories = variationModel.master.getAllCategories();
        if (categories) {
            collections.forEach(categories, function (category) {
                if ('enableBackInStockNotification' in category.custom &&
                category.custom.enableBackInStockNotification) {
                    isEnabled = true;
                }
            });
        }
    }
    return isEnabled;
}

/**
 * Retrieve all attribute values
 *
 * @param {dw.catalog.ProductVariationModel} variationModel - A product's variation model
 * @param {dw.catalog.ProductVariationAttributeValue} selectedValue - Selected attribute value
 * @param {dw.catalog.ProductVariationAttribute} attr - Attribute value'
 * @param {string} endPoint - The end point to use in the Product Controller
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 * @param {dw.util.HashMap} variantFilters - the selected variation attribute values
 * @returns {Object[]} - List of attribute value objects for template context
 */
function getAllAttrValues(
    variationModel,
    selectedValue,
    attr,
    endPoint,
    selectedOptionsQueryParams,
    quantity,
    variantFilters
) {
    var attrValues = variationModel.getAllValues(attr);
    var actionEndpoint = 'Product-' + endPoint;
    var isBackInStock = isBackInStockEnabled(variationModel);
    var shadeHexaCode = '';
    var shadeNumber = '';

    return collections.map(attrValues, function (value) {
        for (var i = 0; i < variationModel.variants.length; i++) {
            var custom = variationModel.variants[i].custom;
            if (custom.color && custom.color === value.ID) {
                shadeHexaCode = custom.shadeHexaCode ? custom.shadeHexaCode : '';
                shadeNumber = custom.shadeNumber ? custom.shadeNumber : '';
            }
        }

        var isSelected = (selectedValue && selectedValue.equals(value)) || false;
        var valueUrl = '';
        var processedAttr = {
            id: value.ID,
            description: value.description,
            displayValue: value.displayValue,
            value: value.value,
            selected: isSelected,
            selectable: variationModel.hasOrderableVariants(attr, value) || isBackInStock,
            shadeHexaCode: shadeHexaCode,
            shadeNumber: shadeNumber
        };

        // put the attribute value to the variant filters
        // if it is selected (will be used for one select further variant(potential variant))
        if (isSelected && !variantFilters.containsKey(attr.ID)) {
            variantFilters.put(attr.ID, value.ID); // attr.ID = size, value.ID = 150ml
        }

        if (processedAttr.selectable) {
            valueUrl = (isSelected && endPoint !== 'Show' && !isBackInStock)
                ? variationModel.urlUnselectVariationValue(actionEndpoint, attr)
                : variationModel.urlSelectVariationValue(actionEndpoint, attr, value);
            processedAttr.url = urlHelper.appendQueryParams(valueUrl, [selectedOptionsQueryParams,
                'quantity=' + quantity]);
        }

        if (isSwatchable(attr.attributeID)) {
            processedAttr.images = new ImageModel(value, { types: ['swatch'], quantity: 'all' });
        }

        return processedAttr;
    });
}

/**
 * Gets the Url needed to relax the given attribute selection, this will not return
 * anything for attributes represented as swatches.
 *
 * @param {Array} values - Attribute values
 * @param {string} attrID - id of the attribute
 * @returns {string} -the Url that will remove the selected attribute.
 */
function getAttrResetUrl(values, attrID) {
    var urlReturned;
    var value;

    for (var i = 0; i < values.length; i++) {
        value = values[i];
        if (!value.images) {
            if (value.selected) {
                urlReturned = value.url;
                break;
            }

            if (value.selectable) {
                urlReturned = value.url.replace(attrID + '=' + value.value, attrID + '=');
                break;
            }
        }
    }

    return urlReturned;
}

/**
 * Set the price to the variant attribute values if there is potential variant (last step(select) to the orderable variant)
 * according to current variationModel
 * @param {dw.catalog.ProductVariationModel} variationModel - current product variation
 * @param {Object} item - the variation attribute and its all values
 * @param {dw.util.HashMap} variantFilters - the variant filters to be used to find the potential variant
 */
function setPotentialVariantPrice(variationModel, item, variantFilters) {
    if (!item || !item.values || !item.values.length) {
        return;
    }
    // loop all attribute values
    item.values.forEach(function (value) {
        // if attr id is already in filters, remove it
        if (variantFilters.containsKey(item.id)) {
            variantFilters.remove(item.id);
        }

        // add current value to the filter
        variantFilters.put(item.id, value.id);

        // get variant by using the filters
        var variants = variationModel.getVariants(variantFilters);

        // if variants length is 1 then it means one step to select this variant
        if (variants.length === 1) {
            // call it as potential variant and get priceModel of it
            var potentialVariantPriceModel = variants[0].priceModel;
            if (potentialVariantPriceModel && potentialVariantPriceModel.price) {
                value.price = renderTemplateHelper.getRenderedHtml( // eslint-disable-line no-param-reassign
                    { price: potentialVariantPriceModel.price },
                    'product/components/pricing/currencyFormatted'
                );
            }
        }
    });
    variantFilters.remove(item.id);
    if (item.selectedValue) {
        variantFilters.put(item.id, item.selectedValue.id);
    }
}

/**
 * @constructor
 * @classdesc Get a list of available attributes that matches provided config
 *
 * @param {dw.catalog.ProductVariationModel} variationModel - current product variation
 * @param {Object} attrConfig - attributes to select
 * @param {Array} attrConfig.attributes - an array of strings,representing the
 *                                        id's of product attributes.
 * @param {string} attrConfig.attributes - If this is a string and equal to '*' it signifies
 *                                         that all attributes should be returned.
 *                                         If the string is 'selected', then this is comming
 *                                         from something like a product line item, in that
 *                                         all the attributes have been selected.
 *
 * @param {string} attrConfig.endPoint - the endpoint to use when generating urls for
 *                                       product attributes
 * @param {string} selectedOptionsQueryParams - Selected options query params
 * @param {string} quantity - Quantity selected
 */
function VariationAttributesModel(variationModel, attrConfig, selectedOptionsQueryParams, quantity) {
    var allAttributes = variationModel.productVariationAttributes;
    var variantFilters = new HashMap();
    var result = [];
    collections.forEach(allAttributes, function (attr) {
        var selectedValue = variationModel.getSelectedValue(attr);
        var values = getAllAttrValues(variationModel, selectedValue, attr, attrConfig.endPoint,
            selectedOptionsQueryParams, quantity, variantFilters);
        var resetUrl = getAttrResetUrl(values, attr.ID);

        for (var i = 0; i < values.length; i++) {
            if (values[i].selected) {
                selectedValue = values[i];
                break;
            }
        }

        if (attr.attributeID === 'color') {
            sortVariationAttributeColorValues(values);
        }

        if ((Array.isArray(attrConfig.attributes)
            && attrConfig.attributes.indexOf(attr.attributeID) > -1)
            || attrConfig.attributes === '*') {
            result.push({
                attributeId: attr.attributeID,
                displayName: attr.displayName,
                id: attr.ID,
                swatchable: isSwatchable(attr.attributeID),
                values: values,
                resetUrl: resetUrl,
                selectedValue: selectedValue
            });
        } else if (attrConfig.attributes === 'selected') {
            result.push({
                displayName: attr.displayName,
                displayValue: selectedValue && selectedValue.displayValue ? selectedValue.displayValue : '',
                attributeId: attr.attributeID,
                selectedValue: selectedValue,
                id: attr.ID
            });
        }
    });
    // check potential variant price only if on step further to select variant or variant selected states
    var isSetPotentialVariantPrice = (variantFilters.size() >= (allAttributes.length - 1));
    result.forEach(function (item) {
        if (isSetPotentialVariantPrice) {
            setPotentialVariantPrice(variationModel, item, variantFilters);
        }
        this.push(item);
    }, this);
}

VariationAttributesModel.prototype = [];

module.exports = VariationAttributesModel;
