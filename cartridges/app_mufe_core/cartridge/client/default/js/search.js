'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./search/search'));
    processInclude(require('./productTile'));
    processInclude(require('plugin_wishlists/product/wishlistHeart'));
    processInclude(require('plugin_giftcard/product/giftCardDetail'));
    processInclude(require('./backToTop'));
});
