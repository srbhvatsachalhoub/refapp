'use strict';

var base = require('brand_core/search/search');
var userAgent = require('brand_core/helpers/userAgent');
var selectpicker = require('../components/selectpicker');
var debounce = require('lodash/debounce');
var throttle = require('lodash/throttle');

// Constants
var infiniteScrollLock = 'infinite-scroll-lock';
var infiniteScrollActive = 'infinite-scroll-active';
var infiniteScrollDelay = 75;
var firstFirePosition = 0;
var $win = $(window);

/**
 * Resize Filter Modal
 */
base.headerModalTop = function () {
    var $headerModalTop = $('.header').height();
    $('.filter-modal-box').css('top', $headerModalTop);
    $(document)
        .on('click', '.btn-filter-modal', function () {
            $('.filter-modal-box').removeClass('d-none');
        })
        .on('click', '.js-filter-close', function () {
            $('.filter-modal-box').addClass('d-none');
        });
};

base.sortOption = function () {
    $(document)
        .on('click', '.js-sort-option', function (e) {
            e.preventDefault();
            $('.js-sort-option').removeClass('selected');
            $(this).addClass('selected');
        })
        .on('click', '.js-apply-sort-option-btn', function (e) {
            e.preventDefault();
            var $selected = $('.js-sort-option.selected');
            var $sortSelecbox = $('.js-sort-options-selectbox');
            var newUrl = $selected.attr('href');
            var oldUrl = $sortSelecbox.val();

            if (oldUrl !== newUrl) {
                $sortSelecbox.val(newUrl).trigger('change');
            }
        })
        .on('state:add:open', '.js-sort-container', function () {
            $('body').addClass('disable-scroll');
            var $sortSelecbox = $('.js-sort-options-selectbox');
            var url = $sortSelecbox.val();
            $('.js-sort-option[href="' + url + '"]').trigger('click');
        })
        .on('state:remove:open', '.js-sort-container', function () {
            $('body').removeClass('disable-scroll');
        });
};

base.initSelectpicker = function () {
    selectpicker('.js-search-results select');

    $(document)
        .off('search:refinement:success', base.initSelectpicker)
        .on('search:refinement:success', base.initSelectpicker);
};

base.initOnRefinementSuccess = function () {
    var $previousRefinements = null;

    $(document)
        .on('search:refinement:start', function () {
            var $currentRefinements = $('.js-refinement-container [data-refinement]');
            if ($previousRefinements && $previousRefinements.length) {
                $previousRefinements.each(function () {
                    var $this = $(this);
                    var id = $this.data('refinement');
                    if ($currentRefinements.filter('[data-refinement="' + id + '"]').length === 0) {
                        $currentRefinements.push(this);
                    }
                });
            }
            $previousRefinements = $currentRefinements;
        })
        .on('search:refinement:success', function () {
            var $refinementContainer = $('.js-refinement-container');
            var $openRefinements = $previousRefinements.filter('.open');
            $refinementContainer.find('.open').removeClass('open');
            $refinementContainer.find('[data-refinement]').each(function () {
                var $this = $(this);
                var id = $this.data('refinement');
                if ($openRefinements.filter('[data-refinement="' + id + '"]').length ||
                    $previousRefinements.filter('[data-refinement="' + id + '"]').length === 0) {
                    $this.addClass('open');
                }
            });
        });
};

base.refinements = function () {
    var $body = $('body');
    $(document)
        .on('state:add:open', '.js-refinement-container', function () {
            $body.addClass('disable-scroll');
            if (userAgent.isMobileSafari) {
                $body.addClass('mobile-safari');
            }
        })
        .on('state:remove:open', '.js-refinement-container', function () {
            $body.removeClass('disable-scroll');
        })
        .on('search:refinement:success', function () {
            if ($('.js-no-result-reset-filter-btn').length) {
                $body.removeClass('disable-scroll');
            }
        });
};

base.initContentSearch = function () {
    var $container = $('.js-content-result-container');

    // Don't go further, if there isn't a content result element in the page
    if ($container.length === 0) return;

    // Don't go further, if content search results are empty
    if ($container.find('.js-content-result-empty').length) $container.remove();

    // Show "Related Content" title
    $container.find('.js-content-result-title').removeClass('d-none');

    // Add view more functionality
    $(document).on('click', '.js-content-view-more-btn', function () {
        var $btn = $(this);
        $.spinner().start();
        $.ajax({
            method: 'GET',
            url: $btn.data('url'),
            success: function (response) {
                $btn.closest('.js-content-view-more-container').remove();
                $container.find('.js-content-result-placeholder').append(response);
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    });
};

base.setSearchKeyword = function () {
    var $keyword = $('.js-search-keyword');
    if ($keyword.length) {
        var $input = $('.js-sws-input');
        $input.val($keyword.val());
        $('.js-toggle-search-mobile').attr('data-no-focus', true).trigger('click');
    }
};

base.infiniteScroll = function () {
    $(document).on('infinitescroll:enable', function () {
        var isInfiniteScrollEnabled = $('body').hasClass(infiniteScrollActive);
        // this will not be called when infinite-scroll-activated in page so it will binded just once.
        if (!isInfiniteScrollEnabled) {
            var showMoreContainer = $('div.show-more');
            $('body').addClass(infiniteScrollActive);
            var scrollHandler = function () {
                // early quit if it is already updating.
                if ($('body').hasClass(infiniteScrollLock)) {
                    return;
                }

                var hasMore = parseInt(showMoreContainer.data('hasMore'), 0);
                // quit early if there is no more product to load
                if (hasMore < 1) {
                    return;
                }

                var scrollBottom = $(window).scrollTop() + $(window).height();
                // find last grid tile top position
                var firePosition = $('.product:last').offset().top;
                if (firePosition < scrollBottom) {
                    firstFirePosition = firePosition;
                    // lock trigger to prevent multiple requests.
                    $('body').addClass(infiniteScrollLock);
                    if ($('.js-infinite-scroll').length) {
                        $('.js-infinite-scroll').trigger('click');
                    } else {
                        // remove lock if there is no button to trigger
                        $('body').removeClass(infiniteScrollLock);
                    }
                }
            };
            // bind scroll events
            $(document).on('scroll', debounce(scrollHandler, infiniteScrollDelay))
                .on('touchmove', debounce(scrollHandler, infiniteScrollDelay));
        }
    });

    $(document)
        .on('search:showMore:success', function () {
            var scrollBottom = $(window).scrollTop() + $(window).height();
            var firePosition = $('.product:last').offset().top;
            if (firePosition < scrollBottom) {
                $('html, body').animate({
                    scrollTop: firstFirePosition - $('header').height()
                }, 500);
            }
            $(document).trigger('infinitescroll:enable');
            $('body').removeClass(infiniteScrollLock);
        });
    $(document).trigger('infinitescroll:enable');
};

base.filter =
base.closeRefinements =
base.resize = function () {
    // Intentionally empty. Don't remove this function.
};

base.headerModalTop();
$win.on('resize', throttle(base.headerModalTop, 100));

module.exports = base;
