'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./productTile'));
});

/**
 * @typedef {Object} QuickViewHtml
 * @property {string} body - Main Quick View body
 */

/**
 * Parse HTML code in Ajax response
 * @param {string} html - Rendered HTML from quickview template
 * @return {QuickViewHtml} - QuickView content components
 */
function parseHtml(html) {
    var $html = $('<div>').append($.parseHTML(html));

    var body = $html.find('.js-register-container');

    return { body: body };
}

/**
 * Generates the modal window on the first call.
 */
function getModalHtmlElement() {
    if ($('#subscribeModalForm').length !== 0) {
        $('#subscribeModalForm').remove();
    }
    var htmlString = '<!-- Modal -->'
        + '<div class="subscribe-modal">'
        + '<div class="modal fade" id="subscribeModalForm" role="dialog">'
        + '<div class="modal-dialog">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + '<button type="button" class="close" data-dismiss="modal"></button>'
        + '<h3 class="modal-header-text"></h3>'
        + '</div>'
        + '<div class="modal-body"></div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>';
    $('body').append(htmlString);
}


/**
 * replaces the content in the modal window on for the selected product variation.
 * @param {string} selectedValueUrl - url to be used to retrieve a new product model
 */
function fillModalElement(selectedValueUrl) {
    $('.body').spinner().start();
    var $form = $('.subscribe-email-form');

    $.ajax({
        url: selectedValueUrl,
        method: 'GET',
        dataType: 'json',
        data: $form.serialize(),
        success: function (data) {
            var parsedHtml = parseHtml(data.renderedTemplate);
            $('#subscribeModalForm .modal-body').empty();
            $('#subscribeModalForm .modal-body').html(parsedHtml.body);
            $('#subscribeModalForm .modal-header-text').text(data.headerText);
            $('#subscribeModalForm .modal-header .close .sr-only').text(data.closeButtonText);
            $('#subscribeModalForm').modal('show');

            $.spinner().stop();
        },
        error: function () {
            $.spinner().stop();
        }
    });
}

$('body').on('click', '.js-footer-newsletter-btn', function (e) {
    e.preventDefault();
    var $this = $('#newsletterEmail');
    setTimeout(function () {
        if (!$this.hasClass('is-invalid')) {
            var selectedValueUrl = $this.closest('form').attr('action');
            getModalHtmlElement();
            fillModalElement(selectedValueUrl);
        }
    }, 500);
});
