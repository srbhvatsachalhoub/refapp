'use strict';
var base = require('brand_core/product/wishlist');
/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {jQuery} $button - button that was clicked to add a product to the wishlist
 */
function displayMessage(data, $button) {
    // Stop spinner
    $.spinner().stop();

    var status;
    var showMessage = true;
    var pid = $button.attr('data-pid');

    // Check result
    if (data.success) {
        $('.js-wishlist-product-added[data-pid="' + pid + '"]').removeClass('d-none');
        $('.add-to-wish-list[data-pid="' + pid + '"]').addClass('d-none');
        status = 'alert-success';

        if (!$button.data('show-success-message')) {
            showMessage = false;
        }
    } else {
        status = 'alert-danger';
    }

    if (showMessage) {
        var $messageContainer = $('.add-to-wishlist-messages');

        if ($messageContainer.length === 0) {
            $messageContainer = $('<div class="add-to-wishlist-messages"></div>');
            $('body').append($messageContainer);
        }

        $messageContainer.append('<div class="add-to-wishlist-alert text-center ' + status + '">' + data.msg + '</div>');

        setTimeout(function () {
            $messageContainer.remove();
            $button.removeAttr('disabled');
        }, 5000);
    }
}

module.exports = {
    removeToWishlist: function () {
        $(document).on('click', '.js-wishlist-product-added', function (e) {
            e.preventDefault();

            var $this = $(this);
            var url = $this.data('url');
            var pid = $this.data('pid');

            // Don't go further, if we don't have a url or product id
            if (!url || !pid) return;

            var $productContainer = $this.closest('.product-detail');
            var optionId = $productContainer.find('.product-option').data('option-id');
            var optionVal = $productContainer.find('.options-select option:selected').data('value-id');

            // Show loding animation
            $.spinner().start();

            // Call add to wishlist endpoint
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    pid: pid,
                    optionId: optionId || null,
                    optionVal: optionVal || null
                },
                success: function () {
                    $('.js-wishlist-product-added[data-pid="' + pid + '"]').addClass('d-none');
                    $('.add-to-wish-list[data-pid="' + pid + '"]').removeClass('d-none');
                    $.spinner().stop();
                },
                error: function (err) {
                    displayMessage(err, $this);
                }
            });
        });
    },

    addToWishlist: base.addToWishlist
};
