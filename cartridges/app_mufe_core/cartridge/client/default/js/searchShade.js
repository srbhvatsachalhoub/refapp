/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
'use strict';

var debounce = require('lodash/debounce');
var endpoint = $('#suggestions-shade-url').data('url');
var minChars = 3;

/**
 * Retrieves Suggestions element relative to scope
 *
 * @param {Object} scope - Search input field DOM element
 * @return {JQuery} - .suggestions-wrapper element
 */
function getSuggestionsShadeWrapper(scope) {
    return $(scope).parents('.custom-shade-search').find('.suggestions-shade-wrapper');
}

/**
 * Determines whether DOM element is inside the .search-mobile class
 *
 * @param {Object} scope - DOM element, usually the input.search-field element
 * @return {boolean} - Whether DOM element is inside  div.search-mobile
 */
function isMobileSearch(scope) {
    return !!$(scope).closest('.search-mobile').length;
}


/**
 * Toggle search field icon from search to close and vice-versa
 *
 * @param {string} action - Action to toggle to
 */
function toggleSuggestionsIcon(action) {
    var mobileSearchIcon = '.search-mobile span.';
    var iconSearch = 'icon-SEARCH';
    var iconSearchClose = 'fa-close';

    if (action === 'close') {
        $(mobileSearchIcon + iconSearch).removeClass(iconSearch).addClass(iconSearchClose);
    } else {
        $(mobileSearchIcon + iconSearchClose).removeClass(iconSearchClose).addClass(iconSearch);
    }
}

/**
 * Determines whether the "More Content Below" icon should be displayed
 *
 * @param {Object} scope - DOM element, usually the input.search-field element
 */
function handleMoreContentBelowIcon(scope) {
    if (($(scope).scrollTop() + $(scope).innerHeight()) >= $(scope)[0].scrollHeight) {
        $('.more-below').fadeOut();
    } else {
        $('.more-below').fadeIn();
    }
}

/**
 * Positions Suggestions panel on page
 *
 * @param {Object} scope - DOM element, usually the input.search-field element
 */
function positionSuggestions(scope) {
    var outerHeight;
    var $scope;
    var $suggestions;
    var top;

    if (isMobileSearch(scope)) {
        $scope = $(scope);
        top = $scope.offset().top;
        outerHeight = $scope.outerHeight();

        // $suggestions = getSuggestionsWrapper(scope).find('.suggestions');
        // $suggestions.css('top', top + outerHeight);

        handleMoreContentBelowIcon(scope);

        // Unfortunately, we have to bind this dynamically, as the live scroll event was not
        // properly detecting dynamic suggestions element's scroll event
        $suggestions.scroll(function () {
            handleMoreContentBelowIcon(this);
        });
    }
}

/**
 * Process Ajax response for SearchServices-GetSuggestionsShade
 *
 * @param {Object|string} response - Empty object literal if null response or string with rendered
 *                                   suggestions template contents
 */
function processResponseShade(response) {
    var $suggestionsWrapper = getSuggestionsShadeWrapper(this).empty();
    $.spinner().stop();
    if (!(typeof (response) === 'object')) {
        $suggestionsWrapper.append(response).fadeIn();
        positionSuggestions(this);
        if (isMobileSearch(this)) {
            toggleSuggestionsIcon('close');
        }
        $('#no-result-search').text($('.search-field').val());
    } else {
        $suggestionsWrapper.fadeOut();
    }
    $('.custom-shade-search').show();
    $('#YMK-wrapper').focus();
}

/**
 * Retrieve suggestions
 *
 * @param {Object} scope - Search field DOM element
 */
function getSuggestionsShadeProduct(scope) {
    if ($(scope).attr('data-value').length >= minChars) {
        $.spinner().start();
        $.ajax({
            context: scope,
            url: $(scope).attr('data-url') + $(scope).attr('data-value'),
            method: 'GET',
            success: processResponseShade,
            error: function () {
                $.spinner().stop();
            }
        });
        $('.product-view-suggestion').removeClass('col-md-4').addClass('col-md-12');
    } else {
        toggleSuggestionsIcon('search');
        getSuggestionsShadeWrapper(scope).empty();
    }
}

module.exports = function () {
    $('#threshold').on('shadefinder:UpdateShadeList', function (event, a, b) {
        getSuggestionsShadeProduct($(this).parent('.shade-circle-wrapper'), b);
    });

    $('.skintone-click1, .skintone-click2, .skintone-click3').on('click', function (event, a, b) {
        $('.shadefinder-shadesquare').removeClass('active');
        $(this).find('svg').addClass('active');
        $('html, body').animate({ scrollTop: 150 }, 1000);
        getSuggestionsShadeProduct($(this), b);
        YMK.applyMakeupBySku($(this).attr('data-value'));
    });
};
