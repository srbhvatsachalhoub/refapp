<isinclude template="/components/modules" sf-toolkit="off" />

<isset name="paymentOptionID" value="${ 'paymentOption' in this ? paymentOption.ID : '' }" scope="page" />
<isset name="isCreditCardSelected" value="${
    pdict.order &&
    pdict.order.billing &&
    pdict.order.billing.payment &&
    pdict.order.billing.payment.appliedPaymentInstruments &&
    (!methodId || methodId === paymentOptionID) &&
    (pdict.order.billing.payment.appliedPaymentInstruments.length === 0 ||
    pdict.order.billing.payment.appliedPaymentInstruments.indexOf(paymentOptionID) !== -1)
}" scope="page" />

<isloop items="${pdict.customer.customerPaymentInstruments}" var="paymentInstrument" status="loopStatus">
    <div class="nav-link radio-button-link nav-item saved-payment-instrument ${loopStatus.first && isCreditCardSelected ? 'selected-payment' : ''}"
        data-method-id="${paymentOptionID}"
        data-uuid="${paymentInstrument.UUID}">
        <i></i>
        <div class="saved-credit-card-info">
            <div class="saved-payment-information">
                <isset name="cardType" value="${paymentInstrument.creditCardType.toLowerCase().replace(/\s/g, '')}" scope="page"/>
                <div class="d-flex align-items-center">
                    <img class="card-image"
                        src="${paymentInstrument.cardTypeImage.src}"
                        alt="${paymentInstrument.cardTypeImage.alt}" />
                    <span class="mx-2"></span>
                    <div class="d-flex flex-column flex-xl-shrink-0">
                        <div class="d-flex text-nowrap">
                            <span class="mx-1">&nbsp;</span>
                            <span class="saved-credit-card-number">${paymentInstrument.maskedCreditCardNumber.slice(-5)}</span>
                        </div>
                        <div class="nav-link-note">
                            <iscontentasset aid="${'checkout-saved-credit-card-cta-text-' + cardType}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="mx-3"></div>
            <div class="required saved-security-code">
                <div class="security-code-input ${loopStatus.first ? '' : 'checkout-hidden'}">
                    <span class="info-icon">
                        <i class="sc-icon-credit-card-with-cvv-code"></i>
                        <div class="tooltip d-none">
                            ${Resource.msg('tooltip.security.code','creditCard',null)}
                        </div>
                    </span>

                    <input type="text"
                        class="form-control saved-payment-security-code"
                        maxlength="${paymentInstrument.creditCardType.toLowerCase() === 'amex' ? '4' : '3'}"
                        placeholder="${Resource.msg('label.credit.card-security.code','checkout',null)}"
                        aria-describedby="savedPaymentSecurityCodeInvalidMessage"/>
                    <div class="invalid-feedback" id="savedPaymentSecurityCodeInvalidMessage">${Resource.msg('error.message.security.code.required', 'checkout', null)}</div>
                </div>
            </div>
        </div>
        <isif condition="${loopStatus.first}">
            <span class="label-default-method ml-auto text-right">
                ${Resource.msg('label.default.method', 'creditCard', null)}
            </span>
        </isif>
    </div>
</isloop>
