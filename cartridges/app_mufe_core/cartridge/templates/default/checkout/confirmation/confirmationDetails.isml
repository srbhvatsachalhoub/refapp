<div class="order-detail-section">
    <div class="row">
        <div class="col-lg-6">
            <div class="order-detail-item">
                <span class="order-number-label">
                    ${Resource.msg('label.orderhistory.ordernumber', 'account', null)}
                </span>
                <span class="order-number">
                    <isprint value="${pdict.order.orderNumber}" />
                </span>
            </div>
            <div class="order-detail-item">
                <span class="order-date-label">${Resource.msg('label.orderhistory.dateordered', 'account', null)}</span>
                <span class="order-date">
                    <isprint value="${pdict.order.creationDate}" style="DATE_SHORT" />
                </span>
            </div>
            <div class="order-detail-item grand-total">
                <span>${Resource.msg('label.orderhistory.totalprice', 'account', null)}</span>
                <span class="grand-total-sum">
                    <isprint value="${pdict.order.totals.grandTotal}" />
                </span>
            </div>
            <div class="order-detail-item">
                <span>${Resource.msg('label.orderhistory.status', 'account', null)}</span>
                <span>
                    <isprint value="${Resource.msg('order.status.' + pdict.order.orderStatus.displayValue, 'order', pdict.order.orderStatus.displayValue)}" />
                </span>
            </div>
        </div>
        <isif condition="${pdict.order.shipping && pdict.order.shipping.length > 0 && pdict.order.shipping[0].trackingUrl && pdict.order.orderStatus.value !== dw.order.Order.ORDER_STATUS_CANCELLED}">
            <div class="col-lg-6 track-order-button-wrapper">
                <div class="w-100 w-xl-auto">
                    <a href="${pdict.order.shipping[0].trackingUrl}" class="btn btn-primary" target="_blank">
                        ${Resource.msg('link.track.your.order', 'account', null)}
                    </a>
                </div>
            </div>
        </isif>
    </div>
</div>
<div class="order-detail-section">
    <div class="row payment-shipping-details">
        <div class="col-xl-6 col-md-7">
            <div class="order-detail-section-header">
                ${Resource.msg('label.payment.details', 'account', null)}
            </div>

            <iscomment>Subtotal</iscomment>
            <div class="order-detail-item subtotal-item">
                <span>${Resource.msg('label.sub.total', 'cart', null)}</span>
                <span class="sub-total">
                    <isprint value="${pdict.order.totals.subTotal}" />
                </span>
            </div>

            <iscomment>Payment Service Fee</iscomment>
            <isif condition="${pdict.order.totals.serviceFee && pdict.order.totals.serviceFee.value > 0}">
                <div class="order-detail-item">
                    <span>${Resource.msg('payment.service.fee', 'order', null)}</span>
                    <span>
                        <isprint value=" ${pdict.order.totals.serviceFee.formatted}" />
                    </span>
                </div>
            </isif>

            <iscomment>Shipping Cost</iscomment>
            <isif
                condition="${pdict.order.totals.shippingLevelDiscountTotal.isFree}">
                <div class="order-detail-item">
                    <span>${Resource.msg('label.order.shipping.cost', 'confirmation', null)}</span>
                    <span class="text-uppercase">${Resource.msg('label.free', 'pricing', null)}</span>
                </div>
            <iselseif condition="${pdict.order.totals.shippingLevelDiscountTotal.value === 0}" />
                <div class="order-detail-item">
                    <span>${Resource.msg('label.order.shipping.cost', 'confirmation', null)}</span>
                    <span>
                        <isprint value="${pdict.order.totals.totalShippingCost}" />
                    </span>
                </div>
            <iselse/>
                <div class="order-detail-item">
                    <span>${Resource.msg('label.order.shipping.cost', 'confirmation', null)}</span>
                    <span>
                        <isprint value="${pdict.order.totals.totalShippingCost}" />
                    </span>
                </div>
                <div class="order-detail-item">
                    <span>${Resource.msg('label.shipping.discount', 'common', null)}</span>
                    <span class="is-discount">
                        -
                        <isprint value="${pdict.order.totals.shippingLevelDiscountTotal.formatted}" />
                    </span>
                </div>
            </isif>

            <iscomment>Order Discount</iscomment>
            <isif condition="${pdict.order.totals.orderLevelDiscountTotal.value > 0}">
                <div class="order-detail-item">
                    <span>${Resource.msg('label.order.discount', 'common', null)}</span>
                    <span class="is-discount">
                        -
                        <isprint value="${pdict.order.totals.orderLevelDiscountTotal.formatted}" />
                    </span>
                </div>
            </isif>

            <iscomment>Grand Total</iscomment>
            <div class="order-detail-item grand-total">
                <span>
                    ${Resource.msg('label.orderhistory.ordertotal', 'account', null)}
                    <span class="vat-label">
                        <isif condition="${'taxRate' in pdict.order.totals}">
                            (${Resource.msgf('label.including.x.percent.vat', 'account', null, pdict.order.totals.taxRate)})
                        <iselse/>
                            (${Resource.msg('label.including.vat', 'account', null)})
                        </isif>
                    </span>
                </span>
                <span class="grand-total-sum">
                    <isprint value="${pdict.order.totals.grandTotal}" />
                </span>
            </div>

            <iscomment>Payment Info</iscomment>
            <div class="order-payment-info">
                <isloop var="shippingModel" items="${pdict.order.shipping}" status="shippingLoop">
                    <isif condition="${shippingLoop.first}">
                        <isloop items="${pdict.order.billing.payment.selectedPaymentInstruments}" var="payment">
                            <isif condition="${payment.paymentMethod === 'CREDIT_CARD'}">
                                <isset name="cardType" value="${payment.type.toLowerCase().replace(/\s/g, '')}" scope="page"/>
                                <div class="d-flex">
                                    <span class="text-uppercase">
                                        ${Resource.msg('label.' + cardType, 'creditCard', null)}
                                    </span>
                                    <span>&nbsp;</span>
                                    <span>*${payment.lastFour}</span>
                                    <span class="mx-3"></span>
                                    <iscreditcard creditcard_type="${payment.type}" />
                                </div>
                            </isif>
                            <isif condition="${payment.paymentMethod === 'COD'}">
                                <span class="text-uppercase">
                                    ${Resource.msg('msg.payment.type.cod', 'confirmation', 'Cash on Delivery')}
                                </span>
                            </isif>
                            <isif condition="${payment.paymentMethod === 'GIFT_CERTIFICATE'}">
                                <span class="text-uppercase">
                                    ${Resource.msg('msg.payment.giftcertificate', 'confirmation', 'Gift Certificate')}
                                </span>
                            </isif>
                            <isif condition="${payment.paymentMethod === 'DW_APPLE_PAY'}">
                                <span class="text-uppercase">
                                    ${Resource.msg('msg.paid.using.apple.pay', 'applePay', 'Apple Pay')}
                                </span>
                            </isif>
                        </isloop>
                    </isif>
                </isloop>
            </div>
        </div>
        <div class="seperator-mobile d-block d-sm-none">
        </div>
        <div class="col-xl-6 col-md-5">
            <div class="order-detail-section-header">
                ${Resource.msg('label.shipping.details', 'account', null)}
            </div>
            <div class="shipping-method-wrapper">
                <isloop var="shippingModel" items="${pdict.order.shipping}" status="shippingLoop">
                    <isif condition="${shippingLoop.first}">
                        <span class="shipping-method-title">
                            ${Resource.msgf('label.orderhistory.shippingmethod', 'account', null, shippingModel.selectedShippingMethod.displayName)}
                        </span>
                    </isif>
                </isloop>
            </div>
        </div>
    </div>
</div>
<div class="order-detail-section">
    <div class="order-detail-section-header">
        ${Resource.msg('confirmation.your.items', 'confirmation', null)}
    </div>
    <isloop items="${pdict.order.shipping}" var="shippingModel">
        <div class="confirmation-items">
            <isloop items="${shippingModel.productLineItems.items}" var="lineItem">
                <isif condition="${lineItem.isBonusProductLineItem}">
                    <iscontinue/>
                </isif>
                <isinclude template="checkout/confirmation/confirmationProducts" />
            </isloop>
        </div>
    </isloop>
    <isloop items="${pdict.order.shipping}" var="shippingModel">
        <isif condition="${shippingModel.hasBonusProduct}">
            <div class="confirmation-gift-bonus">
                <isinclude template="checkout/confirmation/confirmationBonusItems" />
            </div>
        </isif>
    </isloop>
    <isset name="foundGiftWrapping" value="${false}" scope="page" />
    <isloop items="${pdict.order.shipping}" var="shippingModel">
        <isif condition="${shippingModel.isGift && foundGiftWrapping === false}">
            <isset name="foundGiftWrapping" value="${true}" scope="page" />
            <div class="confirmation-gift-items">
                <isinclude template="checkout/confirmation/confirmationGift" />
            </div>
        </isif>
    </isloop>
</div>
<div class="order-detail-section">
    <div class="row shipping-billing-address">
        <div class="col-sm-6">
            <div class="order-detail-section-header">
                ${Resource.msg('label.order.shipping.address', 'confirmation', null)}
            </div>
            <div class="order-summary-item">
                <isloop var="shippingModel" items="${pdict.order.shipping}" status="shippingLoop">
                    <isif condition="${shippingLoop.first}">
                        <div class="single-shipping" data-shipment-summary="${shippingModel.UUID}">
                            <isif condition="${shippingModel.shippingAddress !== null}">
                                <isset name="address" value="${shippingModel.shippingAddress}" scope="page" />
                            <iselse/>
                                <isset name="address" value="{}" scope="page" />
                            </isif>
                            <div class="summary-details shipping">
                                <isinclude template="checkout/addressSummary" />
                                <div class="shipping-phone" dir="ltr">
                                    ${shippingModel.shippingAddress && shippingModel.shippingAddress.phone ?
                                    shippingModel.shippingAddress.phone : ''}
                                </div>
                            </div>
                        </div>
                    </isif>
                </isloop>
            </div>
        </div>
        <div class="seperator-mobile d-block d-sm-none"></div>
        <div class="col-sm-6">
            <div class="order-detail-section-header">
                ${Resource.msg('label.order.billing.address', 'confirmation', null)}
            </div>
            <div class="order-summary-item">
                <isif condition="${pdict.order.billing.billingAddress.address !== null}">
                    <isset name="address" value="${pdict.order.billing.billingAddress.address}" scope="page" />
                <iselse/>
                    <isset name="address" value="{}" scope="page" />
                </isif>
                <div class="summary-details billing">
                    <isinclude template="checkout/addressSummary" />
                    <isif condition="${pdict.order.billing.billingAddress.address.phone}">
                        <span class="order-summary-phone" dir="ltr">${pdict.order.billing.billingAddress.address.phone}</span>
                    </isif>
                </div>
            </div>
        </div>
    </div>
</div>
