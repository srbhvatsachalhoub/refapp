'use strict';

/* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["model"] }] */
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');

/**
 * Helper to encapsulate common code for building a carousel
 *
 * @param {Object} model - model object for a component
 * @param {Object} context - model object for a component
 * @return {Object} model - prepared model
 */
function init(model, context) {
    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    var content = context.content;
    var config = context.content.jsonConfig;
    var numberOfSlides = model.regions.slides.region.size;

    if (config) {
        if (config.cssClass) {
            model.carouselClass = config.cssClass;
        }

        if (config.cssSlideClass) {
            model.regions.slides.setClassName(config.cssSlideClass);
        }

        model.regions.slides.setAttribute('data-initial-item-count', numberOfSlides);

        if (config.slidesToShow && config.slidesToShow.length > 0) {
            model.regions.slides.setAttribute(
                'data-carousel-slides-to-show',
                '[' + config.slidesToShow.join() + ']'
            );
        }

        if (config.carouselCenterMode && config.carouselCenterMode.length > 0) {
            model.regions.slides.setAttribute(
                'data-carousel-center-mode',
                '[' + config.carouselCenterMode.join() + ']'
            );
        }

        if (config.carouselCenterPadding && config.carouselCenterPadding.length > 0) {
            model.regions.slides.setAttribute(
                'data-carousel-center-padding',
                '[' + config.carouselCenterPadding.join() + ']'
            );
        }

        if (config.itemtype) {
            model.regions.slides.setAttribute('itemtype', config.itemtype);
        }

        if (config.itemid) {
            model.regions.slides.setAttribute('itemid', config.itemid);
        }
    }

    model.regions.slides.setComponentClassName('carousel-item');
    model.regions.slides.setComponentClassName('carousel-item active');

    for (var i = 0; i < numberOfSlides; i++) {
        model.regions.slides.setComponentAttribute('data-position', i, { position: i });
    }

    if (context.component.typeID === 'einstein.einsteinCarousel'
        || context.component.typeID === 'einstein.einsteinCarouselProduct'
        || context.component.typeID === 'einstein.einsteinCarouselCategory') {
        numberOfSlides = context.content.count;
    }

    model.numberOfSlides = model.regions.slides.region.size;
    if (context.component.typeID === 'einstein.einsteinCarousel'
        || context.component.typeID === 'einstein.einsteinCarouselProduct'
        || context.component.typeID === 'einstein.einsteinCarouselCategory') {
        model.numberOfSlides = context.content.count - 1;
    }
    model.id = 'carousel-' + context.component.getID();
    model.fullWidth = content.fullWidth;
    model.title = content.textHeadline ? context.content.textHeadline : null;
    model.product = content.productCarousel ? context.content.productCarousel : false;
    model.bannerCarousel = content.bannerCarousel;
    model.mode = content.mode ? content.mode : 'black';
    return model;
}

module.exports = {
    init: init
};
