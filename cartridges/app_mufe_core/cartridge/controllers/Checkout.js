'use strict';

var server = require('server');

server.extend(module.superModule);
/**
 * Appends guestemail form to Checkout Login
 */
server.append('Login', function (req, res, next) {
    if (req.currentCustomer.profile) {
        return next();
    }
    var guestEmailForm = server.forms.getForm('guestemail');
    guestEmailForm.clear();
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();
    if (!currentBasket) {
        var URLUtils = require('dw/web/URLUtils');
        res.setStatusCode(301);
        res.redirect(URLUtils.url('Cart-Show'));
        return next();
    }
    if (currentBasket && currentBasket.getCustomerEmail()) {
        guestEmailForm.email.value = currentBasket.getCustomerEmail();
    }

    res.setViewData({
        guestEmailForm: guestEmailForm
    });
    return next();
});
/**
 * Force guests to provide e-mail address
 */
server.prepend('Begin', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket || (!req.currentCustomer.profile && !currentBasket.getCustomerEmail())) {
        res.setStatusCode(301);
        res.redirect(URLUtils.url('Cart-Show'));
        return next();
    }

    return next();
});

module.exports = server.exports();
