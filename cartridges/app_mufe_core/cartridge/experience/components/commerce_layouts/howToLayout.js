'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
/**
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    var content = context.content;
    model.header = content.header;
    model.showBackToTop = content.showBacktoTop;

    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('label.how.to', 'common', null)
        }
    ];
    model.breadcrumbs = breadcrumbs;

    return new Template('experience/components/commerce_layouts/howToLayout').render(model).text;
};
