'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var csrfProtection = require('dw/web/CSRFProtection');
var server = require('server');
var Resource = require('dw/web/Resource');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var subscribeForm = server.forms.getForm('subscribe');
    subscribeForm.clear();
    var model = new HashMap();
    model.conciergeToolImageDesktopURL = content.conciergeToolImageDesktop.file.URL;
    model.conciergeToolImageTabletURL = content.conciergeToolImageTablet.file.URL;
    model.conciergeToolImageMobileURL = content.conciergeToolImageMobile.file.URL;
    model.conciergeToolImageAlt = content.conciergeToolImageAlt;
    model.conciergeToolTitle = content.conciergeToolTitle;
    model.conciergeToolText = content.conciergeToolText;
    model.conciergeToolLink = content.conciergeToolLink;
    model.imageDesktopURL = content.imageDesktop.file.URL;
    model.imageTabletURL = content.imageTablet.file.URL;
    model.imageMobileURL = content.imageMobile.file.URL;
    model.fullWidth = content.fullWidthNewsletter ? content.fullWidthNewsletter : false;
    model.title = content.title;
    model.subTitle = content.subTitle;
    model.label = content.label;
    model.buttonDisplayName = content.buttonDisplayName;
    model.subscribeForm = subscribeForm;
    model.formImage = content.formImage ? content.formImage.file.URL : '';
    model.formImageAltText = content.formImageAltText ? content.formImageAltText : '';
    model.formButtonDisplayName = content.formButtonDisplayName ? content.formButtonDisplayName : Resource.msg('button.form.newsletter.offer', 'forms', null);

    var csrf = {
        tokenName: csrfProtection.getTokenName(),
        token: csrfProtection.generateToken()
    };
    model.csrf = csrf;
    return new Template('experience/components/commerce_assets/footerNewsletter').render(model).text;
};
