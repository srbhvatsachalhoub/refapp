'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.imageURL = content.image.file.URL;
    model.imageAlt = content.imageAlt ? content.imageAlt : '';
    model.title = content.title ? content.title : '';
    model.link = content.link;
    model.linkDisplay = content.linkDisplay ? content.linkDisplay : '';
    var cat = content.category;
    if (cat) {
        model.link = content.link ? content.link : URLUtils.url('Search-Show', 'cgid', cat.getID()).toString();
    }

    return new Template('experience/components/commerce_assets/categoryTile').render(model).text;
};
