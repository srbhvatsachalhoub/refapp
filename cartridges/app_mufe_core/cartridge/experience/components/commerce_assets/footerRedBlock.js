'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();

    model.title = content.title ? content.title : '';
    model.icon = content.icon;
    model.form = content.form;
    model.id = content.form.header.replace(/\s/g, '');

    return new Template('experience/components/commerce_assets/footerRedBlock').render(model).text;
};
