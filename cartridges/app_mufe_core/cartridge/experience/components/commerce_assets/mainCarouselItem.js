'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.imageURL = content.image.file.URL;
    model.title = content.title;
    model.imageAltText = content.imageAltText;
    model.link = content.link;
    var cat = content.category;
    if (content.alternativeLink) {
        model.link = URLUtils.url('Page-Show', 'cid', content.alternativeLink);
    } else if (content.link) {
        model.link = content.link ? content.link : URLUtils.url('Search-Show', 'cgid', cat.getID()).toString();
    }

    var mobile = {
        ImageURL: content.mobileImage.file.URL,
        header: content.header,
        buttonName: content.buttonName
    };
    model.mobile = mobile;
    return new Template('experience/components/commerce_assets/mainCarouselItem').render(model).text;
};
