'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.imageURL = content.image.file.URL;
    model.title = content.title;
    model.description = content.description;
    model.buttonDisplayName = content.buttonDisplayName;
    model.link = content.link;

    return new Template('experience/components/commerce_assets/brandContent').render(model).text;
};
