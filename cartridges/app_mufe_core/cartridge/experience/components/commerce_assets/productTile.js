'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    var ProductFactory = require('*/cartridge/scripts/factories/product');

    var productTileParams = { pview: 'tile', pid: context.content.product.ID };
    var product = ProductFactory.get(productTileParams);
    var productUrl = URLUtils.url('Product-Show', 'pid', product.id).relative().toString();
    var swatchLength = context.content.product.variants.length;
    if (swatchLength <= 0 && context.content.product.masterProduct) {
        swatchLength = context.content.product.masterProduct.variants.length;
    }
    model.product = product;
    model.swatchLength = swatchLength;
    model.display = {
        showAddToBagButton: content.showAddToBagButton,
        showShopNowButton: content.showShopNowButton,
        showQuickView: content.showQuickView,
        ratings: content.showRatings,
        showBadges: content.showBadges,
        showSwatches: content.showSwatches,
        showWishlistButton: content.showWishlistButton ? content.showWishlistButton : true
    };

    model.urls = {
        product: productUrl
    };

    return new Template('experience/components/commerce_assets/productTile').render(model).text;
};
