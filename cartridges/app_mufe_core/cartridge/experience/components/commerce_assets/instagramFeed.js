'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var instagramHelpers = require('*/cartridge/scripts/helpers/instagramHelpers');
/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.title = content.title ? content.title : '';
    model.buttonTitle = content.buttonTitle;
    model.accountId = content.accountId;
    var maxId = '';

    var result = instagramHelpers.getInstagramMedia(model.accountId, maxId);
    model.maxId = maxId;
    model.data = result;

    return new Template('experience/components/commerce_assets/instagramFeed').render(model).text;
};
