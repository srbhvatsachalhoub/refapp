'use strict';

// GIFTCERT : Module contains all the functions for GiftCertificate Checkout.
var giftCert = (function () {
    var giftCertificatePaymentMethod = 'GIFT_CERTIFICATE';

    /**
     * method responsible to to refresh the UI components once a GC is applied or removed.
     * Accepts an object of totals, gift cards currently in the basket and renders in UI
     * @param {Object} totals - the totals data
     */
    var refreshGiftCardList = function (totals) {
        if (!totals || !totals.giftCardTotals || !totals.giftCardTotals.paymentInstruments) {
            return;
        }
        var $giftCardList = $('.js-gift-card-list');

        // Hide all Gift card related stuffs
        $giftCardList.hide().empty();


        if (totals.giftCardTotals.paymentInstruments.length > 0) {
            var $appliedGiftCardTpl = $('#appliedGiftCardTpl');
            for (var i = 0; i < totals.giftCardTotals.paymentInstruments.length; i++) {
                var giftCardPayment = totals.giftCardTotals.paymentInstruments[i];
                var $tpl = $appliedGiftCardTpl
                    .clone()
                    .removeClass('d-none')
                    .removeAttr('id');

                $tpl.find('.js-btn-remove-gift-code').data('transaction', giftCardPayment.transactionID);
                $tpl.find('.js-gift-card-code').text(giftCardPayment.maskedCode);
                $tpl.find('.js-gift-card-amount').text(giftCardPayment.amount);

                $giftCardList.append($tpl);
            }

            $giftCardList.show();
        }

        if (totals.nonComplimentaryPaymentAmount) {
            if (totals.nonComplimentaryPaymentAmount.value > 0) {
                $('.payment-options .nav-item:first-child .nav-link').click();
                $('.billing-payment-options-block').removeClass('d-none');
            } else {
                $('.billing-payment-options-block').addClass('d-none');
                $("input[name$='_paymentMethod']").val(giftCertificatePaymentMethod);
            }
        }
    };

    /**
     * Add method is used to apply a gift card, method invoked on click of apply button.
     * once a card is successfully applied refresh the Order total.
     * @param {Object} $giftCertCode JQuery GiftCertificated Code Input object
     * @param {string} addGCUrl DW service url to add the gift card
     */
    var addGiftCard = function ($giftCertCode, addGCUrl) {
        if (!$giftCertCode || !addGCUrl) {
            return;
        }

        $.spinner().start();
        $.ajax({
            url: addGCUrl,
            type: 'POST',
            data: {
                giftCertCode: $giftCertCode.val()
            },
            success: function (data) {
                if (data.error) {
                    if (data.redirectUrl) {
                        window.location.href = data.redirectUrl;
                    }

                    giftCert.inValidateField($giftCertCode, data.message);
                    return;
                }

                $('body').trigger('checkout:updateCheckoutView', {
                    order: data.order,
                    customer: data.customer
                });

                refreshGiftCardList(data.order.totals);
            },
            complete: function () {
                $.spinner().stop();
            }
        });
    };

    /**
     * removeGiftCard method is used to remove gift card, method invoked on click of remove button in billing section
     * once removed the Order total needs to be refreshed.
     * @param {string} transactionID Transaction id for the Gift Card
     * @param {string} removeGCUrl DW service url to remove the gift card
     */
    var removeGiftCard = function (transactionID, removeGCUrl) {
        if (!transactionID || !removeGCUrl) {
            return;
        }

        $.spinner().start();
        $.ajax({
            url: removeGCUrl,
            type: 'POST',
            data: {
                transactionID: transactionID
            },
            success: function (data) {
                if (data.error) {
                    if (data.redirectUrl) {
                        window.location.href = data.redirectUrl;
                    }

                    return;
                }

                $('body').trigger('checkout:updateCheckoutView', {
                    order: data.order,
                    customer: data.customer
                });

                refreshGiftCardList(data.order.totals);
            },
            complete: function () {
                $.spinner().stop();
            }
        });
    };

    /**
     * Invalidate given jQuery object
     * @param {Object} $field - JQuery object to be invalidated
     * @param {string} message - error message to be shown
     */
    var inValidateField = function ($field, message) {
        $field.addClass('is-invalid');
        $field.parent().find('.invalid-feedback').text(message).show();
    };

    /**
     * Validate given jQuery object
     * @param {Object} $field - JQuery object to be validated
     */
    var validateField = function ($field) {
        $field.removeClass('is-invalid');
        $field.parent().find('.invalid-feedback').text('').hide();
    };

    /**
     * updates the totals summary for gift card fields
     * @param {Object} totals - the totals data
     */
    function updateTotals(totals) {
        if (totals.giftCardTotals && totals.giftCardTotals.show) {
            $('.js-amount-gift-card').text(totals.giftCardTotals.total.formatted);
            $('.js-amount-due').text(totals.giftCardTotals.due.formatted);
            $('.js-row-amount-gift-card').removeClass('d-none');
            $('.js-row-amount-due').removeClass('d-none');
        } else {
            $('.js-row-amount-gift-card').addClass('d-none');
            $('.js-row-amount-due').addClass('d-none');
        }
    }

    /**
     * Updates the payment information in checkout, based on the supplied order model
     * @param {Object} order - checkout model to use as basis of new truth
     */
    function updatePaymentInformation(order) {
        // update payment details
        var $paymentSummary = $('.payment-details');
        var htmlToAppend = '';

        if (order.billing.payment && order.billing.payment.selectedPaymentInstruments
            && order.billing.payment.selectedPaymentInstruments.length > 0) {
            for (var i = 0; i < order.billing.payment.selectedPaymentInstruments.length; i++) {
                var paymentInstrument = order.billing.payment.selectedPaymentInstruments[i];
                if (paymentInstrument.paymentMethod === 'CREDIT_CARD') {
                    htmlToAppend += '<div><span>' + order.resources.cardType + ' '
                        + paymentInstrument.type
                        + '</span></div><div><span>'
                        + paymentInstrument.maskedCreditCardNumber
                        + '</span></div><div><span>'
                        + order.resources.cardEnding + ' '
                        + paymentInstrument.expirationMonth
                        + '/' + paymentInstrument.expirationYear
                        + '</span></div>';
                } else if (paymentInstrument.paymentMethod === 'GIFT_CERTIFICATE') {
                    htmlToAppend += '<div><span>Gift Certificate</span></div>'
                        + '<div>'
                        + paymentInstrument.maskedGiftCertificateCode
                        + '</div>';
                }
            }
        }

        $paymentSummary.empty().append(htmlToAppend);
    }


    // Public API
    return {
        addGiftCard: addGiftCard,
        removeGiftCard: removeGiftCard,
        inValidateField: inValidateField,
        validateField: validateField,
        updateTotals: updateTotals,
        updatePaymentInformation: updatePaymentInformation
    };
}());

var exports = {
    /**
     * initilization of the gift certificate on checkout
     */
    initialize: function () {

    },

    applyGiftCode: function () {
        /**
         * event triggered on click of apply button in checkout-payment stage.
         * Gift card Code sent to server. on successful validation it added to cart.
         */
        $(document).on('click', '.js-btn-apply-gift-code', function () {
            var addGCUrl = $(this).attr('data-action');
            var $giftCertCode = $('#gift-code');
            var hasError = false;

            giftCert.validateField($giftCertCode);

            var giftCertCodeVal = $giftCertCode.val();
            if (!giftCertCodeVal) {
                giftCert.inValidateField($giftCertCode, $giftCertCode.data('missing-error'));
                hasError = true;
            }

            if (!hasError) {
                giftCert.addGiftCard($giftCertCode, addGCUrl);
            }
        });
    },
    removeGiftCode: function () {
        /**
        * event triggered on click of remove button in checkout-payment stage.
        * if the transaction ID is valid cossosponding Gift card removed from cart and
        * amount is re claimed.
        */
        $(document).on('click', '.js-btn-remove-gift-code', function () {
            var transactionID = $(this).data('transaction');
            var removeGCUrl = $(this).data('action');
            giftCert.removeGiftCard(transactionID, removeGCUrl);
        });
    },

    updateCheckoutView: function () {
        /**
         * extention to checkout:updateCheckoutView
         * update order summary UI for gift certificate
        */
        $('body').on('checkout:updateCheckoutView', function (e, data) {
            giftCert.updateTotals(data.order.totals);
            // giftCert.updatePaymentInformation(data.order); // it is already on app_brand_core:updatePaymentInformation
        });
    }
};

module.exports = exports;
