'use strict';

var server = require('server');

server.extend(module.superModule);

/*
 * CheckoutServices-PlaceOrder extention
 * to check the successfully created order line items
 * to create relevant GiftCertificates
 * and to send GiftCertificate email to the receipent(s)
 */
server.append('PlaceOrder', function (req, res, next) {
    var viewData = res.getViewData();

    if (!viewData.error && viewData.orderID) {
        var OrderMgr = require('dw/order/OrderMgr');
        var Site = require('dw/system/Site');

        // check if the site is GiftCardEnabled
        if (!Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
            return next();
        }

        var order = OrderMgr.getOrder(viewData.orderID);
        if (order) {
            var giftCardHelper = require('*/cartridge/scripts/helpers/giftCardHelper');
            giftCardHelper.createGiftCertificates(order);
        }
    }

    return next();
});

module.exports = server.exports();
