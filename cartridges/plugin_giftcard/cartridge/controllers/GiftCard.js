'use strict';

var server = require('server');

/**
 * Validates gift product add/update action.
 * @param {dw.order.Basket} currentBasket - current user basket.
 * @param {Object} giftCardDetailsForm - The gift card form object
 * @param {Object} res - Response object
 * @returns {boolean} true if valid, false otherwise
*/
function validateAddUpdate(currentBasket, giftCardDetailsForm, res) {
    var Site = require('dw/system/Site');
    var Resource = require('dw/web/Resource');

    // check if the site is GiftCardEnabled and check if the current basket is not empty
    if (!Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled') || !currentBasket) {
        res.json({
            success: false,
            error: [Resource.msg('giftcard.details.error.technical', 'forms', null)],
            message: Resource.msg('giftcard.details.error.technical', 'forms', null)
        });
        return false;
    }

    if (giftCardDetailsForm.valid
        && giftCardDetailsForm.giftCardRecipientEmail.mandatory
        && giftCardDetailsForm.giftCardRecipientEmailConfirm.mandatory
        && giftCardDetailsForm.giftCardRecipientEmail.value !== giftCardDetailsForm.giftCardRecipientEmailConfirm.value.toLowerCase()) {
        giftCardDetailsForm.giftCardRecipientEmailConfirm.valid = false; // eslint-disable-line no-param-reassign
        giftCardDetailsForm.giftCardRecipientEmailConfirm.error = Resource.msg('error.message.mismatch.email', 'forms', null); // eslint-disable-line no-param-reassign
        giftCardDetailsForm.valid = false; // eslint-disable-line no-param-reassign
    }

    // check the form is valid,
    // if not return with form errors
    if (!giftCardDetailsForm.valid) {
        var formErrors = require('*/cartridge/scripts/formErrors');
        res.json({
            success: false,
            error: [Resource.msg('giftcard.details.form.error.message', 'forms', null)],
            message: Resource.msg('giftcard.details.form.error.message', 'forms', null),
            fields: formErrors.getFormErrors(giftCardDetailsForm)
        });
        return false;
    }

    return true;
}

/**
 * Controller To add giftcard product to the customer's basket.
 */
server.post('AddProduct', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');

    var giftCardDetailsForm = server.forms.getForm('giftCardDetails');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();

    if (!validateAddUpdate(currentBasket, giftCardDetailsForm, res)) {
        return next();
    }

    var Transaction = require('dw/system/Transaction');
    var CartModel = require('*/cartridge/models/cart');
    var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var productId = req.form.pid;
    var childProducts = Object.hasOwnProperty.call(req.form, 'childProducts')
        ? JSON.parse(req.form.childProducts)
        : [];
    var options = req.form.options ? JSON.parse(req.form.options) : [];
    var quantity;
    var result;

    if (currentBasket) {
        Transaction.wrap(function () {
            quantity = parseInt(req.form.quantity, 10);
            result = cartHelper.addProductToCart(
                currentBasket,
                productId,
                quantity,
                childProducts,
                options
            );

            if (!result.error) {
                cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
                basketCalculationHelpers.calculateTotals(currentBasket);

                var collections = require('*/cartridge/scripts/util/collections');
                collections.forEach(currentBasket.allProductLineItems, function (pli) {
                    if (pli.UUID === result.uuid) {
                        pli.custom.giftCardFromName = giftCardDetailsForm.giftCardFromName.value; // eslint-disable-line no-param-reassign
                        pli.custom.giftCardRecipientName = giftCardDetailsForm.giftCardRecipientName.value; // eslint-disable-line no-param-reassign
                        pli.custom.giftCardRecipientEmail = giftCardDetailsForm.giftCardRecipientEmail.value; // eslint-disable-line no-param-reassign
                        pli.setGiftMessage(giftCardDetailsForm.giftMessage.value);
                        pli.gift = true; // eslint-disable-line no-param-reassign
                    }
                });
            }
        });
    }

    var quantityTotal = ProductLineItemsModel.getTotalQuantity(currentBasket.productLineItems);
    var cartModel = new CartModel(currentBasket);

    res.json({
        success: true,
        quantityTotal: quantityTotal,
        message: result.message,
        cart: cartModel,
        newBonusDiscountLineItem: {},
        error: result.error,
        pliUUID: result.uuid
    });

    return next();
});

/**
 * Controller To update giftcard product to the customer's basket.
 */
server.post('UpdateProduct', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var giftCardDetailsForm = server.forms.getForm('giftCardDetails');
    var currentBasket = BasketMgr.getCurrentBasket();

    if (!validateAddUpdate(currentBasket, giftCardDetailsForm, res)) {
        return next();
    }

    var ProductMgr = require('dw/catalog/ProductMgr');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var CartModel = require('*/cartridge/models/cart');
    var collections = require('*/cartridge/scripts/util/collections');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var uuid = req.form.uuid;
    var productId = req.form.pid;
    var updateQuantity = parseInt(req.form.quantity, 10);

    var productLineItems = currentBasket.allProductLineItems;
    var requestLineItem = collections.find(productLineItems, function (item) {
        return item.UUID === uuid;
    });

    var uuidToBeDeleted = null;
    var pliToBeDeleted;
    var newPidAlreadyExist = collections.find(productLineItems, function (item) {
        if (item.productID === productId && item.UUID !== uuid) {
            uuidToBeDeleted = item.UUID;
            pliToBeDeleted = item;
            updateQuantity += parseInt(item.quantity, 10);
            return true;
        }
        return false;
    });

    var availableToSell = 0;
    var totalQtyRequested = 0;
    var qtyAlreadyInCart = 0;
    var minOrderQuantity = 0;
    var canBeUpdated = false;
    var availabilityModel;

    if (requestLineItem) {
        availabilityModel = inventoryHelpers.getProductAvailabilityModel(requestLineItem.product);
        availableToSell = availabilityModel.inventoryRecord.ATS.value;
        qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
            productId,
            productLineItems,
            requestLineItem.UUID
        );
        totalQtyRequested = updateQuantity + qtyAlreadyInCart;
        minOrderQuantity = requestLineItem.product.minOrderQuantity.value;
        canBeUpdated = (totalQtyRequested <= availableToSell) &&
            (updateQuantity >= minOrderQuantity);
    }

    var error = false;
    if (canBeUpdated) {
        var product = ProductMgr.getProduct(productId);

        try {
            Transaction.wrap(function () {
                if (newPidAlreadyExist) {
                    var shipmentToRemove = pliToBeDeleted.shipment;
                    currentBasket.removeProductLineItem(pliToBeDeleted);
                    if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
                        currentBasket.removeShipment(shipmentToRemove);
                    }
                }

                requestLineItem.replaceProduct(product);
                requestLineItem.setQuantityValue(updateQuantity);

                requestLineItem.custom.giftCardFromName = giftCardDetailsForm.giftCardFromName.value;
                requestLineItem.custom.giftCardRecipientName = giftCardDetailsForm.giftCardRecipientName.value;
                requestLineItem.custom.giftCardRecipientEmail = giftCardDetailsForm.giftCardRecipientEmail.value;
                requestLineItem.setGiftMessage(giftCardDetailsForm.giftMessage.value);
                requestLineItem.gift = true;

                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        } catch (e) {
            error = true;
        }
    }

    if (!error && requestLineItem && canBeUpdated) {
        var cartModel = new CartModel(currentBasket);

        var responseObject = {
            success: true,
            cartModel: cartModel,
            newProductId: productId
        };

        if (uuidToBeDeleted) {
            responseObject.uuidToBeDeleted = uuidToBeDeleted;
        }

        res.json(responseObject);
    } else {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product', 'cart', null)
        });
    }

    return next();
});

/**
 * Controller To add giftcard certificate on checkout page.
 * Using the SFCC GiftCertificate mechanism.
 */
server.post('AddGiftCertificate', function (req, res, next) {
    var Resource = require('dw/web/Resource');

    var giftCertCode = req.form.giftCertCode;
    // check submitted giftCertCode
    if (!giftCertCode) {
        res.json({
            error: true,
            message: Resource.msg('error.message.required', 'forms', null)
        });
        return next();
    }

    var BasketMgr = require('dw/order/BasketMgr');
    var URLUtils = require('dw/web/URLUtils');

    var currentBasket = BasketMgr.getCurrentBasket();

    // check the current basket
    if (!currentBasket) {
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }

    var giftCardHelper = require('*/cartridge/scripts/helpers/giftCardHelper');

    var giftCertificateResult = giftCardHelper.getGiftCertificateByCode(giftCertCode, currentBasket.getCurrencyCode());
    if (giftCertificateResult.error) {
        res.json({
            error: true,
            message: giftCertificateResult.status.message
        });
        return next();
    }

    // create gift card payment instrument
    var gcPaymentInstrument = giftCardHelper.createGiftCertificatePaymentInstrument(giftCertificateResult.giftCertificate, currentBasket);
    if (!gcPaymentInstrument) {
        res.json({
            error: true,
            message: Resource.msg('giftcertificate.error.unexpected', 'forms', null)
        });
        return next();
    }

    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var Locale = require('dw/util/Locale');

    var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
    var currentLocale = Locale.getLocale(req.locale.id);

    var basketModel = new OrderModel(
        currentBasket,
        { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' }
    );

    var accountModel = new AccountModel(req.currentCustomer);
    res.json({
        customer: accountModel,
        order: basketModel,
        error: false
    });

    return next();
});

/**
 * Controller To remove giftcard certificate on checkout page.
 * to be called only if gift card already applied to basket.
 * it scans all the payment instrument and removes the one having req.form.transactionID
 */
server.post('RemoveGiftCertificate', function (req, res, next) {
    var transactionID = req.form.transactionID;
    // check submitted transactionID
    if (!transactionID) {
        res.json({
            error: true
        });
        return next();
    }

    var BasketMgr = require('dw/order/BasketMgr');
    var URLUtils = require('dw/web/URLUtils');

    var currentBasket = BasketMgr.getCurrentBasket();

    // check the current basket
    if (!currentBasket) {
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }

    var giftCardHelper = require('*/cartridge/scripts/helpers/giftCardHelper');
    var success = giftCardHelper.removeGiftCertificatePaymentInstrument(transactionID, currentBasket);

    if (!success) {
        res.json({
            error: true
        });
        return next();
    }

    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var Locale = require('dw/util/Locale');

    var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
    var currentLocale = Locale.getLocale(req.locale.id);

    var basketModel = new OrderModel(
        currentBasket,
        { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' }
    );

    var accountModel = new AccountModel(req.currentCustomer);
    res.json({
        customer: accountModel,
        order: basketModel,
        error: false
    });

    return next();
});

module.exports = server.exports();
