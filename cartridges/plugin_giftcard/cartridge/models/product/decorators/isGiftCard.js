'use strict';

/**
 * Decorate product object with giftCard properties
 *
 * @param {Object} object - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 */
module.exports = function (object, apiProduct) {
    if (apiProduct.custom.giftCardType.value) {
        Object.defineProperties(object, {
            isGiftCard: {
                enumerable: true,
                value: true
            },
            giftCardType: {
                enumerable: true,
                value: apiProduct.custom.giftCardType.value
            }
        });
    } else {
        Object.defineProperty(object, 'isGiftCard', {
            enumerable: true,
            value: false
        });
    }
};
