'use strict';

/**
 * Decorate product object with giftCard properties
 *
 * @param {Object} object - Product Model to be decorated
 * @param {Object} options - Options passed in from the factory
 */
module.exports = function (object, options) {
    if (options.lineItem && options.lineItem.gift) {
        Object.defineProperties(object, {
            giftCardFromName: {
                enumerable: true,
                value: options.lineItem.custom.giftCardFromName
            },
            giftCardRecipientName: {
                enumerable: true,
                value: options.lineItem.custom.giftCardRecipientName
            },
            giftCardRecipientEmail: {
                enumerable: true,
                value: options.lineItem.custom.giftCardRecipientEmail
            },
            giftMessage: {
                enumerable: true,
                value: options.lineItem.giftMessage
            },
            isGift: {
                enumerable: true,
                value: true
            }
        });
    } else {
        Object.defineProperty(object, 'isGift', {
            enumerable: true,
            value: false
        });
    }
};
