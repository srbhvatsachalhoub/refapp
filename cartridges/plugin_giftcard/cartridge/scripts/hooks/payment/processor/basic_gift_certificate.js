'use strict';

var GiftCertificateMgr = require('dw/order/GiftCertificateMgr');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Verifies that basket has GIFT_CERTIFICATE payment instrument
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function Handle(basket, paymentInformation) { // eslint-disable-line no-unused-vars
    var currentBasket = basket;
    var giftErrors = {};
    var serverErrors = [];

    // remove all 0 amount payment instruments
    Transaction.wrap(function () {
        var paymentInstruments = currentBasket.getPaymentInstruments();

        collections.forEach(paymentInstruments, function (item) {
            if (item.paymentTransaction.amount.value === 0) {
                currentBasket.removePaymentInstrument(item);
            }
        });
    });

    return { fieldErrors: giftErrors, serverErrors: serverErrors, error: false };
}

/**
 * Authorizes a payment using a gift certificate.
 * The payment is authorized by redeeming the gift certificate.
 * @param {dw.order.Order} order - The order object
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize/redeem
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @return {Object} returns an error object
 */
function Authorize(order, orderNumber, paymentInstrument, paymentProcessor) {
    var serverErrors = [];
    var fieldErrors = {};
    var error = false;

    try {
        var status;
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);

            status = GiftCertificateMgr.redeemGiftCertificate(paymentInstrument);
        });

        if (status.isError()) {
            throw new Error(status.message);
        }
    } catch (e) {
        var Log = require('dw/system/Logger').getLogger('plugin_giftcard');
        Log.error('Exception on scripts/hooks/payment/processor/basic_gift_certificate:Authorize, ex: {0}', e.message);

        error = true;
        serverErrors.push(
            Resource.msg('error.technical', 'checkout', null)
        );
    }

    return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
}

exports.Authorize = Authorize;
exports.Handle = Handle;
