var commonHelpers = require('app_brand_core/cartridge/scripts/helpers/commonHelpers');
var util = require('brand_core/util');

module.exports = {
    emptyModal: function () {
        /**
         * Inserts dom element into modal
         */
        $('#emptyModal')
            .on('show.bs.modal', function (e) {
                var $relatedTarget = $(e.relatedTarget);
                var selTargetContent = $relatedTarget.data('target-content');

                if (selTargetContent) {
                    // Insert target content
                    var $this = $(this);
                    var $targetContent = $(selTargetContent);
                    $targetContent.replaceWith('<div data-empty-modal-placeholder="' + selTargetContent + '"></div>');
                    $this.data('target-content', selTargetContent);
                    $this.find('.modal-body').html($targetContent);

                    // Find form element
                    var $form = $this.find('.js-login-form');

                    if ($form.length) {
                        // Get wishlist-pid attribute of the clicked element
                        var pid = $relatedTarget.data('wishlist-pid');

                        if (pid) {
                            $form.data('wishlist-pid', pid);
                        } else {
                            $form.removeData('wishlist-pid');
                        }
                    }
                }
            })
            .on('hide.bs.modal', function () {
                // Move target content back to its previous place
                var $this = $(this);
                var selTargetContent = $this.data('target-content');
                var $targetContent = $this.find(selTargetContent);
                $('[data-empty-modal-placeholder="' + selTargetContent + '"]').replaceWith($targetContent);

                // Remove wishlist id from login form
                $targetContent.find('.js-login-form').removeData('wishlist-pid');
            });
    },

    modalEvents: function () {
        $(document).on('show.bs.modal', '.modal', function () {
            if (commonHelpers.isMobileView($(window).width(), window.RA_BREAKPOINTS)) {
                util.disableScroll();
            }
        }).on('hide.bs.modal', function () {
            if (commonHelpers.isMobileView($(window).width(), window.RA_BREAKPOINTS)) {
                util.enableScroll();
            }
        });
    }
};
