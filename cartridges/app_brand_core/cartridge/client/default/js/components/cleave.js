'use strict';

var Cleave = require('cleave.js');
require('cleave.js/dist/addons/cleave-phone.i18n');

var madaRegexp = /^(588845|440647|440795|446404|457865|968208|588846|493428|539931|558848|557606|968210|636120|417633|468540|468541|468542|468543|968201|446393|588847|400861|409201|458456|484783|968205|462220|455708|588848|455036|968203|486094|486095|486096|504300|440533|489317|489318|489319|445564|968211|401757|410685|432328|428671|428672|428673|968206|446672|543357|434107|431361|604906|521076|588850|968202|535825|529415|543085|524130|554180|549760|588849|968209|524514|529741|537767|535989|536023|513213|585265|588983|588982|589005|508160|531095|530906|532013|588851|605141|968204|422817|422818|422819|428331|483010|483011|483012|589206|968207|419593|439954|407197|407395|520058|530060|531196)\d*$/;

// Get an instance of `PhoneNumberUtil`.
// load if any phone input is on page
var phoneUtil = null;

/**
 * Binds Cleave to input
 * @param {jQuery} $elem - jQuery Element or selector
 * @param {string} country - country code
 * @param {string} phoneCode - the country phone code
 * @param {string} phonePrefix - the country phone prefix
 * @param {string} phoneRemovePrefix - the country phone remove prefix
 */
function bindPhoneField($elem, country, phoneCode, phonePrefix, phoneRemovePrefix) {
    var cleavePhone = $elem.data('cleave');
    var rawValue;
    if (cleavePhone) {
        rawValue = cleavePhone.getRawValue();
        cleavePhone.destroy();
    }

    rawValue = (rawValue || $elem.data('raw-value') || '').toString();
    $elem.data('raw-value', '');

    var options = {
        phone: true,
        numericOnly: true,
        prefix: phonePrefix
    };

    if (country) {
        options.phoneRegionCode = country;
    }
    cleavePhone = new Cleave($elem, options);
    var value = rawValue.replace(phoneCode, '');
    if (phoneRemovePrefix !== '' && value.startsWith(phoneRemovePrefix)) {
        value = value.substring(phoneRemovePrefix.length);
    }
    cleavePhone.setRawValue(value);
    $elem.data('cleave', cleavePhone);
    $elem.trigger('input');
}

/**
 * Expose the element country code + country phone code + country phone prefix variables
 * @param {jQuery} $elem - jQuery Element
 * @returns {Object} - the country variables json object
 */
function getCountryPhoneVariables($elem) {
    var phoneCountry = $elem.data('country');
    var phoneCode = $elem.data('phone-code');
    var phonePrefix = $elem.data('phone-prefix');
    var phoneRemovePrefix = $elem.data('phone-removeprefix');

    phoneCountry = (phoneCountry !== null && phoneCountry !== undefined) ? phoneCountry.toString() : '';
    phoneCode = (phoneCode !== null && phoneCode !== undefined) ? phoneCode.toString() : '';
    phonePrefix = (phonePrefix !== null && phonePrefix !== undefined) ? phonePrefix.toString() : '';
    phoneRemovePrefix = (phoneRemovePrefix !== null && phoneRemovePrefix !== undefined) ? phoneRemovePrefix.toString() : '';

    return {
        phoneCountry: phoneCountry,
        phoneCode: phoneCode,
        phonePrefix: phonePrefix,
        phoneRemovePrefix: phoneRemovePrefix
    };
}

/**
 * Checks if the credit card number is mada or not
 * @param {string} number - the credit card number to be checked
 * @returns {boolean} - true if match, false otherwise
 */
function isMada(number) {
    return madaRegexp.test(number);
}

module.exports = {
    handleCreditCardNumber: function (cardFieldSelector, cardTypeSelector) {
        if (!cardFieldSelector) {
            return;
        }

        var $cardNumberWrapper = $('.js-card-number-wrapper');
        var isMadaAppliccable = $cardNumberWrapper.data('ismada-applicable');

        var cleave = new Cleave(cardFieldSelector, {
            creditCard: true,
            onValueChanged: function (e) {
                // e.target = { value: '5100-1234', rawValue: '51001234' }
                if (!isMadaAppliccable || !e.target || !e.target.rawValue) {
                    return;
                }

                if (isMada(e.target.rawValue)) {
                    $cardNumberWrapper.attr('data-type-ismada', 'true');
                } else {
                    $cardNumberWrapper.attr('data-type-ismada', 'false');
                }
            },
            onCreditCardTypeChanged: function (type) {
                var creditCardTypes = {
                    visa: 'Visa',
                    mastercard: 'Master Card',
                    amex: 'Amex',
                    discover: 'Discover',
                    unknown: 'Unknown'
                };

                var cardType = creditCardTypes[Object.keys(creditCardTypes).indexOf(type) > -1
                    ? type
                    : 'unknown'];
                $(cardTypeSelector).val(cardType);
                $cardNumberWrapper.attr('data-type', type);
                if (type === 'visa' || type === 'mastercard' || type === 'discover') {
                    $('#securityCode').attr('maxlength', 3);
                } else {
                    $('#securityCode').attr('maxlength', 4);
                }
            }
        });

        $(cardFieldSelector).data('cleave', cleave);
    },

    serializeData: function (form) {
        if (!form) {
            return;
        }
        var serializedArray = form.serializeArray();

        serializedArray.forEach(function (item) {
            if (item.name.indexOf('cardNumber') > -1) {
                item.value = $('.js-input-credit-card-number').data('cleave').getRawValue(); // eslint-disable-line
            }
        });

        return $.param(serializedArray); // eslint-disable-line
    },
    handlePhoneNumber: function () {
        // use google-libphonenumber if page has phone input
        if ($('.js-phone').length && !phoneUtil) {
            phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
        }

        $('.js-phone').each(function () {
            var $this = $(this);
            var countryPhoneVariables = getCountryPhoneVariables($this);
            bindPhoneField(
                $this,
                countryPhoneVariables.phoneCountry,
                countryPhoneVariables.phoneCode,
                countryPhoneVariables.phonePrefix,
                countryPhoneVariables.phoneRemovePrefix
            );
        });

        $('.js-phone-ghost').on('input', function () {
            var $this = $(this);
            var $phone = $this.siblings('.js-phone');
            var cleave = $phone.data('cleave');
            if (!cleave) {
                return;
            }

            var rawValue = $this.val();
            var countryPhoneVariables = getCountryPhoneVariables($phone);
            var value = rawValue.replace(countryPhoneVariables.phoneCode, '');
            if (countryPhoneVariables.phoneRemovePrefix !== '' && value.startsWith(countryPhoneVariables.phoneRemovePrefix)) {
                value = value.substring(countryPhoneVariables.phoneRemovePrefix.length);
            }
            cleave.setRawValue(value);
            $phone.trigger('input');
        });

        $('.js-phone').on('input', function () {
            var $phone = $(this);
            var cleave = $phone.data('cleave');
            if (!cleave) {
                return;
            }
            var $target = $('input[name="' + $phone.data('target') + '"]');
            var rawValue = cleave.getRawValue();

            var countryPhoneVariables = getCountryPhoneVariables($phone);

            var value = rawValue.replace(countryPhoneVariables.phoneCode, '');
            value = rawValue.replace(countryPhoneVariables.phonePrefix, '');
            if (value.length <= 0) {
                $target.val('');
                return;
            }
            if (countryPhoneVariables.phoneRemovePrefix !== '' && rawValue.startsWith(countryPhoneVariables.phoneRemovePrefix)) {
                rawValue = rawValue.substring(countryPhoneVariables.phoneRemovePrefix.length);
            }

            var number = countryPhoneVariables.phoneCode + rawValue;
            var phoneNumber;
            try {
                phoneNumber = phoneUtil.parse(number, countryPhoneVariables.phoneCountry);
            } catch (e) {
                phoneNumber = null;
            }

            if (phoneNumber && phoneUtil.isValidNumber(phoneNumber)) {
                $target.val(number);
            } else {
                $target.val('X');
            }
        });

        $('.js-phone-country').on('change', function () {
            var $select = $(this);
            var $selectedOption = $select.find('option:selected');

            var selectedCountryPhoneVariables = getCountryPhoneVariables($selectedOption);

            var $container = $select.closest('.form-group');
            var $phone = $('.js-phone', $container);
            $phone.data('country', selectedCountryPhoneVariables.phoneCountry)
                .data('phone-code', selectedCountryPhoneVariables.phoneCode)
                .data('phone-prefix', selectedCountryPhoneVariables.phonePrefix)
                .data('phone-removeprefix', selectedCountryPhoneVariables.phoneRemovePrefix);
            bindPhoneField(
                $phone,
                selectedCountryPhoneVariables.phoneCountry,
                selectedCountryPhoneVariables.phoneCode,
                selectedCountryPhoneVariables.phonePrefix,
                selectedCountryPhoneVariables.phoneRemovePrefix
            );
        });

        $('.js-phone').each(function () {
            $(this).trigger('input');
        });
    }
};
