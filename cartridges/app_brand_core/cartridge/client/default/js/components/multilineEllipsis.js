'use strict';

var throttle = require('lodash/throttle');

/**
 * Applies multiline ellipsis
 */
function multilineEllipsis() {
    var lineSelectors = ['.js-multiline-ellipsis'];
    for (var i = 1; i < 5; i++) {
        lineSelectors.push('.text-lines-' + i);
        lineSelectors.push('.text-max-lines-' + i);
    }

    $(lineSelectors.join(',')).each(function () {
        var $this = $(this);
        var originalText = $this.data('text');

        if (!originalText) {
            originalText = $.trim($this.text());
            $this.data('text', originalText);
        }

        if (originalText !== $.trim($this.text())) {
            $this.text(originalText);
        }

        var words = originalText.trim().split(' ');
        while (this.scrollHeight > this.offsetHeight && words.length) {
            words.pop();
            $this.text(words.join(' ') + '...');
        }

        if (words.length === 0) {
            $this.text(originalText);
        }
    });
}

module.exports = function () {
    multilineEllipsis();
    $(window).on('resize', throttle(multilineEllipsis, 300));
};
