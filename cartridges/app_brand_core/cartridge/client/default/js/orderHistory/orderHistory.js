'use strict';

module.exports = function () {
    $('body').on('change', '.order-history-select', function (e) {
        var url = e.currentTarget.value;

        // Stop here if url is empty
        if (!url) return;

        // Start spinner
        $.spinner().start();

        $.ajax({
            url: url,
            method: 'GET',
            success: function (data) {
                $('.order-list-container').html(data);
                $.spinner().stop();
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                }
                $.spinner().stop();
            }
        });
    });
};
