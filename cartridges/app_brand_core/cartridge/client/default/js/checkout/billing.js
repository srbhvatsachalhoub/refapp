'use strict';

var addressHelpers = require('./address');
var cleave = require('../components/cleave');

/**
 * updates the billing address selector within billing forms
 * @param {Object} order - the order model
 * @param {Object} customer - the customer model
 */
function updateBillingAddressSelector(order, customer) {
    var shippings = order.shipping;

    var form = $('form[name$=billing]')[0];
    var $billingAddressSelector = $('.addressSelector', form);
    var hasSelectedAddress = false;

    if ($billingAddressSelector && $billingAddressSelector.length === 1) {
        $billingAddressSelector.empty();
        // Add New Address option
        $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            null,
            false,
            order,
            { type: 'billing' }));

        // Separator -
        $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            order.resources.shippingAddresses, false, order, {
                // className: 'multi-shipping',
                type: 'billing'
            }
        ));

        shippings.forEach(function (aShipping) {
            var isSelected = order.billing.matchingAddressId === aShipping.UUID;
            hasSelectedAddress = hasSelectedAddress || isSelected;
            // Shipping Address option
            $billingAddressSelector.append(
                addressHelpers.methods.optionValueForAddress(aShipping, isSelected, order,
                    {
                        // className: 'multi-shipping',
                        type: 'billing'
                    }
                )
            );
        });

        if (customer.addresses && customer.addresses.length > 0) {
            $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
                order.resources.accountAddresses, false, order));
            customer.addresses.forEach(function (address) {
                var isSelected = order.billing.matchingAddressId === address.ID;
                hasSelectedAddress = hasSelectedAddress || isSelected;
                // Customer Address option
                $billingAddressSelector.append(
                    addressHelpers.methods.optionValueForAddress({
                        UUID: 'ab_' + address.ID,
                        shippingAddress: address
                    }, isSelected, order, { type: 'billing' })
                );
            });
        }
    }

    if (hasSelectedAddress
        || (!order.billing.matchingAddressId && order.billing.billingAddress.address)) {
        // show
        $(form).attr('data-address-mode', 'edit');
    } else {
        $(form).attr('data-address-mode', 'new');
    }

    $billingAddressSelector.show();
}

/**
 * updates the billing address form values within payment forms
 * @param {Object} order - the order model
 */
function updateBillingAddressFormValues(order) {
    var billing = order.billing;
    if (!billing.billingAddress || !billing.billingAddress.address) return;

    var form = $('form[name=dwfrm_billing]');
    if (!form) return;

    $('input[name$=_firstName]', form).val(billing.billingAddress.address.firstName);
    $('input[name$=_lastName]', form).val(billing.billingAddress.address.lastName);
    $('input[name$=_address1]', form).val(billing.billingAddress.address.address1);
    $('input[name$=_address2]', form).val(billing.billingAddress.address.address2);
    $('input[name$=_city]', form).val(billing.billingAddress.address.city);
    $('input[name$=_postalCode]', form).val(billing.billingAddress.address.postalCode);
    $('select[name$=_stateCode],input[name$=_stateCode]', form)
        .val(billing.billingAddress.address.stateCode);
    $('select[name$=_country],input[name$=_country]', form)
        .val(billing.billingAddress.address.countryCode.value);
    $('input[data-target$=_phone]', form).attr('data-raw-value', billing.billingAddress.address.phone);
    $('input[name$=_phone]', form).val(billing.billingAddress.address.phone).trigger('input');
    $('input[name$=_email]', form).val(order.orderEmail);

    if (billing.payment && billing.payment.selectedPaymentInstruments
        && billing.payment.selectedPaymentInstruments.length > 0) {
        var instrument = billing.payment.selectedPaymentInstruments[0];
        $('select[name$=expirationMonth]', form).val(instrument.expirationMonth);
        $('select[name$=expirationYear]', form).val(instrument.expirationYear);
        // Force security code and card number clear
        $('input[name$=securityCode]', form).val('');
        var cleaveCardNumber = $('input[name$=cardNumber]', form).data('cleave');
        if (cleaveCardNumber) {
            cleaveCardNumber.setRawValue('');
        }
    }
    $(document).trigger('checkout:billingAddressUpdated', order);
}

/**
 * clears the billing address form values
 */
function clearBillingAddressFormValues() {
    updateBillingAddressFormValues({
        billing: {
            billingAddress: {
                address: {
                    countryCode: {}
                }
            }
        }
    });
}

/**
 * Updates the billing information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 * @param {Object} customer - customer model to use as basis of new truth
 * @param {Object} [options] - options
 */
function updateBillingInformation(order, customer) {
    updateBillingAddressSelector(order, customer);

    // update billing address form
    updateBillingAddressFormValues(order);

    // update billing address summary
    addressHelpers.methods.populateAddressSummary('.billing .address-summary',
        order.billing.billingAddress.address);

    // update billing parts of order summary
    $('.order-summary-email').text(order.orderEmail);

    if (order.billing.billingAddress.address) {
        $('.order-summary-phone').text(order.billing.billingAddress.address.phone);
    }
}

/**
 * Updates the payment information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 */
function updatePaymentInformation(order) {
    // update payment details
    var $paymentSummary = $('.payment-details');
    var htmlToAppend = '';

    if (order.billing.payment && order.billing.payment.selectedPaymentInstruments
        && order.billing.payment.selectedPaymentInstruments.length > 0) {
        for (var i = 0; i < order.billing.payment.selectedPaymentInstruments.length; i++) {
            var instrument = order.billing.payment.selectedPaymentInstruments[i];
            if (instrument.paymentMethod === 'CREDIT_CARD') {
                htmlToAppend += '<div><span>' + order.resources.cardType + ' '
                    + instrument.type
                    + '</span><div>'
                    + instrument.maskedCreditCardNumber
                    + '</div><div><span>'
                    + order.resources.cardEnding + ' '
                    + instrument.expirationMonth
                    + '/' + instrument.expirationYear
                    + '</span></div></div>';
            } else if (instrument.paymentMethod === 'COD') {
                var applicableMethod;
                for (var j = 0; j < order.billing.payment.applicablePaymentMethods.length; j++) {
                    var item = order.billing.payment.applicablePaymentMethods[j];
                    if (item.ID === instrument.paymentMethod) {
                        applicableMethod = item;
                        break;
                    }
                }
                htmlToAppend += '<div><span>' + applicableMethod ? applicableMethod.name : instrument.paymentMethod + '</span></div>';
            } else if (instrument.paymentMethod === 'GIFT_CERTIFICATE') {
                htmlToAppend += '<div><span>Gift Certificate</span></div>'
                    + '<div>'
                    + instrument.maskedGiftCertificateCode
                    + '</div>';
            }
        }
    }

    $paymentSummary.empty().append(htmlToAppend);
}

/**
 * Clears the credit card form
 */
function clearCreditCardForm() {
    $('input[name$="_securityCode"]').val('');
    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
    var $month = $('select[name$="_expirationMonth"]').val('');
    var $year = $('select[name$="_expirationYear"]').val('');

    if ($month.selectpicker) $month.selectpicker('refresh');
    if ($year.selectpicker) $year.selectpicker('refresh');
}
/**
 * Creates payment instrument and updates page.
 * @param {string} methodId payment method Id
 */
function createNewPaymentInstrument(methodId) {
    if (!methodId) return;

    var createPaymentInstrumentUrl = $('.payment-information').data('create-payment-instrument');
    if (!createNewPaymentInstrument) {
        return;
    }
    var $csrfToken = $('[name="csrf_token"]');
    if (!$csrfToken || !$csrfToken.val()) {
        return;
    }
    var formData = {
        csrf_token: $csrfToken.val(),
        paymentMethodId: methodId
    };
    $.spinner().start();
    $.ajax({
        url: createPaymentInstrumentUrl,
        method: 'POST',
        dataType: 'json',
        data: formData,
        success: function (data) {
            if (data && data.order && data.customer) {
                $('body').trigger('checkout:updateCheckoutView', { order: data.order, customer: data.customer });
            }
        },
        complete: function () {
            $.spinner().stop();
        }
    });
}

/**
 * Removes saved payment instrument selection
 */
function deselectSavedPaymentInstrument() {
    $('.saved-payment-security-code').val('');
    $('.saved-payment-instrument').removeClass('selected-payment');
    $('.saved-payment-instrument .security-code-input').addClass('checkout-hidden');
}

/**
 * Hides add credit card form
 */
function cancelNewPayment() {
    $('.payment-information').attr('data-is-new-payment', false);
    clearCreditCardForm();
    $('.js-btn-add-new-credit-card').removeClass('active');
    $('.user-payment-instruments').removeClass('checkout-hidden');
    $('.credit-card-form').addClass('checkout-hidden');
}

module.exports = {
    methods: {
        updateBillingAddressSelector: updateBillingAddressSelector,
        updateBillingAddressFormValues: updateBillingAddressFormValues,
        clearBillingAddressFormValues: clearBillingAddressFormValues,
        updateBillingInformation: updateBillingInformation,
        updatePaymentInformation: updatePaymentInformation,
        clearCreditCardForm: clearCreditCardForm
    },

    showBillingDetails: function () {
        $('.btn-show-billing-details').on('click', function () {
            $(this).parents('[data-address-mode]').attr('data-address-mode', 'new');
        });
    },

    hideBillingDetails: function () {
        $('.btn-hide-billing-details').on('click', function () {
            $(this).parents('[data-address-mode]').attr('data-address-mode', 'shipment');
        });
    },

    selectBillingAddress: function () {
        $('.payment-form .addressSelector').on('change', function () {
            var form = $(this).parents('form')[0];
            var selectedOption = $('option:selected', this);
            var optionID = selectedOption[0].value;

            if (optionID === 'new') {
                // Show Address
                $(form).attr('data-address-mode', 'new');
            } else {
                // Hide Address
                $(form).attr('data-address-mode', 'shipment');
            }

            // Copy fields
            var attrs = selectedOption.data();
            var element;

            Object.keys(attrs).forEach(function (attr) {
                element = attr === 'countryCode' ? 'country' : attr;
                if (element === 'cardNumber') {
                    $('.js-input-credit-card-number').data('cleave').setRawValue(attrs[attr]);
                } else {
                    $('[name$=' + element + ']', form).val(attrs[attr]);
                }
            });
        });
    },

    handleCreditCardNumber: function () {
        if ($('.js-input-credit-card-number').length) {
            cleave.handleCreditCardNumber('.js-input-credit-card-number', '#cardType');
        }
    },

    santitizeForm: function () {
        $('body').on('checkout:serializeBilling', function (e, data) {
            var serializedForm = cleave.serializeData(data.form);

            data.callback(serializedForm);
        });
    },

    selectSavedPaymentInstrument: function () {
        $(document).on('click', '.saved-payment-instrument', function (e) {
            e.preventDefault();

            if ($(e.target).hasClass('saved-payment-security-code')) return;

            var $this = $(this);
            var methodId = $this.attr('data-method-id');
            deselectSavedPaymentInstrument();
            $this.addClass('selected-payment');
            $this.find('.security-code-input').removeClass('checkout-hidden');
            $('a[data-method-id="' + methodId + '"]').trigger('click');
            cancelNewPayment();
        });
    },

    addNewPaymentInstrument: function () {
        $('.js-btn-add-new-credit-card').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var methodId = $this.attr('data-method-id');
            $this.addClass('active');
            $('.payment-information').attr('data-is-new-payment', true);
            clearCreditCardForm();
            $('.credit-card-form').removeClass('checkout-hidden');
            $('.user-payment-instruments').addClass('checkout-hidden');
            deselectSavedPaymentInstrument();
            $('a[data-method-id="' + methodId + '"]').tab('show');
        });
    },

    cancelNewPayment: function () {
        $('.cancel-new-payment').on('click', function (e) {
            e.preventDefault();
            cancelNewPayment();
        });
    },

    clearBillingForm: function () {
        $('body').on('checkout:clearBillingForm', function () {
            clearBillingAddressFormValues();
        });
    },

    paymentTabs: function () {
        $(document).on('checkout:paymentMethodChanged', function (e, param) {
            var methodId = param.methodId;
            var $inputPaymentMethod = $('input[name$="paymentMethod"]');

            if (param.initialSelect || $inputPaymentMethod.val() !== methodId) {
                $('.payment-information').attr('data-payment-method-id', methodId);
                $inputPaymentMethod.val(methodId);
                createNewPaymentInstrument(methodId);
                $(document).trigger('psp:changed', methodId);
            }
        });

        $('.payment-options .nav-item').on('click', function (e) {
            e.preventDefault();
            var methodId = $(this).attr('data-method-id');
            $(document).trigger('checkout:paymentMethodChanged', { methodId: methodId, initialSelect: false });
        });

        $('a[data-toggle="tab"]').on('hidden.bs.tab', function (e) {
            var $activeTab = $(e.target);
            var methodId = $activeTab.attr('data-method-id');
            if (methodId === 'CREDIT_CARD') {
                deselectSavedPaymentInstrument();
                if ($activeTab.hasClass('customer-has-payment-instruments')) {
                    cancelNewPayment();
                }
            }
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var $activeTab = $(e.target);
            var methodId = $activeTab.attr('data-method-id');
            var hasSavedPaymentMethods = $activeTab.attr('data-has-payment-payment-instruments') === 'true';
            if (methodId !== 'CREDIT_CARD') {
                if (hasSavedPaymentMethods) {
                    cancelNewPayment();
                } else {
                    $('.payment-information').attr('data-is-new-payment', true);
                    clearCreditCardForm();
                    $('.credit-card-form').removeClass('checkout-hidden');
                }
                deselectSavedPaymentInstrument();
            } else if (!hasSavedPaymentMethods) {
                $('.payment-information').attr('data-is-new-payment', true);
                clearCreditCardForm();
                $('.credit-card-form').removeClass('checkout-hidden');
            }
        });
    }
};
