'use strict';
/* global grecaptcha */

module.exports = {
    submitContacusForm: function () {
        $('.js-contactus-form').submit(function (e) {
            e.preventDefault();

            var $grecaptcha = $('.g-recaptcha');

            if ($grecaptcha.length && grecaptcha !== undefined) {
                var grecaptchaResponse = grecaptcha.getResponse();
                if (!grecaptchaResponse || !grecaptchaResponse.length) {
                    $grecaptcha.parents('.form-group').addClass('is-invalid');
                    return;
                }
                $grecaptcha.parents('.form-group').removeClass('is-invalid');
            }

            var $form = $(this);
            var url = $form.attr('action');

            // Start spinner
            $form.spinner().start();

            // Clear messages
            $('.js-success').addClass('d-none');
            $('.js-errors').addClass('d-none');
            $('.js-error-message').empty();

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: $form.serialize(),
                success: function (data) {
                    if (!data.success) {
                        if (data.serverErrors) {
                            var errorMessages = '';
                            data.serverErrors.forEach(function (error) {
                                errorMessages += error + '<br/>';
                            });
                            $('.js-error-message').html(errorMessages);
                            $('.js-errors').removeClass('d-none');
                        }
                    } else {
                        $('#js-contact-form-submit-notification').modal('show');
                        $form[0].reset();

                        // reset google reCaptcha
                        if ($grecaptcha.length && grecaptcha !== undefined) {
                            grecaptcha.reset();
                        }
                    }
                },
                complete: function () {
                    $form.spinner().stop();
                }
            });
        });
    },
    toggleOrderNumberField: function () {
        $('.js-case-reason').on('change', function () {
            var $field = $('.js-order-number').closest('.form-group');
            if ($(this).val() === 'Order') {
                $field.removeClass('d-none');
            } else {
                $field.addClass('d-none');
                $field.find('input').val('');
            }
        });
    }
};
