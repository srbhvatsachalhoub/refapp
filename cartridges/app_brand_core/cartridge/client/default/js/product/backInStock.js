'use strict';
var formHelpers = require('base/checkout/formErrors');
/**
 * Adds back in stock notification
 */
function addToBackInStock() {
    var formSelector = '.js-bis-form';
    $(formSelector).on('submit', function (e) {
        e.preventDefault();
        var $bisForm = $(this);
        var $bisStatus = $('.js-bis-status', $bisForm);
        $bisStatus
            .addClass('d-none')
            .removeClass('alert alert-success alert-danger')
            .empty();
        var $bisFields = $('.js-bis-form-fields', $bisForm);

        $bisForm.spinner().start();
        $.ajax({
            method: 'POST',
            url: $bisForm.attr('action'),
            data: $bisForm.serialize(),
            success: function (data) {
                if (!data) {
                    return;
                }

                if (!data.success) {
                    if (data.serverErrors.length) {
                        data.serverErrors.forEach(function (error) {
                            $('<li/>')
                                .append($('<p/>').text(error))
                                .appendTo($bisStatus);
                        });
                        $bisStatus
                            .removeClass('d-none')
                            .addClass('alert alert-danger');
                    } else if (data.fieldErrors.length) {
                        data.fieldErrors.forEach(function (error) {
                            if (Object.keys(error).length) {
                                formHelpers.loadFormErrors(formSelector, error);
                            }
                        });
                    } else {
                        $bisStatus.addClass('d-none');
                    }
                    return;
                }

                $bisFields.addClass('d-none');
                if (data.message) {
                    $bisStatus
                        .removeClass('d-none')
                        .addClass('alert alert-success')
                        .text(data.message);
                }
            },
            complete: function () {
                $bisForm.spinner().stop();
            }
        });

        return false;
    });
}
/**
 * Updates display of back in stock notification component based on enableBackInStockNotification flag in product
 * If product is out-of-stock and product enables back in stock, button must be present to save customers.
 */
function updateDisplayOfBackInStock() {
    $('body').on('product:updateAvailability', function (e, response) {
        var $bisPid = $('[name$="backinstock_pid"]');
        if (response && response.product) {
            var product = response.product;
            // update bis pid value in form when variation is changed.
            if ($bisPid.length) {
                $bisPid.val(product.id);
            }

            var $bisContainer = $('.js-product-details').find('.js-bis');
            if ($bisContainer.length &&
            'enableBackInStockNotification' in product &&
            product.enableBackInStockNotification) {
                $bisContainer.removeClass('d-none');
            } else {
                $bisContainer.addClass('d-none');
            }
        }
    });
}

module.exports = {
    addToBackInStock: addToBackInStock,
    updateDisplayOfBackInStock: updateDisplayOfBackInStock
};
