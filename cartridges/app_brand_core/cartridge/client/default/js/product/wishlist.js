'use strict';

var urlHelper = require('../helpers/urlHelper');

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {jQuery} $button - button that was clicked to add a product to the wishlist
 */
function displayMessage(data, $button) {
    // Stop spinner
    $.spinner().stop();

    var status;
    var showMessage = true;
    var pid = $button.attr('data-pid');

    // Check result
    if (data.success) {
        $('.js-wishlist-product-added[data-pid="' + pid + '"]').removeClass('d-none');
        $('.add-to-wish-list[data-pid="' + pid + '"]').addClass('d-none');
        status = 'alert-success';

        if (!$button.data('show-success-message')) {
            showMessage = false;
        }
    } else {
        status = 'alert-danger';
    }

    if (showMessage) {
        var $messageContainer = $('.add-to-wishlist-messages');

        if ($messageContainer.length === 0) {
            $messageContainer = $('<div class="add-to-wishlist-messages"></div>');
            $('body').append($messageContainer);
        }

        $messageContainer.append('<div class="add-to-wishlist-alert text-center ' + status + '">' + data.msg + '</div>');

        setTimeout(function () {
            $messageContainer.remove();
            $button.removeAttr('disabled');
        }, 5000);
    } else {
        $button.removeAttr('disabled');
    }
}

module.exports = {
    addToWishlist: function () {
        $(document).on('click', '.add-to-wish-list', function (e) {
            e.preventDefault();

            var $this = $(this);
            var url = $this.data('href');
            var pid = $this.data('pid');

            // Don't go further, if we don't have a url or product id
            if (!url || !pid) return;

            var $productContainer = $this.closest('.product-detail');
            var optionId = $productContainer.find('.product-option').data('option-id');
            var optionVal = $productContainer.find('.options-select option:selected').data('value-id');

            // Show loding animation
            $.spinner().start();

            // Disable button to prevent multiple ajax request
            $this.attr('disabled', true);

            // Call add to wishlist endpoint
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    pid: pid,
                    optionId: optionId || null,
                    optionVal: optionVal || null
                },
                success: function (data) {
                    displayMessage(data, $this);
                },
                error: function (err) {
                    displayMessage(err, $this);
                }
            });
        });

        // Trigger add to wishlist action if current url includes wishlistpid parameter
        var pid = urlHelper.getParameter('wishlistpid');
        if (pid) {
            window.history.replaceState({}, document.title,
                urlHelper.removeParameter('wishlistpid'));
            $('.add-to-wish-list[data-pid="' + pid + '"]').first().trigger('click');
        }
    }
};
