'use strict';

var base = require('base/product/detail');

base.addToCartMultiple = function () {
    $(document).on('click', '.js-add-to-cart-multiple', function () {
        var $body = $('body');
        var $this = $(this);
        var pids = ($this.data('pids') || '').split(/[\s,]+/);
        var pidsObjList = [];

        // Don't go further, if there isn't any product id
        if (pids.length === 0) return;

        // Start spinner
        $.spinner().start();

        // Trigger start event
        $body.trigger('product:beforeAddToCart', this);

        // Prepare form data
        for (var i = 0; i < pids.length; i++) {
            pidsObjList.push({
                pid: pids[i],
                qty: 1
            });
        }
        var form = {
            pidsObj: JSON.stringify(pidsObjList)
        };

        $this.trigger('updateAddToCartFormData', form);

        $.ajax({
            url: window.RA_URL['Cart-AddProduct'],
            method: 'POST',
            data: form,
            success: function (data) {
                $body.trigger('product:afterAddToCart', data);
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    });
};

base.handleAddToCartError = function () {
    var $body = $('body');
    var $errorMsg = $('.js-add-to-cart-error');

    $body.on('product:beforeAddToCart product:updateAvailability product:afterAttributeSelect', function () {
        $errorMsg.html('').addClass('d-none');
    });

    $body.on('product:beforeAddToCart', function () {
        $errorMsg.html('').addClass('d-none');
    });

    $body.on('product:afterAddToCart', function (e, response) {
        if (response) {
            if (response.error && response.message) {
                $errorMsg.html(response.message).removeClass('d-none');
            }
        } else {
            $errorMsg.html(response.message).removeClass('d-none');
        }
    });
};

module.exports = base;
