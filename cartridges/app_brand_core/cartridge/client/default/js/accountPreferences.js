'use strict';

var processInclude = require('base/util');

/**
 * Preferences form submission.
 */
function initializePreferencesForm() {
    var formSelector = '.js-preferences-form';
    $(formSelector).on('submit', function (e) {
        e.preventDefault();
        var $preferencesForm = $(this);
        $preferencesForm.spinner().start();
        $.ajax({
            method: 'POST',
            url: $preferencesForm.attr('action'),
            data: $preferencesForm.serialize(),
            success: function (data) {
                // this can be listened from brands
                $('body').trigger('account:preferences:save', data);
            },
            complete: function () {
                $preferencesForm.spinner().stop();
            }
        });
        return false;
    });
}

module.exports = {
    initializePrefrences: initializePreferencesForm()
};

processInclude(module.exports);
