/* eslint no-param-reassign:0 */

'use strict';

window.jQuery = window.$ = require('jquery');
var pluginator = require('../pluginator');
var pluginPrefix = 'consent';
var $doc = $(document);

/**
 * Plugin constructor
 * @param {JQuery} $el - Search input element
 * @param {string} id - Id of the current plugin
 * @param {Object} options - Plugin opitons
 */
function ConsentTracking($el, id, options) {
    this.$el = $el;
    this.id = id;
    this.options = options;
    this.$placeholder = null;
}

ConsentTracking.prototype.init = function () {
    var options = this.options;

    // If content asset is offline, don't go further
    if (!options.assetOnline) {
        return;
    }

    // If consent tracking is already shown, dont' go further
    if (this.$el.hasClass('consented') || this.$el.hasClass('api-true') === false) {
        return;
    }

    this.$placeholder = $(this.options.modal ? 'body' : this.options.selPlaceholder);
    this.getContent();
};

ConsentTracking.prototype.destroy = function () {
    this.$placeholder.find('.js-consent-accept-btn').off('click');
    this.$placeholder.find('.js-consent-reject-btn').off('click');
    this.removeConsentTracking();
};

/**
 * Retrieve trackig consent content
 */
ConsentTracking.prototype.getContent = function () {
    // Start loading spinner if only tracking consent will be rendered in modal
    if (this.options.modal) {
        $.spinner().start();
    }

    // Start ajax requst
    $.ajax({
        context: this,
        url: this.options.urlContent,
        method: 'GET',
        success: this.processContentResponse,
        error: function () {
            if (this.options.modal) {
                $.spinner().stop();
            }
        }
    });
};

/**
 * Prepare modal template
 * @param {string} content Consent tracking content
 * @returns {string} HTML string of modal template
 */
ConsentTracking.prototype.getModalTemplate = function (content) {
    return '<!-- Modal -->'
        + '<div class="modal show js-consent-tracking-container consent-tracking-container" role="dialog" style="display: block;">'
        + '<div class="modal-dialog">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + this.options.modalHeading
        + '</div>'
        + '<div class="modal-body">' + content + '</div>'
        + '<div class="modal-footer">'
        + '<div class="button-wrapper">'
        + this.getRejectButtonTemplate()
        + this.getAcceptButtonTemplate()
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '</div>'
        + '<div class="modal-backdrop fade show"></div>';
};

/**
 * Prepare inline template
 * @param {string} content Consent tracking content
 * @returns {string} HTML string of inline container
 */
ConsentTracking.prototype.getInlineTemplate = function (content) {
    return '<div class="js-consent-tracking-container consent-tracking-container">'
        + content
        + '<button type="button" class="js-consent-' + this.options.cancelButtonAction + '-btn ' + this.options.cancelButtonClass + '">'
        + '<i class="' + this.options.cancelButtonIconClass + '"></i>'
        + '</button>'
        + '<div class="consent-tracking-button-group">'
        + this.getRejectButtonTemplate()
        + this.getAcceptButtonTemplate()
        + '</div>'
        + '</div>';
};

/**
 * Prepare reject button template
 * @returns {string} HTML string of reject button
 */
ConsentTracking.prototype.getRejectButtonTemplate = function () {
    var buttonReject = '';
    if (this.options.showRejectButton) {
        buttonReject = [
            '<button class="js-consent-reject-btn ' + this.options.rejectButtonClass + '">',
            this.options.textReject,
            '</button>'
        ].join('');
    }
    return buttonReject;
};

/**
 * Prepare accept button template
 * @returns {string} HTML string of accept button
 */
ConsentTracking.prototype.getAcceptButtonTemplate = function () {
    var buttonAccept = '';
    if (this.options.showAcceptButton) {
        buttonAccept = [
            '<button class="js-consent-accept-btn ' + this.options.acceptButtonClass + '">',
            this.options.textAccept,
            '</button>'
        ].join('');
    }
    return buttonAccept;
};

/**
 * Process Ajax response to prepare content
 * @param {Object|string} response - Empty object literal if null response or
 *                                   string with tracking consent content
 */
ConsentTracking.prototype.processContentResponse = function (response) {
    // Remove previous response
    this.removeConsentTracking();

    // Stop spinner
    $.spinner().stop();

    // If response is an object instead of a string, don't go further
    if (typeof response === 'object') {
        return;
    }

    var template = this.options.modal ? this.getModalTemplate(response) : this.getInlineTemplate(response);
    this.$placeholder.append(template);
    this.attachButtonEvents();
    this.$el.trigger(pluginPrefix + ':ready');
};

/**
 * Attach accept and reject events
 */
ConsentTracking.prototype.attachButtonEvents = function () {
    var self = this;
    this.$placeholder.find('.js-consent-accept-btn').click(function (e) {
        e.preventDefault();
        $.ajax({
            context: self,
            url: self.options.urlAccept,
            method: 'GET',
            dataType: 'json',
            success: self.processAcceptResponse,
            error: self.removeConsentTracking
        });
    });
    this.$placeholder.find('.js-consent-reject-btn').click(function (e) {
        e.preventDefault();
        $.ajax({
            context: self,
            url: self.options.urlReject,
            method: 'GET',
            dataType: 'json',
            success: self.processRejectResponse,
            error: self.removeConsentTracking
        });
    });
};

/**
 * Process accept response
 */
ConsentTracking.prototype.processAcceptResponse = function () {
    this.removeConsentTracking();
    this.$el.trigger(pluginPrefix + ':accept');
};

/**
 * Process reject response
 */
ConsentTracking.prototype.processRejectResponse = function () {
    this.removeConsentTracking();
    this.$el.trigger(pluginPrefix + ':reject');
};

/**
 * Remove consent tracking
 */
ConsentTracking.prototype.removeConsentTracking = function () {
    this.$placeholder.find('.js-consent-tracking-container').remove();
    this.$el.trigger(pluginPrefix + ':close');
    if (this.options.modal) {
        $('.modal-backdrop').remove();
    }
};

module.exports = function () {
    pluginator({
        prefix: pluginPrefix,
        name: 'consentTracking',
        Constructor: ConsentTracking,
        exports: [],
        defaultOptions: {
            selPlaceholder: '.js-consent-placeholder',
            urlContent: null,
            urlAccept: null,
            urlReject: null,
            textAccept: 'Yes',
            textReject: 'No',
            acceptButtonClass: 'btn btn-primary ml-2',
            rejectButtonClass: 'btn btn-outline-primary ml-2',
            cancelButtonClass: 'btn btn-link btn-cancel',
            cancelButtonIconClass: 'fa fa-times',
            cancelButtonAction: 'accept',
            showAcceptButton: true,
            showRejectButton: true,
            assetOnline: true,
            modal: true,
            modalHeading: 'Tracking Consent'
        }
    });

    // Initialize plugin automatically
    $doc.ready(function () {
        $('.js-consent-tracking').consentTracking();
    });
};
