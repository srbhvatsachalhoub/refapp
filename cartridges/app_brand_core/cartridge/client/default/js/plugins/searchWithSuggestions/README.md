# SFRA Search With Sugestions jQuery Plugin

The Search With Sugestions Plugin attaches suggestions to an input search field.

## Usage

You can use following ISML while building search and suggestions.
```html
    <div class="site-search js-sws-container">
        <form role="search" action="${URLUtils.url('Search-Show')}" method="get" name="simpleSearch">
            <div class="form-group">
                <input class="form-control js-sws-input"
                    type="search"
                    name="q"
                    value=""
                    autocomplete="off"
                    placeholder="${Resource.msg('label.header.searchwatermark', 'common', null)}"
                    aria-label="${Resource.msg('label.header.searchwatermark', 'common', null)}"
                    data-sws-suggestions-url="${URLUtils.url('SearchServices-GetSuggestions', 'q')}" />
                <input type="hidden" value="${pdict.locale}" name="lang" />
                <button type="button" class="btn btn-link js-sws-clear-search-btn d-none">
                    <i class="fa fa-search"></i>
                </button>
                <button type="submit" class="btn btn-link js-sws-search-btn">
                    <i class="fa fa-times"></i>
                </button>
            </div>
            <div class="position-relative js-sws-suggestions-placeholder"></div>
        </form>
    </div>
```

You can customize options by using data attributes. (see opitons table below)
```html
    <input class="js-sws-input"
        type="search"
        name="q"
        autocomplete="off"
        data-sws-show-suggestions="false" />
```

If the input element has `js-sws-input` class the plugin initializes itself automatically with supplied data attributes, otherwise you need to call `.searchWithSuggestions()` method manually.
```js
  $('.search-input-with-custom-class').searchWithSuggestions();
```

You can pass options object for customization.  (see opitons table below)
```js
  $('.search-input-with-custom-class').searchWithSuggestions({
    showSuggestions: false
  });
```

---

## Options

All options listed below are also available as HTML5 data attributes. In order to use them on search input element, you need to convert keys from camelCase to data-sws-hyphenated-text as follows. Let's say you don't want to show suggestions, in this case *showSuggestions* should be *data-sws-show-suggestions* as follows;
```html
    <input class="js-sws-input"
        type="search"
        name="q"
        autocomplete="off"
        data-sws-show-suggestions="false" />
```
<br />

| Option | Description | Default |
|---|---|---|
| suggestionsUrl | Url that gets suggestions by using search keyword | null |
| selContainer | Selector for container element. This element should innclude all other elements | <span style="color:#ce8349;">.js-sws-container</span> |
| selMobileContainer | Selector for mobile container element. If *selContainer* element or its parent has this class and *applyModalStylingOnMobile* options is <span style="color:#569cb3;">true</span>, the plugin adds `modal-open` class to `body` and `modal` class to *selSuggestionsContainer* element.  | <span style="color:#ce8349;">.js-sws-mobile-container</span>  |
| selSearchButton | Selector for search button | <span style="color:#ce8349;">.js-sws-search-btn</span> |
| selInputSync | Selector for input elements that need to have same value. | <span style="color:#ce8349;">.js-sws-input</span> |
| selCloseButton | Selector for the button that clears input and removes suggestions | <span style="color:#ce8349;">.js-sws-clear-search-btn</span> |
| selSuggestionsItem | Selector for suggestion items that can be navigated by up and down keys | <span style="color:#ce8349;">.js-sws-suggestions-item</span> |
| selSuggestionsContainer | Selector for suggestions container | <span style="color:#ce8349;">.js-sws-suggestions</span> |
| selSuggestionPlaceholder | Selector for placeholder element that *selSuggestionsContainer* element is appended | <span style="color:#ce8349;">.js-sws-suggestions-placeholder</span> |
| inputDebounceTime | Use debounce to avoid making an Ajax call on every single key press by waiting a few hundred milliseconds before making the request. Without debounce, the user sees the browser blink with every key press. | <span style="color:#93ce89;">300</span> |
| inputMinChars | The minimum character count required to start a new call to *suggestionsUrl*  | <span style="color:#93ce89;">3</span> |
| showSuggestions | Enables suggestions feature if set to <span style="color:#569cb3;">true</span> | <span style="color:#569cb3;">true</span> |
| showSearchButton | Shows search button if set to <span style="color:#569cb3;">true</span> | <span style="color:#569cb3;">true</span> |
| showCloseButton | Shows close button if set to <span style="color:#569cb3;">true</span> | <span style="color:#569cb3;">false</span> |
| applyModalStylingOnMobile | If <span style="color:#569cb3;">true</span> and *selMobileContainer* is available, the plugin adds `modal-open` class to `body` and `modal` class to *selSuggestionsContainer* element | <span style="color:#569cb3;">true</span> |

---

## Methods

You can call following methods after initializing the plugin.

| Method | Description |
|---|---|
| clearSuggestions | Removes suggestions |
| clearSearch | Removes suggestions and clears input text |
| destroy | Removes suggestions and clears all js artifacts |
| refresh | Recreates plugin with supplied options |

These methods can be used as follows;
```js
  $('.js-sws-input').searchWithSuggestions('refresh', {
    // new options
  });
  $('.js-sws-input').searchWithSuggestions('destroy');
  $('.js-sws-input').searchWithSuggestions('clearSearch');
  $('.js-sws-input').searchWithSuggestions('clearSuggestions');
```

---

## Events

You can listen events as follows;

```js
  $('.js-sws-input').on('sws:clearSuggestions', function() {
      // do something
  });
```

| Event | Description |
|---|---|
| <span style="color:#ce8349;">sws:showSuggestions</span> | Triggers when suggestion box is visible |
| <span style="color:#ce8349;">sws:clearSearch</span> | Triggers when search key is cleared |
| <span style="color:#ce8349;">sws:clearSuggestions</span> | Triggers when suggestions are not available any more |
