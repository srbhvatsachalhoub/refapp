/* eslint no-param-reassign:0 */

'use strict';

window.jQuery = window.$ = require('jquery');
var pluginator = require('../pluginator');
var pluginPrefix = 'productLoader';
var $doc = $(document);

/**
 * Plugin constructor
 * @param {JQuery} $el - Search input element
 * @param {string} id - Id of the current plugin
 * @param {Object} options - Plugin opitons
 */
function ProductLoader($el, id, options) {
    this.$el = $el;
    this.id = id;
    this.options = options;
}

ProductLoader.prototype.init = function () {
    var self = this;
    var productIds = [];

    // Prepare product ids
    self.$el.each(function () {
        productIds.push($(this).attr('data-load-pid'));
    });

    // Load products
    if (productIds.length) {
        var requestData = $.extend({}, self.options.requestData);
        requestData.pids = productIds.join(',');

        self.$el.trigger(pluginPrefix + ':before:load');

        $.ajax({
            url: window.RA_URL['Search-ProductsById'],
            type: 'get',
            data: requestData,
            success: function (data) {
                var $data = $(data);
                self.$el
                    .filter('[data-load-pid="' + productIds.join(',') + '"]')
                    .html($data.filter(':not(script)'));
                self.$el.parent().prepend($data.filter('script'));
                self.$el.trigger(pluginPrefix + ':loaded');
            }
        });
    }
};

ProductLoader.prototype.destroy = function () {
    var self = this;

    // Remove products
    self.$el.each(function () {
        var $this = $(this);
        var pid = $this.attr('data-load-pid');
        $this.find('.js-loaded-product-' + pid).remove();
    });
};

module.exports = function () {
    pluginator({
        prefix: pluginPrefix,
        name: 'productLoader',
        Constructor: ProductLoader,
        exports: [],
        defaultOptions: {
            requestData: {}
        }
    });

    // Initialize plugin automatically
    $doc.ready(function () {
        $('[data-load-pid]').productLoader();
    });
};
