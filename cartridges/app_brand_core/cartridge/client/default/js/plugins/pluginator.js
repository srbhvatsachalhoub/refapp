/* eslint no-param-reassign:0 */

'use strict';

window.jQuery = window.$ = require('jquery');
var prefix = 'plugin';
var callCount = 0;
var initCount = 0;

var defaultPluginOptions = {
    prefix: '',
    name: '',
    Constructor: null,
    exports: [],
    defaultOptions: {}
};

/**
 * Determines whether a string begins with the characters of a
 * specified string, returning true or false as appropriate.
 * @param {string} str The string that needs to be tested
 * @param {string} search The characters to be searched for at the start of this string
 * @param {number} pos The position in this string at which to begin searching for searchString; defaults to 0
 * @returns {boolean} true if the given characters are found at the beginning of the string; otherwise, false.
 */
function startsWith(str, search, pos) {
    pos = !pos || pos < 0 ? 0 : +pos;
    return str.substring(pos, pos + search.length) === search;
}

/**
 * Converts first letter of a string to lower case
 * @param {string} str The string with upper case first letter
 * @returns {string} The string with lower case first letter
 */
function lowercaseFirstLetter(str) {
    return str.charAt(0).toLowerCase() + str.slice(1);
}

/**
 * Removes plugin prefix from data attribute options and
 * returns a new options object
 * @param {Object} prefixedAttributeOptions Prefixed data attribute options
 * @param {Object} pluginOptions Plugin options that needs to include prefix property
 * @returns {Object} Options object without prefixed properties
 */
function getAttributeOptions(prefixedAttributeOptions, pluginOptions) {
    var attributeOptions = {};
    Object.keys(prefixedAttributeOptions).forEach(function (key) {
        if (startsWith(key, pluginOptions.prefix)) {
            var keyWithoutPrefix = lowercaseFirstLetter(key.replace(pluginOptions.prefix, ''));
            attributeOptions[keyWithoutPrefix] = prefixedAttributeOptions[key];
        }
    });
    return attributeOptions;
}

module.exports = function (pluginOptions) {
    pluginOptions = $.extend({}, defaultPluginOptions, pluginOptions);
    callCount += 1;

    var pluginIdKey = prefix + callCount;
    var cache = {};
    var methods = {
        init: function (options) {
            options = $.extend({}, $.fn[pluginOptions.name].options, options);
            return this.each(function () {
                var $el = $(this);
                var attributeOptions = getAttributeOptions($el.data(), pluginOptions);
                var mergedOptions = $.extend({}, options, attributeOptions);
                var id = $el.data(pluginIdKey);
                if (!cache[id]) {
                    initCount += 1;
                    $el.data(pluginIdKey, initCount);
                    cache[initCount] = new pluginOptions.Constructor($el, initCount, mergedOptions);
                    cache[initCount].init();
                }
            });
        },
        destroy: function () {
            return this.each(function () {
                var $el = $(this);
                var id = $el.data(pluginIdKey);
                if (cache[id]) {
                    if (cache[id].destroy) cache[id].destroy();
                    cache[id] = null;
                    $el.removeData(pluginIdKey);
                }
            });
        },
        refresh: function (options) {
            return this.each(function () {
                var $el = $(this);
                var id = $el.data(pluginIdKey);
                if (cache[id]) {
                    if (options) {
                        // Set new options
                        $el.data(options);
                        options = $.extend({}, cache[id].options, options);
                    } else {
                        // Update current options with data-attrbiutes
                        var attributeOptions = getAttributeOptions($el.data(), pluginOptions);
                        options = $.extend({}, cache[id].options, attributeOptions);
                    }
                    cache[id].destroy();
                    cache[id] = new pluginOptions.Constructor($el, id, options);
                    cache[id].init();
                }
            });
        }
    };

    pluginOptions.exports.forEach(function (methodName) {
        if (!methods[methodName]) {
            var args = Array.prototype.slice.call(arguments);
            methods[methodName] = function () {
                return this.each(function () {
                    var $el = $(this);
                    var id = $el.data(pluginIdKey);
                    var instance = cache[id];
                    if (instance && instance[methodName]) {
                        instance[methodName].apply(instance, args);
                    }
                });
            };
        }
    });

    $.fn[pluginOptions.name] = function (methodOrOptions) { // eslint-disable-line no-param-reassign
        if (methods[methodOrOptions]) {
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            return methods.init.apply(this, arguments);
        }
        return $.error('Method ' + methodOrOptions + ' does not exist on this plugin');
    };

    $.fn[pluginOptions.name].options = pluginOptions.defaultOptions;
};
