'use strict';

var urlHelper = require('../helpers/urlHelper');
var notificationHelper = require('../helpers/notificationHelper')('.error-messaging');
var formValidation = require('base/components/formValidation');
var googleMap = require('./../components/googleMap');

var adressPinSelector = '.js-address-form';
var url;
var isDefault;
var modalId;

module.exports = {
    deleteAddress: function () {
        $('.js-button-delete-address').on('click', function () {
            var $btn = $(this);
            modalId = $btn.data('target');
            isDefault = $btn.data('default');
            url = urlHelper.setParameter('addressId', $btn.data('id'), $btn.data('url'));
            url = urlHelper.setParameter('isDefault', isDefault, url);
            $(modalId).find('.js-container-item-name').html($btn.data('address'));
        });
    },

    deleteAddressConfirmation: function () {
        $('.js-button-confirm-delete-address').click(function () {
            var $modal = $(modalId);
            var $dialog = $modal.find('modal-dialog');

            $dialog.spinner().start();
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if (!data) {
                        notificationHelper.generalError();
                        return;
                    }

                    // Remove address from UI
                    var $removedAddressItem = $('#uuid-' + data.UUID).remove();

                    // Redirect to address list page if the remove action is called on edit address page
                    if ($removedAddressItem.length === 0) {
                        window.location.href = window.RA_URL['Address-List'];
                    }

                    // Hide confirmation dialog
                    $modal.modal('hide');

                    // Stop spinner
                    $dialog.spinner().stop();

                    // Update data of remaining addresses
                    var $preferredAddress = $('.js-address-item').first();
                    if ($preferredAddress.length === 0) {
                        // Show no records message
                        $('body').addClass('show-no-records show-no-addresses');
                    } else if (isDefault) {
                        // Update default address
                        $preferredAddress.find('.js-text-default').removeClass('d-none');
                        $preferredAddress.find('.js-button-delete-address').data('default', true);
                        $preferredAddress.find('.js-link-set-default').remove();
                    }
                },
                error: function (err) {
                    if (err && err.responseJSON) {
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        } else {
                            $modal.modal('hide');
                            notificationHelper.error(err.responseJSON.errorMessage);
                        }
                    } else {
                        notificationHelper.generalError();
                    }
                    $dialog.spinner().stop();
                }
            });
        });
    },

    submitAddress: function () {
        $('.js-address-form').submit(function (e) {
            e.preventDefault();

            var $form = $(this);

            $form.spinner().start();

            $('.js-address-form').trigger('address:submit', e);

            $.ajax({
                url: $form.attr('action'),
                type: 'post',
                dataType: 'json',
                data: $form.serialize(),
                success: function (data) {
                    $form.spinner().stop();
                    if (data) {
                        if (!data.success) {
                            formValidation($form, data);
                        } else {
                            location.href = data.redirectUrl;
                        }
                    }
                },
                error: function (err) {
                    if (err.responseJSON && err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    } else {
                        notificationHelper.generalError();
                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });
    },

    initGoogleMap: function () {
        // Inint Google Map component
        googleMap.initAddressMap(adressPinSelector);
    }
};
