'use strict';

var base = require('base/search/search');
var urlHelper = require('brand_core/helpers/urlHelper');
var commonHelpers = require('../../../../scripts/helpers/commonHelpers');

/**
 * Update DOM elements with Ajax results in .grid-header
 *
 * @param {Object} $updates - Found jQuery DOM element
 * @param {string} selector - DOM element to look up in the $results
 * @return {undefined}
 */
function updateGridHeader($updates, selector) {
    var $refContainer = $(selector).find('.js-refinement-container');

    if ($refContainer.hasClass('show') && $refContainer.data('refresh-item')) {
        $refContainer.removeClass('show');

        var attrNames = ['x-placement', 'style'];
        var activeDiv = $updates.find('.' + $refContainer[0].className.replace(/ /g, '.'));

        attrNames.forEach(function (attrName) {
            var attrValue = $refContainer.attr(attrName);
            activeDiv.attr(attrName, attrValue);
        });

        activeDiv.addClass('show');
    }
}

/**
 * Parse Ajax results and updated select DOM elements
 *
 * @param {string} response - Ajax response HTML code
 * @return {undefined}
 */
function parseResults(response) {
    var $results = $(response);
    var $noResultResetFilterBtn = $results.find('.js-no-result-reset-filter-btn');
    var $searchResults = $('.js-search-results');

    if ($noResultResetFilterBtn.length) {
        var $refinementContainer = $('.js-refinement-container');
        $noResultResetFilterBtn
            .removeClass('d-none')
            .attr('href', $refinementContainer.data('reset-link'));
        $searchResults.html($results);
        $('html, body').animate({
            scrollTop: 0
        });
        return;
    } else if ($searchResults.find('.js-no-result-reset-filter-btn').length) {
        $searchResults.replaceWith($results);
        return;
    }

    $searchResults.removeClass()
        .addClass($results.filter('.js-search-results').attr('class'));

    var specialHandlers = {
        '.refinements': base.handleRefinements
    };

    // Update DOM elements that do not require special handling
    [
        '.grid-header',
        '.js-grid-header',
        '.header-bar',
        '.header.page-title',
        '.product-grid',
        '.show-more',
        '.js-selected-filters'
    ].forEach(function (selector) {
        base.updateDom($results, selector);
    });

    Object.keys(specialHandlers).forEach(function (selector) {
        specialHandlers[selector]($results);
    });
}

/**
 * Performs an ajax request with supplied refinement url
 * and updates DOM with incoming response.
 *
 * @param {string} url - Refinement url
 * @param {string} type - Type of the request; refinement, sort, etc.
 * @param {boolean} disablePushState - If true, new refine url won't be pushed to history
 */
function applyFilter(url, type, disablePushState) {
    var $doc = $(document);
    var event = 'search:' + type;

    $doc.trigger(event + ':start', url);
    $.spinner().start();
    $.ajax({
        method: 'GET',
        url: url,
        data: {
            page: $('.grid-footer').data('page-number'),
            selectedUrl: url
        },
        success: function (response) {
            var res = {
                response: response,
                url: url,
                type: type,
                disablePushState: disablePushState
            };
            $doc.trigger(event + ':success', res);
            $doc.trigger('search:success', res);
            $.spinner().stop();
        },
        error: function () {
            $doc.trigger(event + ':error');
            $.spinner().stop();
        }
    });
}

/**
 * Returns push state parameters
 *
 * @param {Object} result - Result of the apply filter method that is triggered by success event
 * @param {string} url - New url that will be pushed to history
 */
function pushState(result, url) {
    if (!result.disablePushState && window.location.href !== url) {
        history.pushState({
            refineUrl: decodeURIComponent(result.url),
            type: result.type
        }, '', decodeURIComponent(url));
    }
}

/**
 * Returns products count to be shown in search result
 * @return {number} productsCount - ProductsCount
 */
function getProductsCount() {
    var searchResultProductsCount = $('.js-refinement-container').data('searchResultProductsCount');
    if (searchResultProductsCount && searchResultProductsCount.productsCount) {
        var width = $(window).width();
        var sz = searchResultProductsCount.productsCount.desktop;

        if (commonHelpers.isTabletView(width, window.RA_BREAKPOINTS)) {
            sz = searchResultProductsCount.productsCount.tablet;
        }
        if (commonHelpers.isMobileView(width, window.RA_BREAKPOINTS)) {
            sz = searchResultProductsCount.productsCount.mobile;
        }
        return sz;
    }
    return null;
}

base.showMore = function () {
    // Show more products
    var sz = getProductsCount();
    $(document)
        .on('click', '.show-more button', function (e) {
            e.stopPropagation();
            e.preventDefault();

            // Get paging url from the value and call it
            var url = $(this).data('url');
            if (sz) {
                // update url if sz is defined (config from BM)
                url = urlHelper.updateViewMoreUrl(url, sz);
            }
            applyFilter(url, 'showMore');
        })
        .on('search:showMore:success', function (e, result) {
            // Get start and size parameters from paging url
            var start = urlHelper.getParameter('start', result.url);
            var size = urlHelper.getParameter('sz', result.url);

            // Set size parameters to current url, as sum of start and size
            var url = urlHelper.setParameter('sz', (+size + +start));

            // Update url on address bar
            pushState(result, url);

            // Update DOM
            $('.grid-footer').replaceWith(result.response);
        });
};

/**
 * Keep refinement panes expanded/collapsed after Ajax refresh
 *
 * @param {Object} $results - jQuery DOM element
 * @return {undefined}
 */

base.handleRefinements = function ($results) {
    if (!$results) {
        return;
    }
    $('.refinement.active').each(function () {
        $(this).removeClass('active');
        var activeDiv = $results.find('.' + $(this)[0].className.replace(/ /g, '.'));
        activeDiv.addClass('active');
        activeDiv.find('button.title').attr('aria-expanded', 'true');
    });

    base.updateDom($results, '.refinement-bar');
};

/**
 * Update DOM elements with Ajax results
 *
 * @param {Object} $results - jQuery DOM element
 * @param {string} selector - DOM element to look up in the $results
 * @return {undefined}
 */

base.updateDom = function ($results, selector) {
    if (!$results) {
        return;
    }

    var $updates = $results.find(selector);

    if (selector === '.grid-header' && $updates.length) {
        updateGridHeader($updates, selector);
    }

    $(selector).empty().html($updates.html());
};


base.sort = function () {
    // Handle sorting
    $(document)
        .on('change', '[name=sort-order]', function (e) {
            e.preventDefault();
            e.stopPropagation();

            // Get url from the value of current option and call it
            applyFilter(this.value, 'sort');
        })
        .on('search:sort:success', function (e, result) {
            // Get sort option from sort url
            var sortOption = urlHelper.getParameter('srule', result.url);

            // Set sort option to current url
            var url = urlHelper.setParameter('srule', sortOption);

            // Update url on address bar
            pushState(result, url);

            // Update DOM
            $('.product-grid').empty().html(result.response);
        });
};

base.applyFilter = function () {
    // Handle refining
    var sz = getProductsCount();
    $(document)
        .on('click', '.js-refine-link', function (e) {
            e.preventDefault();
            e.stopPropagation();

            // Get sort option from current url
            var sortOption = urlHelper.getParameter('srule');

            // Set sort option to refinement url
            var url = urlHelper.setParameter('srule', sortOption, (e.currentTarget.href || $(this).data('href')));
            if (sz) {
                url = urlHelper.setParameter('sz', sz, (e.currentTarget.href || $(this).data('href')));
            }

            // Call refinement url to get new results
            applyFilter(url, 'refinement');
        })
        .on('search:refinement:success', function (e, result) {
            // Update link on address bar
            var url = window.location.href.split('?')[0];
            var qs = result.url.split('?')[1];
            pushState(result, url + (qs ? '?' + qs : ''));

            // Update DOM
            parseResults(result.response);
        });
};

base.supportHistoryNavigation = function () {
    window.onpopstate = function (event) {
        if (event && event.state) {
            applyFilter(event.state.refineUrl, event.state.type, true);
        } else {
            applyFilter($('.js-refinement-container').data('reset-link'), 'refinement', true);
        }
    };
};

base.getApplyFilterMethod = function () {
    return applyFilter;
};

module.exports = base;
