var ImageTransformation = {};
var BREAKPOINTS = require('*/cartridge/experience/breakpoints.json');
var Image = require('dw/experience/image/Image');
var MediaFile = require('dw/content/MediaFile');

var transformationCapabilities = [
    'scaleWidth',
    'scaleHeight',
    'scaleMode',
    'imageX',
    'imageY',
    'imageURI',
    'cropX',
    'cropY',
    'cropWidth',
    'cropHeight',
    'format',
    'quality',
    'strip'
];

/**
 * Calculates the required DIS transformation object based on the given parameters. Currently
 * only downscaling is performed.
 *
 * @param {Object} metaData the image meta data containing image width and height
 * @param {string} device the device the image should be scaled for (supported values: mobile, desktop)
 * @return {Object} The scaled object
 */
ImageTransformation.scale = function (metaData, device) {
    var transformObj = null;
    if (metaData && device) {
        var targetWidth = BREAKPOINTS[device];
        // only downscale if image is larger than desired width
        if (targetWidth && targetWidth < metaData.width) {
            transformObj = {
                scaleWidth: targetWidth,
                format: 'jpg',
                scaleMode: 'fit'
            };
        }
    }
    return transformObj;
};

/**
 * Calculates the required DIS transformation object based on the given parameters. Currently
 * only downscaling is performed.
 *
 * @param {Image} image the image file, meta data containing image width and height
 * @param {string} device the device the image should be scaled for (supported values: mobile, desktop)
 * @return {Object} The scaled object
 */
ImageTransformation.crop = function (image, device) {
    var transformObj = null;
    if (image && device) {
        var targetWidth = BREAKPOINTS[device];
        // only crop if image is larger than desired width
        if (targetWidth && targetWidth < image.metaData.width) {
            var targetHeight = targetWidth * 0.66;
            var startX = Math.max(0, (image.focalPoint.x * image.metaData.width) - (targetWidth / 2));
            var startY = Math.max(0, (image.focalPoint.y * image.metaData.height) - (targetHeight / 2));
            var endX = Math.min(startX + targetWidth, image.metaData.width);
            var endY = Math.min(startY + targetHeight, image.metaData.height);
            transformObj = {
                cropX: Math.max(0, endX - targetWidth),
                cropY: Math.max(0, endY - targetHeight),
                cropWidth: targetWidth,
                cropHeight: targetHeight,
                format: 'jpg'
            };
        }
    }
    return transformObj;
};

/**
 * Creates a cleaned up transformation object
 * @param {*} options the paarmaters object which may hold additional properties not suppoerted by DIS e.g. option.device == 'mobile'
 * @param {*} transform a preconstructed transformation object
 * @return {Object} The transformed object.
 */
function constructTransformationObject(options, transform) {
    var result = transform || {};
    Object.keys(options).forEach(function (element) {
        if (transformationCapabilities.indexOf(element)) {
            result[element] = options[element];
        }
    });
    return result;
}


/**
 * Provides a url to the given media file image. DIS transformation will be applied as given.
 *
 * @param {Image|MediaFile} image the image for which the url should be obtained. In case of an Image type the options may add a device property to scale the image to
 * @param {Object} options the (optional) DIS transformation parameters or option with devices
 *
 * @return {string} The Absolute url
 */
ImageTransformation.url = function (image, options) {
    var transform = {};
    var mediaFile = image instanceof MediaFile ? image : image.file;
    var imageFile = image instanceof Image ? image.file : image;

    if (imageFile instanceof Image && options.device && options.action === 'crop') {
        transform = ImageTransformation.crop(imageFile, options.device);
    } else if (imageFile instanceof Image && options.device) {
        transform = ImageTransformation.scale(imageFile.metaData, options.device);
    }
    transform = constructTransformationObject(options, transform);

    if (transform && Object.keys(transform).length) {
        return mediaFile.getImageURL(transform);
    }

    return mediaFile.getAbsURL();
};

/**
 * Return an object containing the scaled image for mobile, table and desktop.  Other included image details
 * are: alt text, focalPoint x, focalPoint y.
 *
 * @param {Image} image the image for which to be scaled.
 * @param {Object} The object containing the scaled image
 *
 * @return {string} The Absolute url
 */
ImageTransformation.getScaledImage = function (image) {
    return {
        src: {
            mobile: encodeURI(ImageTransformation.url(image.file, { device: 'mobile', action: 'scale' })),
            tablet: encodeURI(ImageTransformation.url(image.file, { device: 'tablet', action: 'scale' })),
            desktop: encodeURI(ImageTransformation.url(image.file, { device: 'desktop', action: 'scale' }))
        },
        alt: image.file.getAlt(),
        focalPointX: (image.focalPoint.x * 100) + '%',
        focalPointY: (image.focalPoint.y * 100) + '%'
    };
};

/**
 * Return an object containing the crop image for mobile, table and desktop.  Other included image details
 * are: alt text, focalPoint x, focalPoint y.
 *
 * @param {Image} image the image for which to be cropped.
 * @param {Object} The object containing the cropped image
 *
 * @return {string} The Absolute url
 */
ImageTransformation.getCroppedImage = function (image) {
    return {
        src: {
            mobile: encodeURI(ImageTransformation.url(image, { device: 'mobile', action: 'crop' })),
            tablet: encodeURI(ImageTransformation.url(image, { device: 'tablet', action: 'crop' })),
            desktop: encodeURI(ImageTransformation.url(image, { device: 'desktop', action: 'crop' }))
        },
        alt: image.file.getAlt(),
        focalPointX: (image.focalPoint.x * 100) + '%',
        focalPointY: (image.focalPoint.y * 100) + '%'
    };
};

module.exports = ImageTransformation;
