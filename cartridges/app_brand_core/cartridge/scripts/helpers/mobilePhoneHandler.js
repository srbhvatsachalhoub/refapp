'use strict';

/**
 * Saves mobilePhone field to system object if the previous chain event was successful.
 * @param {Object} routeHandler 'this' of the calling action
 * @param {Object} form submitted form
 * @param {Response} res response object filled in previous chain events
 */
function persistMobilePhone(routeHandler, form, res) {
    var response = res;

    routeHandler.on('route:Complete', function () {
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Transaction = require('dw/system/Transaction');

        if (!response.viewData.success) {
            return;
        }

        var customerProfile = CustomerMgr.queryProfile('email={0}', form.customer.email.value);

        if (customerProfile && form.customer.phoneMobile && form.customer.phoneMobile.value &&
            customerProfile.phoneMobile !== form.customer.phoneMobile.value) {
            Transaction.wrap(function () {
                customerProfile.setPhoneMobile(form.customer.phoneMobile.value);
            });
        }
    });
}

/**
 * Adds mobile phone to the view data (pdict.profile.account.phoneMobile).
 * @param {Response} res response object
 * @param {string} mobilePhone mobile phone number
 */
function addMobilePhoneToViewData(res, mobilePhone) {
    var viewData = res.getViewData();
    if (!viewData || !viewData.account || !viewData.account.profile) {
        return;
    }

    viewData.account.profile.phoneMobile = mobilePhone;
    res.setViewData({
        account: viewData.account
    });
}

/**
 * Checks if the given string is in email format
 * @param {string} input - string to check if in email format
 * @returns {boolean} Indicates if string is in email format
 */
function isEmail(input) {
    var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    return regex.test(input);
}

/**
 * Checks if login via mobile phone is possible
 * @param {Request} req request object
 * @param {Response} res response object
 * @returns {{boolean: possible, dw.customer.Profile: profile}} wrapper object
 * that holds values for possibility of mobile phone login and profile to be used
 */
function canLoginViaPhone(req, res) {
    // if the loginEmail input is in email format, then we don't try to login with phone number
    if (res.viewData.success || isEmail(req.form.loginEmail)) {
        return {
            possible: false
        };
    }

    // find the user with given phone number
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var profile = CustomerMgr.queryProfile('phoneMobile={0}', req.form.loginEmail);
    return {
        possible: profile !== null,
        profile: profile
    };
}

module.exports = {
    persistMobilePhone: persistMobilePhone,
    addMobilePhoneToViewData: addMobilePhoneToViewData,
    canLoginViaPhone: canLoginViaPhone
};
