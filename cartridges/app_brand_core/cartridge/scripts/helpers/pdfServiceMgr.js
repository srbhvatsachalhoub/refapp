'use strict';

/**
 * @module ServiceMgr
 */

/**
 * RefApp Service Manager
 */

/**
 * @type {dw.svc.LocalServiceRegistry}
 */
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * @type {module:services/rest}
 */
var pdfService = require('../services/pdfService');

var SERVICES = {
    pdfService: 'invoicing.pdf'
};

/**
 * Returns the service related to the given {serviceID} initialized with the given {definition}
 *
 * @param {string} serviceID id of the service
 * @param {Object} definition definition of the service
 *
 * @returns {dw/svc/Service} returns created service
 */
function getService(serviceID, definition) {
    return LocalServiceRegistry.createService(serviceID, definition);
}

module.exports = {
    /**
     * Returns a new instance of the PdfService initialized with the necessary definitions
     *
     * @returns {dw/svc/Service} returns service responsible of zipped invoice download
     */
    getInvoicePDFsZipped: function () {
        return getService(SERVICES.pdfService, pdfService.definitions.downloadZip);
    }
};
