'use strict';

/* global request, response */

var originalExports = Object.keys(module.superModule || {});
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

var countries = require('*/cartridge/config/countries');

/**
 * Gets the Controller-Action from request.httpPath
 * Sample1 request.httpPath -> /on/demandware.store/Site-RefApp-Site/en_AE/Default-Start
 * Sample2(if root domain requested) -> request.httpPath -> /on/demandware.store/Site-RefApp-Site,
 * return 'Default-Start' as default for Sample2
 * @returns {string} Controller-Action string, Home-Show if not generated
 */
function getControllerActionFromHttpPath() {
    var defaultControllerAction = 'Home-Show';
    if (!request || !request.httpPath) {
        return defaultControllerAction;
    }

    var minExpectedPathPartsLength = 6;
    var splittedHttpPath = request.httpPath.split('/');
    if (splittedHttpPath.length >= minExpectedPathPartsLength) {
        return splittedHttpPath[minExpectedPathPartsLength - 1];
    }

    return defaultControllerAction;
}

/**
 * Get locale host value if set in countries json
 * @param {string} locale - the locale to be checked for host
 * @return {string} - the locale host value if defined, null otherwise
 */
function getLocaleHost(locale) {
    if (!locale || !countries) {
        return null;
    }

    var countryLocale = countries.filter(function (country) {
        return country.id === locale;
    });

    if (!countryLocale || !countryLocale.length || !countryLocale[0].host) {
        return null;
    }

    return countryLocale[0].host;
}

/**
 * Set Host to the url
 * Checks and finds the given locale object inside in countries json
 * if it has host value then set this host to the given url object
 * @param {dw.web.URL} url - the url object
 * @param {string} locale - the url object
 * @returns {string} - the string of the url that is updated with host
 */
function setHostToUrl(url, locale) {
    locale = locale || request.locale; // eslint-disable-line no-param-reassign
    var localeHost = getLocaleHost(locale);

    if (localeHost) {
        url = url.host(localeHost); // eslint-disable-line no-param-reassign
    }

    return url.toString();
}

/**
 * Removes the parameter from the url and returns new url
 *
 * @param {string} key - Parameter that will be search in querystring
 * @param {string} url - Url that includes query string
 * @return {string} new url
 */
function removeQueryStringParameter(key, url) {
    // prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {
        var prefix = encodeURIComponent(key) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        // reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            // idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
}
/**
 * prepares querystring for redirection
 * @param {Object} queryString queryStringObject
 * @param {Array} queryArr queryArray
 * @returns {Array} URLParameters
 */
function normalizeQueryStringParameters(queryString, queryArr) {
    var queryArr = queryArr || []; // eslint-disable-line
    var URLParameter = require('dw/web/URLParameter');
    if (queryString && Object.keys(queryString).length) {
        Object.keys(queryString).forEach(function (propName) {
            if (typeof queryString[propName] === 'object') {
                normalizeQueryStringParameters(queryString[propName], queryArr);
            } else {
                queryArr.push(new URLParameter(propName, queryString[propName]));
            }
        });
    }
    return queryArr;
}

base.getControllerActionFromHttpPath = getControllerActionFromHttpPath;
base.getLocaleHost = getLocaleHost;
base.setHostToUrl = setHostToUrl;
base.removeQueryStringParameter = removeQueryStringParameter;
base.normalizeQueryStringParameters = normalizeQueryStringParameters;

module.exports = base;
