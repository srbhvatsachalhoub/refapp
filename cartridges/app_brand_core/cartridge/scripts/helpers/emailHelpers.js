'use strict';

var base = module.superModule || {};

/** Checks site preference to decide if mc order emails are disabled
 * @returns {boolean} true if disabled false otherwise
 */
function isMCOrderEmailsDisabled() {
    var Site = require('dw/system/Site');

    if (Site.getCurrent().getCustomPreferenceValue('disableOrderEmails')) {
        var Logger = require('dw/system/Logger');
        Logger.warn('MC order related emails are disabled from site preference');
        return true;
    }

    return false;
}

base.isMCOrderEmailsDisabled = isMCOrderEmailsDisabled;
base.emailTypes = {
    registration: 1,
    passwordReset: 2,
    passwordChanged: 3,
    orderConfirmation: 4,
    accountLocked: 5,
    accountEdited: 6,
    orderShipped: 7,
    orderCancelled: 8,
    backInStock: 9,
    contactUs: 10,
    abandonedCart: 11
};

module.exports = base;
