'use strict';

var base = module.superModule;
var ArrayList = require('dw/util/ArrayList');
var CustomerMgr = require('dw/customer/CustomerMgr');
var AddressModel = require('*/cartridge/models/address');
var collections = require('*/cartridge/scripts/util/collections');
var URLUtils = require('dw/web/URLUtils');

/**
 * Creates an account model for the current customer
 * @param {string} redirectUrl - rurl of the req.querystring
 * @param {string} privacyCache - req.session.privacyCache
 * @param {boolean} newlyRegisteredUser - req.session.privacyCache
 * @returns {string} a redirect url
 */
function getLoginRedirectURL(redirectUrl, privacyCache, newlyRegisteredUser) {
    if (redirectUrl && redirectUrl.indexOf('http') !== -1) {
        redirectUrl = 1; // eslint-disable-line
    }
    return isNaN(parseInt(redirectUrl, 10)) ? redirectUrl :
        base.getLoginRedirectURL(redirectUrl, privacyCache, newlyRegisteredUser);
}

/**
 * Returns preferred address for current customer and country
 * @param {Object} req - Global request object
 * @returns {Object} an object that contains information about current customer's preferred address
 */
function getPreferredAddress(req) {
    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

    var customer = CustomerMgr.getCustomerByCustomerNumber(
        req.currentCustomer.profile.customerNo
    );
    var addressBook = customer.getProfile().getAddressBook();
    var countryCode = localeHelpers.getCurrentCountryCode();
    var preferredAddress = null;

    if (addressBook.addresses && addressBook.addresses.length > 0) {
        preferredAddress = collections.find(new ArrayList(addressBook.addresses), function (address) {
            return countryCode === address.countryCode.value;
        });
    }

    if (preferredAddress) {
        preferredAddress = new AddressModel(preferredAddress);
    }

    return preferredAddress;
}
/**
 * Gets the password reset token of a customer
 * @param {Object} customer - the customer requesting password reset token
 * @returns {string} password reset token string
 */
function getPasswordResetToken(customer) {
    var Transaction = require('dw/system/Transaction');

    var passwordResetToken;
    Transaction.wrap(function () {
        passwordResetToken = customer.profile.credentials.createResetPasswordToken();
    });
    return passwordResetToken;
}

/**
 * Sends the email with password reset instructions
 * @param {string} email - email for password reset
 * @param {Object} resettingCustomer - the customer requesting password reset
 */
function sendPasswordResetEmail(email, resettingCustomer) {
    var Resource = require('dw/web/Resource');
    var Site = require('dw/system/Site');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');

    var passwordResetToken = getPasswordResetToken(resettingCustomer);
    var url = URLUtils.https('Account-SetNewPassword', 'Token', passwordResetToken);
    var objectForEmail = {
        passwordResetToken: passwordResetToken,
        firstName: resettingCustomer.profile.firstName,
        lastName: resettingCustomer.profile.lastName,
        url: url,
        resettingCustomer: resettingCustomer
    };

    var emailObj = {
        to: email,
        subject: Resource.msg('subject.profile.resetpassword.email', 'login', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.passwordReset
    };

    emailHelpers.sendEmail(emailObj, 'account/password/passwordResetEmail', objectForEmail);
}

/**
 * Send an email that would notify the user that account was created
 * @param {obj} registeredUser - object that contains user's email address and name information.
 */
function sendCreateAccountEmail(registeredUser) {
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
    var Site = require('dw/system/Site');
    var Resource = require('dw/web/Resource');

    var userObject = {
        email: registeredUser.email,
        firstName: registeredUser.firstName,
        lastName: registeredUser.lastName,
        url: URLUtils.https('Login-Show'),
        currentCustomer: registeredUser.customer
    };

    var emailObj = {
        to: registeredUser.email,
        subject: Resource.msg('email.subject.new.registration', 'registration', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.registration
    };

    emailHelpers.sendEmail(emailObj, 'checkout/confirmation/accountRegisteredEmail', userObject);
}

/**
 * Returns subscribe infos for current customer
 * @param {Object} req - Global request object
 * @returns {Object} an object that contains information about current customer's subscribe infos
 */
function subscribeOption(req) {
    var customer = CustomerMgr.getCustomerByCustomerNumber(
        req.currentCustomer.profile.customerNo
    );
    var allowSubscribe = false;
    var subscribeOptionModel = null;

    if (customer.profile.custom.emailOptin || customer.profile.custom.smsOptin || customer.profile.custom.telephoneOptin || customer.profile.custom.whatsappOptin) {
        allowSubscribe = true;
    }

    subscribeOptionModel = {
        emailOptin: customer.profile.custom.emailOptin,
        smsOptin: customer.profile.custom.smsOptin,
        telephoneOptin: customer.profile.custom.telephoneOptin,
        whatsappOptin: customer.profile.custom.whatsappOptin,
        allowSubscribe: allowSubscribe
    };

    return subscribeOptionModel;
}

module.exports = {
    getLoginRedirectURL: getLoginRedirectURL,
    getPreferredAddress: getPreferredAddress,
    sendPasswordResetEmail: sendPasswordResetEmail,
    sendCreateAccountEmail: sendCreateAccountEmail,
    subscribeOption: subscribeOption,
    sendAccountEditedEmail: base.sendAccountEditedEmail
};
