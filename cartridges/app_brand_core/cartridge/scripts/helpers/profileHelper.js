'use strict';

/**
 * Adds custom customer data from global customer object to viewData for Account-EditProfile action (edit profile).
 * @param {Response} req request object
 * @param {Response} res response object
 * @param {dw.customer.Customer} customer global customer object
 */
function addCustomProfileDataToEditProfileAction(req, res, customer) {
    var profileForm = res.getViewData().profileForm;
    if (!profileForm || !customer || !customer.profile) {
        return;
    }

    profileForm.customer.phoneMobile.value = customer.profile.phoneMobile; // eslint-disable-line no-param-reassign
    profileForm.customer.birthday.value = customer.profile.birthday; // eslint-disable-line no-param-reassign

    var titleOptions = profileForm.title.titleList.options;
    if (customer.profile.title) {
        for (var i = 0; i < titleOptions.length; i++) {
            if (titleOptions[i].htmlValue === customer.profile.title) {
                titleOptions[i].checked = true;
            }
        }
    }

    res.setViewData({
        profileForm: profileForm
    });
}

/**
 * Checks if a customer with the given fieldValue does not exists.
 * @param {string} fieldKey key of the field to check for uniqueness
 * @param {string} fieldValue value to check against the system objects
 * @returns {boolean} true if customer does not exists, false otherwise.
 */
function customerWithFieldValueDoesNotExists(fieldKey, fieldValue) {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var customer = CustomerMgr.queryProfile(fieldKey + '={0}', fieldValue);
    return customer === null;
}

/**
 * Checks appropriateness of the field.
 * @param {Object} form current form
 * @param {string} fieldKey key of the field to check for uniqueness
 * @param {dw.customer.Customer} currentCustomer global customer object
 * @param {boolean} ignoreCurrentCustomer flag to ignore current customer
 * @returns {boolean} indicates if the field value is OK to use
 */
function fieldOK(form, fieldKey, currentCustomer, ignoreCurrentCustomer) {
    var sameAsCurrent = currentCustomer && currentCustomer.profile &&
        currentCustomer.profile[fieldKey] === form.customer[fieldKey].value;

    if (ignoreCurrentCustomer && sameAsCurrent) {
        return true;
    }

    return customerWithFieldValueDoesNotExists(fieldKey, form.customer[fieldKey].value);
}

/**
 * Validates request against existing uniqueness of the field.
 * @param {Object} routeHandler 'this' of the calling action
 * @param {Request} req req object from action
 * @param {Response} res res object from action
 * @param {Object} form submitted form
 * @param {string} fieldKey key of the field to check for uniqueness
 * @param {boolean} ignoreCurrentCustomer flag to tell if we should ignore current customer
 * @param {dw.customer.Customer} currentCustomer global customer object
 * @returns {boolean} call next chain method if true, return immediately otherwise
 */
function checkUniquenessOfField(routeHandler, req, res, form, fieldKey, ignoreCurrentCustomer, currentCustomer) {
    var StringUtils = require('dw/util/StringUtils');
    var Resource = require('dw/web/Resource');

    var field = form.customer[fieldKey];

    // form validation
    if (!field || !field.value) {
        field.valid = false; // eslint-disable-line no-param-reassign
        field.error = Resource.msg('error.message.required', 'forms', null); // eslint-disable-line no-param-reassign
        form.valid = false; // eslint-disable-line no-param-reassign
    } else if (!fieldOK(form, fieldKey, currentCustomer, ignoreCurrentCustomer)) {
        field.valid = false; // eslint-disable-line no-param-reassign
        field.error = Resource.msg(StringUtils.format('error.message.{0}.alreadyexists', fieldKey), 'forms', null); // eslint-disable-line no-param-reassign
        form.valid = false; // eslint-disable-line no-param-reassign
    }

    if (!form.valid) {
        return false;
    }
    return true;
}

/**
 * Extracts email address from the external profile object
 * @param {Object} externalProfile Object to extract email address from
 * @returns {string} email address
 */
function getEmailFromExternalProfile(externalProfile) {
    var email = externalProfile['email-address'] || externalProfile.email;

    if (!email) {
        var emails = externalProfile.emails;

        if (emails && emails.length) {
            email = externalProfile.emails[0].value;
        }
    }

    return email;
}

/**
 * Saves opt in information to customer.
 * @param {Object} customerProfile customer profile to assign information
 * @param {Object} optinlist optin options of the form
 */
function saveOptinInformation(customerProfile, optinlist) {
    var Transaction = require('dw/system/Transaction');
    Transaction.wrap(function () {
        customerProfile.custom.emailOptin = optinlist.optinemail.checked; // eslint-disable-line no-param-reassign
        customerProfile.custom.smsOptin = optinlist.optinsms.checked; // eslint-disable-line no-param-reassign
        customerProfile.custom.telephoneOptin = optinlist.optintelephone.checked; // eslint-disable-line no-param-reassign
        customerProfile.custom.whatsappOptin = optinlist.optinwhatsapp.checked; // eslint-disable-line no-param-reassign
    });
}

/**
 * Saves extra profile data to system object if the previous chain event was successful.
 * @param {Object} routeHandler 'this' of the calling action
 * @param {Object} profileForm profile form
 * @param {dw.customer.Customer} currentCustomer (optional) global customer object
 */
function saveProfile(routeHandler, profileForm, currentCustomer) {
    routeHandler.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var viewData = res.getViewData();

        var authenticatedCustomer = currentCustomer || viewData.authenticatedCustomer;
        // if success and authenticatedCustomer is provided then
        // sets the extra data into Profile
        if (authenticatedCustomer && viewData.success) {
            var Transaction = require('dw/system/Transaction');

            Transaction.wrap(function () {
                authenticatedCustomer.profile.setPreferredLocale(req.locale.id);

                if (profileForm.title && profileForm.title.titleList) {
                    var title = profileForm.title.titleList.value;
                    authenticatedCustomer.profile.setTitle(title);
                }

                if (profileForm.customer.birthday && profileForm.customer.birthday.value) {
                    authenticatedCustomer.profile.setBirthday(profileForm.customer.birthday.value);
                }

                authenticatedCustomer.profile.setPhoneMobile(profileForm.customer.phoneMobile.value);
                if (profileForm.customer.addtoemaillist.checked) {
                    saveOptinInformation(authenticatedCustomer.profile, profileForm.accountpreferences.optinlist);
                }
            });
        }
    });
}

/**
 * Makes speedbus newsletter service request with given customer and locale id
 * @param {dw.customer.Profile} customerProfile global profile object
 * @param {string} localeID value to get country and preferred language
 * @returns {Objet} speedbus newsletter service response.
 */
function updateNewsletter(customerProfile, localeID) {
    var Locale = require('dw/util/Locale');
    var reqLocale = Locale.getLocale(localeID);
    var StringUtils = require('dw/util/StringUtils');
    var requestPayLoad = {
        request: {
            firstName: customerProfile.firstName,
            lastName: customerProfile.lastName,
            email: customerProfile.email,
            mobileNumber: customerProfile.phoneMobile || '',
            country: reqLocale.country,
            title: customerProfile.title,
            preferredLanguage: reqLocale.language,
            blockEmail: !customerProfile.custom.emailOptin,
            blockSms: !customerProfile.custom.smsOptin,
            blockTelephone: !customerProfile.custom.telephoneOptin,
            blockWhatsapp: !customerProfile.custom.whatsappOptin
        }
    };

    var birthDate = customerProfile.birthday;

    if (birthDate) {
        var cal = require('dw/util/Calendar')(birthDate);
        birthDate = StringUtils.formatCalendar(cal, 'yyyy-MM-dd');
        requestPayLoad.request.birthDate = birthDate;
    }
    var speedBusNewsletterService = require('*/cartridge/scripts/services/speedBusNewsletterService');
    var result = speedBusNewsletterService.call(requestPayLoad);
    return result;
}

/**
 * Saves address to address book if the previous chain event was successful.
 * @param {Object} routeHandler 'this' of the calling action
 * @param {Object} profileForm profile form
 * @param {dw.customer.Customer} currentCustomer (optional) global customer object
 */
function addAddress(routeHandler, profileForm, currentCustomer) {
    routeHandler.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

        var viewData = res.getViewData();

        var authenticatedCustomer = currentCustomer || viewData.authenticatedCustomer;
        // if success and authenticatedCustomer is provided then
        // sets the address to address book
        if (authenticatedCustomer && viewData.success) {
            var Transaction = require('dw/system/Transaction');
            var Resource = require('dw/web/Resource');

            Transaction.wrap(function () {
                var addressBook = authenticatedCustomer.profile.getAddressBook();
                var address = addressBook.createAddress(Resource.msg('label.register.default', 'forms', null));

                // phoneMobile must be required throughout the system
                var phoneMobile = profileForm.customer.phoneMobile.value;
                address.setPhone(phoneMobile);

                var title = profileForm.title.titleList.value;
                if (title) {
                    address.setTitle(title);
                }

                var firstname = profileForm.customer.firstname.value;
                if (firstname) {
                    address.setFirstName(firstname);
                }

                var lastname = profileForm.customer.lastname.value;
                if (lastname) {
                    address.setLastName(lastname);
                }

                var addressInput = profileForm.address.value;
                if (addressInput) {
                    address.setAddress1(addressInput);
                }

                var postalCode = profileForm.postalCode.value;
                if (postalCode) {
                    address.setPostalCode(postalCode);
                }

                var city = profileForm.city.value;
                if (city) {
                    address.setCity(city);
                }

                var country = profileForm.country.value || localeHelpers.getCurrentCountryCode();
                if (country) {
                    address.setCountryCode(country);
                }
            });
        }
    });
}

/**
 * Gets the PasswordConstraints as object
 * minLength, minSpecialChars, isForceLetters, isForceMixedCase, isForceMixedCase, isForceNumbers
 * @returns {Object} PasswordConstraints object
 */
function getPasswordConstraints() {
    var CustomerMgr = require('dw/customer/CustomerMgr');

    var passwordConstraints = CustomerMgr.getPasswordConstraints();
    return {
        minLength: passwordConstraints.getMinLength(),
        minSpecialChars: passwordConstraints.getMinSpecialChars(),
        isForceLetters: passwordConstraints.isForceLetters(),
        isForceMixedCase: passwordConstraints.isForceMixedCase(),
        isForceNumbers: passwordConstraints.isForceNumbers()
    };
}

module.exports = {
    saveProfile: saveProfile,
    addAddress: addAddress,
    checkUniquenessOfField: checkUniquenessOfField,
    customerWithFieldValueDoesNotExists: customerWithFieldValueDoesNotExists,
    addCustomProfileDataToEditProfileAction: addCustomProfileDataToEditProfileAction,
    getEmailFromExternalProfile: getEmailFromExternalProfile,
    getPasswordConstraints: getPasswordConstraints,
    updateNewsletter: updateNewsletter,
    saveOptinInformation: saveOptinInformation
};
