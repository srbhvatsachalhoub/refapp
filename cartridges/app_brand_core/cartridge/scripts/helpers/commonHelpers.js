'use strict';

/* global request, response */

/**
 * Checks the current request is ajax request
 * Checks the http header x-requested-with from request object's httpHeaders.x-requested-with
 * @returns {boolean} true if request.httpHeaders.x-requested-with == XMLHttpRequest, false otherwise
 */
function isAjaxRequest() {
    if (!request || !request.httpHeaders) {
        return false;
    }

    return Object.hasOwnProperty.call(request.httpHeaders, 'x-requested-with')
        && request.httpHeaders['x-requested-with'] === 'XMLHttpRequest';
}

/**
 * Checks the given object is array
 * @param {Object} obj - the object to be checked if it is array
 * @returns {boolean} true if the obj is array, false otherwise
 */
function isArray(obj) {
    if (typeof Array.isArray === 'undefined') {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }

    return Array.isArray(obj);
}

/**
 * Checks if device has mobile width
 * @param {number} width - current device width
 * @param {Object} config - device width config
 * @returns {boolean} true if device has mobile with, false otherwise
 */
function isMobileView(width, config) {
    if (width < config.sm) {
        return true;
    }

    return false;
}

/**
 * Checks if device is tablet
 * @param {number} width - current device width
 * @param {Object} config - device width config
 * @returns {boolean} true if device has tablet with, false otherwise
 */
function isTabletView(width, config) {
    if (width < config.lg && width > config.sm) {
        return true;
    }

    return false;
}

/**
 * Checks if device is Desktop
 * @param {number} width - current device width
 * @param {Object} config - device width config
 * @returns {boolean} true if device has Desktop with, false otherwise
 */
function isDesktop(width, config) {
    if (width > config.lg) {
        return true;
    }

    return false;
}

module.exports = {
    isAjaxRequest: isAjaxRequest,
    isArray: isArray,
    isMobileView: isMobileView,
    isTabletView: isTabletView,
    isDesktop: isDesktop
};
