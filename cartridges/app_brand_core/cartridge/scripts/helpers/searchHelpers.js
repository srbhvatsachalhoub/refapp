'use strict';

var originalExports = Object.keys(module.superModule);
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

/**
 * Set search configuration values
 * Removed checking category online for the category landing page
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @param {Object} params - Provided HTTP query parameters
 * @return {dw.catalog.ProductSearchModel} - API search instance
 */
function setupSearch(apiProductSearch, params) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var searchModelHelper = require('*/cartridge/scripts/search/search');

    var sortingRule = params.srule ? CatalogMgr.getSortingRule(params.srule) : null;
    var selectedCategory = CatalogMgr.getCategory(params.cgid);
    selectedCategory =
        selectedCategory && selectedCategory.online && selectedCategory.custom.localeOffline.value !== 'true'
            ? selectedCategory
            : null;

    searchModelHelper.setProductProperties(apiProductSearch, params, selectedCategory, sortingRule);

    if (params.preferences) {
        searchModelHelper.addRefinementValues(apiProductSearch, params.preferences);
    }

    return apiProductSearch;
}

/**
 * Retrieve a category's template filepath
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @return {string} - Category's template filepath
 */
function getCategoryTemplate(apiProductSearch) {
    return apiProductSearch.category && apiProductSearch.category.template
        ? apiProductSearch.category.template
        : 'search/searchResults';
}

/**
 * Set content search configuration values
 *
 * @param {Object} params - Provided HTTP query parameters
 * @return {Object} - content search instance
 */
function setupContentSearch(params) {
    var ContentSearchModel = require('dw/content/ContentSearchModel');
    var ContentSearch = require('*/cartridge/models/search/contentSearch');
    var apiContentSearchModel = new ContentSearchModel();

    apiContentSearchModel.setRecursiveFolderSearch(true);
    apiContentSearchModel.setSearchPhrase(params.q);
    apiContentSearchModel.search();
    var contentSearchResult = apiContentSearchModel.getContent();
    var count = Number(apiContentSearchModel.getCount());
    var contentSearch = new ContentSearch(contentSearchResult, count, params.q, params.startingPage, params.pageSize);

    return contentSearch;
}

/**
 * performs a search
 *
 * @param {Object} req - Provided HTTP query parameters
 * @param {Object} res - Provided HTTP query parameters
 * @return {Object} - an object with relevant search information
 */
function search(req, res) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var URLUtils = require('dw/web/URLUtils');
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var Logger = require('dw/system/Logger');

    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
    var ProductSearch = require('*/cartridge/models/search/productSearch');
    var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
    var schemaHelper = require('*/cartridge/scripts/helpers/structuredDataHelper');
    var currentSite = require('dw/system/Site').getCurrent();

    var categoryTemplate = '';
    var maxSlots = 4;
    var productSearch;
    var reportingURLs;

    var apiProductSearch = res.getViewData().apiProductSearch;
    if (!apiProductSearch) {
        apiProductSearch = new ProductSearchModel();
    }

    var searchRedirect = req.querystring.q ? apiProductSearch.getSearchRedirect(req.querystring.q) : null;

    if (searchRedirect) {
        return { searchRedirect: searchRedirect.getLocation() };
    }

    apiProductSearch = setupSearch(apiProductSearch, req.querystring);
    apiProductSearch.search();

    if (!apiProductSearch.personalizedSort) {
        base.applyCache(res);
    }
    categoryTemplate = getCategoryTemplate(apiProductSearch);
    productSearch = new ProductSearch(
        apiProductSearch,
        req.querystring,
        req.querystring.srule,
        CatalogMgr.getSortingOptions(),
        CatalogMgr.getSiteCatalog().getRoot()
    );

    pageMetaHelper.setPageMetaTags(req.pageMetaData, productSearch);

    var canonicalUrl = URLUtils.url('Search-Show', 'cgid', req.querystring.cgid);
    var refineurl = URLUtils.url('Search-Refinebar');
    var whitelistedParams = ['q', 'cgid', 'pmin', 'pmax', 'srule'];
    var isRefinedSearch = false;

    Object.keys(req.querystring).forEach(function (element) {
        if (whitelistedParams.indexOf(element) > -1) {
            refineurl.append(element, req.querystring[element]);
        }

        if (['pmin', 'pmax'].indexOf(element) > -1 && req.querystring[element] > -1) {
            isRefinedSearch = true;
        }

        if (element === 'preferences') {
            var i = 1;
            isRefinedSearch = true;
            Object.keys(req.querystring[element]).forEach(function (preference) {
                refineurl.append('prefn' + i, preference);
                refineurl.append('prefv' + i, req.querystring[element][preference]);
                i++;
            });
        }
    });

    refineurl.append('refined', isRefinedSearch ? 1 : 0);

    if (productSearch.searchKeywords !== null && !isRefinedSearch) {
        reportingURLs = reportingUrlsHelper.getProductSearchReportingURLs(productSearch);
    }

    // Prepare search term synonyms that will be used for including search term based content assets.
    var searchTermSynonyms = {};
    try {
        searchTermSynonyms = currentSite.getCustomPreferenceValue('searchTermSynonyms')
            ? JSON.parse(currentSite.getCustomPreferenceValue('searchTermSynonyms')) : {};
    } catch (error) {
        // Invalid JSON string.
        Logger.warn('Invalid JSON string in searchTermSynonyms: {0}', error.message);
    }
    var searchTerm = (productSearch.searchKeywords || '').toLowerCase().trim();
    var searchTermSynonym = searchTermSynonyms[searchTerm];
    var srpContentAssetId = (searchTermSynonym || searchTerm).toLowerCase().trim().replace(/\s/g, '-');

    var result = {
        productSearch: productSearch,
        maxSlots: maxSlots,
        reportingURLs: reportingURLs,
        refineurl: refineurl,
        canonicalUrl: canonicalUrl,
        srpContentAssetId: srpContentAssetId
    };

    if (productSearch.isCategorySearch && !productSearch.isRefinedCategorySearch) {
        pageMetaHelper.setPageMetaData(req.pageMetaData, productSearch.category);
        result.category = apiProductSearch.category;
        result.categoryTemplate = categoryTemplate;
    }

    if (!categoryTemplate || categoryTemplate === 'rendering/category/categoryproducthits') {
        result.schemaData = schemaHelper.getListingPageSchema(productSearch.productIds);
    }

    return result;
}

/**
 * @param {dw.catalog.Category} category - category
 * @return {Array} - array of sub categories
 */
function getSubCatgories(category) {
    var subCategoriesCLP = [];
    var URLUtils = require('dw/web/URLUtils');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    if (category.custom.clpSubcategoryMenuItems) {
        category.custom.clpSubcategoryMenuItems.forEach(function (subCategoryID) {
            var subCategory = CatalogMgr.getCategory(subCategoryID);
            if (subCategory) {
                var subCategoryCLPModel = {
                    name: subCategory.getDisplayName(),
                    online: subCategory.getOnlineFlag(),
                    URL: subCategory.custom && 'alternativeUrl' in subCategory.custom && subCategory.custom.alternativeUrl
                        ? subCategory.custom.alternativeUrl
                        : URLUtils.url('Search-Show', 'cgid', subCategory.getID()).toString()
                };
                subCategoriesCLP.push(subCategoryCLPModel);
            }
        });
    }
    return subCategoriesCLP;
}

base.setupSearch = setupSearch;
base.getCategoryTemplate = getCategoryTemplate;
base.setupContentSearch = setupContentSearch;
base.search = search;
base.getSubCatgories = getSubCatgories;
module.exports = base;
