'use strict';
/* global request */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * Creates an instance of the google.recaptcha service.
 * {dw.svc.LocalServiceRegistry} - google.recaptcha service object
 */
var reCaptchaService = LocalServiceRegistry.createService('google.recaptcha', {
    /**
     * google.recaptcha Service ServiceCallback createRequest function
     * @param {dw.svc.Service} svc - The service
     * @param {Object} payload - the request JSON object
     * @returns {object} the request data object
     */

    createRequest: function (svc, payload) {
        svc.setAuthentication('NONE');
        svc.addParam('secret', payload.secret);
        svc.addParam('response', payload.response);
        if (request.httpRemoteAddress) {
            svc.addParam('remoteip', request.httpRemoteAddress);
        }
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    }
});

/**
 * Gets the reCaptcha javascript api url with language parameter
 * @returns {string} the url of the javascript reCaptcha api url
 */
function getApiUrl() {
    var Locale = require('dw/util/Locale');
    var StringUtils = require('dw/util/StringUtils');

    var language = 'en';
    var locale = Locale.getLocale(request.locale); // eslint-disable-line no-undef
    if (locale) {
        language = locale.language;
    }

    return StringUtils.format('https://www.google.com/recaptcha/api.js?hl={0}', language);
}

/**
 * Gets the reCaptcha settings for the given form
 * If no reCaptcha settings defined or reCaptcha settings is not well formed or
 * does not contain siteKey or does not contain secretKey or does not contain forms
 * or given form id is not in forms array
 * then return false
 * otherwise return JSON object thak has enabled:true, siteKey and secretKey
 * @param {string} formId - the form id to be checked for reCaptcha
 * @returns {Object} false if setting is not suitable for given form or JSON object for reCaptcha setting
 */
function getReCaptcha(formId) {
    if (!formId) {
        return false;
    }

    var Site = require('dw/system/Site');

    var googleReCaptchaSettings = Site.current.getCustomPreferenceValue('googleReCaptchaSettings');
    if (!googleReCaptchaSettings) {
        return false;
    }

    try {
        googleReCaptchaSettings = JSON.parse(googleReCaptchaSettings);
    } catch (e) {
        var logger = require('dw/system/Logger').getLogger('reCaptchaHelpers');
        logger.error('Custom Preference googleReCaptchaSettings is not well-formed, message: {0}!', e.message);

        return false;
    }

    if (!googleReCaptchaSettings.siteKey
        || !googleReCaptchaSettings.secretKey
        || !googleReCaptchaSettings.forms
        || !googleReCaptchaSettings.forms.indexOf(formId) < 0) {
        return false;
    }

    return {
        enabled: true,
        siteKey: googleReCaptchaSettings.siteKey,
        secretKey: googleReCaptchaSettings.secretKey,
        apiUrl: getApiUrl()
    };
}

/**
 * Does server side validation
 * if recaptcha is defined and enabled for the given form
 * @param {string} formId - the form id to be checked for reCaptcha
 * @param {Object} form - the posted form
 * @returns {Object} reCatptcha server side validation object
 */
function verifyReCaptcha(formId, form) {
    var reCapthcha = getReCaptcha(formId);
    // if reCaptcha is not defined or not enabled for the formId then return as valid: true
    if (!reCapthcha || !reCapthcha.enabled) {
        return {
            valid: true
        };
    }

    // get recaptcha response from client side
    var reCaptchaResponse = form['g-recaptcha-response'];
    if (!reCaptchaResponse) {
        return {
            valid: false
        };
    }

    // do server side validation with client side response
    var reCaptchaServiceResult = reCaptchaService.call({
        secret: reCapthcha.secretKey,
        response: reCaptchaResponse
    });

    // if call is cussess then retur valid: true
    if (reCaptchaServiceResult.isOk() && reCaptchaServiceResult.object && reCaptchaServiceResult.object.success) {
        return {
            valid: true
        };
    }

    // return valid: false
    return {
        valid: false
    };
}

module.exports = {
    getReCaptcha: getReCaptcha,
    verifyReCaptcha: verifyReCaptcha
};
