'use strict';
/**
 * Define constants used in project.
 */
module.exports = {
    paymentServiceFeeKey: 'PAYMENT_SERVICE_FEE',
    siteLocaleCookieName: 'site_locale'
};
