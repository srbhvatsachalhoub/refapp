'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var StringUtils = require('dw/util/StringUtils');
var currentSite = require('dw/system/Site').getCurrent();

var serviceImpl = LocalServiceRegistry.createService('pvilio.otp', {
    createRequest: function (svc, payload) { // eslint-disable-line consistent-return
        if (!svc.setCredentialID(currentSite.getCustomPreferenceValue('smsVerificationCredentialId'))) {
            throw new Error(StringUtils.format('Please set service credentials : {0}', currentSite.getCustomPreferenceValue('smsVerificationCredentialId')));
        }
        var credential = svc.getConfiguration().getCredential();
        svc.setURL(StringUtils.format(credential.URL, credential.user));
        svc.addHeader('Content-Type', 'application/json');
        if (payload) {
            return JSON.stringify(payload);
        }
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    },
    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: JSON.stringify({
                api_id: '14117d3e-6f3d-11e9-9987-0242ac110004',
                message: 'message(s) queued',
                message_uuid: [
                    '14179688-6f3d-11e9-9987-0242ac110004'
                ]
            })
        };
    }
});


module.exports = serviceImpl;

