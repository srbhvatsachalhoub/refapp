'use strict';

/**
 * @module services/pdfService
 */

/**
 * PDF Generation service definition
 */

var downloadZipDefinition = {
    /**
     * Creates the request
     *
     * @param {dw.svc.HTTPService} svc Service object to configure the request
     * @param {Object} invoiceRequests Request to send to PdfService
     * @param {string} outputFile File handle to write the response stream into.
     *
     * @returns {string} Request body
     */
    createRequest: function (svc, invoiceRequests, outputFile) {
        svc.addHeader('Content-Type', 'application/json');
        svc.setOutFile(outputFile);
        return invoiceRequests;
    },
    /**
     * This method is required in order for the service to run properly,
     * since we don't need any response parsing it is empty in our case
     */
    parseResponse: function () {}
};

module.exports = {
    definitions: {
        downloadZip: downloadZipDefinition
    }
};
