'use strict';
/* global session */

var originalExports = Object.keys(module.superModule || {});
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

var Site = require('dw/system/Site');
var Resource = require('dw/web/Resource');
var HookMgr = require('dw/system/HookMgr');
var OrderMgr = require('dw/order/OrderMgr');
var BasketMgr = require('dw/order/BasketMgr');
var PaymentMgr = require('dw/order/PaymentMgr');
var Order = require('dw/order/Order');
var Status = require('dw/system/Status');
var StringUtils = require('dw/util/StringUtils');
var Transaction = require('dw/system/Transaction');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var collections = require('*/cartridge/scripts/util/collections');
var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');
var logger = require('dw/system/Logger').getLogger('app.brand.core.checkoutHelpers');

/**
 * Checks productLineItems if any of the product is gift card product
 * @param {dw.util.Collection} productLineItems - the productLineItems for order/basket/shipment
 * @returns {boolean} true if any of them is gift card product, false otherwise
 */
function hasGiftCardProduct(productLineItems) {
    if (!productLineItems) {
        return false;
    }

    // check current (basket/order/shipment)'s productlineitems
    // if there is any product as marked with gift card(giftCard product)
    return collections.some(productLineItems, function (pli) {
        return pli.gift;
    });
}

/**
 * Checks productLineItems if any of the products is engraved
 * @param {dw.util.Collection} productLineItems - the productLineItems for order/basket/shipment
 * @returns {boolean} true if any of them is engraved product, false otherwise
 */
function hasEngravedProduct(productLineItems) {
    if (!productLineItems) {
        return false;
    }

    // check current (basket/order/shipment)'s productlineitems
    // if there is any product ordered with engraving
    return collections.some(productLineItems, function (pli) {
        return pli.custom.engravingRequested;
    });
}

/**
 * Checks productLineItems if any of the products is personalized
 * @param {dw.util.Collection} productLineItems - the productLineItems for order/basket/shipment
 * @returns {boolean} true if any of them is personalized product, false otherwise
 */
function hasPersonalizedProduct(productLineItems) {
    if (!productLineItems) {
        return false;
    }

    // check current (basket/order/shipment)'s productlineitems
    // if there is any product ordered with personalization
    return collections.some(productLineItems, function (pli) {
        return pli.custom.personalizationRequested;
    });
}

/**
 * Check if the Apple Pay payment method is applicable
 * applicable: if session.custom.applepaysession is equal to 'yes'
 * @returns {boolean} true if applicable, false otherwise
 */
function isApplePayApplicable() {
    return session.custom.applepaysession === 'yes'; // eslint-disable-line no-undef
}

/**
 * Creates an array of objects containing applicable payment methods
 * @param {dw.util.ArrayList<dw.order.dw.order.PaymentMethod>} paymentMethods - An ArrayList of
 *      applicable payment methods that the user could use for the current basket.
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.util.ArrayList<dw.order.dw.order.PaymentMethod>} of payment methods for the current cart
 */
function getApplicablePaymentMethods(paymentMethods, currentBasket) {
    // Stop here if paymentMethods is undefined
    if (!paymentMethods) return null;

    var ArrayList = require('dw/util/ArrayList');
    var result = paymentMethods;

    // Exclude payment methods that are not available with gift card products
    // Exclude ApplePay Payment Method if session.custom.applepaysession is not equal to 'yes'
    var hasGiftCard = hasGiftCardProduct(currentBasket.productLineItems);
    result = collections.filter(paymentMethods, function (method) {
        return (method.ID !== PaymentInstrument.METHOD_DW_APPLE_PAY || isApplePayApplicable())
            && (!hasGiftCard || (!method.custom || method.custom.isAvailableForGiftCardProducts === true));
    });
    result = new ArrayList(result);

    return result;
}

/**
 * Sends a confirmation to the current user
 * @param {dw.order.Order} order - The current user's order
 * @returns {void}
 */
function sendConfirmationEmail(order) {
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');

    if (emailHelpers.isMCOrderEmailsDisabled()) {
        return;
    }

    var orderObject = { Order: order };

    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msg('subject.order.confirmation.email', 'order', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.orderConfirmation
    };

    emailHelpers.sendEmail(emailObj, 'checkout/confirmation/confirmationEmail', orderObject);
}

/**
 * Attempts to place the order
 * @param {dw.order.Order} order - The order object to be placed
 * @param {Object} fraudDetectionStatus - an Object returned by the fraud detection hook
 * @returns {Object} an error object
 */
function placeOrder(order, fraudDetectionStatus) {
    var result = { error: false };

    try {
        Transaction.begin();
        var placeOrderStatus = OrderMgr.placeOrder(order);
        if (placeOrderStatus === Status.ERROR) {
            throw new Error();
        }

        if (fraudDetectionStatus.status === 'flag') {
            hooksHelper('app.order.status.setConfirmationStatus', 'setConfirmationStatus', [order, Order.CONFIRMATION_STATUS_NOTCONFIRMED], function () {
                order.setConfirmationStatus(Order.CONFIRMATION_STATUS_NOTCONFIRMED);
            });
        } else {
            hooksHelper('app.order.status.setConfirmationStatus', 'setConfirmationStatus', [order, Order.CONFIRMATION_STATUS_CONFIRMED], function () {
                order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
            });
        }

        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_READY], function () {
            order.setExportStatus(Order.EXPORT_STATUS_READY);
        });

        Transaction.commit();
    } catch (e) {
        logger.error('Exception on scripts/checkout/checkoutHelpers:placeOrder, ex: {0}', e.message);

        Transaction.wrap(function () {
            hooksHelper('app.order.status.failOrder', 'failOrder', [order, 'placeOrder fail'], function () {
                OrderMgr.failOrder(order);
            });
        });
        result.error = true;
    }

    return result;
}

/**
 * Sets the given email address to the current shopping basket
 * @param {string} email - the email address
 * @param {Object} basket - the current shopping basket
 */
function setBasketCustomerEmail(email, basket) {
    if (empty(email) || empty(basket)) { // eslint-disable-line no-undef
        return;
    }
    Transaction.wrap(function () {
        basket.setCustomerEmail(email);
    });
}

/**
 * handles the payment authorization for each payment instrument
 * @param {dw.order.Order} order - the order object
 * @param {string} orderNumber - The order number for the order
 * @param {Object} additionalParam - The additional param as object if needed, paypal use this param
 * @returns {Object} an error object
 */
function handlePayments(order, orderNumber, additionalParam) {
    var result = {};

    if (order.totalNetPrice !== 0.00) {
        var paymentInstruments = order.paymentInstruments;

        if (paymentInstruments.length === 0) {
            Transaction.wrap(function () {
                hooksHelper('app.order.status.failOrder', 'failOrder', [order, true, 'paymentInstruments fail'], function () {
                    OrderMgr.failOrder(order, true);
                });
            });
            result.error = true;
        }

        if (!result.error) {
            var sortedPaymentInstrumentsArray = paymentMethodHelpers.sortPaymentInstrumentsIntoArray(order);

            for (var i = 0; i < sortedPaymentInstrumentsArray.length; i++) {
                var paymentInstrument = sortedPaymentInstrumentsArray[i];

                var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).paymentProcessor;
                var authorizationResult;
                if (paymentProcessor === null) {
                    Transaction.wrap(function () { // eslint-disable-line no-loop-func
                        paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
                    });
                } else {
                    var hook = StringUtils.format('app.payment.processor.{0}', paymentProcessor.ID.toLowerCase());
                    var hookFunc = (paymentInstrument.paymentMethod === PaymentInstrument.METHOD_DW_APPLE_PAY ? 'AuthorizeApplePay' : 'Authorize');

                    if (HookMgr.hasHook(hook)) {
                        authorizationResult = HookMgr.callHook(
                            hook,
                            hookFunc,
                            order,
                            orderNumber,
                            paymentInstrument,
                            paymentProcessor,
                            additionalParam
                        );
                    } else {
                        authorizationResult = HookMgr.callHook(
                            'app.payment.processor.default',
                            'Authorize'
                        );
                    }

                    if (authorizationResult.error) {
                        Transaction.wrap(function () { // eslint-disable-line no-loop-func
                            hooksHelper('app.order.status.failOrder', 'failOrder', [order, true, StringUtils.format('authorization fail, hook: {0}, hookFunc: {1}', hook, hookFunc)], function () {
                                OrderMgr.failOrder(order, true);
                            });
                        });
                        result.error = true;
                        break;
                    }

                    // check if the result has the form to be posted
                    // if so include it in the result object
                    if (authorizationResult.form) {
                        result.form = authorizationResult.form;
                        break;
                    }

                    // check if the result has the 3d url to be redirected
                    // if so include it in the result object
                    if (authorizationResult.redirect) {
                        result.redirect = true;
                        result.redirectUrl = authorizationResult.redirectUrl;
                        break;
                    }
                }
            }
        }
    }

    return result;
}

/**
 * Determines if the basket already contains a payment instrument
 * and removes it from the basket except complimentary ones.
 * MUST BE called in Transaction
 * @param {dw.order.LineItemCtnr} lineItemCtnr Current users's basket
 */
function removeExistingPaymentInstruments(lineItemCtnr) {
    var paymentInstruments = lineItemCtnr.getPaymentInstruments();
    collections.forEach(paymentInstruments, function (item) {
        if (!paymentMethodHelpers.isComplementaryPaymentMethod(item.paymentMethod)) {
            lineItemCtnr.removePaymentInstrument(item);
        }
    });
}

/**
 * Sets the payment transaction amount to Non-GiftCard Payment instrument
 * Extended with gift certificates payment instrument
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {Object} an error object
 */
function calculatePaymentTransaction(currentBasket) {
    var result = { error: false };

    var nonComplimentaryPaymentAmountTotal = basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket);
    var nonComplimentaryPaymentInstruments = [];


    collections.forEach(currentBasket.paymentInstruments, function (paymentInstrument) {
        if (paymentMethodHelpers.isComplementaryPaymentMethod(paymentInstrument.paymentMethod)) {
            if (!paymentInstrument.paymentTransaction || !paymentInstrument.paymentTransaction.amount || !paymentInstrument.paymentTransaction.amount.available) {
                result.error = true;
            }
        } else {
            nonComplimentaryPaymentInstruments.push(paymentInstrument);
        }
    });

    // not allow for multiple non complimentary payment
    if (!result.error && nonComplimentaryPaymentInstruments.length > 1) {
        result.error = true;
    }

    var orderTotal = currentBasket.totalGrossPrice;
    if (!result.error && nonComplimentaryPaymentAmountTotal.value > orderTotal.value) {
        result.error = true;
    }

    if (!result.error && nonComplimentaryPaymentInstruments.length && nonComplimentaryPaymentInstruments[0]) {
        try {
            Transaction.wrap(function () {
                nonComplimentaryPaymentInstruments[0].paymentTransaction.setAmount(nonComplimentaryPaymentAmountTotal);
            });
        } catch (e) {
            logger.error('Exception on scripts/checkout/checkoutHelpers:calculatePaymentTransaction, ex: {0}', e.message);

            result.error = true;
        }
    }
    return result;
}

/**
 * saves payment instrument to customers wallet with order payment instrument
 * @param {dw.order.PaymentInstrument} paymentInstrument - the paymentInstrument to be saved to wallet
 * @param {dw.customer.Customer} customer - The current customer
 * @returns {dw.customer.CustomerPaymentInstrument} newly stored payment Instrument
 */
function savePaymentInstrumentToWalletWithPaymentInstrument(paymentInstrument, customer) {
    var wallet = customer.getProfile().getWallet();

    return Transaction.wrap(function () {
        var storedPaymentInstrument = wallet.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD);

        storedPaymentInstrument.setCreditCardHolder(
            paymentInstrument.getCreditCardHolder()
        );
        storedPaymentInstrument.setCreditCardNumber(
            paymentInstrument.getCreditCardNumber()
        );
        storedPaymentInstrument.setCreditCardType(
            paymentInstrument.getCreditCardType()
        );
        storedPaymentInstrument.setCreditCardExpirationMonth(
            paymentInstrument.getCreditCardExpirationMonth()
        );
        storedPaymentInstrument.setCreditCardExpirationYear(
            paymentInstrument.getCreditCardExpirationYear()
        );

        storedPaymentInstrument.setCreditCardToken(
            paymentInstrument.getCreditCardToken()
        );

        // set payment processor
        // since we can use multiple PSP
        // storedPaymentInstrument should be visible for active PSP
        // payfort or checkout.com for now
        if (paymentInstrument.paymentTransaction
            && paymentInstrument.paymentTransaction
            && paymentInstrument.paymentTransaction.paymentProcessor) {
            storedPaymentInstrument.custom.paymentProcessor = paymentInstrument.paymentTransaction.paymentProcessor.ID;
            storedPaymentInstrument.custom.referenceOrderNo = paymentInstrument.paymentTransaction.transactionID;
            storedPaymentInstrument.custom.locale = request.locale; // eslint-disable-line no-undef
        }

        return storedPaymentInstrument;
    });
}

/**
 * sets the gift message on a shipment
 * @param {dw.order.Shipment} shipment - Any shipment for the current basket
 * @param {boolean} isGift - is the shipment a gift
 * @param {string} giftMessage - The gift message the user wants to attach to the shipment
 * @param {boolean} isPriceHidden - flag for specifying if the price will be shown on gift package or not
 * @param {boolean} isGiftWrapped  - flag for specifying if the package will be wrapped as a gift or not
 * @returns {Object} result object
 */
function setGift(shipment, isGift, giftMessage, isPriceHidden, isGiftWrapped) {
    var result = { error: false, errorMessage: null };

    try {
        Transaction.wrap(function () {
            /* eslint-disable no-param-reassign */
            shipment.setGift(isGift);
            if (isGift || isGiftWrapped) {
                shipment.custom.isPriceHidden = isPriceHidden;
                shipment.custom.isGiftWrapped = isGiftWrapped;
                shipment.setGiftMessage(giftMessage);
            } else {
                shipment.custom.isPriceHidden = null;
                shipment.custom.isGiftWrapped = null;
                shipment.setGiftMessage(null);
            }
        });
    } catch (e) {
        logger.error('Exception on scripts/checkout/checkoutHelpers:setGift, ex: {0}', e.message);

        result.error = true;
        result.errorMessage = Resource.msg('error.message.could.not.be.attached', 'checkout', null);
    }

    return result;
}

/**
 * Validates payment
 * @param {Object} req - The local instance of the request object
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {Object} an object that has error information
 */
function validatePayment(req, currentBasket) {
    var Money = require('dw/value/Money');
    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

    var applicablePaymentCards;
    var applicablePaymentMethods;
    var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
    var paymentAmount = basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket).value;
    var countryCode = currentBasket.billingAddress.countryCode.value || localeHelpers.getCurrentCountryCode();
    var currentCustomer = req.currentCustomer.raw;
    var paymentInstruments = currentBasket.paymentInstruments;
    var result = {};

    applicablePaymentMethods = getApplicablePaymentMethods(
        PaymentMgr.getApplicablePaymentMethods(
            currentCustomer,
            countryCode,
            paymentAmount
        ),
        currentBasket
    );
    applicablePaymentCards = creditCardPaymentMethod.getApplicablePaymentCards(
        currentCustomer,
        countryCode,
        paymentAmount
    );

    var invalid = true;
    var paymentInstrumentsAmount = new Money(0, currentBasket.currencyCode);
    for (var i = 0; i < paymentInstruments.length; i++) {
        var paymentInstrument = paymentInstruments[i];
        paymentInstrumentsAmount = paymentInstrumentsAmount.add(paymentInstrument.paymentTransaction.amount);

        if (paymentMethodHelpers.isComplementaryPaymentMethod(paymentInstrument.paymentMethod)) {
            invalid = false;
        }

        var paymentMethod = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod);

        if (paymentMethod && applicablePaymentMethods.contains(paymentMethod)) {
            if (PaymentInstrument.METHOD_CREDIT_CARD.equals(paymentInstrument.paymentMethod)) {
                var card = PaymentMgr.getPaymentCard(paymentInstrument.creditCardType);

                // Checks whether payment card is still applicable.
                if (card && applicablePaymentCards.contains(card)) {
                    invalid = false;
                }
            } else {
                invalid = false;
            }
        }

        if (invalid) {
            break; // there is an invalid payment instrument
        }
    }

    if (!invalid && paymentInstrumentsAmount.value !== currentBasket.totalGrossPrice.value) {
        invalid = true;
    }

    result.error = invalid;
    return result;
}

/**
 * Processes the place order
 * Be called from PSPs and app_brand_core (RefApp)
 * @param {Object} req - the SFCC request object
 * @param {dw.order.LineItemCtnr} order - The current order
 * @returns {Object} the JSON result object
 */
function processPlaceOrder(req, order) {
    var URLUtils = require('dw/web/URLUtils');

    var fraudDetectionStatus = hooksHelper('app.fraud.detection', 'fraudDetection', order, require('*/cartridge/scripts/hooks/fraudDetection').fraudDetection);
    if (fraudDetectionStatus.status === 'fail') {
        Transaction.wrap(function () {
            hooksHelper('app.order.status.failOrder', 'failOrder', [order, true, 'fraud fail'], function () {
                OrderMgr.failOrder(order, true);
            });
        });

        // fraud detection failed
        req.session.privacyCache.set('fraudDetectionStatus', true);

        return {
            error: true,
            cartError: true,
            redirectUrl: URLUtils.url('Error-ErrorCode', 'err', fraudDetectionStatus.errorCode).toString(),
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        };
    }

    // Places the order
    var placeOrderResult = base.placeOrder(order, fraudDetectionStatus);
    if (placeOrderResult.error) {
        return {
            error: true,
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        };
    }

    if (req.currentCustomer.addressBook) {
        var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');

        // save all used shipping addresses to address book of the logged in customer
        var allAddresses = addressHelpers.gatherShippingAddresses(order);
        allAddresses.forEach(function (address) {
            if (!addressHelpers.checkIfAddressStored(address, req.currentCustomer.addressBook.addresses)) {
                addressHelpers.saveAddress(address, req.currentCustomer, addressHelpers.generateAddressName(address));
            }
        });
    }

    // check if the site is GiftCardEnabled
    // if so then check the plis if any gift card product to create gift certificates
    if (Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
        var giftCardHelper = require('*/cartridge/scripts/helpers/giftCardHelper');
        giftCardHelper.createGiftCertificates(order);
    }

    try {
        sendConfirmationEmail(order);
    } catch (e) {
        Transaction.wrap(function () {
            order.custom.smcStatus = 'confirmed'; // eslint-disable-line no-param-reassign
        });
    }


    // Reset usingMultiShip after successful Order placement
    req.session.privacyCache.set('usingMultiShipping', false);

    // Exposing a direct route to an Order, without at least encoding the orderID
    //  is a serious PII violation.  It enables looking up every customers orders, one at a
    //  time.
    return {
        error: false,
        orderID: order.orderNo,
        orderToken: order.orderToken,
        continueUrl: URLUtils.url('Order-Confirm').toString()
    };
}
/**
 * Updates custom attributes for order
 * @param {dw.order.Order} order order to be updated
 */
function updateOrderCustomAttributes(order) {
    var allLineAllItems = order.getAllProductLineItems();
    collections.forEach(allLineAllItems, function (pli) {
        if (pli.product && 'chalhoub_id' in pli.product.custom && pli.product.custom.chalhoub_id && !pli.custom.chalhoub_id) {
            Transaction.wrap(function () {
                pli.custom.chalhoub_id = pli.product.custom.chalhoub_id;
            });
        }
    });
}

/**
 * Attempts to create an order from the current basket
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.order.Order} The order object created from the current basket
 */
function createOrder(currentBasket) {
    var order;

    try {
        order = Transaction.wrap(function () {
            var LineItemCtnr = require('dw/order/LineItemCtnr');
            currentBasket.setChannelType(LineItemCtnr.CHANNEL_TYPE_STOREFRONT);
            var createdOrder = OrderMgr.createOrder(currentBasket);
            updateOrderCustomAttributes(createdOrder);
            return createdOrder;
        });
    } catch (e) {
        logger.error('Create order is failed with error : {0}', e.message);
        return null;
    }
    return order;
}

/**
 * Copies information from the shipping form to the associated shipping address
 * @param {Object} shippingData - the shipping data
 * @param {dw.order.Shipment} [shipmentOrNull] - the target Shipment
 */
function copyShippingAddressToShipment(shippingData, shipmentOrNull) {
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;

    var shippingAddress = shipment.shippingAddress;

    Transaction.wrap(function () {
        if (shippingAddress === null) {
            shippingAddress = shipment.createShippingAddress();
        }

        shippingAddress.setFirstName(shippingData.address.firstName);
        shippingAddress.setLastName(shippingData.address.lastName);
        shippingAddress.setAddress1(shippingData.address.address1);
        shippingAddress.setAddress2(shippingData.address.address2);
        shippingAddress.setCity(shippingData.address.city);
        shippingAddress.setPostalCode(shippingData.address.postalCode);

        if (shippingData.address.stateCode) {
            shippingAddress.setStateCode(shippingData.address.stateCode);
        }

        if (shippingData.address.title) {
            shippingAddress.setTitle(shippingData.address.title);
        }

        var countryCode = shippingData.address.countryCode.value ? shippingData.address.countryCode.value : shippingData.address.countryCode;
        if (!countryCode) {
            var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

            countryCode = localeHelpers.getCurrentCountryCode();
        }

        shippingAddress.setCountryCode(countryCode);
        shippingAddress.setPhone(shippingData.address.phone);

        ShippingHelper.selectShippingMethod(shipment, shippingData.shippingMethod);
    });
}

/**
 * Sets the special request value to the given shipment
 * @param {string} specialRequest - the special request from cusstomer
 * @param {dw.order.Shipment} [shipmentOrNull] - the target Shipment
 */
function setSpecialRequest(specialRequest, shipmentOrNull) {
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;

    if (!shipment) {
        return;
    }

    Transaction.wrap(function () {
        shipment.custom.specialRequest = specialRequest;
    });
}

/**
 * Copies information from the shipping form to the associated shipping address
 * @param {string} countryCode - Current country code
 */
function removeShippingAddressFromBasket(countryCode) {
    var currentBasket = BasketMgr.getCurrentBasket();

    // Stop here if current basket is undefined
    if (!currentBasket) return;

    var shipment = currentBasket.defaultShipment;

    // Stop here if shipment is undefined
    if (!shipment) return;

    var shippingAddress;

    if (!countryCode) {
        var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

        countryCode = localeHelpers.getCurrentCountryCode();
    }

    Transaction.wrap(function () {
        shippingAddress = shipment.createShippingAddress();
        shippingAddress.setCountryCode(countryCode);
    });
}

/**
 * Copies a raw address object to the baasket billing address
 * @param {Object} address - an address-similar Object (firstName, ...)
 * @param {Object} currentBasket - the current shopping basket
 */
function copyBillingAddressToBasket(address, currentBasket) {
    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

    var billingAddress = currentBasket.billingAddress;

    Transaction.wrap(function () {
        if (!billingAddress) {
            billingAddress = currentBasket.createBillingAddress();
        }

        billingAddress.setFirstName(address.firstName);
        billingAddress.setLastName(address.lastName);
        billingAddress.setAddress1(address.address1);
        billingAddress.setAddress2(address.address2);
        billingAddress.setCity(address.city);
        billingAddress.setPostalCode(address.postalCode);
        billingAddress.setStateCode(address.stateCode);
        billingAddress.setCountryCode(address.countryCode.value || localeHelpers.getCurrentCountryCode());
        billingAddress.setPhone(address.phone);
    });
}

/**
 * Checks if the Mada PaymentCard is exists and active and applicable for the current session,basket and customer
 * If all is true, then returns true
 * otherwise returns false
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {dw.customer.Customer} customer - The customer
 * @param {string} countryCode - the country code
 * @param {number} paymentAmount - the payment amount
 * @return {boolean} returns true if Mada PaymentCard is applicable, false otherwise
 */
function isMadaPaymentCardApplicable(currentBasket, customer, countryCode, paymentAmount) {
    var madaPaymentCard = PaymentMgr.getPaymentCard('Mada');

    if (!madaPaymentCard || !madaPaymentCard.active) {
        return false;
    }

    if (!customer && currentBasket) {
        customer = currentBasket.customer;
    }

    if (!countryCode) {
        var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

        countryCode = localeHelpers.getCurrentCountryCode();
    }

    if (!paymentAmount && currentBasket) {
        paymentAmount = basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket).value;
    }

    return madaPaymentCard.isApplicable(customer, countryCode, paymentAmount);
}

base.createOrder = createOrder;
base.setGift = setGift;
base.calculatePaymentTransaction = calculatePaymentTransaction;
base.handlePayments = handlePayments;
base.validatePayment = validatePayment;
base.setBasketCustomerEmail = setBasketCustomerEmail;
base.removeExistingPaymentInstruments = removeExistingPaymentInstruments;
base.savePaymentInstrumentToWalletWithPaymentInstrument = savePaymentInstrumentToWalletWithPaymentInstrument;
base.processPlaceOrder = processPlaceOrder;
base.copyShippingAddressToShipment = copyShippingAddressToShipment;
base.setSpecialRequest = setSpecialRequest;
base.hasGiftCardProduct = hasGiftCardProduct;
base.hasEngravedProduct = hasEngravedProduct;
base.hasPersonalizedProduct = hasPersonalizedProduct;
base.getApplicablePaymentMethods = getApplicablePaymentMethods;
base.removeShippingAddressFromBasket = removeShippingAddressFromBasket;
base.copyBillingAddressToBasket = copyBillingAddressToBasket;
base.sendConfirmationEmail = sendConfirmationEmail;
base.placeOrder = placeOrder;
base.isMadaPaymentCardApplicable = isMadaPaymentCardApplicable;
module.exports = base;
