'use strict';

var originalExports = Object.keys(module.superModule || {});
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

var collections = require('*/cartridge/scripts/util/collections');
var ShippingMgr = require('dw/order/ShippingMgr');
var ShippingMethodModel = require('*/cartridge/models/shipping/shippingMethod');

/**
 * Checks if the all productlineitems in shipment products are virtual gift card product
 * true -> if all products are virtual gift card product
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @returns {boolean} true if every products in shipment is Virtual Gift Card Product, false otherwise
 */
function isAllProductsVirtualGiftCard(shipment) {
    return collections.every(shipment.productLineItems, function (pli) {
        return (pli.product && pli.product.custom.giftCardType.value === 'virtual');
    });
}

/**
 * Filter given applicableShippingMethods
 * with virtualGiftCardEnabled methods
 * @param {dw.util.Collection} methods - shipment API applicableShippingMethods
 * @returns {dw.util.Collection} - filtered applicableShippingMethods
 */
function filterShippingMethodsWithVirtualGiftCardEnabled(methods) {
    var ArrayList = require('dw/util/ArrayList');

    var applicableShippingMethods = new ArrayList();
    collections.forEach(methods, function (method) {
        if (method.custom.virtualGiftCardEnabled) {
            applicableShippingMethods.add(method);
        }
    });

    return applicableShippingMethods;
}

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {Object} [address] - optional address object
 * @returns {dw.util.Collection} an array of ShippingModels
 */
function getApplicableShippingMethods(shipment, address) {
    if (!shipment) return null;

    var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);

    var shippingMethods;
    if (address) {
        shippingMethods = shipmentShippingModel.getApplicableShippingMethods(address);
    } else {
        shippingMethods = shipmentShippingModel.getApplicableShippingMethods();
    }

    // check products for if all is virtual gift card product
    var allVirtualGiftCard = isAllProductsVirtualGiftCard(shipment);

    // Filter out whatever the method associated with in store pickup and virtual gift card
    var filteredMethods = [];
    collections.forEach(shippingMethods, function (shippingMethod) {
        if (!shippingMethod.custom.storePickupEnabled
            && (!allVirtualGiftCard || shippingMethod.custom.virtualGiftCardEnabled)) {
            filteredMethods.push(new ShippingMethodModel(shippingMethod, shipment));
        }
    });

    return filteredMethods;
}

/**
 * Returns the first shipping method (and maybe prevent in store pickup)
 * @param {dw.util.Collection} methods - Applicable methods from ShippingShipmentModel
 * @param {boolean} filterPickupInStore - whether to exclude PUIS method
 * @returns {dw.order.ShippingMethod} - the first shipping method (maybe non-PUIS)
 */
function getFirstApplicableShippingMethod(methods, filterPickupInStore) {
    var method;
    var iterator = methods.iterator();
    while (iterator.hasNext()) {
        method = iterator.next();
        if (!filterPickupInStore || (filterPickupInStore && !method.custom.storePickupEnabled)) {
            break;
        }
    }

    return method;
}

/**
 * Sets the default ShippingMethod for a Shipment, if absent
 * @param {dw.order.Shipment} shipment - the target Shipment object
 */
function ensureShipmentHasMethod(shipment) {
    var shippingMethod = shipment.shippingMethod;


    // check shipping method for virtual gift card products
    // shipping method should be virtual gift card shipping method
    // if not check the shipping method is not virtual gift card shipping method
    // if all products are virtual gift card product in the shipment
    var allVirtualGiftCard = isAllProductsVirtualGiftCard(shipment);
    if (shippingMethod
        && ((allVirtualGiftCard && !shippingMethod.custom.virtualGiftCardEnabled)
            || (!allVirtualGiftCard && shippingMethod.custom.virtualGiftCardEnabled))) {
        shippingMethod = null;
    }

    if (!shippingMethod) {
        var methods = ShippingMgr.getShipmentShippingModel(shipment).applicableShippingMethods;

        // if all products are gift card product then
        // filter methods with only virtualGiftCardEnabled shippingMethods
        if (allVirtualGiftCard) {
            methods = filterShippingMethodsWithVirtualGiftCardEnabled(methods);
        }

        var defaultMethod = ShippingMgr.getDefaultShippingMethod();

        if (!defaultMethod) {
            // If no defaultMethod set, just use the first one
            shippingMethod = getFirstApplicableShippingMethod(methods, true);
        } else {
            // Look for defaultMethod in applicableMethods
            shippingMethod = collections.find(methods, function (method) {
                return method.ID === defaultMethod.ID;
            });
        }

        // If found, use it.  Otherwise return the first one
        if (!shippingMethod && methods && methods.length > 0) {
            shippingMethod = getFirstApplicableShippingMethod(methods, true);
        }

        if (shippingMethod) {
            shipment.setShippingMethod(shippingMethod);
        }
    }
}

base.getApplicableShippingMethods = getApplicableShippingMethods;
base.ensureShipmentHasMethod = ensureShipmentHasMethod;
module.exports = base;
