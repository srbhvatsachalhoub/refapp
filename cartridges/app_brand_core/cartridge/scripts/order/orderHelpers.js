'use strict';

var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Locale = require('dw/util/Locale');

var OrderModel = require('*/cartridge/models/order');

/**
* Returns a list of filter options
* @param {string} displayValue - display value of filter option
* @param {string} qsValue - querystring value of filter option
* @returns {Object} - filter value object
* */
function createOption(displayValue, qsValue) {
    return {
        displayValue: displayValue,
        optionValue: URLUtils.url('Order-Filtered', 'orderFilter', qsValue).abs().toString()
    };
}

/**
* Returns a list of orders for the current customer
* @param {Object} currentCustomer - object with customer properties
* @param {Object} querystring - querystring properties
* @param {string} locale - the current request's locale id
* @returns {Object} - orderModel of the current dw order object
* */
function getOrders(currentCustomer, querystring, locale) {
    // get all orders for this user
    var customerNo = currentCustomer.profile.customerNo;
    var customerOrders = OrderMgr.searchOrders(
        'customerNo={0} AND status!={1} AND status!={2}',
        'creationDate desc',
        customerNo,
        Order.ORDER_STATUS_REPLACED,
        Order.ORDER_STATUS_FAILED
    );

    var orders = [];
    var years = [];
    var now = Date.now();
    var day = 1000 * 60 * 60 * 24;
    var year = day * 365;
    var halfYear = year / 2;
    var currentLocale = Locale.getLocale(locale);
    var config = {
        numberOfLineItems: 'single'
    };

    var filterDefinitions = {
        6: {
            displayValue: Resource.msg('orderhistory.sixmonths.option', 'order', null),
            condition: function (qsValue, orderTime) {
                return qsValue === '6' && (now - halfYear) < orderTime;
            }
        },
        12: {
            displayValue: Resource.msg('orderhistory.twelvemonths.option', 'order', null),
            condition: function (qsValue, orderTime) {
                return qsValue === '12' && (now - year) < orderTime;
            }
        }
    };

    var filterValues = Object.keys(filterDefinitions).map(function (key) {
        return createOption(filterDefinitions[key].displayValue, key);
    });

    while (customerOrders.hasNext()) {
        var customerOrder = customerOrders.next();
        var orderYear = customerOrder.getCreationDate().getFullYear().toString();
        var orderTime = customerOrder.getCreationDate().getTime();
        var qsValue = querystring.orderFilter;

        if (years.indexOf(orderYear) === -1) {
            filterValues.push(createOption(orderYear, orderYear));
            years.push(orderYear);
        }

        if ((filterDefinitions[qsValue] && filterDefinitions[qsValue].condition(qsValue, orderTime)) ||
            (qsValue && qsValue === orderYear) || (halfYear < orderTime)) {
            orders.push(new OrderModel(
                customerOrder,
                { config: config, countryCode: currentLocale.country }
            ));
        }
    }

    return {
        orders: orders,
        filterValues: filterValues
    };
}

module.exports = {
    getOrders: getOrders
};
