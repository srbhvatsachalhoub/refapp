'use strict';

var ProductMgr = require('dw/catalog/ProductMgr');
var recommendationModel = require('*/cartridge/models/product/recommendation');
var ProductFactory = require('*/cartridge/scripts/factories/product');

var modules = {
    'ratings-reviews': function (productId) {
        return {
            template: 'product/components/ratingsReviews/ratingsReviews',
            product: ProductFactory.get({ pid: productId }) || Object.create(null),
            rrTemplateHelper: require('*/cartridge/scripts/helpers/rrTemplateHelper')
        };
    },
    'recommendations-alternative': function (productId) {
        var model = Object.create(null);
        var apiProduct = ProductMgr.getProduct(productId);
        return {
            template: 'product/components/recommendationsAlternative',
            product: apiProduct ? (recommendationModel(model, apiProduct) || model) : model
        };
    },
    'recommendations-complementary': function (productId) {
        var model = Object.create(null);
        var apiProduct = ProductMgr.getProduct(productId);
        return {
            template: 'product/components/recommendationsComplementary',
            product: apiProduct ? (recommendationModel(model, apiProduct) || model) : model
        };
    }
};

module.exports = {
    modules: modules,
    get: function (moduleId, productId) {
        return this.modules[moduleId] ? this.modules[moduleId](productId) : null;
    }
};
