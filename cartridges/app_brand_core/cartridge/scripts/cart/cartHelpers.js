'use strict';

var originalExports = Object.keys(module.superModule);
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

var ProductMgr = require('dw/catalog/ProductMgr');
var Resource = require('dw/web/Resource');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');
/**
 * Removes GiftWrappingProduct from basket if this product is also giftwrapping product
 * @param {dw.catalog.Product} product apiProduct
 * @param {dw.basket.Basket} currentBasket currentBasket
 */
function removeGiftWrappingProduct(product, currentBasket) {
    if (product && product.custom.isGiftWrappingProduct) {
        var pliItr = currentBasket.getAllProductLineItems().iterator();
        while (pliItr && pliItr.hasNext()) {
            var pli = pliItr.next();
            if (pli.product && pli.product.custom.isGiftWrappingProduct) {
                var Transaction = require('dw/system/Transaction');
                Transaction.wrap(function () { // eslint-disable-line
                    currentBasket.removeProductLineItem(pli);
                    var defaultShipment = currentBasket.getDefaultShipment();
                    if (defaultShipment && defaultShipment.custom.giftWrapDescription) {
                        defaultShipment.custom.giftWrapDescription = null;
                    }
                });
            }
        }
    }
}
/**
 * Adds giftWrapping Custom Text to default Shipment for Order Exports.
 * @param {dw.catalog.Product} product apiProduct
 * @param {dw.basket.Basket} currentBasket currentBasket
 */
function postAddGiftWrappingProduct(product, currentBasket) {
    if (product && product.custom.isGiftWrappingProduct && product.custom.giftWrappingText) {
        var Transaction = require('dw/system/Transaction');
        Transaction.wrap(function () { // eslint-disable-line
            var defaultShipment = currentBasket.getDefaultShipment();
            if (defaultShipment) {
                defaultShipment.custom.giftWrapDescription = product.custom.giftWrappingText;
            }
        });
    }
}

/**
 * Checks if the product being added to cart is sellable (isGiftWrappingProduct)
 * @param {string} productId id of the product to be checked
 * @returns {boolean} true if item is sellable
 */
function checkSellable(productId) {
    var product = ProductMgr.getProduct(productId);

    return product && (product.custom.sellable !== false || product.custom.isGiftWrappingProduct === true);
}

/**
 * Checks if the quantity for the product is under max orderable quantity
 * @param {string} productId id of the product to be checked
 * @param {string} quantity quantity of the product to be added to cart
 * @param {boolean} adding flag to specify if the operation is adding or updating the product
 * @returns {boolean} true if quantity is appropriate
 */
function checkMaxOrderQuantity(productId, quantity, adding) {
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();

    var product = ProductMgr.getProduct(productId);

    if (product && product.custom.maxOrderQuantity && product.custom.maxOrderQuantity > 0) {
        if (adding) {
            var plis = currentBasket.getProductLineItems(productId);
            var currentQuantity = plis && plis.length > 0 ? plis[0].quantity : 0;
            return product.custom.maxOrderQuantity >= quantity + currentQuantity;
        }
        return product.custom.maxOrderQuantity >= quantity;
    }

    return true;
}

/**
 * Check if the bundled product can be added to the cart
 * @param {string[]} childProducts - the products' sub-products
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - Collection of the Cart's
 *     product line items
 * @param {number} quantity - the number of products to the cart
 * @return {boolean} - return true if the bundled product can be added
 */
function checkBundledProductCanBeAdded(childProducts, productLineItems, quantity) {
    var atsValueByChildPid = {};
    var totalQtyRequested = 0;
    var canBeAdded = false;

    childProducts.forEach(function (childProduct) {
        var apiChildProduct = ProductMgr.getProduct(childProduct.pid);
        var availabilityModel = inventoryHelpers.getProductAvailabilityModel(apiChildProduct);
        atsValueByChildPid[childProduct.pid] = availabilityModel.inventoryRecord.ATS.value;
    });

    canBeAdded = childProducts.every(function (childProduct) {
        var bundleQuantity = quantity;
        var itemQuantity = bundleQuantity * childProduct.quantity;
        var childPid = childProduct.pid;
        totalQtyRequested = itemQuantity + base.getQtyAlreadyInCart(childPid, productLineItems);
        return totalQtyRequested <= atsValueByChildPid[childPid];
    });

    return canBeAdded;
}

/**
 * Adds a product to the cart. If the product is already in the cart it increases the quantity of
 * that product.
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {string} productId - the productId of the product being added to the cart
 * @param {number} quantity - the number of products to the cart
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 *  @return {Object} returns an error object
 */
function addProductToCart(currentBasket, productId, quantity, childProducts, options) {
    var availableToSell;
    var defaultShipment = currentBasket.defaultShipment;
    var perpetual;
    var product = ProductMgr.getProduct(productId);
    var productInCart;
    var productLineItems = currentBasket.productLineItems;
    var productQuantityInCart;
    var quantityToSet;
    var availabilityModel;
    var optionModel = productHelper.getCurrentOptionModel(product.optionModel, options);
    var result = {
        error: false,
        message: Resource.msg('text.alert.addedtobasket', 'product', null)
    };

    var totalQtyRequested = 0;
    var canBeAdded = false;
    removeGiftWrappingProduct(product, currentBasket);
    if (product.bundle) {
        canBeAdded = checkBundledProductCanBeAdded(childProducts, productLineItems, quantity);
    } else {
        totalQtyRequested = quantity + base.getQtyAlreadyInCart(productId, productLineItems);
        availabilityModel = inventoryHelpers.getProductAvailabilityModel(product);
        perpetual = availabilityModel.inventoryRecord.perpetual;
        canBeAdded =
            (perpetual
            || totalQtyRequested <= availabilityModel.inventoryRecord.ATS.value);
    }

    if (!canBeAdded) {
        result.error = true;
        result.message = Resource.msgf(
            'error.alert.selected.quantity.cannot.be.added.for',
            'product',
            null,
            product.availabilityModel.inventoryRecord.ATS.value,
            product.name
        );
        return result;
    }

    productInCart = base.getExistingProductLineItemInCart(
        product, productId, productLineItems, childProducts, options);

    if (productInCart) {
        productQuantityInCart = productInCart.quantity.value;
        quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
        availabilityModel = inventoryHelpers.getProductAvailabilityModel(productInCart.product);
        availableToSell = availabilityModel.inventoryRecord.ATS.value;

        if (availableToSell >= quantityToSet || perpetual) {
            productInCart.setQuantityValue(quantityToSet);
            result.uuid = productInCart.UUID;

            // set the inventory list to the pli
            inventoryHelpers.setProductInventoryList(productInCart);
        } else {
            result.error = true;
            result.message = availableToSell === productQuantityInCart
                ? Resource.msg('error.alert.max.quantity.in.cart', 'product', null)
                : Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
        }
    } else {
        var productLineItem;
        productLineItem = base.addLineItem(
            currentBasket,
            product,
            quantity,
            childProducts,
            optionModel,
            defaultShipment
        );

        // set the inventory list to the pli
        inventoryHelpers.setProductInventoryList(productLineItem);

        result.uuid = productLineItem.UUID;
    }

    if (result.uuid) {
        postAddGiftWrappingProduct(product, currentBasket);
    }

    return result;
}

/**
 * Price Adjustments
 * @param {dw.order.PriceAdjustment} priceAdjustments priceAdjustments
 * @returns {boolean} status of exclusion
 */
function checkIfExcludedThroughAdjustments(priceAdjustments) {
    if (!priceAdjustments) {
        return false;
    }
    var adjItr = priceAdjustments.iterator();
    while (adjItr.hasNext()) {
        var adjustment = adjItr.next();
        if (adjustment && adjustment.promotion && 'isServiceFeeExcluded' in adjustment.promotion.custom && adjustment.promotion.custom.isServiceFeeExcluded) {
            return true;
        }
    }
    return false;
}

/**
 * Checks isServiceFeeExcluded attribute from applied promotions and returns boolean to indicate if COD fee needs to be applied or not.
 * @param {dw.order.Basket} basket The basket to be calculated
 * @returns {boolean} status of exclusion
 */
function checkIfServiceFeeExcluded(basket) {
    var isServiceFeeExcluded = false;
    // check order level
    isServiceFeeExcluded = checkIfExcludedThroughAdjustments(basket.getPriceAdjustments());
    if (isServiceFeeExcluded) {
        return isServiceFeeExcluded;
    }
    // check shipping, shipment level
    isServiceFeeExcluded = checkIfExcludedThroughAdjustments(basket.getAllShippingPriceAdjustments());
    if (isServiceFeeExcluded) {
        return isServiceFeeExcluded;
    }
    var plis = basket.getAllProductLineItems();
    // check product line item level adjustments
    var plisItr = plis.iterator();
    while (plisItr.hasNext() && !isServiceFeeExcluded) {
        var pli = plisItr.next();
        var pliAdjustments = pli.getPriceAdjustments();
        isServiceFeeExcluded = checkIfExcludedThroughAdjustments(pliAdjustments);
    }
    return isServiceFeeExcluded;
}

base.checkSellable = checkSellable;
base.checkMaxOrderQuantity = checkMaxOrderQuantity;
base.addProductToCart = addProductToCart;
base.checkBundledProductCanBeAdded = checkBundledProductCanBeAdded;
base.checkIfServiceFeeExcluded = checkIfServiceFeeExcluded;
base.removeGiftWrappingProduct = removeGiftWrappingProduct;
base.postAddGiftWrappingProduct = postAddGiftWrappingProduct;

module.exports = base;
