'use strict';

/* globals request:false, response:false, customer:false, session:false */

var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var logger = require('dw/system/Logger').getLogger('payment.feedback');

/**
 * Removes the given feedback custom object
 * Also logs the given log message
 * @param {dw.object.CustomObject} feedback - the feedback custom object
 * @param {string} logMessage - the log message to be logged
 * @param {boolean} isError - the log type, if true will be logged as error, info otherwise
 */
function removeFeedback(feedback, logMessage, isError) {
    if (logMessage) {
        if (isError) {
            logger.error('{0}, key: {1}, orderNo: {2}, status: {3}, payload: {4}',
                logMessage,
                feedback.custom.key,
                feedback.custom.orderNo,
                feedback.custom.status,
                feedback.custom.payload);
        } else {
            logger.info('{0}, key: {1}, orderNo: {2}, status: {3}, payload: {4}',
                logMessage,
                feedback.custom.key,
                feedback.custom.orderNo,
                feedback.custom.status,
                feedback.custom.payload);
        }
    }

    // remove custom object
    Transaction.wrap(function () { // eslint-disable-line no-loop-func
        CustomObjectMgr.remove(feedback);
    });
}

/**
 * Iterates all PSP feedback custom objects to check/set the order payment statuses
 *
 * @param {dw.util.HashMap} args - the arguments
 * @param {dw.job.JobStepExecution} jobStepExecution - the job step execution object
 * @returns {dw.system.Status} status object
 */
function execute(args, jobStepExecution) { // eslint-disable-line no-unused-vars
    var Calendar = require('dw/util/Calendar');

    var OrderMgr = require('dw/order/OrderMgr');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var HookMgr = require('dw/system/HookMgr');
    var StringUtils = require('dw/util/StringUtils');
    var Status = require('dw/system/Status');
    var Request = require('modules/server/request');
    var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

    // create SFCC request object
    var req = new Request(
        typeof request !== 'undefined' ? request : {},
        typeof customer !== 'undefined' ? customer : {},
        typeof session !== 'undefined' ? session : {});

    var hasError = false;
    var feedbackAged = args.FeedbackAgedInMinutes || 5;

    var calendar = new Calendar();
    calendar.add(Calendar.MINUTE, -1 * feedbackAged);

    // get PSPFeedback customobjects which are older than feedbackAged minutes
    var feedbacksIterator = CustomObjectMgr.queryCustomObjects(
        'PSPFeedback',
        'creationDate < {0}',
        'creationDate desc',
        calendar.time
    );

    var orders = [];
    while (feedbacksIterator.hasNext()) {
        var feedback = feedbacksIterator.next();
        var orderNo = feedback.custom.orderNo;
        if (!orderNo) {
            hasError = true;
            removeFeedback(feedback, 'PSP Feedback Order No is empty', true);

            // continue to process next custom object
            continue;
        }

        // last order feedback is already processed
        // skip the previous ones
        if (orders.indexOf(orderNo) > -1) {
            removeFeedback(feedback, 'Last PSP Feedback already processed', false);

            // continue to process next custom object
            continue;
        }

        var order = OrderMgr.getOrder(orderNo);
        if (!order) {
            hasError = true;
            removeFeedback(feedback, 'PSP Feedback Order could not be found', true);

            // continue to process next custom object
            continue;
        }

        var paymentInstruments = order.getPaymentInstruments();
        if (paymentInstruments.empty) {
            hasError = true;
            removeFeedback(feedback, 'PSP Feedback Order has no payment instrument', true);

            // continue to process next custom object
            continue;
        }

        var paymentInstrumentsIterator = paymentInstruments.iterator();

        while (paymentInstrumentsIterator.hasNext()) {
            var paymentInstrument = paymentInstrumentsIterator.next();
            if (paymentMethodHelpers.isComplementaryPaymentMethod(paymentInstrument.paymentMethod)) {
                continue;
            }

            var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).paymentProcessor;
            if (!paymentProcessor) {
                hasError = true;
                removeFeedback(feedback, 'PSP Feedback Order paymentInstrument has no payment payment processor', true);

                // continue to process next custom object
                continue;
            }

            var feedbackResult;
            var hookId = StringUtils.format('app.payment.processor.{0}', paymentProcessor.ID.toLowerCase());
            if (HookMgr.hasHook(hookId)) {
                // set order locale to request
                request.setLocale(order.customerLocaleID);

                feedbackResult = HookMgr.callHook(
                    hookId,
                    'Feedback',
                    req,
                    order,
                    paymentInstrument,
                    paymentProcessor,
                    feedback
                );

                if (feedbackResult.error) {
                    hasError = true;
                    removeFeedback(feedback, feedbackResult.message || 'Error on processing PSP Feedback', true);

                    // continue to process next custom object
                    continue;
                }

                removeFeedback(feedback, 'PSP Feedback has been processed', false);
                orders.push(orderNo);
            } else {
                hasError = true;
                removeFeedback(feedback,
                    StringUtils.format('PSP Feedback can not be processed, processor: {0} does not have hook: {1}', paymentProcessor.ID, hookId),
                    true);
            }
        }
    }

    if (hasError) {
        return new Status(Status.ERROR, '500', 'Error on PSP feedback job, please check log files.');
    }

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
