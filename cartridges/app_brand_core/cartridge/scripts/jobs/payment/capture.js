'use strict';
var logger = require('dw/system/Logger').getLogger('payment.capture');

/**
 * Iterates all CONFIRMATION_STATUS_CONFIRMED AND SHIPPING_STATUS_SHIPPED AND PAYMENT_STATUS_NOTPAID orders
 * to make them as PAID with capture calls to PSP
 * @returns {dw.system.Status} status object
 */
function execute() {
    var OrderMgr = require('dw/order/OrderMgr');
    var Order = require('dw/order/Order');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var HookMgr = require('dw/system/HookMgr');
    var StringUtils = require('dw/util/StringUtils');
    var Status = require('dw/system/Status');

    var ordersIterator = OrderMgr.queryOrders(
        'confirmationStatus = {0} AND shippingStatus = {1} AND paymentStatus = {2}',
        'orderNo asc',
        Order.CONFIRMATION_STATUS_CONFIRMED,
        Order.SHIPPING_STATUS_SHIPPED,
        Order.PAYMENT_STATUS_NOTPAID
    );

    while (ordersIterator.hasNext()) {
        var order = ordersIterator.next();
        var paymentInstruments = order.getPaymentInstruments();
        if (paymentInstruments.empty) {
            logger.info('Order: {0} has no payment instrument', order.orderNo);
            continue;
        }

        var paymentInstrumentsIterator = paymentInstruments.iterator();

        while (paymentInstrumentsIterator.hasNext()) {
            var paymentInstrument = paymentInstrumentsIterator.next();
            if ([PaymentInstrument.METHOD_CREDIT_CARD, PaymentInstrument.METHOD_DW_APPLE_PAY].indexOf(paymentInstrument.paymentMethod) === -1) {
                continue;
            }

            var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).paymentProcessor;
            if (!paymentProcessor) {
                logger.error('Order: {0} paymentInstrument has no payment payment processor', order.orderNo);
                continue;
            }
            var captureResult;
            var hookId = StringUtils.format('app.payment.processor.{0}', paymentProcessor.ID.toLowerCase());
            if (HookMgr.hasHook(hookId)) {
                captureResult = HookMgr.callHook(
                    hookId,
                    'Capture',
                    order,
                    paymentInstrument,
                    paymentProcessor
                );

                if (captureResult.error) {
                    logger.error(captureResult.message);
                }
            } else {
                logger.error('Order: {0} cant be captured, processor: {1} doesnt have hook: {2}', order.orderNo, paymentProcessor.ID, hookId);
            }
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
