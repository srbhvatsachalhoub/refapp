var Status = require('dw/system/Status');
var Resource = require('dw/web/Resource');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var Site = require('dw/system/Site');
var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var Mail = require('dw/net/Mail');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

var logger = require('dw/system/Logger').getLogger('LowStock');

/**
 * extracts low stock product list
 * @param {ProductSearchHit} products product search hits
 * @param {int} stockThreshold threshold for products to be considered low stock
 * @param {dw.catalog.ProductInventoryList} productInventoryList - the current inventory list
 * @returns {product[]} list of low stock products
 */
function collectLowStockProducts(products, stockThreshold, productInventoryList) {
    if (!stockThreshold) {
        return [];
    }

    var lowStockProducts = [];
    var distinctProducts = [];

    while (products.hasNext()) {
        var productHit = products.next();
        var representedProducts = productHit.representedProducts.iterator();
        while (representedProducts.hasNext()) {
            var product = representedProducts.next();
            if (distinctProducts.indexOf(product.ID) !== -1) {
                continue;
            }
            distinctProducts.push(product.ID);
            var availabilityModel = product.getAvailabilityModel(productInventoryList);
            if (availabilityModel.inventoryRecord &&
                availabilityModel.inventoryRecord.ATS.value > stockThreshold) {
                continue;
            }

            lowStockProducts.push({
                id: product.ID,
                name: product.name,
                stockValue: availabilityModel.inventoryRecord.ATS.value,
                inventoryListId: productInventoryList.ID
            });
        }
    }
    return lowStockProducts;
}

/**
 * Collects low stock products in a table and sends it as a mail to given addresses
 * @param {Object[]} lowStockProducts list of low stock products
 * @param {string} emailAddresses comma seperated email addresses
 * @param {int} threshold the low stock threshold value
 * @param {dw.catalog.ProductInventoryList} productInventoryList - the current inventory list
 * @returns {boolean} true if successful
 */
function sendAsMail(lowStockProducts, emailAddresses, threshold, productInventoryList) {
    if (lowStockProducts.length === 0 || emailAddresses.length === 0) {
        return true;
    }

    try {
        var email = new Mail();
        emailAddresses.split(',').forEach(function (emailAddress) {
            email.addTo(emailAddress);
        });
        email.setSubject(Resource.msgf('subject', 'lowstock', null, Site.current.name, productInventoryList.ID));
        email.setFrom(Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com');
        email.setContent(
            renderTemplateHelper.getRenderedHtml(
                { products: lowStockProducts, threshold: threshold, productInventoryList: productInventoryList },
                'lowstock/notification'
            ),
            'text/html',
            'UTF-8'
        );
        email.send();
        return true;
    } catch (e) {
        logger.error('While firing low stock email, an unexpected error occured! {0}', e.message);
        return false;
    }
}

/**
 * @param {dw.util.HashMap} args inputs from the BM
 * Entry function for the job: traverses every product and sends low stock mails for satisfying products
 * @returns {dw.system.Status} status of the job step
 */
function notifyLowStocks(args) {
    var emailAddresses = args.LowStockContacts;
    var stockThreshold = args.StockThreshold;

    // get all inventories that the current site uses
    // add default inventory
    var inventoryLists = [ProductInventoryMgr.getInventoryList().ID];

    // add if any additional is defined in settings
    var inventories = Site.current.getCustomPreferenceValue('inventories');
    if (inventories) {
        var inventoriesObject = JSON.parse(inventories);
        Object.keys(inventoriesObject).forEach(function (key) {
            if (inventoryLists.indexOf(inventoriesObject[key]) === -1) {
                inventoryLists.push(inventoriesObject[key]);
            }
        });
    }

    var psm;
    var products;
    var lowStockProducts;
    var success = true;

    inventoryLists.forEach(function (listId) {
        var productInventoryList = ProductInventoryMgr.getInventoryList(listId);
        if (productInventoryList) {
            psm = new ProductSearchModel();
            psm.setCategoryID('root');
            psm.orderableProductsOnly = true;
            psm.recursiveCategorySearch = true;
            psm.search();
            products = psm.getProductSearchHits();

            lowStockProducts = collectLowStockProducts(products, stockThreshold, productInventoryList);
            success = success && sendAsMail(lowStockProducts, emailAddresses, stockThreshold, productInventoryList);
        }
    });

    return new Status(success ? Status.OK : Status.ERROR);
}

module.exports = {
    notifyLowStocks: notifyLowStocks
};
