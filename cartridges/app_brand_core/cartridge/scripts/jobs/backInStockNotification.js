'use strict';
/* global request */

var logger = require('dw/system/Logger').getLogger('BackInStock');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');

/**
 * Prepares back in stock e-mail and sends it.
 * @param {dw.object.CustomObject} customObject BackInStock Object
 * @param {dw.catalog.Product} product apiProduct
 * @returns {boolean} status of operation.
 */
function triggerEmail(customObject, product) {
    // check the custom object locale to send the email in correct language
    if (request.locale !== customObject.custom.locale) {
        request.setLocale(customObject.custom.locale);
    }
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
    var emailObj = {
        to: customObject.custom.email,
        subject: Resource.msgf('backinstock.mail.subject', 'backinstock', null, product.name),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.backInStock
    };
    var ProductImageDIS = require('*/cartridge/scripts/helpers/productImageDIS');

    var backInStock = {
        email: customObject.custom.email,
        productId: product.ID,
        productName: product.name,
        productImage: ProductImageDIS.getImage(product, 'medium').getURL(),
        productUrl: require('dw/web/URLUtils').https('Product-Show', 'pid', product.ID).toString(),
        Locale: customObject.custom.locale
    };

    try {
        emailHelpers.sendEmail(emailObj, 'backInStock/backInStockEmail', { BackInStock: backInStock });
        return true;
    } catch (e) {
        logger.error('While firing back in stock e-mail for {0}, an unexpected error occured! {1}', customObject.UUID, e.message);
        return false;
    }
}

/**
 * Removes Back In Stock Notification
 * @param {dw.object.CustomObject} customObject custom object
 */
function removeCustomObject(customObject) {
    var Transaction = require('dw/system/Transaction');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    Transaction.wrap(function () {
        CustomObjectMgr.remove(customObject);
    });
}

/**
 * Iterates back in stock objects and fires e-mails when products get back in stock.
 */
function execute() {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var backInStockObjectsItr = CustomObjectMgr.getAllCustomObjects('BackInStock');

    if (backInStockObjectsItr.count < 1) {
        logger.info('There are no back in stock notifications to send!');
        return;
    }

    while (backInStockObjectsItr.hasNext()) {
        var backInStockObject = backInStockObjectsItr.next();

        if (!backInStockObject.custom.productId || !backInStockObject.custom.email) {
            logger.info('Custom object has no necessary information, it is being removed {0}', backInStockObject.UUID);
            removeCustomObject(backInStockObject);
            continue;
        }
        var product = ProductMgr.getProduct(backInStockObject.custom.productId);
        if (!product) {
            logger.warn('Product {0} is not in catalog!', backInStockObject.custom.productId);
            removeCustomObject(backInStockObject);
            continue;
        }

        var availabilityModel = inventoryHelpers.getProductAvailabilityModel(product, backInStockObject.custom.locale, true);
        if (availabilityModel && availabilityModel.inStock) {
            // Product is back in stock.
            try {
                if (triggerEmail(backInStockObject, product)) {
                    removeCustomObject(backInStockObject);
                } else {
                    logger.error('Submitting back in stock notification for {0} was not successful. It will be tried in next iteration', backInStockObject.UUID);
                }
            } catch (e) {
                logger.error('While processing {0} an unexpected error occured! {1}', backInStockObject.UUID, e.message);
                continue;
            }
        }
    }
}

module.exports = {
    execute: execute
};
