'use strict';
/* global request */
/**
 * Iterates all newly created imported legacy customers
 * to send them password reset email
 * @param {dw.util.HashMap} args job parameters
 * @returns {dw.system.Status} status object
 */
function execute(args) {
    var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Status = require('dw/system/Status');
    var query = args.query || 'customerNo like \'L*\' or customerNo like \'FM*\'';
    // query can be made more accurate, ex: not send if one was sent before
    var profilesIterator = CustomerMgr.queryProfiles(
        query,
        null,
        null
    );
    while (profilesIterator.hasNext()) {
        var profile = profilesIterator.next();
        if (!profile) {
            continue;
        }
        accountHelpers.sendPasswordResetEmail(profile.email, profile.customer);
    }

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
