'use strict';
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var Money = require('dw/value/Money');
var originalExports = Object.keys(module.superModule);
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;
/**
 * Returns the string representation of an amount, using specified currencyCode/countryCode
 * @param {number} value - the currency value
 * @param {string} currencyCode - the currency code
 * @return {string} formatted currency string
 */
function formatCurrency(value, currencyCode) {
    var price = new Money(value, currencyCode);
    return renderTemplateHelper.getRenderedHtml({ price: price }, 'product/components/pricing/currencyFormatted');
}
base.formatCurrency = formatCurrency;
module.exports = base;
