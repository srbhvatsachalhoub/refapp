'use strict';

var originalExports = module.superModule ? Object.keys(module.superModule) : [];
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

/**
 * Determines whether some list item meets callback's truthy conditional
 *
 * @param {dw.util.Collection} collection - Collection subclass instance to map over
 * @param {Function} callback - Callback function for each item
 * @return {boolean} - Whether some list item meets callback's truthy conditional
 */
function some(collection, callback) {
    if (typeof callback !== 'function') {
        throw new TypeError(callback + ' is not a function');
    }

    var iterator = collection.iterator();
    while (iterator.hasNext()) {
        var item = iterator.next();

        if (callback(item)) {
            return true;
        }
    }
    return false;
}

/**
 * creates a new array with all elements that pass the test implemented by the provided function.
 *
 * @param {dw.util.Collection} collection - Collection subclass instance to map over
 * @param {Function} callback - Callback function for each item
 * @param {Object} [scope] - Optional execution scope to pass to callback
 * @returns {Array} Array of results of fliter
 */
function filter(collection, callback, scope) {
    if (typeof callback !== 'function') {
        throw new TypeError(callback + ' is not a function');
    }

    var iterator = Object.hasOwnProperty.call(collection, 'iterator')
        ? collection.iterator()
        : collection;
    var index = 0;
    var result = [];

    while (iterator.hasNext()) {
        var item = iterator.next();
        var matched = scope ?
            callback.call(scope, item, index, collection) :
            callback(item, index, collection);
        if (matched) {
            result.push(item);
        }
        index++;
    }
    return result;
}

base.some = some;
base.filter = filter;
module.exports = base;
