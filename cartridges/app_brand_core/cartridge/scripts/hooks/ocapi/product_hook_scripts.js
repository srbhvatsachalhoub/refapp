'use strict';

/* global request */

/**
 * product_hook_scripts.js
 *
 * Handles OCAPI hooks for product calls
 */

var Status = require('dw/system/Status');

/**
 * The modifyGETResponse hook - called after product get
 * @param {dw.catalog.Product} scriptProduct - an instance of dw.catalog.Product
 * @param {Object} doc the document
 * @return {dw.system.Status} returns a status code
 */
exports.modifyGETResponse = function (scriptProduct, doc) { // eslint-disable-line no-unused-vars
    var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');

    hookHelpers.addProductInformation([doc]);

    return new Status(Status.OK);
};
