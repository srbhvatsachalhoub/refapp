'use strict';
/* global request */

/**
 * customer_hook_scripts.js
 *
 * Handles OCAPI hooks for customer calls
 */
var Status = require('dw/system/Status');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');

/**
 * Checks if the email value entered is correct format
 * @param {string} email - email string to check if valid
 * @returns {boolean} Whether email is valid
 */
function validateEmail(email) {
    var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    return regex.test(email);
}

/**
 * Parses stringified data
 * @param {string} data - stringified identification data
 * @returns {Object} parsed object or error object
 */
function parseInformation(data) {
    try {
        return JSON.parse(data);
    } catch (e) {
        return {
            errorMessage: 'Error parsing json, message: ' + e.message
        };
    }
}

/**
 * Handles contactus functionality of the hook
 * @param {string} data - stringified identification data
 * @returns {dw.system.Status} status
 */
function contactUS(data) {
    var formErrors = require('*/cartridge/scripts/formErrors');
    var server = require('server');
    var reCapthaHelpers = require('*/cartridge/scripts/helpers/reCaptchaHelpers');

    var requestCustomer = JSON.parse(data);
    if (requestCustomer.errorMessage) {
        return new Status(Status.ERROR, 101, requestCustomer.errorMessage);
    }

    var contactusform = server.forms.getForm('contactus');
    Transaction.wrap(function () {
        contactusform.firstname.value = requestCustomer.first_name;
        contactusform.lastname.value = requestCustomer.last_name;
        contactusform.email.value = requestCustomer.email;
        contactusform.phone.value = requestCustomer.phone;
        contactusform.title.value = requestCustomer.title;
        contactusform.reason.value = requestCustomer.reason;
        contactusform.ordernumber.value = requestCustomer.order_number;
        contactusform.comment.value = requestCustomer.comment;
        contactusform['g-recaptcha-response'] = requestCustomer.recaptcha_response;
    });
    var verifyReCaptchaResult = reCapthaHelpers.verifyReCaptcha('contactus', contactusform);
    if (contactusform.valid && verifyReCaptchaResult.valid) {
        /**
         * @type {dw.system.HookMgr}
         */
        var HookMgr = require('dw/system/HookMgr');
        var hookID = 'app.case.created';
        // call hook if defined, otherwise send out email
        if (HookMgr.hasHook(hookID)) {
            var result = HookMgr.callHook(
                hookID,
                hookID.slice(hookID.lastIndexOf('.') + 1),
                contactusform
            );
            if (!result || result.status !== 'OK') {
                return new Status(Status.ERROR, 101, Resource.msg('error.message.case.creation.failed', 'forms', null));
            }

            var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
            var emailObj = {
                to: contactusform.email.value,
                subject: contactusform.title.value,
                from: require('dw/system/Site').current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                type: emailHelpers.emailTypes.contactUs
            };
            emailHelpers.sendEmail(emailObj, 'case/contactusEmail', contactusform);
        } else {
            var Mail = require('dw/net/Mail');

            var email = new Mail();
            email.addTo(require('dw/system/Site').current.getCustomPreferenceValue('customerServiceEmail')
            || 'no-reply@salesforce.com');
            email.setSubject(contactusform.reason.value);
            email.setFrom(contactusform.email.value);
            email.setContent(contactusform.comment.value, 'text/html', 'UTF-8');
            email.send();
        }

        return new Status(Status.ERROR, 200, 'Contact us case created and mail sent');
    }
    if (!verifyReCaptchaResult.valid) {
        return new Status(Status.ERROR, 'ReCaptcha validation failed');
    }
    try {
        return new Status(Status.ERROR, 101, JSON.stringify(formErrors.getFormErrors(contactusform)));
    } catch (e) {
        return new Status(Status.ERROR, 101, 'Error parsing form errors, message: {0}!', e.message);
    }
}

/**
 * Handles newsletter functionality of the hook
 * @param {string} data - stringified identification data
 * @returns {dw.system.Status} status
 */
function newsletter(data) {
    var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');

    var requestCustomer = parseInformation(data);
    if (requestCustomer.errorMessage) {
        return new Status(Status.ERROR, 101, requestCustomer.errorMessage);
    }

    var localeID = request.locale;
    var customerInfo = {
        profile: {
            firstName: requestCustomer.first_name,
            lastName: requestCustomer.last_name,
            email: requestCustomer.email,
            phoneMobile: requestCustomer.phone_mobile || '',
            title: requestCustomer.title || '',
            birthday: requestCustomer.birthday || '',
            custom: {
                emailOptin: requestCustomer.c_optinemail,
                smsOptin: requestCustomer.c_optinsms,
                telephoneOptin: requestCustomer.c_optintelephone,
                whatsappOptin: requestCustomer.c_optinwhatsapp
            }
        }
    };

    var currentCustomer = CustomerMgr.getCustomerByLogin(requestCustomer.email);
    if (currentCustomer && currentCustomer.profile) {
        customerInfo = currentCustomer;
        localeID = customerInfo.profile.preferredLocale;
        Transaction.wrap(function () {
            customerInfo.profile.custom.emailOptin = requestCustomer.c_optinemail; // eslint-disable-line no-param-reassign
            customerInfo.profile.custom.smsOptin = requestCustomer.c_optinsms; // eslint-disable-line no-param-reassign
            customerInfo.profile.custom.telephoneOptin = requestCustomer.c_optintelephone; // eslint-disable-line no-param-reassign
            customerInfo.profile.custom.whatsappOptin = requestCustomer.c_optinwhatsapp; // eslint-disable-line no-param-reassign
        });
    }

    var result = ProfileHelper.updateNewsletter(customerInfo.profile, localeID);

    var resultMessage = result.object && result.object.message ? result.object.message : result.errorMessage;
    if (result.isOk()) {
        return new Status(Status.ERROR, 200, resultMessage || 'Customer added to newsletter list');
    }
    return new Status(Status.ERROR, 101, resultMessage || 'Error processing the request');
}

/**
 * @param {string} email customer's email
 * @param {string} pid product id to send backinstock email
 * @return {dw.system.Status} returns a status code
 */
function backInStock(email, pid) {
    if (!email) {
        return new Status(Status.ERROR, 'Please provide value in email query parameter');
    }
    if (!pid) {
        return new Status(Status.ERROR, 'Please provide pid (product id) in pid query parameter');
    }
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');

    var result;
    var validEmail = validateEmail(email);

    if (!validEmail) {
        result = {
            success: false,
            fieldErrors: ['Not a valid email address'],
            serverErrors: []
        };
        return new Status(Status.ERROR, JSON.stringify(result));
    }

    email = email.toLowerCase(); // eslint-disable-line no-param-reassign
    var backInStockObject = CustomObjectMgr.queryCustomObject('BackInStock', 'custom.email = {0} AND custom.productId = {1}', email, pid);

    if (backInStockObject) {
        result = {
            success: true,
            message: Resource.msg('backinstock.save.success', 'backinstock', null)
        };

        return new Status(Status.ERROR, JSON.stringify(result));
    }

    try {
        // create custom object.
        var UUIDUtils = require('dw/util/UUIDUtils');
        Transaction.wrap(function () {
            backInStockObject = CustomObjectMgr.createCustomObject('BackInStock', UUIDUtils.createUUID());
            backInStockObject.custom.email = email;
            backInStockObject.custom.productId = pid;
            backInStockObject.custom.locale = request.locale;
        });
        result = {
            success: true,
            message: Resource.msg('backinstock.save.success', 'backinstock', null)
        };

        return new Status(Status.ERROR, JSON.stringify(result));
    } catch (e) {
        var logger = require('dw/system/Logger').getLogger('BackInStock');
        logger.error('An unpexted error occured : {0}', e.message);
        result = {
            success: false,
            fieldErrors: [],
            serverErrors: [Resource.msg('backinstock.general.error', 'backinstock', null)]
        };

        return new Status(Status.ERROR, JSON.stringify(result));
    }
}

/**
 * The beforePOST hook - Allows resetting a customers password.
 * The validation of the customer information provided in
 * the the password reset document is done after this hook is called.
 * @param {PasswordReset} passwordReset - password reset request
 * @return {dw.system.Status} returns a status code
 */
exports.beforePOST = function (passwordReset) {
    var URLUtils = require('dw/web/URLUtils');
    var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

    var result;
    var params = request.httpParameters;

    // For contactus api this end point needs contactus=true in querystring and stringified payload in identification
    if (params.contactus && params.contactus[0] === 'true') {
        return contactUS(passwordReset.identification);
    }

    // For newsletter api this end point needs newsletter=true in querystring and stringified payload in identification
    if (params.newsletter && params.newsletter[0] === 'true') {
        return newsletter(passwordReset.identification);
    }

    // For backinstock api this end point needs backInStock=true, email and pid in query string
    if (params.backInStock && params.backInStock[0] === 'true') {
        var backInStockEmail = params.email ? params.email[0] : null;
        var pid = params.pid ? params.pid[0] : null;

        return backInStock(backInStockEmail, pid);
    }

    // Regular password reset behavior
    var mobile = params.mobile ? params.mobile[0] : true;
    var email = passwordReset.identification;
    var errorMsg;
    var isValid;
    var resettingCustomer;
    var receivedMsgHeading = Resource.msg('label.resetpasswordreceived', 'login', null);
    var receivedMsgBody = Resource.msg('msg.requestedpasswordreset', 'login', null);
    var buttonText = Resource.msg('button.text.loginform', 'login', null);
    var returnUrl = URLUtils.url('Login-Show').toString();
    if (email) {
        isValid = validateEmail(email);
        if (isValid) {
            resettingCustomer = CustomerMgr.getCustomerByLogin(email);
            if (resettingCustomer) {
                accountHelpers.sendPasswordResetEmail(email, resettingCustomer);
                result = {
                    success: true,
                    receivedMsgHeading: receivedMsgHeading,
                    receivedMsgBody: receivedMsgBody,
                    buttonText: buttonText,
                    mobile: mobile,
                    returnUrl: returnUrl
                };
            } else {
                result = {
                    fields: {
                        loginEmail: Resource.msg('error.customer.notfound', 'login', null)
                    }
                };
            }
        } else {
            errorMsg = Resource.msg('error.message.passwordreset', 'login', null);
            result = {
                fields: {
                    loginEmail: errorMsg
                }
            };
        }
    } else {
        errorMsg = Resource.msg('error.message.required', 'login', null);
        result = {
            fields: {
                loginEmail: errorMsg
            }
        };
    }

    try {
        return new Status(Status.ERROR, JSON.stringify(result));
    } catch (e) {
        return new Status(Status.ERROR, 'Error parsing regular password reset json, message: {0}!', e.message);
    }
};
