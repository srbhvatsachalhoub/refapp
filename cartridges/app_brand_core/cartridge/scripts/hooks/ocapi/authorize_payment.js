'use strict';
/* global request */

/**
 * authorize_credit_card.js
 *
 * Handles OCAPI hooks for payments
 */
var Status = require('dw/system/Status');

/**
 * The authorizeCreditCard hook - Custom payment authorization of a credit card - modify the order as needed.
 * @param {dw.order.Order} order - the order
 * @param {dw.order.OrderPaymentInstrument} paymentDetails - specified payment details
 * @param {string} cvn - the credit card verification number
 * @return {dw.system.Status} returns a status code
 */
exports.authorizeCreditCard = function (order, paymentDetails, cvn) { // eslint-disable-line
    var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');
    var server = require('server');
    var params = request.httpParameters;
    var saveCard = false;

    if (params.savecard) {
        var Transaction = require('dw/system/Transaction');
        saveCard = params.savecard[0] === 'true';

        Transaction.wrap(function () {
            order.custom.saveCreditCard = saveCard; // eslint-disable-line no-param-reassign
        });
    }

    var creditCardForm = server.forms.getForm('billing').creditCardFields;
    creditCardForm.cardNumber.value = paymentDetails.creditCardNumber;
    creditCardForm.saveCard.checked = saveCard;
    creditCardForm.securityCode.value = cvn;

    return hookHelpers.authorizePayment(order);
};

exports.authorize = function (order, paymentDetails) {
    var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');

    if (paymentDetails.custom.applePayPayload) {
        var HookMgr = require('dw/system/HookMgr');
        var Resource = require('dw/web/Resource');
        var payload = JSON.parse(paymentDetails.custom.applePayPayload);
        var authorizationResult = new Status(Status.ERROR, Resource.msg('error.applepay.success', 'applepay', null));

        if (HookMgr.hasHook('dw.extensions.applepay.paymentAuthorized.authorizeOrderPayment')) {
            authorizationResult = HookMgr.callHook(
                'dw.extensions.applepay.paymentAuthorized.authorizeOrderPayment',
                'authorizeOrderPayment',
                order,
                payload
            );
        }

        return authorizationResult;
    }

    return hookHelpers.authorizePayment(order);
};
