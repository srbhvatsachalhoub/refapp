'use strict';

/* global request */

var Status = require('dw/system/Status');

/**
 * The modifyGETResponse hook - called after the request has been processed
 * @param {Object} doc the document
 * @return {dw.system.Status} returns a status code
 */
exports.modifyGETResponse = function (doc) {
    var queryString = request.httpParameters.q && request.httpParameters.q.length > 0 ?
        request.httpParameters.q[0] : '';
    if (queryString === 'cities') {
        var cities = require('*/cartridge/config/cities');

        try {
            doc.query = JSON.stringify(cities); // eslint-disable-line no-param-reassign
        } catch (e) {
            return new Status(Status.ERROR, 'Error parsing cities.json');
        }
    }
    return new Status(Status.OK);
};
