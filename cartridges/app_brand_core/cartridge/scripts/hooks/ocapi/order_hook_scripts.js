'use strict';
/* global request */
var Status = require('dw/system/Status');

exports.modifyGETResponse = function (order, orderResponse) {
    var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');
    hookHelpers.addOrderInformation(order, orderResponse);
    if (request.session && request.session.isUserAuthenticated()) {
        return new Status(Status.OK);
    }
    hookHelpers.addProductInformation(orderResponse.productItems);
    if (orderResponse.bonusDiscountLineItems) {
        var bonusDiscountLineItems = [];
        for (var i = 0; i < orderResponse.bonusDiscountLineItems.length; i++) {
            var bonusLineSet = orderResponse.bonusDiscountLineItems[i];
            var copiedBonusLineItem = { promotionId: bonusLineSet.promotionId, ID: bonusLineSet.ID, maxBonusItems: bonusLineSet.maxBonusItems, bonusProducts: [] };
            for (var j = 0; j < bonusLineSet.bonusProducts.length; j++) {
                var bonusModifiedItems = {};
                for (var prop in bonusLineSet.bonusProducts[j]) { // eslint-disable-line
                    bonusModifiedItems[prop] = bonusLineSet.bonusProducts[j][prop];
                }
                hookHelpers.adjustProductAttributes(bonusModifiedItems.productId, bonusModifiedItems, 'tile');
                copiedBonusLineItem.bonusProducts.push(bonusModifiedItems);
            }
            bonusDiscountLineItems.push(copiedBonusLineItem);
        }
        orderResponse.c_bonusDiscountLineItems = bonusDiscountLineItems; // eslint-disable-line no-param-reassign
    }
    return new Status(Status.OK);
};

exports.modifyPOSTResponse = function (order, orderResponse) {
    if (request.session && request.session.isUserAuthenticated()) {
        return new Status(Status.OK);
    }
    var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');
    hookHelpers.addProductInformation(orderResponse.productItems);
    if (orderResponse.bonusDiscountLineItems) {
        var bonusDiscountLineItems = [];
        for (var i = 0; i < orderResponse.bonusDiscountLineItems.length; i++) {
            var bonusLineSet = orderResponse.bonusDiscountLineItems[i];
            var copiedBonusLineItem = { promotionId: bonusLineSet.promotionId, ID: bonusLineSet.ID, maxBonusItems: bonusLineSet.maxBonusItems, bonusProducts: [] };
            for (var j = 0; j < bonusLineSet.bonusProducts.length; j++) {
                var bonusModifiedItems = {};
                for (var prop in bonusLineSet.bonusProducts[j]) { // eslint-disable-line
                    bonusModifiedItems[prop] = bonusLineSet.bonusProducts[j][prop];
                }
                hookHelpers.adjustProductAttributes(bonusModifiedItems.productId, bonusModifiedItems, 'tile');
                copiedBonusLineItem.bonusProducts.push(bonusModifiedItems);
            }
            bonusDiscountLineItems.push(copiedBonusLineItem);
        }
        orderResponse.c_bonusDiscountLineItems = bonusDiscountLineItems; // eslint-disable-line no-param-reassign
    }
    return new Status(Status.OK);
};
