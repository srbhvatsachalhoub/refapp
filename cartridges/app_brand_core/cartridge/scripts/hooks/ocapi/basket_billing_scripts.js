'use strict';
var Status = require('dw/system/Status');
exports.afterPUT = function (basket, billingAddress) {
    // enforce billing address assignment.
    var basketBillingAddress = basket.getBillingAddress();
    basketBillingAddress.setTitle(billingAddress.title);
    basketBillingAddress.setFirstName(billingAddress.firstName);
    basketBillingAddress.setLastName(billingAddress.lastName);
    basketBillingAddress.setPhone(billingAddress.phone);
    basketBillingAddress.setStateCode(billingAddress.stateCode);
    basketBillingAddress.setAddress1(billingAddress.address1);
    basketBillingAddress.setAddress2(billingAddress.address2);
    basketBillingAddress.setCity(billingAddress.city);
    basketBillingAddress.setCountryCode(billingAddress.countryCode);
    return new Status(Status.OK);
};
