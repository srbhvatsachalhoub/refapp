'use strict';
/* global request */

/**
 * customer_hook_scripts.js
 *
 * Handles OCAPI hooks for customer calls
 */

var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');

/**
 * Saves opt in information to customer profile.
 * @param {Object} customerProfile customer profile to assign information
 * @param {Object} payload parsed body of request
 */
function saveOptinInformation(customerProfile, payload) {
    Transaction.wrap(function () {
        customerProfile.custom.emailOptin = payload.customer.c_optinemail_b; // eslint-disable-line no-param-reassign
        customerProfile.custom.smsOptin = payload.customer.c_optinsms_b; // eslint-disable-line no-param-reassign
        customerProfile.custom.telephoneOptin = payload.customer.c_optintelephone_b; // eslint-disable-line no-param-reassign
        customerProfile.custom.whatsappOptin = payload.customer.c_optinwhatsapp_b; // eslint-disable-line no-param-reassign
    });
}

/**
 * The beforePOST hook - called before customer register
 * @param {CustomerRegistration} registration - customer registration object
 * @return {dw.system.Status} returns a status code
 */
exports.beforePOST = function (registration) {
    var SystemObjectMgr = require('dw/object/SystemObjectMgr');
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Resource = require('dw/web/Resource');

    var params = request.httpParameters;

    if (params.provider && params.externalId) {
        var requestCustomer;
        var payload;
        var oauthProviderID = params.provider[0];
        requestCustomer = registration.customer;
        var userID = params.externalId[0];

        var authenticatedCustomerProfile = CustomerMgr.getExternallyAuthenticatedCustomerProfile(oauthProviderID, userID);

        if (!authenticatedCustomerProfile) {
            // check if profile with provided email already exists
            var existedCustomers = SystemObjectMgr.querySystemObjects(
                'Profile',
                'email={0}',
                'creationDate ASC',
                requestCustomer.email
            );

            if (existedCustomers.count) {
                return new Status(Status.ERROR, 100, Resource.msg('error.oauth.login.existing', 'login', null));
            }

            // Create new profile
            Transaction.wrap(function () {
                var newCustomer = CustomerMgr.createExternallyAuthenticatedCustomer(oauthProviderID, userID);
                authenticatedCustomerProfile = newCustomer.getProfile();
                authenticatedCustomerProfile.setFirstName(requestCustomer.firstName);
                authenticatedCustomerProfile.setLastName(requestCustomer.lastName);
                authenticatedCustomerProfile.setEmail(requestCustomer.email);
            });

            try {
                payload = JSON.parse(registration.customer);
            } catch (e) {
                return new Status(Status.ERROR, 'Error parsing registration json');
            }
            saveOptinInformation(authenticatedCustomerProfile, payload);
            ProfileHelper.updateNewsletter(authenticatedCustomerProfile, request.locale);
        }

        var credentials = authenticatedCustomerProfile.getCredentials();
        if (credentials.isEnabled()) {
            Transaction.wrap(function () {
                CustomerMgr.loginExternallyAuthenticatedCustomer(oauthProviderID, userID, false);
            });
        } else {
            return new Status(Status.ERROR, 101, Resource.msg('error.oauth.login.enabled', 'login', null));
        }

        return new Status(Status.ERROR, 200);
    }

    return new Status(Status.OK);
};

/**
 * The afterPOST hook - called after a new customer registration
 * @param {dw.customer.Customer} customer - the customer information for customer creation
 * @param {Object} registration - representing the registration information for a customer including password.
 * @return {dw.system.Status} returns a status code
 */
exports.afterPOST = function (customer, registration) {
    var payload;
    try {
        payload = JSON.parse(registration.customer);
    } catch (e) {
        return new Status(Status.ERROR, 'Error parsing registration json');
    }
    // set optin status to profile
    if (payload.customer.c_addtoemaillist_b) {
        saveOptinInformation(customer.profile, payload);
    }
    ProfileHelper.updateNewsletter(customer.profile, request.locale);
    return new Status(Status.OK);
};

/**
 * The beforePATCH hook - called after a customer was updated
 * @param {dw.customer.Customer} customer - the customer to be updated
 * @param {Object} customerInput - the input customer containing the patch changes
 * @return {dw.system.Status} returns a status code
 */
exports.beforePATCH = function (customer, customerInput) { // eslint-disable-line
    var params = request.httpParameters;
    if (params.default && params.paymentInstumentId) {
        if (customer && customer.profile && customer.profile.wallet && !customer.profile.wallet.getPaymentInstruments().empty) {
            var pInstruments = customer.profile.wallet.getPaymentInstruments().iterator();
            while (pInstruments.hasNext()) {
                var pInstrument = pInstruments.next();
                if (pInstrument.UUID === params.paymentInstumentId[0]) {
                    pInstrument.custom.isDefault = true;
                } else {
                    pInstrument.custom.isDefault = false;
                }
            }
        }
    }
    return new Status(Status.OK);
};

/**
 * The afterPATCH hook - called after a customer was updated
 * @param {dw.customer.Customer} customer - the customer to be updated
 * @param {Object} customerInput - the input customer containing the patch changes
 * @return {dw.system.Status} returns a status code
 */
exports.afterPATCH = function (customer, customerInput) {
    if (!customer || !customer.profile) {
        return new Status(Status.OK);
    }
    var payload;
    try {
        payload = JSON.parse(customerInput);
    } catch (e) {
        return new Status(Status.ERROR, 'Error parsing registration json');
    }
    // set optin status to profile
    if (payload.customer.c_addtoemaillist_b) {
        saveOptinInformation(customer.profile, payload);
    }
    ProfileHelper.updateNewsletter(customer.profile, request.locale);
    return new Status(Status.OK);
};

exports.modifyGETResponse_v2 = function (customer, customerOrderResultResponse) {
    if (customerOrderResultResponse && customerOrderResultResponse.count) {
        var orders = customerOrderResultResponse.data;
        var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');
        for (var orderIndex = 0; orderIndex < orders.length; orderIndex++) {
            var orderResponse = orders[orderIndex];
            if (!orderResponse) {
                continue;
            }
            hookHelpers.addProductInformation(orderResponse.productItems);
            if (orderResponse.bonusDiscountLineItems) {
                var bonusDiscountLineItems = [];
                for (var i = 0; i < orderResponse.bonusDiscountLineItems.length; i++) {
                    var bonusLineSet = orderResponse.bonusDiscountLineItems[i];
                    var copiedBonusLineItem = { promotionId: bonusLineSet.promotionId, ID: bonusLineSet.ID, maxBonusItems: bonusLineSet.maxBonusItems, bonusProducts: [] };
                    for (var j = 0; j < bonusLineSet.bonusProducts.length; j++) {
                        var bonusModifiedItems = {};
                        for (var prop in bonusLineSet.bonusProducts[j]) { // eslint-disable-line
                            bonusModifiedItems[prop] = bonusLineSet.bonusProducts[j][prop];
                        }
                        hookHelpers.adjustProductAttributes(bonusModifiedItems.productId, bonusModifiedItems, 'tile');
                        copiedBonusLineItem.bonusProducts.push(bonusModifiedItems);
                    }
                    bonusDiscountLineItems.push(copiedBonusLineItem);
                }
                orderResponse.c_bonusDiscountLineItems = bonusDiscountLineItems; // eslint-disable-line no-param-reassign
            }
        }
    }
    return new Status(Status.OK);
};
