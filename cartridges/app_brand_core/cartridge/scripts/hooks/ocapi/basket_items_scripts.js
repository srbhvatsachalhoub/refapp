'use strict';
var Status = require('dw/system/Status');
var cartHelpers = require('*/cartridge/scripts/cart/cartHelpers');
var ProductMgr = require('dw/catalog/ProductMgr');

exports.beforePOST = function (basket, items) {
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        var apiProduct = ProductMgr.getProduct(item.productId);
        cartHelpers.removeGiftWrappingProduct(apiProduct, basket);
    }
    if (basket.getDefaultShipment() && !basket.getDefaultShipment().getShippingMethod()) {
        var ShippingMgr = require('dw/order/ShippingMgr');
        var defaultShippingMethod = ShippingMgr.defaultShippingMethod;
        if (defaultShippingMethod) {
            basket.getDefaultShipment().setShippingMethod(defaultShippingMethod);
        }
    }
    return new Status(Status.OK);
};

exports.afterPOST = function (basket, items) {
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        var apiProduct = ProductMgr.getProduct(item.productId);
        cartHelpers.postAddGiftWrappingProduct(apiProduct, basket);
    }
    var HookMgr = require('dw/system/HookMgr');
    return HookMgr.callHook('dw.order.calculate', 'calculate', basket);
};
