'use strict';

var base = module.superModule;

var HashMap = require('dw/util/HashMap');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var Status = require('dw/system/Status');
var HookMgr = require('dw/system/HookMgr');
var collections = require('*/cartridge/scripts/util/collections');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');
var cartHelpers = require('*/cartridge/scripts/cart/cartHelpers');

var Transaction = require('dw/system/Transaction');
var Resource = require('dw/web/Resource');
var PaymentMgr = require('dw/order/PaymentMgr');
var logger = require('dw/system/Logger').getLogger('app.cart.calculate');
var constants = require('*/cartridge/scripts/helpers/constants');


/**
 * Appends payment fee cost to basket as promotion through payment instrument & payment method custom attribute
 * @param {dw.order.Basket} basket The basket to be calculated
 */
function adjustServiceFee(basket) {
    try {
        // remove excise taxes if it is added before so we will apply the logic again without duplicate excise taxes in line items
        var existingPaymentServiceFee = basket.getPriceAdjustmentByPromotionID(constants.paymentServiceFeeKey);

        if (existingPaymentServiceFee) {
            Transaction.wrap(function () {
                basket.removePriceAdjustment(existingPaymentServiceFee);
            });
        }
    } catch (e) {
        logger.error('An error occured while removing payment service fee from basket {0}, message {1}', basket.UUID, e.message);
    }
    // Check promo exclusions for COD Fee
    var isServiceFeeExcluded = cartHelpers.checkIfServiceFeeExcluded(basket);
    if (isServiceFeeExcluded) {
        // if it is excluded, do not add COD fee to Basket.
        return;
    }
    var paymentInstruments = basket.getPaymentInstruments();
    if (paymentInstruments.length) {
        var totalServiceFeeCost = 0;
        var speedBusSkuID;
        var speedBusVpn;
        // iterate all payment instruments and calculate sum of payment service fee, in case of multi payment amount could be doubled
        for (var i = 0; i < paymentInstruments.length; i++) {
            var paymentInstrument = paymentInstruments[i];
            var paymentMethodID = paymentInstrument.getPaymentMethod();
            var paymentMethod = PaymentMgr.getPaymentMethod(paymentMethodID);
            if ('serviceFee' in paymentMethod.custom && paymentMethod.custom.serviceFee) {
                totalServiceFeeCost += paymentMethod.custom.serviceFee;
                speedBusSkuID = paymentMethod.custom.speedBusSkuID;
                speedBusVpn = paymentMethod.custom.speedBusVpn;
            }
        }
        if (totalServiceFeeCost > 0) {
            try {
                var Money = require('dw/value/Money');
                var serviceFee = new Money(totalServiceFeeCost, basket.getCurrencyCode());
                var serviceFeeAdjustment = basket.createPriceAdjustment(constants.paymentServiceFeeKey);
                serviceFeeAdjustment.setLineItemText(Resource.msg('payment.service.fee', 'order', 'Payment Service Fee'));
                serviceFeeAdjustment.setPriceValue(serviceFee.value);
                serviceFeeAdjustment.setTaxRate(0);
                serviceFeeAdjustment.updateTax(0);
                serviceFeeAdjustment.custom.speedBusSkuID = speedBusSkuID;
                serviceFeeAdjustment.custom.speedBusVpn = speedBusVpn;
                serviceFeeAdjustment.custom.serviceFee = serviceFee.value;
            } catch (e) {
                logger.error('An unexpected error occured while adding payment service fee to basket {0}, message {1}', basket.UUID, e.message);
            }
        }
    }
}

/**
 * calculateProductPrices
 *
 * Calculates product prices based on line item quantities. Set calculates prices
 * on the product line items.  This updates the basket and returns nothing
 *
 * @param {Object} basket The basket containing the elements to be computed
 */
function calculateProductPrices(basket) {
    // get total quantities for all products contained in the basket
    var productQuantities = basket.getProductQuantities();
    var productQuantitiesIt = productQuantities.keySet().iterator();

    // get product prices for the accumulated product quantities
    var productPrices = new HashMap();

    while (productQuantitiesIt.hasNext()) {
        var prod = productQuantitiesIt.next();
        var quantity = productQuantities.get(prod);
        productPrices.put(prod, prod.priceModel.getPrice(quantity));
    }

    // iterate all product line items of the basket and set prices
    var productLineItems = basket.getAllProductLineItems().iterator();
    while (productLineItems.hasNext()) {
        var productLineItem = productLineItems.next();

        // handle non-catalog products
        if (!productLineItem.catalogProduct) {
            productLineItem.setPriceValue(productLineItem.basePrice.valueOrNull);
            continue;
        }

        var product = productLineItem.product;

        // handle option line items
        if (productLineItem.optionProductLineItem) {
            // for bonus option line items, we do not update the price
            // the price is set to 0.0 by the promotion engine
            if (!productLineItem.bonusProductLineItem) {
                productLineItem.updateOptionPrice();
            }
        // handle bundle line items, but only if they're not a bonus
        } else if (productLineItem.bundledProductLineItem) {
            // no price is set for bundled product line items
        // handle bonus line items
        // the promotion engine set the price of a bonus product to 0.0
        // we update this price here to the actual product price just to
        // provide the total customer savings in the storefront
        // we have to update the product price as well as the bonus adjustment
        } else if (productLineItem.bonusProductLineItem && product !== null) {
            var price = product.priceModel.price;
            var adjustedPrice = productLineItem.adjustedPrice;
            productLineItem.setPriceValue(price.valueOrNull);
            // get the product quantity
            var quantity2 = productLineItem.quantity;
            // we assume that a bonus line item has only one price adjustment
            var adjustments = productLineItem.priceAdjustments;
            if (!adjustments.isEmpty()) {
                var adjustment = adjustments.iterator().next();
                var adjustmentPrice = price.multiply(quantity2.value).multiply(-1.0).add(adjustedPrice);
                adjustment.setPriceValue(adjustmentPrice.valueOrNull);
            }


            // set the product price. Updates the 'basePrice' of the product line item,
            // and either the 'netPrice' or the 'grossPrice' based on the current taxation
            // policy

        // handle product line items unrelated to product
        } else if (product === null) {
            productLineItem.setPriceValue(null);
        // handle normal product line items
        } else {
            productLineItem.setPriceValue(productPrices.get(product).valueOrNull);
        }
    }
}

/**
 * calculateGiftCertificates
 *
 * Function sets either the net or gross price attribute of all gift certificate
 * line items of the basket by using the gift certificate base price. It updates the basket in place.
 *
 * @param {Object} basket The basket containing the gift certificates
 */
function calculateGiftCertificatePrices(basket) {
    var giftCertificates = basket.getGiftCertificateLineItems().iterator();
    while (giftCertificates.hasNext()) {
        var giftCertificate = giftCertificates.next();
        giftCertificate.setPriceValue(giftCertificate.basePrice.valueOrNull);
    }
}

/**
 * Chekcs the bonus products availabilities if the site use multiple inventory list
 * bonus products custom.isSample value must be TRUE if the site uses multiple inventory list
 * bonus products inventory ATS value is 10 minimum, if it is 10 then it means it is not available
 * @param {dw.order.Basket} basket - the current basket
 */
function checkBonusProductAvailabilities(basket) {
    if (!inventoryHelpers.hasSiteMultipleInventory()) {
        return;
    }

    collections.forEach(basket.bonusLineItems, function (bonusLineItem) {
        if (!bonusLineItem.product) {
            return;
        }

        var availabilityModel = inventoryHelpers.getProductAvailabilityModel(bonusLineItem.product);
        if (!availabilityModel) {
            return;
        }

        var inventoryRecord = availabilityModel.inventoryRecord;
        if (!availabilityModel.orderable
            || (inventoryRecord
                && inventoryRecord.ATS
                && inventoryRecord.ATS.value < (bonusLineItem.quantityValue + (bonusLineItem.product.custom.isSample ? 10 : 0)))) {
            basket.removeProductLineItem(bonusLineItem);
            return;
        }

        // set the inventory list to the bonusLineItem
        inventoryHelpers.setProductInventoryList(bonusLineItem);
    });
}

/**
 * calculate is the arching logic for computing the value of a basket.  It makes
 * calls into cart/calculate.js and enables both SG and OCAPI applications to share
 * the same cart calculation logic.
 *
 * @param {dw.order.Basket} basket The basket to be calculated
 * @returns {dw.system.Status} The status object
 */
function calculate(basket) {
    // ===================================================
    // =====    ADJUST SERVICE FEE IF APPLICABLE     =====
    // ===================================================
    adjustServiceFee(basket);

    // ===================================================
    // =====   CALCULATE PRODUCT LINE ITEM PRICES    =====
    // ===================================================

    calculateProductPrices(basket);

    // ===================================================
    // =====    CALCULATE GIFT CERTIFICATE PRICES    =====
    // ===================================================

    calculateGiftCertificatePrices(basket);

    // ===================================================
    // =====   Note: Promotions must be applied      =====
    // =====   after the tax calculation for         =====
    // =====   storefronts based on GROSS prices     =====
    // ===================================================

    // ===================================================
    // =====   APPLY PROMOTION DISCOUNTS			 =====
    // =====   Apply product and order promotions.   =====
    // =====   Must be done before shipping 		 =====
    // =====   calculation. 					     =====
    // ===================================================

    PromotionMgr.applyDiscounts(basket);

    // ===================================================
    // =====        CALCULATE SHIPPING COSTS         =====
    // ===================================================

    // apply product specific shipping costs
    // and calculate total shipping costs
    HookMgr.callHook('dw.order.calculateShipping', 'calculateShipping', basket);

    // ===================================================
    // =====   APPLY PROMOTION DISCOUNTS			 =====
    // =====   Apply product and order and 			 =====
    // =====   shipping promotions.                  =====
    // ===================================================

    PromotionMgr.applyDiscounts(basket);

    // checks bonus products availabilities
    // if site uses multiple inventory list
    // bonus products are being added automatically since they have minimum ATS=10
    // then checks the relevant inventory list
    // if ATS value is bigger then 10 then keep it in the basket otherwise remove from basket
    // Must be called after last PromotionMgr.applyDiscounts
    // Also sets the correct inventorylist to the lineitem
    checkBonusProductAvailabilities(basket);

    // since we might have bonus product line items, we need to
    // reset product prices
    calculateProductPrices(basket);

    // ===================================================
    // =====         CALCULATE TAX                   =====
    // ===================================================

    HookMgr.callHook('dw.order.calculateTax', 'calculateTax', basket);

    // ===================================================
    // =====         CALCULATE BASKET TOTALS         =====
    // ===================================================

    basket.updateTotals();

    // ===================================================
    // =====            DONE                         =====
    // ===================================================

    return new Status(Status.OK);
}

module.exports = {
    calculateTax: base.calculateTax,
    calculateShipping: base.calculateShipping,
    calculate: calculate
};
