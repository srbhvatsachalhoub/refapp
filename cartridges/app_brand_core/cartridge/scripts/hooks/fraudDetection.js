'use strict';

var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

/**
 * fraud detection hook
 * @param {dw.order.Order} order - The order object to be placed
 * @returns {Object} an error object. Status can have three values 'success', 'fail' or 'flag'
 *         error code that could be mapped to localized resources error Message a string with the
 *         error message, that could be used as a fall-back if error code doesn't have a mapping
 */
function fraudDetection(order) { // eslint-disable-line
    var errorCode;
    var errorMessage;
    var status = 'success';

    var isCODOrder = false;
    if (order) {
        var paymentInstruments = order.getPaymentInstruments();
        if (paymentInstruments.length) {
            var collections = require('*/cartridge/scripts/util/collections');
            isCODOrder = !!collections.find(paymentInstruments, function (paymentInstrument) {
                return paymentInstrument.paymentMethod && paymentInstrument.paymentMethod === paymentMethodHelpers.getCODPaymentMethodId();
            });
        }
    }

    if (isCODOrder) {
        status = 'flag';
    }

    return {
        status: status,
        errorCode: errorCode,
        errorMessage: errorMessage
    };
}

exports.fraudDetection = fraudDetection;
