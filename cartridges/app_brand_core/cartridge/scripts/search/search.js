'use strict';

var base = module.superModule;

/**
 * Sets the relevant product search model properties, depending on the parameters provided
 * Extended with setting PriceMin to -1 if pmin is not provided
 * Extended with setting PriceMax to (PriceMax+1) in order to list the products which priced same with pricemax
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - Query params
 * @param {dw.catalog.Category} selectedCategory - Selected category
 * @param {dw.catalog.SortingRule} sortingRule - Product grid sort rule
 * @param {boolean} notIncreasePmax - boolean when true, not increasing pmax.
 */
function setProductProperties(productSearch, httpParams, selectedCategory, sortingRule, notIncreasePmax) {
    base.setProductProperties.call(this, productSearch, httpParams, selectedCategory, sortingRule);
    // check if pmax is provided
    if (!notIncreasePmax && httpParams.pmax) {
        // get priceMax
        var priceMax = productSearch.getPriceMax();

        // if pmax is set
        // then set max to max+1, otherwise products are not found if productprice=max
        if (priceMax) {
            productSearch.setPriceMax(priceMax + 1);
        }
    }

    var globalSearchCriteriaPromotionId = require('dw/system/Site').current.getCustomPreferenceValue('globalSearchCriteriaPromotionId');
    if (globalSearchCriteriaPromotionId) {
        var PromotionMgr = require('dw/campaign/PromotionMgr');

        var globalSearchCriteriaPromotion = PromotionMgr.getPromotion(globalSearchCriteriaPromotionId);
        if (globalSearchCriteriaPromotion && globalSearchCriteriaPromotion.enabled) {
            productSearch.setPromotionID(globalSearchCriteriaPromotionId);
        }
    }
}

module.exports = {
    addRefinementValues: base.addRefinementValues,
    setProductProperties: setProductProperties
};
