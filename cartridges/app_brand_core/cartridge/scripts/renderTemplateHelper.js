'use strict';

var originalExports = module.superModule ? Object.keys(module.superModule) : [];
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

var HashMap = require('dw/util/HashMap');
var Template = require('dw/util/Template');

/**
 * gets the render html for the given isml template
 * @param {Object} templateContext - object that will fill template placeholders
 * @param {string} templateName - the name of the isml template to render.
 * @returns {string} the rendered isml.
 */
function getRenderedHtml(templateContext, templateName) {
    var context = new HashMap();

    Object.keys(templateContext).forEach(function (key) {
        context.put(key, templateContext[key]);
    });

    var template = new Template(templateName);
    var html = template.render(context).text;

    return html ? html.replace(/[\n]/g, '') : html;
}

base.getRenderedHtml = getRenderedHtml;
module.exports = base;
