'use strict';

var HashMap = require('dw/util/HashMap');
var Resource = require('dw/web/Resource');
var Template = require('dw/util/Template');
var constants = require('*/cartridge/scripts/helpers/constants');
var collections = require('*/cartridge/scripts/util/collections');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * Accepts a total object and formats the value
 * @param {dw.value.Money} total - Total price of the cart
 * @returns {string} the formatted money value
 */
function getTotals(total) {
    if (total.available) {
        return renderTemplateHelper.getRenderedHtml({ price: total }, 'product/components/pricing/currencyFormatted');
    }
    return '-';
}

/**
 * Gets the order discount amount by subtracting the basket's total including the discount from
 *      the basket's total excluding the order discount.
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the value and formatted value of the order discount
 */
function getOrderLevelDiscountTotal(lineItemContainer) {
    var totalExcludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(false);
    var totalIncludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(true);

    var serviceFeeAdjustment = lineItemContainer.getPriceAdjustmentByPromotionID(constants.paymentServiceFeeKey);
    if (serviceFeeAdjustment) {
        totalIncludingOrderDiscount = totalIncludingOrderDiscount.subtract(serviceFeeAdjustment.price);
    }

    var orderDiscount = totalExcludingOrderDiscount.subtract(totalIncludingOrderDiscount);

    var orderLevelDiscount = {
        value: orderDiscount.value,
        formatted: getTotals(orderDiscount)
    };

    if (orderDiscount.value > 0) {
        orderLevelDiscount.discountedSubTotal = {
            value: totalIncludingOrderDiscount.value,
            formatted: getTotals(totalIncludingOrderDiscount)
        };
    }

    return orderLevelDiscount;
}
/**
 * Gets the order discount amount by subtracting the basket's total including the discount from
 *      the basket's total excluding the order discount.
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the value and formatted value of the order discount
 */
function getServiceFee(lineItemContainer) {
    var serviceFeeAdjustment = lineItemContainer.getPriceAdjustmentByPromotionID(constants.paymentServiceFeeKey);
    if (serviceFeeAdjustment) {
        return {
            value: serviceFeeAdjustment.price.value,
            formatted: getTotals(serviceFeeAdjustment.price)
        };
    }
    return null;
}
/**
 * Gets avarageRate of Tax.
 * @param {dw.order.LineItemCtnr} lineItemContainer lineItemContainer Basket/Order
 * @returns {int} number
 */
function getTaxRate(lineItemContainer) {
    var allProductLineItems = lineItemContainer.getAllProductLineItems();
    var taxRate = 0;
    collections.forEach(allProductLineItems, function (pli) {
        taxRate += pli.getTaxRate();
    });
    return Math.round((taxRate / allProductLineItems.length) * 100);
}

/**
 * Gets the shipping discount total by subtracting the adjusted shipping total from the
 *      shipping total price
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the value and formatted value of the shipping discount
 */
function getShippingLevelDiscountTotal(lineItemContainer) {
    var totalExcludingShippingDiscount = lineItemContainer.shippingTotalPrice;
    var totalIncludingShippingDiscount = lineItemContainer.adjustedShippingTotalPrice;
    var shippingDiscount = totalExcludingShippingDiscount.subtract(totalIncludingShippingDiscount);

    return {
        value: shippingDiscount.value,
        formatted: getTotals(shippingDiscount),
        shippingTotalValue: totalExcludingShippingDiscount.value,
        isFree: shippingDiscount.value === totalExcludingShippingDiscount.value
    };
}

/**
 * Gets total shipping cost
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the formatted value of the shipping cost
 */
function getTotalShippingCost(lineItemContainer) {
    var totalExcludingShippingDiscount = lineItemContainer.shippingTotalPrice;
    var totalIncludingShippingDiscount = lineItemContainer.adjustedShippingTotalPrice;
    var shippingDiscount = totalExcludingShippingDiscount.subtract(totalIncludingShippingDiscount);

    return shippingDiscount.value === totalExcludingShippingDiscount.value ?
        Resource.msg('label.free', 'pricing', null) : getTotals(totalExcludingShippingDiscount);
}

/**
 * Adds discounts to a discounts object
 * @param {dw.util.Collection} collection - a collection of price adjustments
 * @param {Object} discounts - an object of price adjustments
 * @returns {Object} an object of price adjustments
 */
function createDiscountObject(collection, discounts) {
    var result = discounts;
    collections.forEach(collection, function (item) {
        if (!item.basedOnCoupon) {
            result[item.UUID] = {
                UUID: item.UUID,
                lineItemText: item.lineItemText,
                price: getTotals(item.price),
                type: 'promotion',
                callOutMsg: item.promotion && item.promotion.calloutMsg ? item.promotion.calloutMsg : item.lineItemText
            };
        }
    });

    return result;
}

/**
 * creates an array of discounts.
 * @param {dw.order.LineItemCtnr} lineItemContainer - the current line item container
 * @returns {Array} an array of objects containing promotion and coupon information
 */
function getDiscounts(lineItemContainer) {
    var discounts = {};

    collections.forEach(lineItemContainer.couponLineItems, function (couponLineItem) {
        var priceAdjustments = collections.map(
            couponLineItem.priceAdjustments, function (priceAdjustment) {
                return {
                    callOutMsg: priceAdjustment.promotion && priceAdjustment.promotion.calloutMsg ?
                        priceAdjustment.promotion.calloutMsg : priceAdjustment.lineItemText
                };
            });
        discounts[couponLineItem.UUID] = {
            type: 'coupon',
            UUID: couponLineItem.UUID,
            couponCode: couponLineItem.couponCode,
            applied: couponLineItem.applied,
            valid: couponLineItem.valid,
            relationship: priceAdjustments
        };
    });

    discounts = createDiscountObject(lineItemContainer.priceAdjustments, discounts);
    discounts = createDiscountObject(lineItemContainer.allShippingPriceAdjustments, discounts);

    return Object.keys(discounts).map(function (key) {
        return discounts[key];
    });
}

/**
 * create the discount results html
 * @param {Array} discounts - an array of objects that contains coupon and priceAdjustment
 * information
 * @returns {string} The rendered HTML
 */
function getDiscountsHtml(discounts) {
    var context = new HashMap();
    var object = { totals: { discounts: discounts } };

    Object.keys(object).forEach(function (key) {
        context.put(key, object[key]);
    });

    var template = new Template('cart/cartCouponDisplay');
    return template.render(context).text;
}

/**
 * returns the object that is filled with gift card/certificate values
 * @param {dw.order.Basket} lineItemContainer - current user basket.
 * @returns {Object} the object that keep gitCardCertificate attributes
 */
function getGiftCardTotals(lineItemContainer) {
    var giftCardTotals = {
        show: false,
        total: null,
        due: null,
        paymentInstruments: []
    };

    if (!lineItemContainer) {
        return giftCardTotals;
    }

    var Money = require('dw/value/Money');

    var giftCardTotal = new Money(0, lineItemContainer.currencyCode);
    collections.forEach(lineItemContainer.getGiftCertificatePaymentInstruments(), function (giftCertificatePaymentInstrument) {
        var giftCertificatePaymentInstrumentTransaction = giftCertificatePaymentInstrument.getPaymentTransaction();
        if (giftCertificatePaymentInstrumentTransaction) {
            var giftCertificatePaymentInstrumentAmount = giftCertificatePaymentInstrumentTransaction.getAmount();
            giftCardTotals.paymentInstruments.push({
                maskedCode: giftCertificatePaymentInstrument.getMaskedGiftCertificateCode(),
                amount: getTotals(giftCertificatePaymentInstrumentAmount),
                transactionID: giftCertificatePaymentInstrumentTransaction.getTransactionID()
            });

            giftCardTotal = giftCardTotal.add(giftCertificatePaymentInstrumentAmount);
        }
    });

    if (giftCardTotal.value > 0) {
        // total amount of Gift cards applied
        giftCardTotals.total = {
            value: giftCardTotal.value,
            formatted: getTotals(giftCardTotal)
        };

        var due = lineItemContainer.totalGrossPrice.subtract(giftCardTotal);
        // total due amount to be paid
        giftCardTotals.due = {
            value: due.value,
            formatted: getTotals(due)
        };

        giftCardTotals.show = true;
    }

    return giftCardTotals;
}

/**
 * returns the object that is filled not certificicate amount's value and formatted value
 * @param {dw.ordre.Basket} lineItemContainer - current user basket.
 * @returns {Object} the object that have NonComplimentaryPaymentAmount
 */
function getNonComplimentaryPaymentAmount(lineItemContainer) {
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var nonComplimentaryPaymentAmount = basketCalculationHelpers.getNonComplimentaryPaymentAmount(lineItemContainer);

    return {
        value: nonComplimentaryPaymentAmount.value,
        formatted: getTotals(nonComplimentaryPaymentAmount)
    };
}

/**
 * @constructor
 * @classdesc totals class that represents the order totals of the current line item container
 * extends with giftCardTotals object that have giftcard/certificate related values
 *
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 */
function totals(lineItemContainer) {
    if (lineItemContainer) {
        this.subTotal = getTotals(lineItemContainer.getAdjustedMerchandizeTotalPrice(false));
        this.totalShippingCost = getTotalShippingCost(lineItemContainer);
        if (this.totalShippingCost === '-') {
            this.totalTax = '-';
            this.grandTotal = '-';
        } else {
            this.grandTotal = getTotals(lineItemContainer.totalGrossPrice);
            this.totalTax = getTotals(lineItemContainer.totalTax);
        }
        this.orderLevelDiscountTotal = getOrderLevelDiscountTotal(lineItemContainer);
        this.serviceFee = getServiceFee(lineItemContainer);
        this.taxRate = getTaxRate(lineItemContainer);
        this.shippingLevelDiscountTotal = getShippingLevelDiscountTotal(lineItemContainer);
        this.discounts = getDiscounts(lineItemContainer);
        this.discountsHtml = getDiscountsHtml(this.discounts);
    } else {
        this.subTotal = '-';
        this.grandTotal = '-';
        this.totalTax = '-';
        this.totalShippingCost = '-';
        this.orderLevelDiscountTotal = '-';
        this.shippingLevelDiscountTotal = '-';
        this.priceAdjustments = null;
    }

    // gift card certificate/payment attributes
    this.giftCardTotals = getGiftCardTotals(lineItemContainer);

    // nonComplimentaryPaymentAmount
    this.nonComplimentaryPaymentAmount = getNonComplimentaryPaymentAmount(lineItemContainer);
}

module.exports = totals;
