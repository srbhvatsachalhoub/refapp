'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');

/**
 * Decorate product with recommendation information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function (product, apiProduct) {
    Object.defineProperty(product, 'id', {
        enumerable: true,
        value: apiProduct.ID
    });

    decorators.custom(product, apiProduct);
    decorators.recommendations(product, apiProduct);
    return product;
};
