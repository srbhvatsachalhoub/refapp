
'use strict';

/**
 * Returns category of product
 * @param {dw.catalog.Product} apiProduct api product object
 * @param {string} productType type of the product
 * @returns {dw.catalog.Category} returns category of product
 */
function getCategory(apiProduct, productType) {
    var category = apiProduct.getPrimaryCategory();
    if (!category && (productType === 'variant' || productType === 'variationGroup')) {
        category = apiProduct.getMasterProduct().getPrimaryCategory();
    }
    return category;
}

module.exports = function (object, apiProduct, productType) {
    Object.defineProperty(object, 'category', {
        enumerable: true,
        value: getCategory(apiProduct, productType)
    });
};
