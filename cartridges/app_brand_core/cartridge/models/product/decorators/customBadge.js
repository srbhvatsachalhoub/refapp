'use strict';

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'customBadge', {
        enumerable: true,
        value: {
            image: 'badgeImage' in apiProduct.custom && apiProduct.custom.badgeImage ? apiProduct.custom.badgeImage.getURL().toString() : null,
            text: apiProduct.custom.badgeText,
            bgColor: apiProduct.custom.badgeBgColor,
            textColor: apiProduct.custom.badgeTextColor
        }
    });
};
