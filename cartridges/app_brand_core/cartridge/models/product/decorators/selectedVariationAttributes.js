'use strict';

var VariationAttributesModel = require('*/cartridge/models/product/productAttributes');
var standardVariationAttributes = require('*/cartridge/models/product/decorators/standardVariationAttributes');


module.exports = function (object, apiProduct, variationModel) {
    Object.defineProperty(object, 'selectedVariationAttributes', {
        enumerable: true,
        value: (function () {
            if (variationModel) {
                return (new VariationAttributesModel(
                    variationModel,
                    {
                        attributes: 'selected'
                    },
                    null,
                    object.selectedQuantity)).slice(0);
            }

            // if no variationmodel, check and load for the standard product variation attributes (size for now)
            return standardVariationAttributes(object, apiProduct, variationModel);
        }())
    });
};
