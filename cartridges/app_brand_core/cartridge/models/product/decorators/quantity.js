'use strict';

var preferences = require('*/cartridge/config/preferences');
var DEFAULT_MAX_ORDER_QUANTITY = preferences.maxOrderQty || 10;


/**
 * Gets min order quantity
 * @param {dw.catalog.Product} product - Product information returned by the script API
 * @returns {number} minimum orderable quantity
 */
function getMinOrderQuantity(product) {
    return product && product.minOrderQuantity ? product.minOrderQuantity.value : 1;
}

/**
 * Gets max order quantity
 * @param {Object} object - Product Model to be decorated
 * @param {dw.catalog.Product} product - Product information returned by the script API
 * @returns {number} maximum orderable quantity
 */
function getMaxOrderQuantity(object, product) {
    var availableQuantity = object.availability.availableQuantity;
    var maxQuantity = product && product.custom.maxOrderQuantity ? product.custom.maxOrderQuantity : DEFAULT_MAX_ORDER_QUANTITY;
    return Math.min(availableQuantity, maxQuantity);
}

/**
 * Gets selected quantity
 * @param {Object} object - Product Model to be decorated
 * @param {dw.catalog.Product} product - Product information returned by the script API
 * @param {number} selectedQuantity selected quantity
 * @returns {number} maximum orderable quantity
 */
function getSelectedQuantity(object, product, selectedQuantity) {
    var min = getMinOrderQuantity(product);
    var max = getMaxOrderQuantity(object, product);
    return Math.max(Math.min(parseInt(selectedQuantity, 10) || min, max), min);
}

module.exports = function (object, product, quantity) {
    Object.defineProperty(object, 'selectedQuantity', {
        enumerable: true,
        value: getSelectedQuantity(object, product, quantity)
    });
    Object.defineProperty(object, 'minOrderQuantity', {
        enumerable: true,
        value: getMinOrderQuantity(product)
    });
    Object.defineProperty(object, 'maxOrderQuantity', {
        enumerable: true,
        // added maxOrderQuantity value here
        value: getMaxOrderQuantity(object, product)
    });
    Object.defineProperty(object, 'stepQuantity', {
        enumerable: true,
        value: product.stepQuantity.value || 1
    });
};
