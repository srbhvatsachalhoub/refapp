'use strict';

var SHOW_AS_BADGE = 'showAsBadge';
var currentSite = require('dw/system/Site').current;
var BADGE_LIMIT = currentSite.getCustomPreferenceValue('badgeCount');

/**
 * Gathers badges from products custom badges attribute
 * @param {dw.catalog.Product} apiProduct api product object
 * @param {Object[]} badges badge array to push the badged into
 */
function gatherBadgesFromProduct(apiProduct, badges) {
    if (!apiProduct || !apiProduct.custom.badges) {
        return;
    }

    for (var i = 0; i < apiProduct.custom.badges.length; i++) {
        var badge = apiProduct.custom.badges[i];
        badges.push({
            value: badge.value,
            displayValue: badge.displayValue
        });
    }
}

/**
 * Gathers badges from promotions with SHOW_AS_BADGE tag
 * @param {Collection-dw.campaign.Promotion} promotions promotions attached to the product
 * @param {Object[]} badges badge array to push the badged into
 */
function gatherBadgesFromPromotions(promotions, badges) {
    var promotionIterator = promotions.iterator();
    while (promotionIterator.hasNext()) {
        var promotion = promotionIterator.next();

        if (promotion.tags.indexOf(SHOW_AS_BADGE) !== -1) {
            badges.push({
                value: promotion.ID,
                displayValue: promotion.name
            });
        }
    }
}

module.exports = function (object, apiProduct, promotions) {
    Object.defineProperty(object, 'badges', {
        enumerable: true,
        value: (function () {
            var badges = [];

            gatherBadgesFromPromotions(promotions, badges);
            gatherBadgesFromProduct(apiProduct, badges);

            return badges.slice(0, BADGE_LIMIT);
        }())
    });
};
