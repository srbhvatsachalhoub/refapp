'use strict';

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'sellable', {
        enumerable: true,
        value: apiProduct.custom.sellable !== false
    });
};
