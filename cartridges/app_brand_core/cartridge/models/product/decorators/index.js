'use strict';

var base = module.superModule;

base.recommendations = require('*/cartridge/models/product/decorators/recommendations');
base.backInStock = require('*/cartridge/models/product/decorators/backInStock');
base.sellable = require('*/cartridge/models/product/decorators/sellable');
base.badges = require('*/cartridge/models/product/decorators/badges');
base.category = require('*/cartridge/models/product/decorators/category');
base.custom = require('*/cartridge/models/product/decorators/custom');
base.standardVariationAttributes = require('*/cartridge/models/product/decorators/standardVariationAttributes');
base.selectedVariationAttributes = require('*/cartridge/models/product/decorators/selectedVariationAttributes');
base.customBadge = require('*/cartridge/models/product/decorators/customBadge');
base.backgroundImage = require('*/cartridge/models/product/decorators/backgroundImage');
base.tabs = require('*/cartridge/models/product/decorators/tabs');
base.customization = require('*/cartridge/models/product/decorators/customization');
base.contentAssets = require('*/cartridge/models/product/decorators/contentAssets');

module.exports = base;
