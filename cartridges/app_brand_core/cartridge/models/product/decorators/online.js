'use strict';

module.exports = function (object, apiProduct) {
    if ('online' in object) {
        return;
    }

    Object.defineProperty(object, 'online', {
        enumerable: true,
        value: apiProduct
            ? !!apiProduct.online && apiProduct.custom.localeOffline.value !== 'true' && apiProduct.onlineFrom < new Date()
            : false
    });
};
