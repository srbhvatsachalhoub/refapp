'use strict';

var Resource = require('dw/web/Resource');

/**
 * Gets pre-defined variation attributes as master/variant product
 * Current Set values: size (can be extended later)
 * @param {Object} object - object to be wrapped
 * @param {dw.catalog.Product} apiProduct api product object
 * @returns {Array} array of standard variation attributes if set on apiProduct
 */

module.exports = function (object, apiProduct, variationModel) {
    if (variationModel) {
        return null;
    }

    // standard product variation attributes
    var standardProductVariationAttributes = [];
    if (apiProduct && apiProduct.custom && apiProduct.custom.size) {
        standardProductVariationAttributes.push({
            id: 'size',
            displayValue: apiProduct.custom.size,
            displayName: Resource.msg('label.attribute.size', 'product', null),
            disabled: true,
            selectedValue: {
                displayValue: apiProduct.custom.size
            },
            values: [{
                id: 'size',
                selected: true,
                displayValue: apiProduct.custom.size,
                value: apiProduct.custom.size,
                price: object.price && object.price.sales ? object.price.sales.formatted : ''
            }]
        });
    }
    return standardProductVariationAttributes.length ? standardProductVariationAttributes : null;
};
