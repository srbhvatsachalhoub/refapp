'use strict';

module.exports = function (object, product, quantity) {
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    Object.defineProperty(object, 'isOrderable', {
        enumerable: true,
        value: inventoryHelpers.getProductAvailabilityModel(product).isOrderable(quantity)
    });
};
