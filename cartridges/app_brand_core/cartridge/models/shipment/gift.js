'use strict';

var Site = require('dw/system/Site');

/**
 * @param {Object} object - the model object to be wrapped
 * @param {dw.order.Shipment} shipment - the default(or other if multi shipping) shipment of the current basket
 */
module.exports = function (object, shipment) {
    // check the object and shipment object
    if (!object || !shipment) {
        return;
    }

    Object.defineProperty(object, 'gift', {
        enumerable: true,
        value: Site.current.getCustomPreferenceValue('enableSendAsGift') ? {} : null
    });

    if (!object.gift) {
        return;
    }

    Object.defineProperties(object.gift, {
        isGift: {
            enumerable: true,
            value: !!shipment.gift
        },
        isGiftWrapped: {
            enumerable: true,
            value: !!shipment.custom.isGiftWrapped
        },
        isPriceHidden: {
            enumerable: true,
            value: !!shipment.custom.isPriceHidden
        },
        giftMessage: {
            enumerable: true,
            value: shipment.giftMessage ? shipment.giftMessage : ''
        }
    });
};
