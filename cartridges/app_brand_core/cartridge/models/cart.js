'use strict';

var collections = require('*/cartridge/scripts/util/collections');

var URLUtils = require('dw/web/URLUtils');
var Resource = require('dw/web/Resource');
var PromotionMgr = require('dw/campaign/PromotionMgr');
var Discount = require('dw/campaign/Discount');

var TotalsModel = require('*/cartridge/models/totals');
var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
var BonusDiscountLineItemsModel = require('*/cartridge/models/bonusDiscountLineItems');

var hasGiftCardProduct = require('*/cartridge/models/productLineItem/hasGiftCardProduct');
var gift = require('*/cartridge/models/shipment/gift');

var ShippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');
var commonHelpers = require('*/cartridge/scripts/helpers/commonHelpers');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * Gets the free shipping approaching product tile params
 * @returns {Object} the product tile parameters object
 */
function getFreeShippingApproachingProductTileParams() {
    return {
        pview: 'tile',
        ratings: 'true',
        swatches: 'false',
        showQuickView: 'true'
    };
}

/**
 * Checks if the basket's discount model has FREE shipping
 * if so then return true, false otherwise
 * @param {dw.order.Basket} basket - Current users's basket
 * @param {dw.campaign.DiscountPlan} discountPlan - set of applicable discounts
 * @returns {boolean} true if qualified, false otherwise
 */
function getIsQualifiedForFreeShipping(basket, discountPlan) {
    var shippingDiscounts = discountPlan.getShippingDiscounts(basket.defaultShipment);
    return collections.some(shippingDiscounts, function (shippingDiscount) {
        return shippingDiscount.type === Discount.TYPE_FREE;
    });
}

/**
 * Get the eligible category to be used on ProductSearch
 * @param {dw.campaign.Promotion} promotion - the free shipping promotion
 * @param {Array} basketCategories - the category ids array that are in current basket
 * @returns {string} - the category id that fits for the current basket
 */
function getEligibleCategoryMappingSearchCategory(promotion, basketCategories) {
    var categoryId;
    if (!promotion.custom.approachingProductsCategoryMapping || !basketCategories || basketCategories.length <= 0) {
        return categoryId;
    }

    var approachingProductsCategoryMapping;
    try {
        approachingProductsCategoryMapping = JSON.parse(promotion.custom.approachingProductsCategoryMapping);
    } catch (e) {
        return categoryId;
    }

    if (!commonHelpers.isArray(approachingProductsCategoryMapping)) {
        return categoryId;
    }

    var CatalogMgr = require('dw/catalog/CatalogMgr');

    var eligibleMap;
    approachingProductsCategoryMapping.forEach(function (map) {
        if (!('basketCategories' in map)
            || !('searchCategory' in map)
            || !commonHelpers.isArray(map.basketCategories)
            || typeof map.searchCategory !== 'string') {
            return;
        }

        // check all map categories are in basket
        var allCategoryInBasket = map.basketCategories.every(function (mapCategoryId) {
            return basketCategories.indexOf(mapCategoryId) > -1;
        });

        // check all in and the searchCategory is exists
        if (allCategoryInBasket && CatalogMgr.getCategory(map.searchCategory)) {
            // set map to eligibleMap if the basketCategories length is bigger then previous one
            if (!eligibleMap || (map.basketCategories.length > eligibleMap.basketCategories.length)) {
                eligibleMap = map;
            }
        }
    });

    return eligibleMap ? eligibleMap.searchCategory : categoryId;
}

/**
 * Gets free shipping approaching
 * the returning object contains 'tileContexts' and 'title'
 * 'tileContexts' is the product context that found for the approaching free shipping products
 * 'title' is the text that has the distance amount to the free shipping criteria
 * @param {dw.order.Basket} basket - Current users's basket
 * @param {dw.campaign.DiscountPlan} discountPlan - set of applicable discounts
 * @returns {Object} the object that has 'tileContexts' and 'title' if any products found for the free shipping promo approaching
 */
function getFreeShippingApproaching(basket, discountPlan) {
    // get free shipping approaching discount
    var approachingShippingDiscounts = discountPlan.getApproachingShippingDiscounts(basket.defaultShipment);
    var approachingShippingFreeDiscount = collections.find(approachingShippingDiscounts, function (approachingShippingDiscount) {
        return approachingShippingDiscount.discount && approachingShippingDiscount.discount.type === Discount.TYPE_FREE;
    });

    // check if there is approaching discount
    if (!approachingShippingFreeDiscount || !approachingShippingFreeDiscount.discount.promotion) {
        return null;
    }

    // get the promotion
    var promotion = approachingShippingFreeDiscount.discount.promotion;

    // check the promotion custom approachingProductsCount value,
    // if it is 0 then no need to search/find supplementary products
    if (promotion.custom.approachingProductsCount <= 0) {
        return null;
    }

    var priceMin = approachingShippingFreeDiscount.distanceFromConditionThreshold.value;
    var priceMax;
    if (promotion.custom.approachingProductsRemainingPricePercentage > 0) {
        priceMax = priceMin + ((priceMin / 100) * promotion.custom.approachingProductsRemainingPricePercentage);
    }

    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var ProductSearchHit = require('dw/catalog/ProductSearchHit');

    var productSearch = new ProductSearchModel();
    productSearch.setOrderableProductsOnly(true);
    productSearch.setRecursiveCategorySearch(true);
    productSearch.excludeHitType(ProductSearchHit.HIT_TYPE_PRODUCT_BUNDLE);
    productSearch.excludeHitType(ProductSearchHit.HIT_TYPE_PRODUCT_SET);
    productSearch.setPriceMin(priceMin);
    if (priceMax) {
        productSearch.setPriceMax(priceMax);
    }

    var basketProducts = [];
    var basketCategories = [];
    collections.forEach(basket.productLineItems, function (productLineItem) {
        var product = productLineItem.product.variant ? productLineItem.product.masterProduct : productLineItem.product;
        if (basketProducts.indexOf(product.ID) === -1) {
            basketProducts.push(product.ID);
        }

        collections.forEach(product.categories, function (category) {
            if (basketCategories.indexOf(category.ID) === -1) {
                basketCategories.push(category.ID);
            }
        });
    });

    var searchCategory = getEligibleCategoryMappingSearchCategory(promotion, basketCategories);
    if (searchCategory) {
        productSearch.setCategoryID(searchCategory);
    }

    productSearch.search();

    if (productSearch.count <= 0) {
        return null;
    }

    var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var freeShippingApproachingProductTileContexts = [];
    var productSearchHits = productSearch.productSearchHits;
    var productTileParams = CartModel.getFreeShippingApproachingProductTileParams(); // eslint-disable-line no-use-before-define

    var excludedCategories;
    if (!searchCategory) {
        excludedCategories = promotion.custom.approachingProductsExcludedCategories;
    }

    while (productSearchHits.hasNext()) {
        // check the limit of search results
        if (promotion.custom.approachingProductsCount === freeShippingApproachingProductTileContexts.length) {
            break;
        }

        var productSearchHit = productSearchHits.next();
        var productId = productSearchHit.product.variant ? productSearchHit.product.masterProduct.ID : productSearchHit.productID;
        // exclude if the product is already in basket
        if (basketProducts.indexOf(productId) > -1) {
            continue;
        }

        // exclude gift card products
        if (productSearchHit.product.custom.giftCardType.value) {
            continue;
        }

        // get first available product from productSearchHit.representedProducts
        var product = collections.find(productSearchHit.representedProducts, function (representedProduct) {
            var availabilityModel = inventoryHelpers.getProductAvailabilityModel(representedProduct);
            return availabilityModel && availabilityModel.orderable;
        });

        // check if orderable product found
        // if not continue with next productSearchHit
        if (!product) {
            continue;
        }

        // check if the product category is not excluded by 'Excluded Categories'
        // if excluded, then exclude the product from search results
        if (excludedCategories && excludedCategories.length > 0) {
            var productCategories = product.variant ? product.masterProduct.categories : product.categories;
            var productCategoryInExcludedCategories = productCategories && excludedCategories.some(function (excludedCategory) { // eslint-disable-line no-loop-func
                return collections.some(productCategories, function (productCategory) {
                    return excludedCategory === productCategory.ID;
                });
            });

            // check if any product category is found in excluded categories
            // if so exclude the product from search list
            if (productCategoryInExcludedCategories) {
                continue;
            }
        }

        productTileParams.pid = product.ID;
        freeShippingApproachingProductTileContexts.push(productHelpers.getProductTileContext(productTileParams));
    }

    if (freeShippingApproachingProductTileContexts.length <= 0) {
        return null;
    }

    var distancePrice = renderTemplateHelper.getRenderedHtml(
        { price: approachingShippingFreeDiscount.distanceFromConditionThreshold },
        'product/components/pricing/currencyFormatted'
    );

    var freeShippingApproaching = {
        tileContexts: freeShippingApproachingProductTileContexts,
        title: Resource.msgf(
            'msg.approachingfreeshippingpromo',
            'cart',
            null,
            distancePrice
        ),
        distancePrice: distancePrice
    };

    if (commonHelpers.isAjaxRequest()) {
        freeShippingApproaching.productsHtml = renderTemplateHelper.getRenderedHtml(
            {
                freeShippingApproaching: freeShippingApproaching
            },
            'cart/freeShippingApproachingProducts'
        );
    }

    return freeShippingApproaching;
}

/**
 * Fills the items with if they are part of current Basket's BonusDiscountLineItems
 * @param {dw.order.Basket} basket - Current users's basket
 * @param {Array} items - product array
 */
function fillItemsBonusDiscountProductLineItems(basket, items) {
    // check if there are items in the basket
    if (!items || items.length === 0) {
        return;
    }

    var bonusDiscountLineItems = basket.getBonusDiscountLineItems();
    // check if there are bonusDiscountLineItems in the basket
    if (!bonusDiscountLineItems || bonusDiscountLineItems.isEmpty()) {
        return;
    }

    items.forEach(function (item) {
        if (!item.isBonusProductLineItem) {
            return;
        }

        item.isBonusDiscountProductLineItem = collections.some(bonusDiscountLineItems, function (bonusDiscountLineItem) { // eslint-disable-line no-param-reassign
            return collections.some(bonusDiscountLineItem.bonusProductLineItems, function (bonusProductLineItem) {
                return bonusProductLineItem.UUID === item.UUID;
            });
        });
    });
}

/**
 * Generates an object of approaching discounts
 * @param {dw.order.Basket} basket - Current users's basket
 * @param {dw.campaign.DiscountPlan} discountPlan - set of applicable discounts
 * @returns {Object} an object of approaching discounts
 */
function getApproachingDiscounts(basket, discountPlan) {
    var approachingOrderDiscounts;
    var approachingShippingDiscounts;
    var orderDiscountObject;
    var shippingDiscountObject;
    var discountObject;

    if (basket && basket.productLineItems) {
        // Account for giftCertificateLineItems once gift certificates are implemented
        approachingOrderDiscounts = discountPlan.getApproachingOrderDiscounts();
        approachingShippingDiscounts =
            discountPlan.getApproachingShippingDiscounts(basket.defaultShipment);

        orderDiscountObject =
            collections.map(approachingOrderDiscounts, function (approachingOrderDiscount) {
                return {
                    discountMsg: Resource.msgf(
                        'msg.approachingpromo',
                        'cart',
                        null,
                        renderTemplateHelper.getRenderedHtml(
                            { price: approachingOrderDiscount.getDistanceFromConditionThreshold() },
                            'product/components/pricing/currencyFormatted'
                        ),
                        approachingOrderDiscount.getDiscount()
                            .getPromotion().getCalloutMsg()
                    )
                };
            });

        shippingDiscountObject = [];
        collections.forEach(approachingShippingDiscounts, function (approachingShippingDiscount) {
            // if the approaching discount is FREE shipping discount, eliminate it
            // since we have freeShippingApproaching for model
            if (approachingShippingDiscount.discount && approachingShippingDiscount.discount.type === Discount.TYPE_FREE) {
                return;
            }

            shippingDiscountObject.push({
                discountMsg: Resource.msgf(
                    'msg.approachingpromo',
                    'cart',
                    null,
                    renderTemplateHelper.getRenderedHtml(
                        { price: approachingShippingDiscount.getDistanceFromConditionThreshold() },
                        'product/components/pricing/currencyFormatted'
                    ),
                    approachingShippingDiscount.getDiscount()
                        .getPromotion().getCalloutMsg()
                )
            });
        });

        discountObject = orderDiscountObject.concat(shippingDiscountObject);
    }
    return discountObject;
}

/**
 * Generates an object of URLs
 * @returns {Object} an object of URLs in string format
 */
function getCartActionUrls() {
    return {
        removeProductLineItemUrl: URLUtils.url('Cart-RemoveProductLineItem').toString(),
        updateQuantityUrl: URLUtils.url('Cart-UpdateQuantity').toString(),
        selectShippingUrl: URLUtils.url('Cart-SelectShippingMethod').toString(),
        submitCouponCodeUrl: URLUtils.url('Cart-AddCoupon').toString(),
        removeCouponLineItem: URLUtils.url('Cart-RemoveCouponLineItem').toString()
    };
}

/**
 * @constructor
 * @classdesc CartModel class that represents the current basket
 * Extended with isQualifiedForFreeShipping
 *
 * @param {dw.order.Basket} basket - Current users's basket
 * @param {dw.campaign.DiscountPlan} discountPlan - set of applicable discounts
 */
function CartModel(basket) {
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

    if (basket !== null) {
        var shippingModels = ShippingHelpers.getShippingModels(basket, null, 'basket');
        var productLineItemsModel = new ProductLineItemsModel(basket.productLineItems, 'basket');
        var totalsModel = new TotalsModel(basket);
        this.hasBonusProduct = Boolean(basket.bonusLineItems && basket.bonusLineItems.length);
        this.actionUrls = getCartActionUrls();
        this.numOfShipments = basket.shipments.length;
        this.totals = totalsModel;

        if (shippingModels) {
            this.shipments = shippingModels.map(function (shippingModel) {
                var result = {};
                result.shippingMethods = shippingModel.applicableShippingMethods;
                if (shippingModel.selectedShippingMethod) {
                    result.selectedShippingMethod = shippingModel.selectedShippingMethod.ID;
                }

                return result;
            });
        }

        var discountPlan = PromotionMgr.getDiscounts(basket);
        if (discountPlan) {
            this.approachingDiscounts = getApproachingDiscounts(basket, discountPlan);
            this.isQualifiedForFreeShipping = getIsQualifiedForFreeShipping(basket, discountPlan);

            if (!this.isQualifiedForFreeShipping) {
                this.freeShippingApproaching = getFreeShippingApproaching(basket, discountPlan);
            }
        }

        this.items = productLineItemsModel.items;
        this.numItems = productLineItemsModel.totalQuantity;
        this.valid = hooksHelper('app.validate.basket', 'validateBasket', basket, false, require('*/cartridge/scripts/hooks/validateBasket').validateBasket);

        // bonus discount items
        this.bonusDiscountItems = new BonusDiscountLineItemsModel(basket);
        fillItemsBonusDiscountProductLineItems(basket, this.items);
        hasGiftCardProduct(this, basket.productLineItems);
        gift(this, basket.defaultShipment);
    } else {
        this.items = [];
        this.numItems = 0;
    }

    this.resources = {
        numberOfItems: Resource.msgf('label.number.items.in.cart', 'cart', null, this.numItems),
        minicartCountOfItems: Resource.msgf('minicart.count', 'common', null, this.numItems),
        emptyCartMsg: Resource.msg('info.cart.empty.msg', 'cart', null)
    };
}

CartModel.getFreeShippingApproachingProductTileParams = getFreeShippingApproachingProductTileParams;
module.exports = CartModel;
