'use strict';

var URLUtils = require('dw/web/URLUtils');
var ImageModel = require('*/cartridge/models/product/productImages');
var ACTION_ENDPOINT = 'Product-Show';
var IMAGE_SIZE = 'small';


/**
 * Get Image URL
 *
 * @param {dw.catalog.Product} product - Suggested product
 * @return {string} - Image URL
 */
function getImageUrl(product) {
    var imageProduct = product;
    if (product.master) {
        imageProduct = product.variationModel.defaultVariant;
    }
    var images = new ImageModel(imageProduct, {
        types: [IMAGE_SIZE],
        quantity: 'single'
    });
    if (!images[IMAGE_SIZE] || !images[IMAGE_SIZE].length) {
        return null;
    }
    return images[IMAGE_SIZE][0].url;
}

/**
 * Compile a list of relevant suggested products
 *
 * @param {dw.util.Iterator.<dw.suggest.SuggestedProduct>} suggestedProducts - Iterator to retrieve
 *                                                                             SuggestedProducts
*  @param {number} maxItems - Maximum number of products to retrieve
 * @return {Object[]} - Array of suggested products
 */
function getProducts(suggestedProducts, maxItems) {
    var product = null;
    var products = [];
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

    for (var i = 0; i < maxItems; i++) {
        if (suggestedProducts.hasNext()) {
            product = suggestedProducts.next().productSearchHit.product;
            // check if the product is not locally offline
            if (product.custom.localeOffline.value === 'true') {
                maxItems++; // eslint-disable-line no-param-reassign
                continue;
            }
            var productPriceModel = product.getPriceModel();
            var price = productPriceModel.price;
            if (!price.available && (productPriceModel.maxPrice.available || productPriceModel.minPrice.available)) {
                price = productPriceModel.minPrice.available ? productPriceModel.minPrice : productPriceModel.maxPrice;
            }
            products.push({
                name: product.name,
                imageUrl: getImageUrl(product),
                url: URLUtils.url(ACTION_ENDPOINT, 'pid', product.ID),
                price: renderTemplateHelper.getRenderedHtml({ price: price }, 'product/components/pricing/currencyFormatted')
            });
        }
    }

    return products;
}

/**
 * @typedef SuggestedPhrase
 * @type Object
 * @property {boolean} exactMatch - Whether suggested phrase is an exact match
 * @property {string} value - Suggested search phrase
 */

/**
 * Compile a list of relevant suggested phrases
 *
 * @param {dw.util.Iterator.<dw.suggest.SuggestedPhrase>} suggestedPhrases - Iterator to retrieve
 *                                                                           SuggestedPhrases
 * @param {number} maxItems - Maximum number of phrases to retrieve
 * @return {SuggestedPhrase[]} - Array of suggested phrases
 */
function getPhrases(suggestedPhrases, maxItems) {
    var phrase = null;
    var phrases = [];

    for (var i = 0; i < maxItems; i++) {
        if (suggestedPhrases.hasNext()) {
            phrase = suggestedPhrases.next();
            phrases.push({
                exactMatch: phrase.exactMatch,
                value: phrase.phrase
            });
        }
    }

    return phrases;
}

/**
 * @constructor
 * @classdesc ProductSuggestions class
 *
 * @param {dw.suggest.SuggestModel} suggestions - Suggest Model
 * @param {number} maxItems - Maximum number of items to retrieve
 */
function ProductSuggestions(suggestions, maxItems) {
    var productSuggestions = suggestions.productSuggestions;

    if (!productSuggestions) {
        this.available = false;
        this.phrases = [];
        this.products = [];
        return;
    }

    var searchPhrasesSuggestions = productSuggestions.searchPhraseSuggestions;

    this.products = getProducts(productSuggestions.suggestedProducts, maxItems);
    if (this.products.length === 0) {
        this.available = false;
        this.phrases = [];
        return;
    }

    this.available = productSuggestions.hasSuggestions();
    this.phrases = getPhrases(searchPhrasesSuggestions.suggestedPhrases, maxItems);
}

module.exports = ProductSuggestions;
