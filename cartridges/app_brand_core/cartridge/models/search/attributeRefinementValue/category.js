'use strict';

var base = module.superModule;

/**
 * @constructor
 * @classdesc Category attribute refinement value model
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 * @param {boolean} selected - Selected flag
 */
function CategoryRefinementValueWrapper(productSearch, refinementDefinition, refinementValue, selected) {
    base.call(this, productSearch, refinementDefinition, refinementValue, selected);
    this.hitCount = refinementValue.products.length;
}

module.exports = CategoryRefinementValueWrapper;
