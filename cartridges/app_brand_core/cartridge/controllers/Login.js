'use strict';

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var CustomerMgr = require('dw/customer/CustomerMgr');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var digitHelpers = require('*/cartridge/scripts/helpers/digitHelpers');
var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var BreadcrumbsHelpers = require('*/cartridge/scripts/helpers/breadcrumbsHelpers');

/**
 * Gets a localized error message regarding the case
 * @param {string} errorCase Error case
 * @returns {string} error message
 */
function getErrorMessage(errorCase) {
    switch (errorCase) {
        case 'EXISTING_CUSTOMER':
            return Resource.msg('message.error.login.existingCustomer', 'login', null);
        case 'NO_PROFILE':
            return Resource.msg('message.error.login.noProfile', 'login', null);
        default: return Resource.msg('message.error.login.unknown', 'login', null);
    }
}

/**
 * Renders given template for login form
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @param {string} template - Template path that needs to be rendered
 */
function includeLoginForm(req, res, next, template) {
    var target = req.querystring.rurl || 1;
    var isCheckoutLogin = req.querystring.isCheckoutLogin || false;
    var rememberMe = false;
    var userName = '';
    var actionUrl = URLUtils.url('Account-Login', 'rurl', target);
    var createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target).relative().toString();

    if (req.currentCustomer.credentials) {
        rememberMe = true;
        userName = req.currentCustomer.credentials.username;
    }

    var profileForm = server.forms.getForm('profile');
    profileForm.clear();

    res.render(template, {
        rememberMe: rememberMe,
        userName: userName,
        actionUrl: actionUrl,
        rurl: target,
        profileForm: profileForm,
        oAuthReentryEndpoint: isCheckoutLogin ? 2 : 1,
        createAccountUrl: createAccountUrl
    });

    next();
}

/**
 * Appends login parameters to view data
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 */
function appendLoginParameters(req, res, next) {
    var Site = require('dw/system/Site');

    var oAuthFacebookProviderID = Site.getCurrent().getCustomPreferenceValue('oAuthFacebookProviderID');
    if (oAuthFacebookProviderID) {
        res.setViewData({
            oAuthFacebookProviderID: oAuthFacebookProviderID
        });
    }

    var oAuthGoogleProviderID = Site.getCurrent().getCustomPreferenceValue('oAuthGoogleProviderID');
    if (oAuthGoogleProviderID) {
        res.setViewData({
            oAuthGoogleProviderID: oAuthGoogleProviderID
        });
    }

    if (req.querystring.error === '1') {
        res.setViewData({
            errorMessage: getErrorMessage(req.querystring.case)
        });
    }

    res.setViewData({
        years: digitHelpers.getYearsAsOptionList(req, 1900)
    });

    next();
}

server.get(
    'IncludeModal',
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        includeLoginForm(req, res, next, '/account/components/loginFormModal');
    },
    appendLoginParameters
);

server.get(
    'Include',
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        includeLoginForm(req, res, next, 'account/components/loginForm');
    },
    appendLoginParameters
);

server.prepend('Show',
    function (req, res, next) {
        // Redirect user to my account page if she is already authenticated.
        if (req.currentCustomer &&
            req.currentCustomer.raw &&
            req.currentCustomer.raw.authenticated) {
            res.redirect(URLUtils.url('Account-Show'));
        }
        next();
    }
);

server.append('Show',
    appendLoginParameters,
    function (req, res, next) {
        var viewData = res.getViewData();
        BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('breadcrumb.' + viewData.navTabValue, 'login', null));
        res.setViewData({
            passwordConstraints: ProfileHelper.getPasswordConstraints()
        });
        next();
    }
);

/**
 * Runs after customer returns from OAuth providers authentication page
 */
server.replace('OAuthReentry',
    server.middleware.https,
    server.middleware.get,
    consentTracking.consent,
    function (req, res, next) {
        var oauthLoginFlowMgr = require('dw/customer/oauth/OAuthLoginFlowMgr');
        var Transaction = require('dw/system/Transaction');

        var destination = req.session.privacyCache.store.oauthLoginTargetEndPoint;

        var finalizeOAuthLoginResult = oauthLoginFlowMgr.finalizeOAuthLogin();

        // finalizeOAuthLoginResult.userInfoResponse is null if user cancels the process from the OAuth providers page
        if (!finalizeOAuthLoginResult || !finalizeOAuthLoginResult.userInfoResponse) {
            res.redirect(URLUtils.url('Login-Show', 'error', 1, 'case', 'NO_PROFILE'));
            return next();
        }

        var response = finalizeOAuthLoginResult.userInfoResponse.userInfo;
        var oauthProviderID = finalizeOAuthLoginResult.accessTokenResponse.oauthProviderId;

        if (!oauthProviderID) {
            res.render('/error', {
                message: Resource.msg('error.oauth.login.failure', 'login', null)
            });

            return next();
        }

        if (!response) {
            res.render('/error', {
                message: Resource.msg('error.oauth.login.failure', 'login', null)
            });

            return next();
        }

        var externalProfile = JSON.parse(response);
        if (!externalProfile) {
            res.render('/error', {
                message: Resource.msg('error.oauth.login.failure', 'login', null)
            });

            return next();
        }

        var userID = externalProfile.id || externalProfile.uid;
        if (!userID) {
            res.render('/error', {
                message: Resource.msg('error.oauth.login.failure', 'login', null)
            });

            return next();
        }

        var authenticatedCustomerProfile = CustomerMgr.getExternallyAuthenticatedCustomerProfile(
            oauthProviderID,
            userID
        );

        // Check if customer has already signed up
        if (!authenticatedCustomerProfile) {
            var session = req.session.raw.custom;
            session.OAuthReentryResult = JSON.stringify({
                externalProfile: externalProfile,
                userID: userID,
                oauthProviderID: oauthProviderID,
                destination: destination
            });

            var email = ProfileHelper.getEmailFromExternalProfile(externalProfile);
            if (!email) {
                res.redirect(URLUtils.url('Login-Show', 'error', 1, 'case', 'NO_PROFILE'));
                return next();
            }

            // We check if a customer with given email address exists
            var existingCustomer = CustomerMgr.queryProfile('email={0}', email);
            if (existingCustomer) {
                res.redirect(URLUtils.url('Login-Show', 'error', 1, 'case', 'EXISTING_CUSTOMER'));
                return next();
            }

            // Redirect to page where we ask for mobile phone
            res.redirect(URLUtils.url('SocialAccount-MobilePhone'));
            return next();
        }

        // user already signed up, attempting to log him/her in
        var credentials = authenticatedCustomerProfile.getCredentials();
        if (credentials.isEnabled()) {
            Transaction.wrap(function () {
                CustomerMgr.loginExternallyAuthenticatedCustomer(oauthProviderID, userID, false);
            });
            // Apply custom price books for customer
            var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');
            localeHelpers.setApplicablePriceBooks();
        } else {
            res.render('/error', {
                message: Resource.msg('error.oauth.login.failure', 'login', null)
            });

            return next();
        }

        req.session.privacyCache.clear();
        res.redirect(URLUtils.url(destination));

        return next();
    }
);

module.exports = server.exports();
