'use strict';

var server = require('server');
var currentSite = require('dw/system/Site').getCurrent();
var ProductFactory = require('*/cartridge/scripts/factories/product');
server.get('Show', function (req, res, next) {
    var enableGiftWrapping = currentSite.getCustomPreferenceValue('enableGiftWrapping');
    var giftWrappingProducts = Array.prototype.slice.call(currentSite.getCustomPreferenceValue('giftWrappingProducts'));
    res.render('giftWrapping/index', {
        enableGiftWrapping: enableGiftWrapping,
        giftWrappingProducts: giftWrappingProducts
    });
    next();
});
server.get('Tile', function (req, res, next) {
    var product = ProductFactory.get({
        pid: req.querystring.pid
    });
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();
    var giftLineItems = currentBasket.getProductLineItems(product.id);
    product.isSelected = !giftLineItems.empty;
    res.render('giftWrapping/giftWrappingTile.isml', { product: product });
    next();
});
module.exports = server.exports();
