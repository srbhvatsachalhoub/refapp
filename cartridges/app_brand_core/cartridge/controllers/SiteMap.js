/* global dw */
'use strict';

/**
 * SiteMap Controller
 * This controller provides endpoint(s) for the Applications Site Map
 */

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');

/**
 * SiteMap-Google
 * Create the Google Site Map endpoint
 *
 * SiteMap Rule:
 * RewriteRule ^/(sitemap([^/]*))$ /on/demandware.store/%{HTTP_HOST}/-/SiteMap-Google?name=$1 [PT,L]
 *
 * @todo remove pipelet code when SendGoogleSiteMap is converted to a Script API
 */
server.get('Google', cache.applyDefaultCache, function (req) {
    new dw.system.Pipelet('SendGoogleSiteMap').execute({
        FileName: req.querystring.name
    });
});

module.exports = server.exports();
