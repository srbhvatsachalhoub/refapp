'use strict';

/* global response */

var server = require('server');

/**
 * End point to display custom feed file placed in IMPEX
 */
server.get('Show', function (req, res, next) {
    var Site = require('dw/system/Site');
    var FileReader = require('dw/io/FileReader');
    var File = require('dw/io/File');
    var StringUtils = require('dw/util/StringUtils');
    var fileName = req.querystring.filename;

    if (!fileName) {
        res.render('/common/customFeed', {
            customFeed: 'Please provide filename in querystring'
        });
        return next();
    }

    // remove _{locale} to get only site name
    var currentSiteId = Site.getCurrent().getID();
    var folderName = currentSiteId.indexOf('_') > -1 ? currentSiteId.slice(0, currentSiteId.length - 3) : currentSiteId;

    var impex = File.getRootDirectory(File.IMPEX);
    var file = new File(impex, StringUtils.format('{0}{1}{2}{1}{3}', 'src/feeds', File.SEPARATOR, folderName, fileName));
    if (!file.exists()) {
        res.render('/common/customFeed', {
            customFeed: 'File is not found'
        });
        return next();
    }
    var fileReader = new FileReader(file);
    var contentType = 'application/xml';
    if (fileName.indexOf('csv') > -1) {
        contentType = 'application/csv';
    }
    response.addHttpHeader('Content-Disposition', StringUtils.format('attachment; filename="{0}"', fileName));
    response.setContentType(StringUtils.format('{0}; charset=utf-8', contentType));
    response.setBuffered(false);
    var out = response.writer;
    var line;
    while ((line = fileReader.readLine()) !== null) { // eslint-disable-line
        out.println(line);
    }
    fileReader.close();
    out.print('');
    out.flush();
    return; // eslint-disable-line
});

module.exports = server.exports();
