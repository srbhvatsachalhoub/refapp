'use strict';

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');


server.prepend('AddProduct', function (req, res, next) {
    var Site = require('dw/system/Site');

    if (Site.getCurrent().getCustomPreferenceValue('disableCheckout')) {
        res.json({
            message: Resource.msg('error.cannot.add.unsellable.product', 'cart', null),
            error: true
        });
        next();
        return;
    }

    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    if (req.form.pidsObj) {
        next();
        return;
    }

    var productId = req.form.pid;
    if (!productId) {
        next();
        return;
    }

    var sellable = cartHelper.checkSellable(productId);
    if (!sellable) {
        res.json({
            message: Resource.msg('error.cannot.add.unsellable.product', 'cart', null),
            error: true
        });
        this.emit('route:Complete', req, res);
        return;
    }

    var quantity = parseInt(req.form.quantity, 10);
    var itemQuantityBelowMax = cartHelper.checkMaxOrderQuantity(productId, quantity, true);
    if (!itemQuantityBelowMax) {
        res.json({
            message: Resource.msg('error.cannot.add.product.max.quantity.exceeded', 'cart', null),
            error: true
        });
        this.emit('route:Complete', req, res);
        return;
    }

    next();
});

/**
 * Calculates the unit and total prices of items
 * that are just added to cart.
 */
server.append('AddProduct', function (req, res, next) {
    var Money = require('dw/value/Money');
    var BasketMgr = require('dw/order/BasketMgr');
    var DefaultPrice = require('*/cartridge/models/price/default');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var currencyCode = currentBasket.getCurrencyCode();
    var viewData = res.getViewData();
    var decimalPriceTotal = 0;
    var pidsObj = [];

    if (req.form.pidsObj) {
        // Multiple products are added to cart
        pidsObj = JSON.parse(req.form.pidsObj);
    } else {
        // Single product is added to cart
        pidsObj = [{
            pid: req.form.pid,
            qty: req.form.quantity,
            options: req.form.options
        }];
    }

    pidsObj.forEach(function (pidObj) {
        viewData.cart.items.forEach(function (itemObj) {
            // Work on correct product data
            if (pidObj.pid === itemObj.id) {
                var q = parseInt(pidObj.qty, 10);
                var decimalPriceUnit = itemObj.priceTotal.decimalPrice / itemObj.quantity;
                var decimalPriceItemTotal = decimalPriceUnit * q;
                var salesPriceUnit = new Money(decimalPriceUnit, currencyCode);
                var salesPriceItemTotal = new Money(decimalPriceItemTotal, currencyCode);
                var listPriceUnit = null;
                var listPriceTotal = null;

                if (itemObj.price.list && decimalPriceUnit < itemObj.price.list.decimalPrice) {
                    // If new price is less than sales price, keep going using list price
                    listPriceUnit = new Money(itemObj.price.list.decimalPrice, currencyCode);
                    listPriceTotal = new Money(itemObj.price.list.decimalPrice * q, currencyCode);
                } else if (itemObj.price.sales && decimalPriceUnit < itemObj.price.sales.decimalPrice) {
                    // If new price is less than sales price, use sales price as list price
                    listPriceUnit = new Money(itemObj.price.sales.decimalPrice, currencyCode);
                    listPriceTotal = new Money(itemObj.price.sales.decimalPrice * q, currencyCode);
                }

                // Set quantity, unit price and total price for single product
                itemObj.instantPriceUnit = new DefaultPrice(salesPriceUnit, listPriceUnit); // eslint-disable-line
                itemObj.instantPriceTotal = new DefaultPrice(salesPriceItemTotal, listPriceTotal); // eslint-disable-line
                itemObj.instantQuantity = q; // eslint-disable-line

                // Calculate total price (in case of multiple products are added to cart)
                decimalPriceTotal += decimalPriceItemTotal;
            }
        });
    });

    viewData.cart.totals.instantPriceTotal = new DefaultPrice(new Money(decimalPriceTotal, currencyCode));
    viewData.addedProducts = pidsObj;

    if (req.form.customizationSettings) {
        var Transaction = require('dw/system/Transaction');
        var collections = require('*/cartridge/scripts/util/collections');
        var lineItems = currentBasket.getAllProductLineItems(req.form.pid);
        try {
            var customizationSettings = JSON.parse(req.form.customizationSettings);
            Transaction.wrap(function () {
                collections.forEach(lineItems, function (lineItem) {
                    /* eslint-disable no-param-reassign */
                    lineItem.custom.personalizationRequested = customizationSettings.personalizationRequested;
                    lineItem.custom.engravingRequested = customizationSettings.engravingRequested;
                    lineItem.custom.engravingText = customizationSettings.engravingText;
                });
            });
        } catch (e) {
            var logger = require('dw/system/Logger').getLogger('Cart');
            logger.error('An unpexted error ocurred {0}', JSON.stringify(e));
        }
    }

    if (req.form.type === 'wishlistAll') {
        var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
        var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
        productListHelper.removeList(req.currentCustomer, list);
    }

    res.setViewData(viewData);

    next();
});

server.prepend('Show',
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');
        var Site = require('dw/system/Site');

        if (Site.getCurrent().getCustomPreferenceValue('disableCheckout')) {
            res.redirect(URLUtils.url('Home-Show'));
            next();
            return;
        }

        next();
    });

/**
 * Extended with forms object
 * forms object contains gift form
 */
server.append(
    'Show',
    function (req, res, next) {
        var viewData = res.getViewData();
        var giftForm = server.forms.getForm('gift');
        giftForm.clear();

        // extend view data
        viewData.forms = {
            gift: giftForm
        };

        // set view data
        res.setViewData(viewData);

        next();
    }
);

/**
 * Extended with multiple inventory implementation
 */
server.replace('UpdateQuantity', server.middleware.get, function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var CartModel = require('*/cartridge/models/cart');
    var collections = require('*/cartridge/scripts/util/collections');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var productId = req.querystring.pid;
    if (!productId) {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product.quantity', 'cart', null)
        });

        return next();
    }

    var updateQuantity = parseInt(req.querystring.quantity, 10);
    var itemQuantityBelowMax = cartHelper.checkMaxOrderQuantity(productId, updateQuantity, false);
    if (!itemQuantityBelowMax) {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product.quantity', 'cart', null)
        });
        this.emit('route:Complete', req, res);

        return next();
    }

    var uuid = req.querystring.uuid;
    var productLineItems = currentBasket.productLineItems;
    var matchingLineItem = collections.find(productLineItems, function (item) {
        return item.productID === productId && item.UUID === uuid;
    });
    var availableToSell = 0;

    var totalQtyRequested = 0;
    var qtyAlreadyInCart = 0;
    var minOrderQuantity = 0;
    var perpetual = false;
    var canBeUpdated = false;
    var bundleItems;
    var bonusDiscountLineItemCount = currentBasket.bonusDiscountLineItems.length;
    var availabilityModel;

    if (matchingLineItem) {
        if (matchingLineItem.product.bundle) {
            bundleItems = matchingLineItem.bundledProductLineItems;
            canBeUpdated = collections.every(bundleItems, function (item) {
                var quantityToUpdate = updateQuantity *
                    matchingLineItem.product.getBundledProductQuantity(item.product).value;
                qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                    item.productID,
                    productLineItems,
                    item.UUID
                );
                totalQtyRequested = quantityToUpdate + qtyAlreadyInCart;
                availabilityModel = inventoryHelpers.getProductAvailabilityModel(item.product, req.locale.id);
                availableToSell = availabilityModel.inventoryRecord.ATS.value;
                perpetual = availabilityModel.inventoryRecord.perpetual;
                minOrderQuantity = item.product.minOrderQuantity.value;
                return (totalQtyRequested <= availableToSell || perpetual) &&
                    (quantityToUpdate >= minOrderQuantity);
            });
        } else {
            availabilityModel = inventoryHelpers.getProductAvailabilityModel(matchingLineItem.product, req.locale.id);
            availableToSell = availabilityModel.inventoryRecord.ATS.value;
            perpetual = availabilityModel.inventoryRecord.perpetual;
            qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                productId,
                productLineItems,
                matchingLineItem.UUID
            );
            totalQtyRequested = updateQuantity + qtyAlreadyInCart;
            minOrderQuantity = matchingLineItem.product.minOrderQuantity.value;
            canBeUpdated = (totalQtyRequested <= availableToSell || perpetual) &&
                (updateQuantity >= minOrderQuantity);
        }
    }

    if (canBeUpdated) {
        Transaction.wrap(function () {
            matchingLineItem.setQuantityValue(updateQuantity);

            var previousBounsDiscountLineItems = collections.map(currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
                return bonusDiscountLineItem.UUID;
            });

            basketCalculationHelpers.calculateTotals(currentBasket);
            if (currentBasket.bonusDiscountLineItems.length > bonusDiscountLineItemCount) {
                var prevItems = JSON.stringify(previousBounsDiscountLineItems);

                collections.forEach(currentBasket.bonusDiscountLineItems, function (bonusDiscountLineItem) {
                    if (prevItems.indexOf(bonusDiscountLineItem.UUID) < 0) {
                        bonusDiscountLineItem.custom.bonusProductLineItemUUID = matchingLineItem.UUID; // eslint-disable-line no-param-reassign
                        matchingLineItem.custom.bonusProductLineItemUUID = 'bonus';
                        matchingLineItem.custom.preOrderUUID = matchingLineItem.UUID;
                    }
                });
            }
        });
    }

    if (matchingLineItem && canBeUpdated) {
        var basketModel = new CartModel(currentBasket);
        res.json(basketModel);
    } else {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product.quantity', 'cart', null)
        });
    }

    return next();
});

server.replace('EditProductLineItem', server.middleware.post, function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var URLUtils = require('dw/web/URLUtils');
    var Transaction = require('dw/system/Transaction');
    var CartModel = require('*/cartridge/models/cart');
    var collections = require('*/cartridge/scripts/util/collections');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }

    var uuid = req.form.uuid;
    var productId = req.form.pid;
    var selectedOptionValueId = req.form.selectedOptionValueId;
    var updateQuantity = parseInt(req.form.quantity, 10);

    var productLineItems = currentBasket.allProductLineItems;
    var requestLineItem = collections.find(productLineItems, function (item) {
        return item.UUID === uuid;
    });

    var uuidToBeDeleted = null;
    var pliToBeDeleted;
    var newPidAlreadyExist = collections.find(productLineItems, function (item) {
        if (item.productID === productId && item.UUID !== uuid) {
            uuidToBeDeleted = item.UUID;
            pliToBeDeleted = item;
            updateQuantity += parseInt(item.quantity, 10);
            return true;
        }
        return false;
    });

    var availableToSell = 0;
    var totalQtyRequested = 0;
    var qtyAlreadyInCart = 0;
    var minOrderQuantity = 0;
    var canBeUpdated = false;
    var perpetual = false;
    var bundleItems;
    var availabilityModel;

    if (requestLineItem) {
        if (requestLineItem.product.bundle) {
            bundleItems = requestLineItem.bundledProductLineItems;
            canBeUpdated = collections.every(bundleItems, function (item) {
                var quantityToUpdate = updateQuantity *
                    requestLineItem.product.getBundledProductQuantity(item.product).value;
                qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                    item.productID,
                    productLineItems,
                    item.UUID
                );
                totalQtyRequested = quantityToUpdate + qtyAlreadyInCart;
                availabilityModel = inventoryHelpers.getProductAvailabilityModel(item.product, req.locale.id);
                availableToSell = availabilityModel.inventoryRecord.ATS.value;
                perpetual = availabilityModel.inventoryRecord.perpetual;
                minOrderQuantity = item.product.minOrderQuantity.value;
                return (totalQtyRequested <= availableToSell || perpetual) &&
                    (quantityToUpdate >= minOrderQuantity);
            });
        } else {
            availabilityModel = inventoryHelpers.getProductAvailabilityModel(requestLineItem.product, req.locale.id);
            availableToSell = availabilityModel.inventoryRecord.ATS.value;
            perpetual = availabilityModel.inventoryRecord.perpetual;
            qtyAlreadyInCart = cartHelper.getQtyAlreadyInCart(
                productId,
                productLineItems,
                requestLineItem.UUID
            );
            totalQtyRequested = updateQuantity + qtyAlreadyInCart;
            minOrderQuantity = requestLineItem.product.minOrderQuantity.value;
            canBeUpdated = (totalQtyRequested <= availableToSell || perpetual) &&
                (updateQuantity >= minOrderQuantity);
        }
    }

    var error = false;
    if (canBeUpdated) {
        var product = ProductMgr.getProduct(productId);
        try {
            Transaction.wrap(function () {
                if (newPidAlreadyExist) {
                    var shipmentToRemove = pliToBeDeleted.shipment;
                    currentBasket.removeProductLineItem(pliToBeDeleted);
                    if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
                        currentBasket.removeShipment(shipmentToRemove);
                    }
                }

                if (!requestLineItem.product.bundle) {
                    requestLineItem.replaceProduct(product);
                }

                // If the product has options
                var optionModel = product.getOptionModel();
                if (optionModel && optionModel.options && optionModel.options.length) {
                    var productOption = optionModel.options.iterator().next();
                    var productOptionValue = optionModel.getOptionValue(productOption, selectedOptionValueId);
                    var optionProductLineItems = requestLineItem.getOptionProductLineItems();
                    var optionProductLineItem = optionProductLineItems.iterator().next();
                    optionProductLineItem.updateOptionValue(productOptionValue);
                }

                requestLineItem.setQuantityValue(updateQuantity);
                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        } catch (e) {
            error = true;
        }
    }

    if (!error && requestLineItem && canBeUpdated) {
        var cartModel = new CartModel(currentBasket);

        var responseObject = {
            cartModel: cartModel,
            newProductId: productId
        };

        if (uuidToBeDeleted) {
            responseObject.uuidToBeDeleted = uuidToBeDeleted;
        }

        res.json(responseObject);
    } else {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.update.product', 'cart', null)
        });
    }

    return next();
});

/**
 * UpdateNonNestedBonusProducts Controller Action
 * Add choice of bonus(es) to the basket
 */
server.post('UpdateNonNestedBonusProducts', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var collections = require('*/cartridge/scripts/util/collections');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var currentBasket = BasketMgr.getCurrentOrNewBasket();

    // check the current basket
    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var bduuid = req.form.uuid; // bonus discount uuid
    var pids = JSON.parse(req.form.pids); // bonus product ids

    if (!bduuid) {
        res.setStatusCode(500);
        res.json({
            error: true
        });

        return next();
    }

    var bonusDiscountLineItems = currentBasket.getBonusDiscountLineItems();
    var bonusDiscountLineItem = collections.find(bonusDiscountLineItems, function (item) {
        return item.UUID === bduuid;
    });

    if (!bonusDiscountLineItem || bonusDiscountLineItem.maxBonusItems < pids.length) {
        res.setStatusCode(500);
        res.json({
            error: true
        });

        return next();
    }

    var productsToBeAdded = [];
    pids.forEach(function (pid) {
        var product = ProductMgr.getProduct(pid);
        var availabilityModel = inventoryHelpers.getProductAvailabilityModel(product);
        if (!product || !availabilityModel || !availabilityModel.orderable) {
            return;
        }

        productsToBeAdded.push(product);
    });


    Transaction.wrap(function () {
        collections.forEach(bonusDiscountLineItem.getBonusProductLineItems(), function (dli) {
            if (dli.product) {
                currentBasket.removeProductLineItem(dli);
            }
        });

        productsToBeAdded.forEach(function (product) {
            var pli = currentBasket.createBonusProductLineItem(
                bonusDiscountLineItem,
                product,
                null,
                null);
            pli.setQuantityValue(1);
        });
    });


    res.json({
        success: true,
        pids: productsToBeAdded.map(function (product) {
            return product.ID;
        })
    });

    return next();
});

/**
 * UpdateCartGift Controller Action
 * Updates gift attributes on basket's defaultShipment
 */
server.post('UpdateCartGift', function (req, res, next) {
    var currentSite = require('dw/system/Site').current;

    // check if the current site gift is enabled
    if (!currentSite.getCustomPreferenceValue('enableSendAsGift')) {
        res.json({
            error: true
        });

        return next();
    }

    var BasketMgr = require('dw/order/BasketMgr');
    var URLUtils = require('dw/web/URLUtils');

    var currentBasket = BasketMgr.getCurrentBasket();

    // check the current basket
    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var formErrors = require('*/cartridge/scripts/formErrors');

    var giftForm = server.forms.getForm('gift');
    var fieldErrors = formErrors.getFormErrors(giftForm);
    if (!giftForm.valid || Object.keys(fieldErrors).length) {
        res.json({
            error: true,
            fieldErrors: [fieldErrors],
            serverErrors: []
        });

        return next();
    }

    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    var isGift = giftForm.isGift.checked;
    var giftMessage = isGift && !currentSite.getCustomPreferenceValue('disableGiftMessage') ? giftForm.giftMessage.value : null;
    var isPriceHidden = isGift ? giftForm.isPriceHidden.checked : null;
    var isGiftWrapped = isGift || giftForm.isGiftWrapped.checked;

    var updateCartGiftResult = COHelpers.setGift(currentBasket.defaultShipment, isGift, giftMessage, isPriceHidden, isGiftWrapped);

    if (updateCartGiftResult.error) {
        res.json({
            error: updateCartGiftResult.error,
            fieldErrors: [],
            serverErrors: [updateCartGiftResult.errorMessage]
        });

        return next();
    }


    res.json({
        success: true
    });

    return next();
});

server.replace(
    'AddCoupon',
    server.middleware.https,
    server.middleware.get,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');
        var URLUtils = require('dw/web/URLUtils');
        var CartModel = require('*/cartridge/models/cart');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

        var currentBasket = BasketMgr.getCurrentBasket();

        if (!currentBasket) {
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });

            return next();
        }

        var error = false;
        var errorMessage;

        try {
            Transaction.wrap(function () {
                return currentBasket.createCouponLineItem(req.querystring.couponCode, true);
            });
        } catch (e) {
            error = true;
            var errorCodes = {
                COUPON_CODE_ALREADY_IN_BASKET: 'error.coupon.already.in.cart',
                COUPON_ALREADY_IN_BASKET: 'error.coupon.cannot.be.combined',
                COUPON_CODE_ALREADY_REDEEMED: 'error.coupon.already.redeemed',
                COUPON_CODE_UNKNOWN: 'error.unable.to.add.coupon',
                COUPON_DISABLED: 'error.unable.to.add.coupon',
                REDEMPTION_LIMIT_EXCEEDED: 'error.unable.to.add.coupon.expired',
                CUSTOMER_REDEMPTION_LIMIT_EXCEEDED: 'error.unable.to.add.coupon.expired',
                TIMEFRAME_REDEMPTION_LIMIT_EXCEEDED: 'error.unable.to.add.coupon.expired',
                NO_ACTIVE_PROMOTION: 'error.unable.to.add.coupon',
                default: 'error.unable.to.add.coupon'
            };

            var errorMessageKey = errorCodes[e.errorCode] || errorCodes.default;
            errorMessage = Resource.msg(errorMessageKey, 'cart', null);
        }

        if (error) {
            res.json({
                error: error,
                errorMessage: errorMessage
            });
            return next();
        }

        Transaction.wrap(function () {
            basketCalculationHelpers.calculateTotals(currentBasket);
        });

        var basketModel = new CartModel(currentBasket);

        res.json(basketModel);
        return next();
    }
);

module.exports = server.exports();
