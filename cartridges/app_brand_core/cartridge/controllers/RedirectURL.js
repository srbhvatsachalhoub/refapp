'use strict';
/* global request */

var server = require('server');
server.extend(module.superModule);

/**
 * Extended with setting session locale to request object
 * if request locale is not equal to session locale
 */
server.replace('Start', server.middleware.get, function (req, res, next) {
    var URLRedirectMgr = require('dw/web/URLRedirectMgr');

    var currentLocale = session.custom.locale; // eslint-disable-line no-undef
    if (currentLocale && req.locale.id !== currentLocale) {
        req.setLocale(currentLocale);
    }

    var redirect = URLRedirectMgr.redirect;
    var location = redirect ? redirect.location : null;
    var redirectStatus = redirect ? redirect.getStatus() : null;

    if (!location) {
        res.setStatusCode(410);
        res.render('error/notFound');
    } else {
        if (redirectStatus) {
            res.setRedirectStatus(redirectStatus);
        }
        res.redirect(location);
    }

    next();
});

/**
 * "chalhoub.refapp.com":[
    {
        "if-site-path": "/.well-known/apple-developer-merchantid-domain-association.txt",
        "pipeline": "RedirectURL-Root",
        "locale": "en_AE",
        "params": {
            "content-type": "text/plain" // optional, default is text/html
            "decorator-template": "common/layout/page" // optional, default is empty
        }
    }

    will read content asset id and give as a response -dot-well-dash-known-slash-apple-dash-developer-dash-merchantid-dash-domain-dash-association-dot-txt
 */
server.get('Root', function (req, res, next) {
    var hpm = request.httpParameterMap;
    var path = request.httpHeaders['x-is-path_info'];

    if (!path) {
        return next();
    }

    path = path.substr(1); // remove the slash at the beginning
    path = path.replace(/\-/g, '-dash-'); // eslint-disable-line
    path = path.replace(/\//g, '-slash-'); // replace / with -slash-
    path = path.replace(/\./g, '-dot-'); // replace . with -dot-

    // ContentAsset
    var content = require('dw/content/ContentMgr').getContent(path);

    var viewData = {};
    viewData.contentBody = (content && content.custom.body) ? content.custom.body.markup : ''; // get the content body with all links rewritten for storefront use

    // ContentType
    viewData.contentType = (hpm['content-type']) ? hpm['content-type'].stringValue : 'text/html'; // get the content type

    // DecoratorTemplate
    viewData.decoratorTemplate = (hpm['decorator-template']) ? hpm['decorator-template'].stringValue : ''; // get the decorator template

    res.render('root', viewData);
    return next();
});

module.exports = server.exports();
