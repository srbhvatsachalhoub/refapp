'use strict';

var Logger = require('dw/system/Logger');
var CurrentSite = require('dw/system/Site').current;

module.exports = (function () {
    var disSitePreferncesValue = CurrentSite.getCustomPreferenceValue('disImageConfig');
    var disConfigJSON;
    var disConfigJSONDefault = {
        viewTypeMapping: {
            large: ['hi-res', 'large'],
            medium: ['hi-res', 'large'],
            small: ['hi-res', 'large'],
            swatch: ['hi-res', 'large']
        },
        large: {
            scaleWidth: 800,
            scaleHeight: 1200
        },
        medium: {
            scaleWidth: 400,
            scaleHeight: 600
        },
        small: {
            scaleWidth: 140,
            scaleHeight: 210
        },
        swatch: {
            scaleWidth: 80,
            scaleHeight: 120
        }
    };
    if (disSitePreferncesValue) {
        try {
            disConfigJSON = JSON.parse(disSitePreferncesValue);
        } catch (e) {
            Logger.warn('Error while parsing dis config json: {0}', e.message);
        }
    }
    return disConfigJSON || disConfigJSONDefault;
}());

