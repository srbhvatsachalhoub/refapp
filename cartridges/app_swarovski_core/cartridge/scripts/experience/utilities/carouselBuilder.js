'use strict';

/* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["model"] }] */
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');

/**
 * Helper to encapsulate common code for building a carousel
 *
 * @param {Object} model - model object for a component
 * @param {Object} context - model object for a component
 * @return {Object} model - prepared model
 */
function init(model, context) {
    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    var content = context.content;
    var numberOfSlides = model.regions.slides.region.size;

    model.regions.slides.setClassName('js-global-carousel carousel');
    model.regions.slides.setAttribute('data-carousel-options',
        JSON.stringify({
            slidesToShow: content.mdCarouselSlidesToDisplay,
            slidesToScroll: 1,
            rows: 1,
            dots: context.content.mdCarouselIndicators,
            arrows: true
        }));
    model.regions.slides.setAttribute('data-carousel-options-tablet',
        JSON.stringify({
            slidesToShow: content.smCarouselSlidesToDisplay,
            slidesToScroll: 1,
            rows: 1,
            dots: context.content.smCarouselIndicators,
            arrows: true
        }));
    model.regions.slides.setAttribute('data-carousel-options-mobile',
        JSON.stringify({
            slidesToShow: content.xsCarouselSlidesToDisplay,
            slidesToScroll: 1,
            rows: 1,
            dots: context.content.xsCarouselIndicators,
            arrows: true
        }));
    model.regions.slides.setComponentClassName('carousel-item');
    model.regions.slides.setComponentClassName('carousel-item active');

    for (var i = 0; i < numberOfSlides; i++) {
        model.regions.slides.setComponentAttribute('data-position', i, { position: i });
    }

    if (context.component.typeID === 'einstein.einsteinCarousel'
        || context.component.typeID === 'einstein.einsteinCarouselProduct'
        || context.component.typeID === 'einstein.einsteinCarouselCategory') {
        numberOfSlides = context.content.count;
    }

    model.id = 'carousel-' + context.component.getID();

    model.numberOfSlides = model.regions.slides.region.size;
    if (context.component.typeID === 'einstein.einsteinCarousel'
        || context.component.typeID === 'einstein.einsteinCarouselProduct'
        || context.component.typeID === 'einstein.einsteinCarouselCategory') {
        model.numberOfSlides = context.content.count - 1;
    }
    model.title = context.content.textHeadline ? context.content.textHeadline : null;
    model.fullWidth = context.content.fullWidth ? context.content.fullWidth : false;
    model.product = context.content.productCarousel ? context.content.productCarousel : false;
    return model;
}

module.exports = {
    init: init
};
