'use strict';
var globalSwatchesOptions = require('./../config/swatchesCarousel.json').options;
var carouselSelector = '.js-global-carousel';

/**
 * Gets scrollbar width
 * @param {jQuery} $element - $element
 * @returns {Object} options
 */
function getOption($element) {
    var options;
    var width = $(window).width();
    var selector = 'carouselOptionsMobile';
    if (width >= window.RA_BREAKPOINTS.md) {
        selector = 'carouselOptionsTablet';
    }
    if (width >= window.RA_BREAKPOINTS.lg) {
        selector = 'carouselOptions';
    }
    if ($element.hasClass('js-swatches-carousel')) {
        options = globalSwatchesOptions;
        // config for swatches is global, it is processed as const config from .json
    } else {
        // if config depends on screen width it is processed via data attr, should be seated in .isml
        options = $element.data(selector);
    }
    return options || {};
}

/**
* init all carousels
* @param {jQuery} $container - $element
*/
function initAllCarousels($container) {
    $($container || carouselSelector).each(function () {
        var $this = $(this);
        var options = getOption($this);
        $this.slick(options);
    });
}

/**
* Reinit carousel
* @param {jQuery} $container - $element
*/
function reinitCarousel($container) {
    $($container || carouselSelector).each(function () {
        var $this = $(this);
        if (this.slick) {
            $this.slick('unslick');
        } else {
            $this.removeClass('slick-initialized');
        }
        var options = getOption($this);
        $this.slick(options);
    });
}

/**
* init events for carousels
* @param {jQuery} $element - $element
*/
function initEvents() {
    $(window).on('resize orientationchange', function () {
        reinitCarousel();
    });

    // init carousel for new elements, after update DOM on PLP
    $(document).on('search:success', function () {
        $(carouselSelector).not('.slick-initialized').each(function () {
            initAllCarousels($(this));
        });
    });

    $(document).on('minicart:popover', function () {
        reinitCarousel($('.js-minicart-carousel'));
    });

    $('.js-miniwishlist-carousel-link').on('mouseenter', function () {
        reinitCarousel($('.js-miniwishlist-carousel'));
    });
}

/**
 * init carousel
 */
function init() {
    initAllCarousels();
    initEvents();
}

module.exports = {
    init: init
};
