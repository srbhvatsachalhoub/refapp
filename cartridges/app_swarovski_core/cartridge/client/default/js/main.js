window.jQuery = window.$ = require('jquery');
var processInclude = require('base/util');
window.RA_BREAKPOINTS = require('./config/breakpoints.json').breakpoints;

$(document).ready(function () {
    processInclude(require('brand_core/dataStates'));
    processInclude(require('brand_core/components/tooltip'));
    processInclude(require('./components/selectpicker'));
    processInclude(require('brand_core/components/login'));
    processInclude(require('brand_core/components/cookieWarning'));
    processInclude(require('brand_core/header'));
    processInclude(require('brand_core/product/addToCartNotification'));
    processInclude(require('base/components/footer'));
    processInclude(require('brand_core/components/miniCart'));
    processInclude(require('base/components/collapsibleItem'));
    processInclude(require('brand_core/components/clientSideValidation'));
    processInclude(require('base/components/countrySelector'));
    processInclude(require('brand_core/newsletterSubscribe/newsletter'));
    processInclude(require('brand_core/components/cleave'));
    processInclude(require('brand_core/components/selectdate'));
    processInclude(require('brand_core/components/password'));
    processInclude(require('brand_core/components/multilineEllipsis'));
    processInclude(require('./product/productCarousel'));
    processInclude(require('./components/bannerCarousel'));
    processInclude(require('./components/findYourStore'));
    processInclude(require('./components/videoEmbed'));
    processInclude(require('./components/viewMore'));
    processInclude(require('./components/readMore'));
    processInclude(require('base/components/toolTip'));
    processInclude(require('./product/productTileAddToCart'));
    processInclude(require('./carousel/globalCarousel'));
    processInclude(require('./components/announcementsCarousel'));
    processInclude(require('./components/menuSlider'));
    processInclude(require('./header'));
});

/* Third party scripts */
require('bootstrap/js/src/util.js');
require('bootstrap/js/src/alert.js');
require('bootstrap/js/src/button.js');
require('bootstrap/js/src/collapse.js');
require('bootstrap/js/src/dropdown.js');
require('bootstrap/js/src/modal.js');
require('bootstrap/js/src/tab.js');
require('bootstrap/js/src/tooltip.js');
require('bootstrap-select');
require('slick-carousel');
require('base/components/spinner');

/* jQuery Plugins */
processInclude(require('brand_core/plugins/sticky'));
processInclude(require('brand_core/plugins/productLoader'));
processInclude(require('brand_core/plugins/consentTracking'));
processInclude(require('brand_core/plugins/searchWithSuggestions'));
processInclude(require('brand_core/plugins/dummyCarousel'));
