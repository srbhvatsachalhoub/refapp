'use strict';

var productCarousel = require('../product/productCarousel');
var quantitySelector = require('../product/quantitySelector');
var countCharacters = require('brand_core/helpers/countCharacters');

module.exports = function () {
    var promoCount = 0;

    // Init quantity selector
    quantitySelector.init();

    // Init char counter
    countCharacters();

    // Toggle gift wrapping textarea
    $('.js-gift-toggle').change(function () {
        if ($(this).is(':checked')) {
            $('.js-gift-form-item').removeClass('d-none');
        } else {
            $('.js-gift-form-item').addClass('d-none');
        }
    });

    // Update discount totals on cart update
    $('body').on('cart:updateTotals', function (e, data) {
        $('.js-error-message').empty();
        if (data.totals.orderLevelDiscountTotal.value > 0) {
            $('.sub-total').addClass('line-through');
            $('.js-discounted-sub-total')
                .removeClass('d-none')
                .html(data.totals.orderLevelDiscountTotal.discountedSubTotal.formatted);
            $('.js-discount-value-line').removeClass('d-none');
            $('.js-discount-value').html(data.totals.orderLevelDiscountTotal.formatted);
        } else {
            $('.js-discounted-sub-total').addClass('d-none');
            $('.js-discount-value-line').addClass('d-none');
            $('.sub-total').removeClass('line-through');
        }
        // Hide promo applied label after removing a promo code
        var NewPromoCount = $('.coupons-and-promos').children().length;
        if (NewPromoCount < promoCount) {
            $('.js-have-promo-label').addClass('d-none');
        }
        promoCount = NewPromoCount;
    });

    // Hide promo applied label on error
    $('body').on('cart:errorMessageUpdate', function () {
        $('.js-have-promo-label').addClass('d-none');
    });

    // Show promo applied label when valid coupon code applied
    $('body').on('cart:promoApplied', function () {
        $('.js-have-promo-label').removeClass('d-none');
    });

    // Reinit free shipping products slider after cart total price update
    $('body').on('cart:updateFreeShippingApproachingProducts', function () {
        productCarousel('.js-free-shipping-approaching-carousel');
    });

    // make sure the gift message is one line
    $('body').on('keypress', '#id_dwfrm_gift_giftMessage', function (e) {
        return (e.keyCode || e.which || e.charCode || 0) !== 13;
    });

    $('body').on('cart:update', function () {
        var $minicart = $('.minicart');

        var count = parseInt($minicart.find('.minicart-quantity').text(), 10);

        if (count === 0) {
            var $popover = $minicart.find('.popover');
            $popover.addClass('popover-empty-cart');
            $popover.html($('.empty-cart-popover-content').html());
        }
    });
};
