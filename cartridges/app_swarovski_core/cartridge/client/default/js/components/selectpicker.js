'use strict';

module.exports = function (selector) {
    $(selector || '.custom-select').selectpicker({
        style: '',
        showSubtext: true
    });
};
