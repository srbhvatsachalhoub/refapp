'use strict';

var carousel = require('brand_core/components/carousel');

module.exports = function (selector) {
    $(selector || '.js-announcement-carousel').each(function () {
        $(this).on('init', function () {
            $(this).closest('.anouncement__slider_all').addClass('ready');
        });

        carousel($(this), {
            dots: false,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 6000
        });
    });
};
