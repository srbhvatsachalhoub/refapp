'use strict';

var noUiSlider = require('nouislider');
var urlHelper = require('brand_core/helpers/urlHelper');
var base = require('brand_core/search/search');
var userAgent = require('brand_core/helpers/userAgent');
var selectpicker = require('../components/selectpicker');
var applyFilter = base.getApplyFilterMethod();
var isPriceSlider = false;

base.initPriceSlider = function () {
    var $sliders = $('.js-price-refinement-slider');
    var selectedMinStr = urlHelper.getParameter('pmin');
    var selectedMaxStr = urlHelper.getParameter('pmax');
    var selectedMin = parseFloat((selectedMinStr || '0').replace(/,(.+?)$/, '.$1'));
    var selectedMax = parseFloat((selectedMaxStr || '0').replace(/,(.+?)$/, '.$1'));

    $sliders.each(function () {
        if (this.noUiSlider && this.noUiSlider.destroy) {
            this.noUiSlider.destroy();
        }

        var $slider = $(this);
        var $value = $slider.parent().find('.js-price-refinement-value');
        var url = $slider.data('url');
        var template = $slider.data('template');
        var min = parseFloat($slider.data('min'));
        var max = parseFloat($slider.data('max'));
        var startedMin;
        var startedMax;

        if (selectedMin < min) selectedMin = min;
        if (selectedMax > max) selectedMax = max;

        noUiSlider.create(this, {
            start: [selectedMin || min, selectedMax || max],
            direction: window.RA_DIRECTION,
            connect: true,
            step: 10,
            range: {
                min: min,
                max: max
            }
        });

        this.noUiSlider.on('start', function (values) {
            startedMin = values[0];
            startedMax = values[1];
        });

        this.noUiSlider.on('update', function (values) {
            if (selectedMinStr || selectedMaxStr) {
                $slider.addClass('slider-active');
            } else {
                $slider.removeClass('slider-active');
            }

            $value.html(
                template
                    .replace('{pmin}', Math.floor(values[0]))
                    .replace('{pmax}', Math.ceil(values[1]))
            );
        });

        this.noUiSlider.on('set', function (values, handle) {
            if (handle === 0 && startedMin === values[0]) return;
            if (handle === 1 && startedMax === values[1]) return;
            url = url.replace(/(pmin=)(.+?)(?=&)/, '$1' + values[0]);
            if ((/(pmax=)(.+?)(?=&)/).test(url)) {
                url = url.replace(/(pmax=)(.+?)(?=&)/, '$1' + values[1]);
            } else {
                // if pmax is the last parameter in url
                url = url.replace(/(pmax=)(.+?)(?=$)/, '$1' + values[1]);
            }
            isPriceSlider = true;
            applyFilter(url, 'refinement');
        });
    });

    $(document)
        .off('search:refinement:success', base.initPriceSlider)
        .on('search:refinement:success', base.initPriceSlider);
};

base.sortOption = function () {
    $(document)
        .on('click', '.js-sort-option', function (e) {
            e.preventDefault();
            $('.js-sort-option').removeClass('selected');
            $(this).addClass('selected');
        })
        .on('click', '.js-apply-sort-option-btn', function (e) {
            e.preventDefault();
            var $selected = $('.js-sort-option.selected');
            var $sortSelecbox = $('.js-sort-options-selectbox');
            var newUrl = $selected.attr('href');
            var oldUrl = $sortSelecbox.val();

            if (oldUrl !== newUrl) {
                $sortSelecbox.val(newUrl).trigger('change');
            }
        })
        .on('state:add:open', '.js-sort-container', function () {
            $('body').addClass('disable-scroll');
            var $sortSelecbox = $('.js-sort-options-selectbox');
            var url = $sortSelecbox.val();
            $('.js-sort-option[href="' + url + '"]').trigger('click');
        })
        .on('state:remove:open', '.js-sort-container', function () {
            $('body').removeClass('disable-scroll');
        });
};

base.initSelectpicker = function () {
    selectpicker('.js-search-results select');

    $(document)
        .off('search:refinement:success', base.initSelectpicker)
        .on('search:refinement:success', base.initSelectpicker);
};

base.initOnRefinementSuccess = function () {
    var $previousRefinements = null;

    $(document)
        .on('search:refinement:start', function () {
            var $currentRefinements = $('.js-refinement-container [data-refinement]');
            if ($previousRefinements && $previousRefinements.length) {
                $previousRefinements.each(function () {
                    var $this = $(this);
                    var id = $this.data('refinement');
                    if ($currentRefinements.filter('[data-refinement="' + id + '"]').length === 0) {
                        $currentRefinements.push(this);
                    }
                });
            }
            $previousRefinements = $currentRefinements;
        })
        .on('search:refinement:success', function () {
            var $refinementContainer = $('.js-refinement-container');
            var $openRefinements = $previousRefinements.filter('.open');
            $refinementContainer.find('.open').removeClass('open');
            $refinementContainer.find('[data-refinement]').each(function () {
                var $this = $(this);
                var id = $this.data('refinement');
                if ($openRefinements.filter('[data-refinement="' + id + '"]').length ||
                    $previousRefinements.filter('[data-refinement="' + id + '"]').length === 0) {
                    $this.addClass('open');
                }
            });
        });
};

base.refinements = function () {
    var refinementValue = null;
    var $body = $('body');
    $(document)
        .on('state:add:open', '.js-refinement-container', function () {
            $body.addClass('disable-scroll');
            if (userAgent.isMobileSafari) {
                $body.addClass('mobile-safari');
            }
        })
        .on('state:remove:open', '.js-refinement-container', function () {
            $body.removeClass('disable-scroll');
        })
        .on('search:refinement:start', function (e) {
            refinementValue = $(e.target.activeElement).data('refinement-value');
        })
        .on('search:refinement:success', function () {
            if ($('.js-refinement-container').hasClass('open')) {
                var elPriceSlider = $('.js-price-refinement-slider').get(0);
                var elRefinement = $('a[data-refinement-value="' + refinementValue + '"]').get(0);
                if (elRefinement) {
                    elRefinement.scrollIntoView();
                } else if (isPriceSlider && elPriceSlider) {
                    elPriceSlider.scrollIntoView();
                }
            }
            if ($('.js-no-result-reset-filter-btn').length) {
                $body.removeClass('disable-scroll');
            }
            isPriceSlider = false;
        });
};

base.initContentSearch = function () {
    var $container = $('.js-content-result-container');

    // Don't go further, if there isn't a content result element in the page
    if ($container.length === 0) return;

    // Don't go further, if content search results are empty
    if ($container.find('.js-content-result-empty').length) $container.remove();

    // Show "Related Content" title
    $container.find('.js-content-result-title').removeClass('d-none');

    // Add view more functionality
    $(document).on('click', '.js-content-view-more-btn', function () {
        var $btn = $(this);
        $.spinner().start();
        $.ajax({
            method: 'GET',
            url: $btn.data('url'),
            success: function (response) {
                $btn.closest('.js-content-view-more-container').remove();
                $container.find('.js-content-result-placeholder').append(response);
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    });
};

base.setSearchKeyword = function () {
    var $keyword = $('.js-search-keyword');
    if ($keyword.length) {
        var $input = $('.js-sws-input');
        $input.val($keyword.val());
        $('.js-toggle-search-mobile').attr('data-no-focus', true).trigger('click');
    }
};

base.categoryList = function () {
    $(document).on('click', '.js-category-list-show', function () {
        var $targets = $('.js-category-list');
        if (!$($targets[0]).hasClass('active')) {
            $targets.each(function () {
                var $el = $(this);
                $el.addClass('active');
            });
        } else {
            $targets.each(function () {
                var $el = $(this);
                $el.removeClass('active');
            });
        }
    });
};

base.filter =
base.closeRefinements =
base.resize = function () {
    // Intentionally empty. Don't remove this function.
};

module.exports = base;
