'use strict';

var base = require('brand_core/checkout/billing');
var billingCustom = require('./billingCustom');

base.addressCustom = billingCustom.initialize;

module.exports = base;
