'use strict';

var base = module.superModule;

var countries = require('*/cartridge/config/countries');
var ApiLocale = require('dw/util/Locale');
var Resource = require('dw/web/Resource');

/**
 * returns object needed to render links to change the locale of the site
 * @param {dw.util.Locale} currentLocale - the current loale
 * @returns {Array} - array of Objects representing available locales
 */
function getLocaleLinks(currentLocale) {
    var localeLanguages = [];
    var localeCountries = [];

    countries.forEach(function (locale) {
        // in ar language, because of the Numeric System, the ID of the locale can be like: ar_KW_#u-nu-latn
        if (locale.id === currentLocale.ID.slice(0, 5)) {
            return;
        }

        var countryLocale = ApiLocale.getLocale(locale.id);
        if (countryLocale.country === currentLocale.country && countryLocale.language !== currentLocale.language) {
            localeLanguages.push({
                localID: locale.id,
                language: countryLocale.language,
                displayName: countryLocale.displayLanguage
            });
        }

        if (countryLocale.country !== currentLocale.country
            && locale.default === true
            && !localeCountries.some(function (country) { return country.country === countryLocale.country; })) {
            localeCountries.push({
                localID: locale.id,
                country: countryLocale.country,
                displayName: Resource.msg(countryLocale.country, 'country', countryLocale.ISO3Country),
                currencyCode: locale.currencyCode
            });
        }
    });

    return {
        languages: localeLanguages,
        countries: localeCountries
    };
}
/**
 * returns object needed to render links to change the locale of the site
 * @returns {Array} - array of Objects representing available locales
 */
function getAllLocaleLinks() {
    var localeCountries = {};

    countries.forEach(function (locale) {
        var countryLocale = ApiLocale.getLocale(locale.id);
        localeCountries[countryLocale.country] = localeCountries[countryLocale.country] || {};
        localeCountries[countryLocale.country].languages = localeCountries[countryLocale.country].languages || [];
        if (!('country' in localeCountries[countryLocale.country])) {
            localeCountries[countryLocale.country].country = {
                localID: locale.id,
                country: countryLocale.country,
                language: countryLocale.language,
                displayName: Resource.msg(countryLocale.country, 'country', countryLocale.ISO3Country)
            };
        }

        localeCountries[countryLocale.country].languages.push({
            localID: locale.id,
            currencyCode: locale.currencyCode,
            language: countryLocale.language,
            displayName: countryLocale.displayLanguage
        });
    });

    return {
        countries: localeCountries
    };
}

/**
 * Represents current locale information in plain object
 * @param {dw.util.Locale} currentLocale - current locale of the request
 * @param {string} allowedLocales - list of allowed locales for the site
 * @param {string} siteId - id of the current site
 * @constructor
 */
function Locale(currentLocale, allowedLocales, siteId) {
    base.call(this, currentLocale, allowedLocales, siteId);

    this.locale.localLinks = getLocaleLinks(currentLocale);
    this.locale.allLocalLinks = getAllLocaleLinks(currentLocale);
}

module.exports = Locale;
