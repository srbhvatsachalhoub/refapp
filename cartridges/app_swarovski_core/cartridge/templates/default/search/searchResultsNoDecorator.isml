<isinclude template="/components/modules" sf-toolkit="off" />

<isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
    <isinclude template="reporting/reportingUrls" />
</isif>

<isif condition="${pdict.productSearch.searchKeywords}">
    <input type="hidden" class="js-search-keyword" value="${pdict.productSearch.searchKeywords}" />
</isif>

<isif condition="${pdict.productSearch.productIds.length > 0}">
    <div class="js-search-results search-results-wrapper">
        <isif condition="${pdict.productSearch.isCategorySearch}">
            <div class="container plp-details d-none">
                <isif condition="${pdict.productSearch.category.name}">
                    <h1 class="plp-title">
                        <isprint value="${pdict.productSearch.category.name}" />
                    </h1>
                </isif>
                <isif condition="${pdict.productSearch.category.description}">
                    <p class="plp-description">
                        <isprint value="${pdict.productSearch.category.description}" />
                    </p>
                </isif>
            </div>
            <isslot id="cat-plp-top" context="category" description="Product Listing Pages Top Content"
                context-object="${pdict.productSearch.category}" />
        </isif>
        <div class="search-results ${pdict.productSearch.isCategorySearch ? 'plp-results' : 'srp-results'}">
            <iscomment>Product Grid Header</iscomment>
            <div class="grid-header">
                <div class="container-fluid">
                     <div class="row">
                        <div class="col-5 grid-header-row breadcrumb-container-wrapper">
                            <isinclude template="components/breadcrumbs/pageBreadcrumbs"/>
                        </div>
                        <div class="col-7 grid-header-row">
                            <div class="grid-view-container">
                                <span class="grid-view-item swa-icon swa-icon-th-large js-grid-large" 
                                    data-remove-classes="col-6 col-lg-3" 
                                    data-add-classes=" col-12 col-lg-6 product-tile-list" 
                                    data-target-selector=".js-product-tile-wrapper"
                                    data-add-class-open=".js-product-grid"
                                    data-add-class-active=".js-grid-large"
                                    data-remove-class-active=".js-grid-small">
                                </span>
                                <span class="grid-view-item swa-icon swa-icon-th js-grid-small active" 
                                    data-remove-classes="col-12 col-lg-6 product-tile-list" 
                                    data-add-classes="col-6 col-lg-3" 
                                    data-target-selector=".js-product-tile-wrapper"
                                    data-remove-class-open=".js-product-grid"
                                    data-add-class-active=".js-grid-small"
                                    data-remove-class-active=".js-grid-large">
                                </span>
                            </div>
                            <isif condition="${pdict.productSearch.productIds.length > 0}">
                                <div class="btn-sort-by">
                                    <button type="button" class="btn-sort-by-btn swa-icon swa-icon-dual-arrow"
                                        data-toggle-class-active=".btn-sort-by-btn"
                                        data-toggle-class-open=".js-sort-container"
                                        data-add-class-d-none=".js-refinement-container, .js-refinement-overlay"
                                        data-remove-class-open=".js-refinement-container, .js-refinement-overlay">
                                        <span class="d-none d-lg-inline">${Resource.msg('label.sortby', 'search', null)}</span>
                                    </button>
                                    <isinclude template="search/sortOrderMenu" />
                                </div>
                            </isif>
                            <div class="btn-refine-by">
                                <button type="button" class="btn-refine-by-btn swa-icon swa-icon-preferences"
                                    data-toggle-class-active=".btn-refine-by-btn"
                                    data-toggle-class-open=".js-refinement-container, .js-refinement-overlay"
                                    data-toggle-class-d-none=".js-refinement-container, .js-refinement-overlay"
                                    data-remove-class-open=".js-sort-container">
                                    <span class="d-none d-lg-inline">${Resource.msg('button.showrefinements', 'search', null)}</span>
                                </button>
                            </div>
                        </div>
                     </div>
                </div>
            </div>

            <isif condition="${pdict.category.parent && pdict.category.parent.root === false}">
                <div class="breadcrumb-carousel-wrapper js-category-list">
                    <div class="global-carousel-slider breadcrumb-carousel product-carousel container js-global-carousel" 
                        data-carousel-options='{
                            "slidesToShow": 6,
                            "slidesToScroll": 1,
                            "rows": 1,
                            "dots": false,
                            "arrows": true,
                            "infinite": false
                        }' 
                        data-carousel-options-tablet='{
                            "slidesToShow": 4,
                            "slidesToScroll": 1,
                            "rows": 1,
                            "dots": false,
                            "arrows": true,
                            "infinite": false
                        }' 
                        data-carousel-options-mobile='{
                            "slidesToShow": 3,
                            "slidesToScroll": 1,
                            "rows": 1,
                            "dots": false,
                            "arrows": true,
                            "infinite": false
                        }'>

                        <isloop items="${pdict.category.parent.onlineSubCategories}" var="breadcrumbCategory">
                            <div class="breadcrumb-carousel-item">
                                <a class="breadcrumb-carousel-item-link" href="${URLUtils.url('Search-Show', 'cgid', breadcrumbCategory.ID)}">
                                    <img class="carousel-item-image" src="${breadcrumbCategory.thumbnail ? breadcrumbCategory.thumbnail.url : URLUtils.absStatic('images/blank.gif')}" alt="" />

                                    <div class="carousel-item-title">${breadcrumbCategory.displayName}</div>
                                </a>
                            </div>
                        </isloop>
                    </div>
                </div>
            </isif>

            <iscomment>Product Grid</iscomment>
            <div class="product-grid-wrapper" itemtype="http://schema.org/SomeProducts" itemid="#product">
                <div class="container-fluid">
                    <div class="product-grid row js-product-grid">
                        <isif condition="${pdict.category.custom.clpBannerImage && pdict.category.custom.clpBannerImage.getURL() && pdict.productSearch.category.name}">
                            <div class="col-lg-6 product-tile-wrapper plp-slot-wrapper">
                                <img class="plp-slot-image" src="${pdict.category.custom.clpBannerImage.getURL()}" alt="${pdict.productSearch.category.name}"/>
                                <h1 class="plp-title">
                                    <isprint value="${pdict.productSearch.category.name}" />
                                </h1>
                            </div>
                        </isif>
                        <isinclude template="search/productGrid" />
                    </div>
                </div>

                <iscomment>Refinement bar</iscomment>
                <div class="refinement-bar-overlay js-refinement-overlay d-none" 
                    data-remove-class-open=".js-refinement-container, .js-refinement-overlay"
                    data-add-class-d-none=".js-refinement-container, .js-refinement-overlay">
                </div>
                <div class="refinement-bar mobile-modal js-mobile-modal js-refinement-container d-none" data-reset-link="${pdict.productSearch.resetLink}">
                    <isinclude url="${pdict.refineurl}" />
                </div>
            </div>
        </div>

        <isif condition="${pdict.productSearch.isCategorySearch}">
            <isslot id="cat-plp-bottom" context="category" description="Product Listing Pages Bottom Content"
                context-object="${pdict.productSearch.category}" />
        </isif>
    </div>
<iselse/>
    <isinclude template="search/noResultsProduct" />
</isif>

<div class="js-content-result-container">
    <div class="container mb-0 d-none js-content-result-title">
        <div class="row border-top pt-4">
            <div class="col">
                <h2 class="container-title">${Resource.msg('title.relatedcontent', 'common', null)}</h2>
            </div>
        </div>
    </div>
    <div class="js-content-result-placeholder">
        <isinclude url="${URLUtils.url('Search-Content', 'q', pdict.productSearch.searchKeywords, 'startingPage', 0, 'pageSize', 1)}" />
    </div>
</div>

<isif condition="${pdict.srpContentAssetId}">
    <iscontentasset aid="${'srp-bottom-' + pdict.srpContentAssetId}" />
</isif>
