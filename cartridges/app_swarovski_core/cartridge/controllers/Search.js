'use strict';

var server = require('server');

server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');

server.replace('ShowAjax', cache.applyShortPromotionSensitiveCache, consentTracking.consent, function (req, res, next) {
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');

    var result = searchHelper.search(req, res);

    var currentCategory = CatalogMgr.getCategory(result.productSearch.category.id);
    var breadcrumbs = [];
    while (currentCategory.ID !== 'root' && currentCategory !== null) {
        breadcrumbs.unshift(
            {
                htmlValue: currentCategory.displayName,
                url: URLUtils.url('Search-Show', 'cgid', currentCategory.ID)
            }
        );
        currentCategory = currentCategory.parent;
    }
    if (breadcrumbs && breadcrumbs.length) {
        breadcrumbs.unshift(
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            }
        );
        res.setViewData({
            breadcrumbs: breadcrumbs
        });
    }

    if (result.searchRedirect) {
        res.redirect(result.searchRedirect);
        return next();
    }

    res.render('search/searchResultsNoDecorator', {
        productSearch: result.productSearch,
        maxSlots: result.maxSlots,
        reportingURLs: result.reportingURLs,
        refineurl: result.refineurl,
        srpContentAssetId: result.srpContentAssetId,
        querystring: req.querystring
    });

    return next();
}, pageMetaData.computedPageMetaData);


server.replace('Show', server.middleware.get, cache.applyShortPromotionSensitiveCache, consentTracking.consent, function (req, res, next) {
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
    var template = 'search/searchResults';

    var apiProductSearch = new ProductSearchModel();
    var viewData = {
        apiProductSearch: apiProductSearch
    };
    res.setViewData(viewData);

    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var result = searchHelper.search(req, res);

        if (result.searchRedirect) {
            res.redirect(result.searchRedirect);
            return;
        }

        if (result.category && result.categoryTemplate) {
            template = result.categoryTemplate;
        }

        if (result.category && result.category.pageTitle == null) {
            req.pageMetaData.pageMetaTags.forEach(function (pageMetaTag) {
                if (pageMetaTag.ID === 'title') {
                    pageMetaHelper.setPageMetaData(req.pageMetaData, { pageTitle: pageMetaTag.content });
                }
            });
        }

        // add subCategory for CLP , only if it is configurabled in BM
        if (result.category && result.category.custom.clpSubcategoryMenuItems) {
            result.subCategoriesCLP = searchHelper.getSubCatgories(result.category) || null;
        }

        // get config for products count to be shown in search result
        var searchResultProductsCount = Site.current.getCustomPreferenceValue('searchResultProductsCount');

        // add breadcrumbs
        var breadcrumbs = [];
        if (viewData &&
            result.productSearch &&
            result.productSearch.isCategorySearch &&
            !result.productSearch.isRefinedCategorySearch) {
            var currentCategory = CatalogMgr.getCategory(result.productSearch.category.id);
            while (currentCategory.ID !== 'root' && currentCategory !== null) {
                breadcrumbs.unshift(
                    {
                        htmlValue: currentCategory.displayName,
                        url: URLUtils.url('Search-Show', 'cgid', currentCategory.ID)
                    }
                );
                currentCategory = currentCategory.parent;
            }
            if (breadcrumbs && breadcrumbs.length) {
                breadcrumbs.unshift(
                    {
                        htmlValue: Resource.msg('global.home', 'common', null),
                        url: URLUtils.home().toString()
                    }
                );
                res.setViewData({
                    breadcrumbs: breadcrumbs
                });
            }
        }
        res.render(template, {
            searchResultProductsCount: searchResultProductsCount || null,
            productSearch: result.productSearch,
            subCategoriesCLP: result.subCategoriesCLP,
            maxSlots: result.maxSlots,
            reportingURLs: result.reportingURLs,
            refineurl: result.refineurl,
            category: result.category ? result.category : null,
            canonicalUrl: result.canonicalUrl,
            schemaData: result.schemaData,
            srpContentAssetId: result.srpContentAssetId,
            querystring: req.querystring
        });
    });
    return next();
});

module.exports = server.exports();
