'use strict';

var server = require('server');

server.extend(module.superModule);
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

server.append(
    'Track',
    function (req, res, next) {
        var viewData = res.getViewData();
        viewData.breadcrumbs = [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('link.header.track.order', 'login', null),
                url: URLUtils.url('OrderTracking-Show').toString()
            },
            {
                htmlValue: Resource.msg('label.order.details', 'account', null)
            }
        ];
        res.setViewData(viewData);
        next();
    }
);

module.exports = server.exports();
