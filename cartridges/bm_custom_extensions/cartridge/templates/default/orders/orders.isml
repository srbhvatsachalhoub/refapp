<isdecorate template="application/MenuFrame">
    <!-- Page Navigator -->
    <isinclude template="inc/Modules" />
    <isset name="TOP_URL" value="${URLUtils.url('SiteNavigationBar-ShowMenuitemOverview', 'CurrentMenuItemId', pdict.CurrentHttpParameterMap
                                 .CurrentMenuItemId)}" scope="page" />
    <ISBreadcrumb name1="Merchant Tools" name2="${pdict.CurrentHttpParameterMap.mainmenuname.stringValue}"
        url2="${TOP_URL.toString()}" name3="${pdict.CurrentHttpParameterMap.menuname.stringValue}" />
    <!-- EO Page Navigator -->

    <div>
        <h1 class="overview_title" style="float: left;">
            Order Export
        </h1>
    </div>
    <div style="clear: both;"></div>
    <div class="overview_title_description" style="margin-top: 1em;">
        <p class="bold">
            You're using the custom Order Export tool.
        </p>
        <p>
            This page allows you to export the filtered order list as an excel file. Each criteria you select below is
            combined with the logical operator <b>and</b> and the result is exported to an excel file.
        </p>
        <p>
            If you check <b>Include Line Items</b> checkbox and export, the excel file will include the line items of
            the order as well.
        </p>
    </div>

    <form action="${URLUtils.url('BMOrderExports-ExportOrders')}" method="get">
        <table class="w e s n" width="100%">
            <isif condition="${pdict.noResult === 1}">
                <tr>
                    <td colspan="4">
                        <table border="0" cellspacing="0" cellpadding="4" width="100%" class="confirm_box s">
                            <tbody>
                                <tr>
                                    <td class="confirm" width="100%">
                                        <p>No order has been found satisfying the given filter values!</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </isif>
            <tr>
                <td class="fielditem2">Created Date From:&nbsp;</td>
                <td>
                    <isset name="dateInputFieldName" value="createdDateFrom" scope="page" />
                    <isinclude template="components/dateInput" />
                </td>
            </tr>
            <tr>
                <td class="fielditem2">Created Date To:&nbsp;</td>
                <td>
                    <isset name="dateInputFieldName" value="createdDateTo" scope="page" />
                    <isinclude template="components/dateInput" />
                </td>
            </tr>
            <tr>
                <td class="fielditem2">Modified Date From:&nbsp;</td>
                <td>
                    <isset name="dateInputFieldName" value="modifiedDateFrom" scope="page" />
                    <isinclude template="components/dateInput" />
                </td>
            </tr>
            <tr>
                <td class="fielditem2">Modified Date To:&nbsp;</td>
                <td>
                    <isset name="dateInputFieldName" value="modifiedDateTo" scope="page" />
                    <isinclude template="components/dateInput" />
                </td>
            </tr>
            <isset name="OrderClass" value="${dw.order.Order}" scope="page" />
            <tr>
                <td class="fielditem2" nowrap="nowrap">Order Status:&nbsp;</td>
                <td class="table_detail" width="100%">
                    <select name="orderStatus" class="select">
                        <option value="-1" selected="selected">Any</option>
                        <option value="${OrderClass.ORDER_STATUS_CREATED}">Created</option>
                        <option value="${OrderClass.ORDER_STATUS_NEW}">New</option>
                        <option value="${OrderClass.ORDER_STATUS_OPEN}">Open</option>
                        <option value="${OrderClass.ORDER_STATUS_COMPLETED}">Completed</option>
                        <option value="${OrderClass.ORDER_STATUS_CANCELLED}">Cancelled</option>
                        <option value="${OrderClass.ORDER_STATUS_REPLACED}">Replaced</option>
                        <option value="${OrderClass.ORDER_STATUS_FAILED}">Failed</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="fielditem2" nowrap="nowrap">Payment Status:&nbsp;</td>
                <td class="table_detail" width="100%">
                    <select name="paymentStatus" class="select">
                        <option value="-1" selected="selected">Any</option>
                        <option value="${OrderClass.PAYMENT_STATUS_NOTPAID}">Not Paid</option>
                        <option value="${OrderClass.PAYMENT_STATUS_PARTPAID}">Partly Paid</option>
                        <option value="${OrderClass.PAYMENT_STATUS_PAID}">Paid</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="fielditem2" nowrap="nowrap">Shipping Status:&nbsp;</td>
                <td class="table_detail" width="100%">
                    <select name="shippingStatus" class="select">
                        <option value="-1" selected="selected">Any</option>
                        <option value="${OrderClass.SHIPPING_STATUS_NOTSHIPPED}">Not Shipped</option>
                        <option value="${OrderClass.SHIPPING_STATUS_PARTSHIPPED}">Partly Shipped</option>
                        <option value="${OrderClass.SHIPPING_STATUS_SHIPPED}">Shipped</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="fielditem2" nowrap="nowrap">Confirmation Status:&nbsp;</td>
                <td class="table_detail" width="100%">
                    <select name="confirmationStatus" class="select">
                        <option value="-1" selected="selected">Any</option>
                        <option value="${OrderClass.CONFIRMATION_STATUS_CONFIRMED}">Confirmed</option>
                        <option value="${OrderClass.CONFIRMATION_STATUS_NOTCONFIRMED}">Not Confirmed</option>
                    </select>
                </td>
            </tr>
            <isif condition="${pdict.countries}">
                <tr>
                    <td class="fielditem2" nowrap="nowrap">Country:&nbsp;</td>
                    <td class="table_detail" width="100%">
                        <select name="country" class="select">
                            <option value="" selected="selected">Any</option>
                            <isloop items="${pdict.countries}" var="country">
                                <option value="${country.code}">${country.name}</option>
                            </isloop>
                        </select>
                    </td>
                </tr>
            </isif>
            <tr>
                <td class="fielditem2" nowrap="nowrap">Include Line Items:&nbsp;</td>
                <td class="table_detail" width="100%">
                    <input type="checkbox" name="includeLineItems" value="true" />
                </td>
            </tr>
        </table>

        <table class="w e s n" width="100%">
            <tr>
                <td align="right">
                    <button type="submit" class="button js-export-btn">Export Orders</button>
                </td>
            </tr>
        </table>
        <input class="js-order-type-selector" type="hidden" name="ordersWithLineItems" />
    </form>
</isdecorate>
