'use strict';

/**
 *
 * @param {Object} product - Current product object
 * @returns {Object} - Return the given object with a new property
 */
function getCustom(product) {
    var newObject = {};
    if (!product.custom) return newObject;

    Object.keys(product.custom)
        .forEach(function (key) {
            newObject[key] = product.custom[key];
        });

    return newObject;
}

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'custom', {
        enumerable: true,
        value: getCustom(apiProduct)
    });
};
