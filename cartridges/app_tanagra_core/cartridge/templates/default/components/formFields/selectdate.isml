<iscomment>Date Select Component</iscomment>
<isscript>
    var formfield = pdict.selectdate_formfield;
    var value = pdict.selectdate_value || pdict.selectdate_formfield.value;
    var currentDate = new Date();
    var day, month, year, yearStart, yearEnd, yearCount = 100;
    if (value && (value instanceof Date)) {
        day = value.getDate();
        month = value.getMonth() + 1;
        year = value.getFullYear();
    }
    yearStart = currentDate.getFullYear();
</isscript>
<div class="form-group js-selectdate
    ${formfield.mandatory === true ? 'required' : ''}
    ${formfield.htmlName}">

    <label class="form-control-label">
        ${pdict.selectdate_label || pdict.selectdate_formfield.label}
    </label>

    <div class="row">
        <div class="col-4">
            <select class="form-control custom-select js-day ${pdict.selectdate_class || ''}"
                data-size="6"
                data-width="auto"
                title="${Resource.msg('label.date.day', 'forms', null)}"
                ${pdict.selectdate_disabled ? 'disabled' : ''}>
                <option class="form-control" value="">${Resource.msg('label.date.day', 'forms', null)}</option>
                <isloop begin="1" end="31" step="1" status="status">
                    <option
                        class="form-control"
                        value="${status.index}"
                        ${day && day === status.index ? 'selected' : ''}>
                        ${Resource.msg('label.date.day.' + ((status.index < 10 ? '0' : '') + status.index), 'forms', null)}
                    </option>
                </isloop>
            </select>
        </div>

        <div class="col-4">
            <select class="form-control custom-select js-month ${pdict.selectdate_class || ''}"
                data-size="6"
                data-width="auto"
                title="${Resource.msg('label.date.month', 'forms', null)}"
                ${pdict.selectdate_disabled ? 'disabled' : ''}>
                <option class="form-control" value="">${Resource.msg('label.date.month', 'forms', null)}</option>
                <isloop begin="1" end="12" step="1" status="status">
                    <option
                        class="form-control"
                        value="${status.index-1}"
                        ${month && month === status.index ? 'selected' : ''}>
                        ${Resource.msg('label.date.month.' + ((status.index < 10 ? '0' : '') + status.index), 'forms', null)}
                    </option>
                </isloop>
            </select>
        </div>

        <div class="col-4">
            <select class="form-control custom-select js-year ${pdict.selectdate_class || ''}"
                data-size="6"
                data-width="auto"
                title="${Resource.msg('label.date.year', 'forms', null)}"
                ${pdict.selectdate_disabled ? 'disabled' : ''}>
                <option class="form-control" value="">${Resource.msg('label.date.year', 'forms', null)}</option>
                <isloop begin="0" end="${yearCount}" step="1" status="status">
                    <option
                        class="form-control"
                        value="${(yearStart - status.index).toFixed()}"
                        ${year && year === (yearStart - status.index) ? 'selected' : ''}>
                        ${(yearStart - status.index).toFixed()}
                    </option>
                </isloop>
            </select>
        </div>
    </div>

    <div class="w-100">
        <input  type="text"
                class="form-control js-date"
                style="display:none;"
                <isprint value="${formfield.attributes}" encoding="off" />
                data-missing-error="<isprint value="${Resource.msg('error.message.required', 'forms', null)}" encoding="htmldoublequote" />"
        >
        <div class="invalid-feedback"></div>
    </div>
</div>
