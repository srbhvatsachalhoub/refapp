<isif condition="${pdict.productSearch.refinements && pdict.productSearch.refinements.length}">

    <iscomment>Close button</iscomment>
    <div class="mobile-modal-header d-xl-none">
        <h3 class="mobile-modal-title">${Resource.msg('label.refineby', 'search', null)}</h3>
        <button class="btn btn-link btn-close" data-remove-class-open=".js-refinement-container">
            <i class="sc-icon-cross"></i>
        </button>
    </div>

    <div class="mobile-modal-body">
        <div class="refinements">
            <div class="row refinement-header d-xl-none m-0">${Resource.msg('label.refineby', 'search', null)}</div>
            <isloop items="${pdict.productSearch.refinements}" var="refinement">
                <isset name="attributeID" value="${(
                        refinement.isPriceRefinement ? 'price' :
                        refinement.isCategoryRefinement ? 'category' :
                        refinement.values && refinement.values.length > 0 ? refinement.values[0].id : ''
                    ).toLowerCase()}" scope="page" />
                <div class="card refinement open refinement-${attributeID}"
                    data-refinement="${refinement.displayName}">
                    <div class="card-header" data-toggle-class-open="parent">
                        ${refinement.displayName}
                        <i class="refinement-icon chevron-right">
                            <svg class="bi bi-chevron-right" width="1.2em" height="1.2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                            </svg>
                        </i>
                    </div>
                    <div class="card-body">
                        <iscomment>CATEGORY REFINEMENTS</iscomment>
                        <isif condition="${refinement.isCategoryRefinement}">
                            <isset name="categories" value="${refinement.values}" scope="page" />
                            <isinclude template="search/refinements/categories" />
                        </isif>

                        <iscomment>ATTRIBUTE REFINEMENTS</iscomment>
                        <isif condition="${refinement.isAttributeRefinement}">
                            <isinclude template="search/refinements/attributes" />
                        </isif>

                        <iscomment>PRICE REFINEMENTS</iscomment>
                        <isif condition="${refinement.isPriceRefinement}">
                            <isinclude template="search/refinements/prices" />
                        </isif>
                    </div>
                </div>
            </isloop>
        </div>
    </div>

    <iscomment>Results count and Reset button</iscomment>
    <isif condition="${pdict.querystring && (parseInt(pdict.querystring.refined) || pdict.querystring.pmin || pdict.querystring.pmax)}">
        <div class="mobile-modal-footer">
            <a class="btn btn-primary js-refine-link"
               href="${pdict.productSearch.resetLink}">
                ${Resource.msg('link.clearall', 'search', null)}
            </a>
        </div>
    </isif>
</isif>
