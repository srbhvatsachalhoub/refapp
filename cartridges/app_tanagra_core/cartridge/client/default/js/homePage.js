'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./productTile'));
    processInclude(require('./instagram/instagram'));
});
