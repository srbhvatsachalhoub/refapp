'use strict';
var formHelpers = require('base/checkout/formErrors');
/**
 * initializeEvents for guest login
 */
function initalizeEvents() {
    var formSelector = '.js-guest-email-form';
    var $guestEmailForm = $(formSelector);
    $guestEmailForm.on('submit', function (e) {
        e.preventDefault();
        var url = $guestEmailForm.attr('action');
        var method = $guestEmailForm.attr('method');
        $guestEmailForm.spinner().start();
        $.ajax({
            url: url,
            method: method,
            data: $guestEmailForm.serialize(),
            success: function (data) {
                if (data) {
                    if (data.success && data.redirectUrl) {
                        window.location.href = data.redirectUrl;
                    } else if (data.fieldErrors.length) {
                        data.fieldErrors.forEach(function (error) {
                            if (Object.keys(error).length) {
                                formHelpers.loadFormErrors(formSelector, error);
                            }
                        });
                    }
                }
            },
            complete: function () {
                $guestEmailForm.spinner().stop();
            }
        });
        return false;
    });
}


$(document).ready(function () {
    initalizeEvents();
});
