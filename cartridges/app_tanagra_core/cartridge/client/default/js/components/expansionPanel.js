'use strict';

module.exports = function () {
    $('.expansion-panel-summary').click(function () {
        $(this).parent().toggleClass('open');
    });
};
