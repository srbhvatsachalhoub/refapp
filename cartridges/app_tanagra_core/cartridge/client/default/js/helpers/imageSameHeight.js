'use strict';

// eslint-disable-next-line consistent-return
(function () {
    if (typeof window.imageSameHeight === 'function') return false;

    /**
     * Make sure the given images will have the same height of the attached image.
     * @param {HTMLElement} element - the attached element
     * @param {array} images - collection of images id
     */
    function imageSameHeight(element, images) {
        images.forEach(function (image) {
            var imageElement = document.getElementById(image);
            if (imageElement) {
                imageElement.style.minHeight = element.offsetHeight + 'px';
            }
        });
    }

    window.imageSameHeight = imageSameHeight;
}());
