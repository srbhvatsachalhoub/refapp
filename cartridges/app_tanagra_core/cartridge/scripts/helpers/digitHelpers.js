'use strict';

var Locale = require('dw/util/Locale');

/**
 * Gets the localized value of a number
 *
 * @param {Request} req request object
 * @param {int} number number to be localized
 * @return {string} localized number
 */
function getNumberLocalized(req, number) {
    var Resource = require('dw/web/Resource');
    var localizedResult = '';

    var currentLocale = Locale.getLocale(req.locale);

    if (currentLocale.language !== 'ar') {
        return number;
    }

    var numberArr = number.toString().split('');

    for (var i = 0; i < numberArr.length; i++) {
        localizedResult += Resource.msg('digit.' + numberArr[i], 'forms', null) === 'digit.,' ? numberArr[i] : Resource.msg('digit.' + numberArr[i], 'forms', null);
    }

    return localizedResult;
}

/**
 * Generates year options between from-to parameters
 *
 * @param {Request} req request object
 * @param {int} from starting year (inclusive)
 * @param {int} to ending year (inclusive), if not given, uses the current year
 * @return {Object[]} object array of year options
 */
function getYearsAsOptionList(req, from, to) {
    var year = to || new Date().getFullYear();

    var years = [];
    for (var i = year; i >= from; i--) {
        var yearValue = i.toFixed(0);
        years.push({
            optionid: yearValue,
            label: yearValue,
            value: yearValue
        });
    }
    return years;
}

module.exports = {
    getYearsAsOptionList: getYearsAsOptionList,
    getNumberLocalized: getNumberLocalized
};
