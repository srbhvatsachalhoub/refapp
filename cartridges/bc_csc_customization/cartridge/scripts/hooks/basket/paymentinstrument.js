'use strict';

var Status = require('dw/system/Status');
var PaymentMgr = require('dw/order/PaymentMgr');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Make sure the basket has only one COD payment instrument
 * Adds COD payment instrument to basket as default to get the service fee on basket
 * @param {dw.order.Basket} basket - the basket based on which the order is created
 * @param {Object} paymentInstrument - the payment instrument to be added to the basket
 */
function afterPostPaymentInstrument(basket, paymentInstrument) {
    if(request.clientId === "dw.csc" && paymentInstrument && paymentInstrument.paymentMethodId === 'COD') {
        var paymentMethod = PaymentMgr.getPaymentMethod('COD');
        if (paymentMethod && paymentMethod.active) {
            var paymentInstruments = basket.getPaymentInstruments();

            collections.forEach(paymentInstruments, function (item) {
                basket.removePaymentInstrument(item);
            });

            basket.createPaymentInstrument(
                'COD', basket.totalGrossPrice
            );
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    afterPostPaymentInstrument: afterPostPaymentInstrument
};
