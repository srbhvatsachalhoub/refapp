'use strict';

var Status = require('dw/system/Status');
var PaymentMgr = require('dw/order/PaymentMgr');

/**
 * Authorize COD in Customer Service Center
 * @param {dw.order.Order} order - the order
 * @param {dw.order.OrderPaymentInstrument} orderPaymentInstrument - specified payment details
 */
function authorize(order, orderPaymentInstrument) {
    if(request.clientId === "dw.csc" && orderPaymentInstrument) {
        var paymentMethod = PaymentMgr.getPaymentMethod(orderPaymentInstrument.paymentMethod);
        if (paymentMethod && paymentMethod.active) {
            orderPaymentInstrument.paymentTransaction.setTransactionID(order.orderNo);
            orderPaymentInstrument.paymentTransaction.setPaymentProcessor(paymentMethod.paymentProcessor);
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    authorize: authorize
};
