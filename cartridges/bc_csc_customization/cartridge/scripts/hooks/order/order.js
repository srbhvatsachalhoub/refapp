'use strict';

var Status = require('dw/system/Status');
var Order = require('dw/order/Order');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

/**
 * Sets the order statuses
 * @param {dw.order.Order} order - the order
 */
function afterPOST(order) {
    if(request.clientId === "dw.csc" && order) {
        hooksHelper('app.order.status.setConfirmationStatus', 'setConfirmationStatus', [order, Order.CONFIRMATION_STATUS_NOTCONFIRMED], function () {
            order.setConfirmationStatus(Order.CONFIRMATION_STATUS_NOTCONFIRMED);
        });
        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_READY], function () {
            order.setExportStatus(Order.EXPORT_STATUS_READY);
        });

        // custom attributes
        order.custom.sscSyncStatus = 'created';
    }

    return new Status(Status.OK);
}

module.exports = {
    afterPOST: afterPOST
};
