'use strict';

var notificationHelper = require('brand_core/helpers/notificationHelper')('.error-messaging');
var formValidation = require('base/components/formValidation');

var base = require('brand_core/addressBook/addressBook');

base.submitAddress = function () {
    $('.js-address-form').submit(function (e) {
        e.preventDefault();

        var $form = $(this);

        $form.spinner().start();

        $('.js-address-form').trigger('address:submit', e);

        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                $form.spinner().stop();
                if (data) {
                    if (!data.success) {
                        formValidation($form, data);
                    } else {
                        $('.success-message').removeClass('d-none');
                        location.href = data.redirectUrl;
                    }
                }
            },
            error: function (err) {
                if (err.responseJSON && err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                } else {
                    notificationHelper.generalError();
                }
                $form.spinner().stop();
            }
        });
        return false;
    });
};

module.exports = base;
