'use strict';

module.exports = function () {
    $(document).ready(function () {
        if ($(window).width() < window.RA_BREAKPOINTS.lg) {
            var isApple = /iPhone|iPad|iPod/i.test(navigator.userAgent);
            if (isApple) {
                $('.js-mobile-apps-widget-android').hide();
            } else {
                $('.js-mobile-apps-widget-iphone').hide();
            }
        }
    });

    $(window).on('orientationchange', function () {
        if ($(window).width() < window.RA_BREAKPOINTS.lg) {
            var isApple = /iPhone|iPad|iPod/i.test(navigator.userAgent);
            if (isApple) {
                $('.js-mobile-apps-widget-android').hide();
            } else {
                $('.js-mobile-apps-widget-iphone').hide();
            }
        }
    });
};
