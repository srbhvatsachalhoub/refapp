'use strict';

var scrollY = 0;
var latestScrollY = 0;
var scrollStep = 1;
var isScrollDirDown = true;
var handlers = [];
var handler;
var blocked = false;
var elementNum = 0;

/**
 * @function
 * @description Execute scroll handlers
 */
function executeHandlers() {
    for (var i = 0; i < handlers.length; i++) {
        handler = handlers[i];
        handler.func(scrollY, handler.$element, handler.params, isScrollDirDown, Math.abs(scrollStep));
    }
    blocked = false;
}

/**
 * @function
 * @description Request nearest frame
 */
function requestNearestFrame() {
    if (blocked === false) {
        window.requestAnimationFrame(executeHandlers);
    }
    blocked = true;
}

/**
 * @function
 * @description Update scrollY
 */
function onScroll() {
    if (!window.disableScroll) {
        scrollY = $(window).scrollTop();
        if (scrollY > latestScrollY) {
            isScrollDirDown = true;
        } else {
            isScrollDirDown = false;
        }
        scrollStep = latestScrollY - scrollY;
        latestScrollY = scrollY <= 0 ? 0 : scrollY;

        requestNearestFrame();
    }
}

/**
 * @function
 * @description Find handler in registered handlers array
 * @param {Object} handlerFunc - handler
 * @param {Object} $element - element
 * @returns {number} - foundedIndex
 */
function getHandlerIndex(handlerFunc, $element) {
    var foundedIndex = -1;

    for (var i = 0; i < handlers.length; i++) {
        var registeredHandler = handlers[i];

        if (registeredHandler.func.name === handlerFunc.name) {
            if ($element && registeredHandler.$element) {
                if (registeredHandler.$element.data('elementNum') === $element.data('elementNum')) {
                    foundedIndex = i;
                }
            } else {
                foundedIndex = i;
            }
        }
    }

    return foundedIndex;
}

$(window).on('scroll', onScroll);
$(document).on('touchstart', onScroll);

onScroll();

module.exports = {
    registerHandler: function (handlerFunc, $element, params) {
        if ($element) {
            $element.data('elementNum', elementNum);
            elementNum += 1;
        }

        if (getHandlerIndex(handlerFunc, $element) === -1) {
            handlers.push({ func: handlerFunc, $element: $element, params: params });
        }
    },
    unRegisterHandler: function (handlerFunc, $element) {
        if (getHandlerIndex(handlerFunc, $element) !== -1) {
            handlers.splice(getHandlerIndex(handlerFunc, $element), 1);
        }
    }
};
