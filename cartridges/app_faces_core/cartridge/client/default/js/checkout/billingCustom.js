'use strict';

var formHelpers = require('brand_core/checkout/formErrors');

var comparableAddressFields = [
    'input[name$=_firstName]',
    'input[name$=_lastName]',
    'input[name$=_address1]',
    'input[name$=_address2]',
    'input[name$=_phone]',
    'select[name$=_country],input[name$=_country]',
    'select[name$=_stateCode],input[name$=_stateCode]',
    'input[name$=_city]'
];

/**
 * Compares shipping and billing addresses
 * @returns {boolean} True if billing address fields are same as shipping ones
 */
function isBillingAddressSameAsShippingAddress() {
    var isSame = true;
    var $billingAddressForm = $('.js-form-billing-address');
    var $shippingAddressForm = $('.js-form-shipping-address');

    comparableAddressFields.forEach(function (selector) {
        var $billingField = $billingAddressForm.find(selector);
        var $shippingField = $shippingAddressForm.find(selector);

        if ($billingField.length && $shippingField.length &&
            $billingField.val() && $shippingField.val()) {
            var billingValue = $billingField.val().toLowerCase().trim();
            var shippingValue = $shippingField.val().toLowerCase().trim();
            if (billingValue !== shippingValue) isSame = false;
        }
    });

    return isSame;
}

/**
 * Syncs shipping and billing addresses
 */
function syncBillingAndShippingAddresses() {
    var $billingAddressForm = $('.js-form-billing-address');
    var $shippingAddressForm = $('.js-form-shipping-address');

    comparableAddressFields.forEach(function (selector) {
        var $billingField = $billingAddressForm.find(selector);
        var $shippingField = $shippingAddressForm.find(selector);

        if ($billingField.length && $shippingField.length) {
            $billingField.val($shippingField.val());
            if ($billingField.is('select')) {
                $billingField.trigger('change');
                $billingField.selectpicker('refresh');
            }
            if ($billingField.is('input')) $billingField.trigger('input');
        }
    });

    formHelpers.clearPreviousErrors($billingAddressForm);
}

/**
 * Iniializes billing address form
 */
function initAddressForm() {
    var $billingAddressForm = $('.js-form-billing-address');
    var $city = $billingAddressForm.find('select.js-selectbox-city');
    var $state = $billingAddressForm.find('select.js-selectbox-state');
    var $syncBillingCheckbox = $('.js-checkbox-sync-shipping-and-billing');
    var isBillingSame = isBillingAddressSameAsShippingAddress();

    if ($state.length) {
        if ($state.find(':selected').length > 1) {
            $state.find('.bs-title-option').prop('selected', false);
            $state.selectpicker('refresh');
            $state.trigger('change');
        }
    } else {
        $city.prop('disabled', false);
        $billingAddressForm.find('.js-input-city').trigger('input');
    }

    $syncBillingCheckbox.prop('checked', isBillingSame);
    $billingAddressForm[isBillingSame ? 'addClass' : 'removeClass']('d-none');
}

module.exports = {
    initialize: function () {
        $(document)
            // Sync billing address and shipping address
            .on('change', '.js-checkbox-sync-shipping-and-billing', function () {
                var $this = $(this);
                var isChecked = $this.is(':checked');
                var $billingAddressForm = $('.js-form-billing-address');
                $billingAddressForm[isChecked ? 'addClass' : 'removeClass']('d-none');
                if (isChecked) syncBillingAndShippingAddresses();
            })

            // Fix tab behavior
            .on('psp:changed', function () {
                $('.payment-options .tab-pane').removeClass('active show');
            })

            .on('click', '.js-payment-method-tab', function (e) {
                e.preventDefault();
                var methodId = $(this).attr('data-method-id');
                $(document).trigger('checkout:paymentMethodChanged', { methodId: methodId, initialSelect: false });
            })

            // Initialize billing address form with correct data
            .on('checkout:billingAddressUpdated', initAddressForm);

        initAddressForm();
    }
};
