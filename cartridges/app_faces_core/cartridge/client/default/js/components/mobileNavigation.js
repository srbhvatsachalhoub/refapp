'use strict';

var commonHelpers = require('app_brand_core/cartridge/scripts/helpers/commonHelpers');

module.exports = function () {
    var $closeCategoryButon = $('.js-close-category');

    $closeCategoryButon.on('click', function () {
        var $dropdownToggle = $(this);
        var $dropdownMenu = $dropdownToggle.closest('.dropdown-menu');
        var $categoryTrigger = $dropdownMenu.parent('.js-dropdown-click');
        var $categoryTriggerLink = $categoryTrigger.find('.js-dropdown-toggle-btn.expanded');

        $categoryTrigger.removeClass('show');
        $categoryTriggerLink.removeClass('expanded');
    });

    // Brand navigation

    $('.js-brandheader-nav .js-top-level-nav-item-link').on('click', function (event) {
        var windowWidth = $(window).width();

        if (commonHelpers.isMobileView(windowWidth, window.RA_BREAKPOINTS) || commonHelpers.isTabletView(windowWidth, window.RA_BREAKPOINTS)) {
            event.preventDefault();
        }
    });

    $('.js-dropdown-menu-toggle').on('click', function (event) {
        event.preventDefault();

        var $toggle = $(this);
        var $linkHolder = $toggle.parent();

        $linkHolder.toggleClass('menu-expanded');
        $linkHolder.next('.js-dropdown-menu').toggleClass('expanded');
    });
};
