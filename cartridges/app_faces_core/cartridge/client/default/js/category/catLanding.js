'use strict';

/**
 * init 'view all' functionality
 */
function viewAll() {
    var $viewMoreButton = $('.js-btn-clp-action-more');

    if ($viewMoreButton.length !== 0) {
        var $extraCategoryBlock = $('.js-extra-categories');
        var $viewLessButton = $('.js-btn-clp-action-less');
        $extraCategoryBlock.hide();

        $viewMoreButton.on('click', function (e) {
            e.preventDefault();
            $extraCategoryBlock.slideDown();
            $viewMoreButton.hide(300);
        });

        $viewLessButton.on('click', function (e) {
            e.preventDefault();
            $extraCategoryBlock.slideUp();
            $viewMoreButton.show(300);
        });
    }
}

module.exports = viewAll;
