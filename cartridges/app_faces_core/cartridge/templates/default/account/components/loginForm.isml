<isinclude template="account/components/oauth" />

<isif condition="${pdict.errorMessage}">
    <div class="alert alert-danger alert-dismissible valid-cart-error fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        ${pdict.errorMessage}
    </div>
</isif>

<form action="${pdict.actionUrl}" class="js-login-form" method="POST" name="login-form">
    <div class="form-group required">
        <label class="form-control-label" for="login-form-email">
            ${Resource.msg('label.input.login.emailOrMobile', 'login', null)}
        </label>
        <input  id="login-form-email"
                class="form-control"
                name="loginEmail"
                value="${pdict.userName}"
                pattern="^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$|^[0-9\+]{10,15}$"
                required
                aria-required="true"
                aria-describedby="form-email-error"
                data-missing-error="<isprint value="${Resource.msg('error.message.required.email', 'forms', null)}" encoding="htmldoublequote" />"
                data-pattern-mismatch="<isprint value="${Resource.msg('error.message.parse.email', 'forms', null)}" encoding="htmldoublequote" />"
        />
        <div class="invalid-feedback" id="form-email-error"></div>
    </div>

    <div class="form-group required position-relative">
        <label class="form-control-label" for="login-form-password">
            ${Resource.msg('label.input.login.password', 'login', null)}
        </label>
        <input  type="password"
                id="login-form-password"
                class="form-control js-input-password"
                name="loginPassword"
                required
                aria-required="true"
                aria-describedby="form-password-error"
                minlength="8"
                data-missing-error="<isprint value="${Resource.msg('error.message.required.password', 'forms', null)}" encoding="htmldoublequote" />"
                data-range-error="<isprint value="${Resource.msg('error.message.range.password', 'forms', null)}" encoding="htmldoublequote" />"
        />
        <span class="password-show-toggle js-password-show-toggle">
            <i class="sc-icon-eye"></i>
        </span>
        <div class="invalid-feedback" id="form-password-error"></div>
    </div>

    <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}" />

     <div class="login-option-menu">
        <div class="form-group mb-0">
            <div class="remember-me">
                <input type="checkbox" id="rememberMe" name="loginRememberMe" value="true"
                    <isif condition="${pdict.rememberMe}">checked</isif> >
                <label for="rememberMe">
                    ${Resource.msg('label.checkbox.login.rememberme', 'login', null)}
                </label>
            </div>
        </div>
        <div class="forgot-password">
            <a id="password-reset" class="page-reset-password-btn"
                title="${Resource.msg('link.login.forgotpassword', 'login', null)}" data-toggle="modal"
                href="${URLUtils.url('Account-PasswordResetDialogForm')}"
                data-target="#requestPasswordResetModal"
                data-remove-class-d-none=".js-reset-password-form-container"
                data-add-class-d-none=".js-reset-password-result">
                ${Resource.msg('link.login.forgotpassword', 'login', null)}
            </a>
        </div>
    </div>

    <isif condition="${pdict.navTabValue === 'login'}">
        <button type="submit" class="btn btn-block btn-primary login-btn">
            ${Resource.msg('button.text.loginform', 'login', null)}
        </button>

        <div class="separator">
            <h6>${Resource.msg('label.or', 'common', null)}</h6>
        </div>
    <iselse/>
         <button type="submit" class="btn btn-block btn-primary login-btn">
            ${Resource.msg('button.text.loginform', 'login', null)}
        </button>
    </isif>

    <div class="create-account-wrapper">
        <a class="btn btn-block modal-register-btn"
            href="${URLUtils.url('Login-Show',
                'action', 'register', 'rurl', pdict.rurl
            )}">
            ${Resource.msg('link.create.an.account', 'login', null)}
        </a>
    </div>
</form>
