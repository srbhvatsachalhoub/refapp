<isset name="hasEmbeddedBonusProducts" value="${lineItem.bonusProductLineItemUUID === 'bonus'}" scope="page" />
<isset name="bonusproductlineitem" value="${hasEmbeddedBonusProducts ? 'bonus-product-line-item' : ''}" scope="page" />
<isset name="bonuslineitemrow" value="${hasEmbeddedBonusProducts ? 'bonus-line-item-row' : ''}" scope="page" />
<isset name="bonusproduct" value="${lineItem.isBonusProductLineItem ? 'bonus-product' : ''}" scope="page" />

<isif condition="${!lineItem.isBonusDiscountProductLineItem}">
    <div class="row cart-product product-info ${bonusproductlineitem} uuid-${lineItem.UUID} js-product-info">
        <div class="col-12">
            <isinclude template="cart/productCard/cartProductCardHeader" />
            <div class="row ${bonuslineitemrow} ${bonusproduct}">
                <div class="col-xl-6 d-flex">
                    <div class="cart-product-image">
                        <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                            <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                                <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                                    data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                                </isif>>
                                <img class="product-image" src="${lineItem.images.small[0].url}" alt="${lineItem.images.small[0].alt}" title="${lineItem.images.small[0].title}" />
                            </a>
                        <iselseif condition="${lineItem.isBonusProductLineItem || lineItem.isGiftWrappingProduct}" />
                            <img class="product-image" src="${lineItem.images.small[0].url}" alt="${lineItem.images.small[0].alt}" title="${lineItem.images.small[0].title}" />
                        </isif>
                    </div>
                    <div class="cart-product-details-container">
                        <isif condition="${lineItem.brand}">
                            <div class="cart-product-brand text-uppercase mb-2">
                                ${lineItem.brand}
                            </div>
                        </isif>
                        
                        <div class="cart-product-name">
                            <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                                <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                                    <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                                        data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                                    </isif>>
                                    ${lineItem.productName}
                                </a>
                            <iselseif condition="${lineItem.isBonusProductLineItem || lineItem.isGiftWrappingProduct}" />
                                ${lineItem.productName}
                            </isif>
                        </div>
                        
                        <div class="cart-product-details row">
                            <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                                <div class="col-md-6 d-xl-none line-item-quantity mb-3 mb-md-0">
                                    <div class="cart-product-attribute-title text-uppercase mb-md-1">${Resource.msg('label.quantity', 'common', null)}</div>
                                    <div class="cart-product-quantity simple-quantity quantity-form">
                                        <isinclude template="cart/productCard/quantitySelect" />
                                    </div>
                                </div>
                            </isif>
                            <isif condition="${(lineItem.variationAttributes && lineItem.variationAttributes.length > 0) || (lineItem.options && lineItem.options.length > 0)}">
                                <div class="col-6 d-flex align-items-end">
                                <isloop items="${lineItem.variationAttributes}" var="attribute">
                                    <div class="cart-product-attribute">
                                        <div class="cart-product-attribute-title text-uppercase mb-1">${attribute.displayName}</div>
                                        <div class="cart-product-attribute-value ${attribute.displayName}-${lineItem.UUID}">
                                            ${attribute.displayValue}
                                        </div>
                                    </div>
                                </isloop>
                                <isloop items="${lineItem.options}" var="option">
                                    <isif condition="${!!option}">
                                        <div class="cart-product-attribute">
                                            <div class="lineItem-options-values" data-option-id="${option.optionId}"
                                                data-value-id="${option.selectedValueId}">
                                                <p class="line-item-attributes">${option.displayName}</p>
                                            </div>
                                        </div>
                                    </isif>
                                </isloop>
                                </div>
                            </isif>
                            <iscomment> <div class="col-6 col-xl-12 mt-2 mt-md-3">
                                <div class="cart-product-actions">
                                    <isif condition="${pdict.CurrentCustomer.authenticated && !lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                                        <isinclude url="${URLUtils.url('Wishlist-AddToWishlistButton', 'pid', lineItem.id)}" />
                                    </isif>
                                </div>
                            </div> </iscomment>
                            <div class="cart-product-price-col col-6 d-xl-none mt-2 mt-md-3 ml-auto">
                                <div class="cart-product-price">
                                    <div class="item-total-${lineItem.UUID} price">
                                        <isprint value="${lineItem.priceTotal.renderedPrice}" encoding="off" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                    <div class="col-xl-3">
                        <div class="cart-product-quantity simple-quantity mt-2 d-none d-xl-block quantity-form">
                            <isinclude template="cart/productCard/quantityInput" />
                        </div>
                    </div>
                    <div class="cart-product-price-col col-xl-3">
                        <div class="cart-product-price d-none d-xl-block">
                            <div class="item-total-${lineItem.UUID} price">
                                <isprint value="${lineItem.priceTotal.renderedPrice}" encoding="off" />
                            </div>
                        </div>
                    </div>
                <iselse/>
                    <div class="col-xl-3"></div>
                    <div class="col-xl-3 align-self-end">
                        <div class="cart-bonus-product-no-price">
                            ${Resource.msg('label.free', 'cart', null)}
                        </div>
                    </div>
                </isif>
                <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                    <div class="col-12 line-item-promo item-${lineItem.UUID}">
                        <isinclude template="checkout/productCard/productCardProductPromotions" />
                    </div>
                </isif>
                <div class="col-12">
                    <div class="cart-product-devider dropdown-divider"></div>
                </div>
            </div>
        </div>
    </div>
</isif>
