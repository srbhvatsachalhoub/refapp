'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
var CatalogMgr = require('dw/catalog/CatalogMgr');

/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    var category = CatalogMgr.getCategory(content.category);
    model.category = category;
    model.showMore = content.showMore;
    model.subCategoriesCount = content.subCategoriesCount;
    if (category && category.getCustom().clpSubcategoryMenuItems) {
        model.subCategoriesCLP = searchHelper.getSubCatgories(category);
    }

    return new Template('experience/components/commerce_layouts/clpInfo').render(model).text;
};
