'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');


/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.title = content.title || '';
    model.backgroundColor = content.backgroundColor || '';
    model.backgroundImage = ImageTransformation.getScaledImage(content.backgroundImage);
    model.description = content.description || '';
    model.buttonLink = content.link || '#';
    model.buttonText = content.linkText || '';

    return new Template('experience/components/commerce_layouts/giftingGuide').render(model).text;
};
