'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
// var Resource = require('dw/web/Resource');
// var URLUtils = require('dw/web/URLUtils');

/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');

    var breadcrumbs = [];
    if (!content.firstLevel) {
        breadcrumbs.push({
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        });
    }
    if (content.brandPage) {
        breadcrumbs.push({
            htmlValue: Resource.msg('global.brand', 'common', null),
            url: URLUtils.home().toString()
        });
    }
    breadcrumbs.push({
        htmlValue: content.name || ''
    });

    model.breadcrumbs = breadcrumbs;

    return new Template('experience/components/commerce_layouts/breadCrumbs').render(model).text;
};
