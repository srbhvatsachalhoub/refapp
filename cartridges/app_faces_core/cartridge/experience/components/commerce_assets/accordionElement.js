'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for accordionElement component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.header = content.header || '';
    model.body = content.body || '';
    model.id = content.id || '';

    return new Template('experience/components/commerce_assets/accordionElement').render(model).text;
};
