'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for haveQuestionArea component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.title = content.title || '';
    model.body = content.body || '';
    model.urlText = content.urlText || '';
    model.url = content.url || '#';

    return new Template('experience/components/commerce_assets/haveQuestionArea').render(model).text;
};
