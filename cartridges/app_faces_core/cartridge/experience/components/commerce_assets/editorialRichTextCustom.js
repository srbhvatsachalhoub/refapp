'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for the editorialRichTextCustom component
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.cssClass = content.cssClass || 'align-self-center text-center text-lg-left';
    model.richText = content.richText || null;

    return new Template('experience/components/commerce_assets/editorialRichTextCustom').render(model).text;
};
