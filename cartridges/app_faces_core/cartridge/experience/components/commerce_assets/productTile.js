'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for storefront.productTile component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.display = {
        pid: context.content.product.ID,
        ratings: content.displayRatings
    };

    return new Template('experience/components/commerce_assets/product/productTile').render(model).text;
};
