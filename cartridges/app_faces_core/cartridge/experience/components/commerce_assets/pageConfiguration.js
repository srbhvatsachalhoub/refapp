'use strict';

/**
 * Render logic for component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function () {
    // This function is required for Component definition
};
