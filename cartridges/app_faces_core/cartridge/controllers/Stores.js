var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
/**
 * Extends findYourStoreForm for Faces to make it includable from content assets.
 */
server.get('Form', server.middleware.include, cache.applyDefaultCache, function (req, res, next) {
    res.render('/components/storeLocator/findYourStoreForm');
    next();
});

module.exports = server.exports();
