'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var categoryHelpers = require('*/cartridge/scripts/helpers/categoryHelpers');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');
var URLUtils = require('dw/web/URLUtils');
var levelCounter = 0;

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
    return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl
        ? category.custom.alternativeUrl
        : URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Counts the category: online and localeOffline!=true and searchable products
 * @param {dw.catalog.Category} category - Current category
 * @returns {number} the length of the online and localeOffline!=true and searchable products products
 */
function getCategoryProductCount(category) {
    var localeId = request.locale; // eslint-disable-line no-undef
    return collections.filter(category.onlineProducts, function (onlineProduct) {
        return onlineProduct.searchable
            && (onlineProduct.priceModel && onlineProduct.priceModel.price && onlineProduct.priceModel.price.available && onlineProduct.priceModel.price.value >= 0)
            && !onlineProduct.master
            && (inventoryHelpers.getProductAvailabilityModel(onlineProduct, localeId).inStock
                    || onlineProduct.searchableIfUnavailableFlag === null
                    || onlineProduct.searchableIfUnavailableFlag)
            && onlineProduct.custom.localeOffline.value !== 'true';
    }).length.toFixed();
}

/**
 * Converts a given category from dw.catalog.Category to plain object
 * @param {dw.catalog.Category} category - A single category
 * @param {number} level - Current category level
 * @param {boolean} isMenu - If true, category object for mega menu will be prepared
 * @returns {Object} plain object that represents a category
 */
function categoryToObject(category, level, isMenu) {
    if (!category.custom) {
        return null;
    }

    if (isMenu && !category.custom.showInMenu) {
        return null;
    }

    var showThumbnail = (!isMenu || category.custom.showThumbnailInMenu) && category.getThumbnail();
    var showSubcategories = (!isMenu || category.custom.showSubcateogriesInMenu) && categoryHelpers.hasOnlineSubCategories(category);
    var result = {
        category: category,
        name: category.getDisplayName(),
        url: getCategoryUrl(category),
        id: category.ID,
        level: level,
        productCount: getCategoryProductCount(category),
        thumbnailUrl: showThumbnail ? category.getThumbnail().getURL() : null,
        subcategoryItemLimit: category.custom.subcategoryItemLimit || 8
    };

    if (showSubcategories) {
        levelCounter++;
        level = levelCounter; // eslint-disable-line no-param-reassign
        collections.forEach(categoryHelpers.getOnlineSubCategories(category), function (subcategory) {
            var converted = null;
            if (categoryHelpers.hasOnlineProducts(subcategory) || categoryHelpers.hasOnlineSubCategories(subcategory)) {
                converted = categoryToObject(subcategory, level, isMenu);
            }
            if (converted) {
                if (!result.subCategories) {
                    result.subCategories = [];
                }
                result.subCategories.push(converted);
            }
        });
        if (result.subCategories) {
            result.complexSubCategories = result.subCategories.some(function (item) {
                return !!item.subCategories;
            });
        }
    }

    return result;
}


/**
 * Represents a single category with all of it's children
 * @param {dw.util.ArrayList<dw.catalog.Category>} items - Top level categories
 * @param {boolean} isMenu - If true, category list for mega menu will be prepared
 * @param {boolean} isBrandMenu - If true, brand category list for brand menu will be prepared
 * @constructor
 */
function categories(items, isMenu, isBrandMenu) {
    this.categories = [];
    collections.forEach(items, function (item) {
        levelCounter = 0;
        if (item.online
            && item.custom
            && (categoryHelpers.hasOnlineProducts(item) || categoryHelpers.hasOnlineSubCategories(item))
            && (item.custom.localeOffline.value !== 'true')) {
            if (((!isMenu && !isBrandMenu) || item.custom.showInMenu) || (isBrandMenu && item.custom.showInBrandMenu)) {
                this.categories.push(categoryToObject(item, levelCounter, isMenu));
            }
        }
    }, this);
}

module.exports = categories;
