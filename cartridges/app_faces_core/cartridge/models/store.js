'use strict';

var base = module.superModule;

/**
 * @constructor
 * @classdesc The stores model
 * @param {dw.catalog.Store} storeObject - a Store objects
 */
function store(storeObject) {
    base.call(this, storeObject);
    if (!storeObject) {
        return;
    }

    this.isRetailer = !!storeObject.custom.isRetailer;
    this.isAirport = !!storeObject.custom.isAirport;
    this.isSpa = !!storeObject.custom.isSpa;
    this.isCafe = !!storeObject.custom.isCafe;
}

module.exports = store;
