'use strict';

var server = require('server');

var logger = require('dw/system/Logger').getLogger('checkoutcom.webhooks');
var StringUtils = require('dw/util/StringUtils');

/**
 * Validates the request
 * Checks the httpHeaders.authorization value if it is defined in SitePref
 * @param {Object} req - the SFCC request object
 * @returns {boolean} - true if the key exists, false otherwise
 */
function validateNotification(req) {
    var key = req.httpHeaders.authorization;
    if (!key) {
        logger.error('Authorization is not located in webhook request.');
        return false;
    }

    var Site = require('dw/system/Site');
    var webhooksPrivateSharedKeys = Site.getCurrent().getCustomPreferenceValue('checkoutcomWebhooksPrivateSharedKeys');
    if (!webhooksPrivateSharedKeys || webhooksPrivateSharedKeys.length === 0) {
        logger.error('Sitepref WebhooksPrivateSharedKeys is not well configured.');
        return false;
    }

    if (webhooksPrivateSharedKeys.indexOf(key) === -1) {
        logger.error('Sitepref WebhooksPrivateSharedKeys does not contain key: {0}', key);
        return false;
    }

    // all is fine
    return true;
}

/**
 * Controller To CheckoutcomWebhooks-Process
 * to be called by checkout.com with for notifications
 */
server.post('Process', server.middleware.https, function (req, res, next) {
    if (!validateNotification(req)) {
        res.setStatusCode(500);
        logger.error('Webhook Process could not validate the request.');
        res.render('error', {
            error: req.error || {},
            showError: true,
            message: 'Could not validate request, please check logs.'
        });
        return next();
    }

    var body = req.body;
    if (!body) {
        res.setStatusCode(500);
        logger.error('Webhook Process body is empty.');
        res.render('error', {
            error: req.error || {},
            showError: true,
            message: 'Request body is empty.'
        });
        return next();
    }

    var OrderMgr = require('dw/order/OrderMgr');
    var checkoutcomHelpers = require('*/cartridge/scripts/helpers/checkoutcomHelpers');
    var order;

    try {
        var hook = JSON.parse(body);
        if (!hook.type) {
            throw new Error('type is not provided in request');
        }

        if (!hook.data || !hook.data.reference) {
            throw new Error('data is not well formed in request');
        }

        order = OrderMgr.getOrder(hook.data.reference);
        if (!order) {
            throw new Error(StringUtils.format('Order is not found, orderNo: {0}', hook.data.reference));
        }

        // add notification to the feedback custom object
        checkoutcomHelpers.addFeedback(order.orderNo, hook.type, body);
    } catch (e) {
        var message = StringUtils.format('Error on CheckoutcomWebhooks-Process, exception occured, body: {0}, message: {1}', body, e.message);
        res.setStatusCode(500);
        logger.error(message);
        res.render('error', {
            error: req.error || {},
            showError: true,
            message: message
        });
        return next();
    }

    res.render('webhooks/success', {});
    return next();
});


module.exports = server.exports();
