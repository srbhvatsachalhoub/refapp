'use strict';
/* global request */

var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var StringUtils = require('dw/util/StringUtils');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var Status = require('dw/system/Status');

var collections = require('*/cartridge/scripts/util/collections');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var checkoutcomServiceHelpers = require('*/cartridge/scripts/helpers/checkoutcomServiceHelpers');
var logger = require('dw/system/Logger').getLogger('checkoutcom.helper'); // eslint-disable-line no-unused-vars
var checkoutcomConstants = require('*/cartridge/scripts/util/checkoutcomConstants');
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

/**
 * Sets the last PSP payment status to the order's custom.lastPaymentStatus
 * @param {dw.order.Order} order - The order to be updated
 * @param {string} lastPaymentStatus - the last PSP payment status
 * @param {string} note - the note to be added to the order if provided
 */
function setLastPSPPaymentStatus(order, lastPaymentStatus, note) {
    if (!order) {
        return;
    }

    Transaction.wrap(function () {
        order.custom.lastPaymentStatus = lastPaymentStatus; // eslint-disable-line no-param-reassign

        if (note) {
            order.addNote(checkoutcomConstants.pspPaymentStatusNoteSubject, note);
        }
    });
}

/**
 * Exclude restricted keys from object to be logged/noted
 * @param {Object} payload - the full payload
 * @param {boolean} isNote - indicates if the prepare for note
 * @return {string} returns the logable payload
 */
function getLogablePayload(payload, isNote) {
    if (!payload) {
        return '{}';
    }

    var loopKeys = function (obj, newObj) {
        Object.keys(obj).forEach(function (key) {
            if (checkoutcomConstants.logExcludedKeys.indexOf(key) > -1
                || (isNote && checkoutcomConstants.noteExcludedKeys.indexOf(key) > -1)) {
                return;
            }

            if (typeof obj[key] === 'object' && obj[key] !== null) {
                newObj[key] = loopKeys(obj[key], {}); // eslint-disable-line no-param-reassign
            } else {
                newObj[key] = obj[key]; // eslint-disable-line no-param-reassign
            }
        });
        return newObj;
    };
    var logablePayload = loopKeys(payload, {});

    return JSON.stringify(logablePayload);
}

/**
 * Adds a payload note the the given order
 * @param {dw.order.Order} order The order to be added a note
 * @param {string} subject - the subject of the note
 * @param {Object} payload - the payload to be noted
 */
function addNoteToOrder(order, subject, payload) {
    if (!order) {
        return;
    }

    var note = typeof payload === 'string' ? payload : getLogablePayload(payload, true);
    note = note.length > 1000 ? note.substr(0, 1000) : note;

    Transaction.wrap(function () {
        order.addNote(subject, note);
    });
}

/**
 * Fails the given order if the status is ORDER_STATUS_CREATED
 * @param {dw.order.Order} order The order to be failed
 * @param {string} description - The fail description
 * @returns {dw.system.Status} The status of the failure
 */
function failOrder(order, description) {
    if (!order) {
        return new Status(Status.ERROR);
    }

    if (order.status.value !== Order.ORDER_STATUS_CREATED) {
        return new Status(Status.ERROR);
    }

    return Transaction.wrap(function () {
        return hooksHelper('app.order.status.failOrder', 'failOrder', [order, true, description || 'checkout.com fail'], function () {
            return OrderMgr.failOrder(order, true);
        });
    });
}

/**
 * Prepares the payment information
 * @param {dw.web.FormGroup} creditCardForm - The credit card fields form
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be processed
 * @returns {Object} the payment information data
 */
function preparePaymentInformation(creditCardForm, paymentInstrument) {
    var paymentInformation = {};
    if (paymentInstrument) {
        paymentInformation.cardType = paymentInstrument.creditCardType;
        paymentInformation.cardOwner = paymentInstrument.creditCardHolder;
        paymentInformation.expirationMonth = paymentInstrument.creditCardExpirationMonth;
        paymentInformation.expirationYear = paymentInstrument.creditCardExpirationYear;
        paymentInformation.cardToken = paymentInstrument.creditCardToken || '';
        paymentInformation.paymentMethod = paymentInstrument.paymentMethod;
    }

    if (creditCardForm) {
        paymentInformation.cardNumber = creditCardForm.cardNumber.value || '';
        paymentInformation.saveCard = creditCardForm.saveCard.checked || false;
        paymentInformation.securityCode = creditCardForm.securityCode.value || '';
    }

    if (!paymentInformation.securityCode && request.httpParameterMap.securityCode.stringValue) {
        paymentInformation.securityCode = request.httpParameterMap.securityCode.stringValue;
    }

    return paymentInformation;
}

/**
 * Checks if the Mada PaymentCard is exists and active and applicable for the current card number and customer
 * If all is true, then returns Mada PaymentCard object
 * otherwise returns null
 * @param {dw.order.Basket} currentBasket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {dw.order.PaymentCard} returns the Mada PaymentCard, if applicable, null otherwise
 */
function getMadaPaymentCardIfApplicable(currentBasket, paymentInformation) {
    var madaPaymentCard = PaymentMgr.getPaymentCard('Mada');
    if (madaPaymentCard && madaPaymentCard.active) {
        var cardNumber = paymentInformation.cardNumber.value;
        var cardSecurityCode = paymentInformation.securityCode.value;
        var expirationMonth = paymentInformation.expirationMonth.value;
        var expirationYear = paymentInformation.expirationYear.value;

        var madaCreditCardStatus = madaPaymentCard.verify(
            expirationMonth,
            expirationYear,
            cardNumber,
            cardSecurityCode
        );
        if (madaCreditCardStatus && !madaCreditCardStatus.error) {
            if (madaPaymentCard.isApplicable(currentBasket.customer,
                currentBasket.billingAddress.countryCode.value,
                basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket).value)) {
                return madaPaymentCard;
            }
        }
    }
    return null;
}

/**
 * Verifies a credit card against a valid card number
 * and expiration date and possibly invalidates invalid form fields.
 * If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function handleCreditCard(basket, paymentInformation) {
    var currentBasket = basket;
    var cardErrors = {};
    var cardOwner = paymentInformation.cardOwner.value;
    var cardNumber = paymentInformation.cardNumber.value;
    var cardSecurityCode = paymentInformation.securityCode.value;
    var expirationMonth = paymentInformation.expirationMonth.value;
    var expirationYear = paymentInformation.expirationYear.value;
    var serverErrors = [];
    var creditCardStatus;

    var cardType = paymentInformation.cardType.value;
    var paymentCard = PaymentMgr.getPaymentCard(cardType);

    // check if the provided cardnumber is mada payment card number
    // if so, update cardtype and paymentCard object
    var madaPaymentCard = getMadaPaymentCardIfApplicable(currentBasket, paymentInformation);
    if (madaPaymentCard) {
        paymentCard = madaPaymentCard;
        cardType = madaPaymentCard.cardType;
    }

    if (!paymentInformation.creditCardToken) {
        if (paymentCard) {
            creditCardStatus = paymentCard.verify(
                expirationMonth,
                expirationYear,
                cardNumber,
                cardSecurityCode
            );
        } else {
            cardErrors[paymentInformation.cardNumber.htmlName] =
                Resource.msg('error.invalid.card.number', 'creditCard', null);

            return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
        }

        if (creditCardStatus.error) {
            collections.forEach(creditCardStatus.items, function (item) {
                switch (item.code) {
                    case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
                        cardErrors[paymentInformation.cardNumber.htmlName] =
                            Resource.msg('error.invalid.card.number', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
                        cardErrors[paymentInformation.expirationMonth.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        cardErrors[paymentInformation.expirationYear.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
                        cardErrors[paymentInformation.securityCode.htmlName] =
                            Resource.msg('error.invalid.security.code', 'creditCard', null);
                        break;
                    default:
                        serverErrors.push(
                            Resource.msg('error.card.information.error', 'creditCard', null)
                        );
                }
            });

            return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
        }
    }

    Transaction.wrap(function () {
        // remove existing payment instruments except complimentary payment methods
        COHelpers.removeExistingPaymentInstruments(currentBasket);

        var totalToBePaid = basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket);

        if (totalToBePaid.value > 0) {
            // create new CREDIT_CARD payment instrument with NonComplimentaryPaymentAmount
            var paymentInstrument = currentBasket.createPaymentInstrument(
                PaymentInstrument.METHOD_CREDIT_CARD,
                totalToBePaid
            );

            // set payment instrument credit card data
            paymentInstrument.setCreditCardHolder(cardOwner);
            paymentInstrument.setCreditCardNumber(cardNumber);
            paymentInstrument.setCreditCardType(cardType);
            paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
            paymentInstrument.setCreditCardExpirationYear(expirationYear);
            if (paymentInformation.creditCardToken) {
                paymentInstrument.setCreditCardToken(paymentInformation.creditCardToken);
            }
        }
    });

    return { fieldErrors: cardErrors, serverErrors: serverErrors, error: false };
}

/**
 * Verifies knet payment method.
 * If the information is valid a
 * knet payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function handleKnet(basket, paymentInformation) { // eslint-disable-line no-unused-vars
    var currentBasket = basket;
    var cardErrors = {};
    var serverErrors = [];


    Transaction.wrap(function () {
        // remove existing payment instruments except complimentary payment methods
        COHelpers.removeExistingPaymentInstruments(currentBasket);

        // create new KNET payment instrument with NonComplimentaryPaymentAmount
        var paymentInstrument = currentBasket.createPaymentInstrument( // eslint-disable-line no-unused-vars
            paymentMethodHelpers.getKnetPaymentMethodId(),
            basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket)
        );
    });

    return { fieldErrors: cardErrors, serverErrors: serverErrors, error: false };
}

/**
 * Processes the authorization success response
 * to set relevant data to relevant objects
 * to set relevant status to order
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted payload from checkout.com
 * @return {void}
 */
function processAuthorizedResponse(order, paymentInstrument, paymentProcessor, postedPayload) {
    if (order.customer && order.customer.registered) {
        var profile = order.customer.profile;
        if (profile
            && !profile.custom.checkoutcomCustomerId
            && postedPayload.customer && postedPayload.customer.id) {
            // set checkoutcom customer id
            Transaction.wrap(function () {
                profile.custom.checkoutcomCustomerId = postedPayload.customer.id;
            });
        }
    }

    if (!paymentInstrument.creditCardToken && postedPayload.source && postedPayload.source.id) {
        // set credit card token
        Transaction.wrap(function () {
            paymentInstrument.setCreditCardToken(postedPayload.source.id);
        });

        // save payment instrument to current customer wallet
        // if saveCard selected and current customer is authenticated
        if (order.customer.registered) {
            var server = require('server');

            var creditCardForm = server.forms.getForm('billing').creditCardFields;

            if (('saveCreditCard' in order.custom && order.custom.saveCreditCard) || (creditCardForm && creditCardForm.saveCard.checked)) {
                COHelpers.savePaymentInstrumentToWalletWithPaymentInstrument(
                    paymentInstrument,
                    order.customer
                );
            }
        }
    }

    // save knet related info
    if (postedPayload.source && postedPayload.source.type === checkoutcomConstants.sourceType.knet) {
        Transaction.wrap(function () {
            paymentInstrument.custom.knetPaymentId = postedPayload.source.knet_payment_id || null; // eslint-disable-line no-param-reassign
            paymentInstrument.custom.knetTransactionId = postedPayload.source.knet_transaction_id || null; // eslint-disable-line no-param-reassign
        });
    }

    // if the status is Captured
    if (postedPayload.status === checkoutcomConstants.serviceStatus.captured) {
        Transaction.wrap(function () {
            hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
            });
        });

        // set last payment status as captured
        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.captured);
    } else { // if authorized
        // set last payment status as authorized
        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.authorized);
    }
}

/**
 * Authorizes a payment using a credit card.
 * Prepares the parameters
 * Calls checkout.com for authorization
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} paymentInformation - The paymentInformation object
 * @param {string} action - Additional action param if it is needed to override
 * @param {string} sourceType - Additional sourceType param if it is needed to override
 * @return {Object} returns an error object
 */
function authorizeCreditCard(order, paymentInstrument, paymentProcessor, paymentInformation, action, sourceType) {
    action = action || checkoutcomConstants.action.authorization; // eslint-disable-line no-param-reassign
    sourceType = sourceType || checkoutcomConstants.sourceType.card; // eslint-disable-line no-param-reassign

    var payload = checkoutcomServiceHelpers.initService(
        checkoutcomServiceHelpers.checkoutcomService,
        order,
        paymentInformation,
        paymentInstrument,
        action,
        sourceType,
        [sourceType]
    );

    // add request payload as order note
    addNoteToOrder(order, 'Request Authorization', payload);
    var serviceResult = checkoutcomServiceHelpers.checkoutcomService.call(payload);

    if (serviceResult.isOk()) {
        var authorizationPostedPayload = serviceResult.object;

        if (authorizationPostedPayload.id && paymentInstrument.paymentTransaction) {
            // set transaction id
            Transaction.wrap(function () {
                paymentInstrument.paymentTransaction.setTransactionID(authorizationPostedPayload.id);
            });
        }

        // add response payload as order note
        addNoteToOrder(order, 'Response Authorization', authorizationPostedPayload);

        if (authorizationPostedPayload.approved
            && (authorizationPostedPayload.status === checkoutcomConstants.serviceStatus.authorized
                || authorizationPostedPayload.status === checkoutcomConstants.serviceStatus.captured)) {
            // success
            processAuthorizedResponse(order, paymentInstrument, paymentProcessor, authorizationPostedPayload);

            return {
                success: true
            };
        } else if (authorizationPostedPayload.status === checkoutcomConstants.serviceStatus.pending
                    && authorizationPostedPayload[checkoutcomConstants.serviceParams.links]
                    && authorizationPostedPayload[checkoutcomConstants.serviceParams.links][checkoutcomConstants.serviceParams.redirect]
                    && authorizationPostedPayload[checkoutcomConstants.serviceParams.links][checkoutcomConstants.serviceParams.redirect][checkoutcomConstants.serviceParams.href]) {
            // set last payment status as pending
            setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.pending);

            return {
                redirect: true,
                redirectUrl: authorizationPostedPayload[checkoutcomConstants.serviceParams.links][checkoutcomConstants.serviceParams.redirect][checkoutcomConstants.serviceParams.href]
            };
        }

        // set last payment status as declined
        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.declined);

        return {
            error: true,
            message: StringUtils.format('Authorization call is not succeeded, orderNo: {0}, status: {1}, responseCode: {2}, responseMessage: {3}',
                order.orderNo,
                authorizationPostedPayload.status,
                authorizationPostedPayload.response_code,
                authorizationPostedPayload.response_message
            )
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        checkoutcomConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:Authorization call, status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Authorizes a payment using a KNET.
 * Prepares the parameters
 * Calls checkout.com for authorization
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} paymentInformation - The paymentInformation object
 * @param {string} action - Additional action param if it is needed to override
 * @param {string} sourceType - Additional sourceType param if it is needed to override
 * @return {Object} returns an error object
 */
function authorizeKnet(order, paymentInstrument, paymentProcessor, paymentInformation, action, sourceType) {
    action = checkoutcomConstants.action.knet; // eslint-disable-line no-param-reassign
    sourceType = checkoutcomConstants.sourceType.knet; // eslint-disable-line no-param-reassign
    return authorizeCreditCard(order, paymentInstrument, paymentProcessor, paymentInformation, action, sourceType);
}

/**
 * Tokenize apple pay payment.
 * Prepares the parameters
 * Calls the service
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} applePayData - The applePay data object
 * @return {Object} returns an error object
 */
function tokenizeApplePay(order, paymentInstrument, paymentProcessor, applePayData) {
    var action = checkoutcomConstants.action.token;
    var sourceType = checkoutcomConstants.sourceType.applepay;

    var payload = checkoutcomServiceHelpers.initService(
        checkoutcomServiceHelpers.checkoutcomService,
        order,
        applePayData,
        paymentInstrument,
        action,
        sourceType,
        null
    );

    // add request payload as order note
    addNoteToOrder(order, 'Request ApplePay Token', payload);
    var serviceResult = checkoutcomServiceHelpers.checkoutcomService.call(payload);

    if (serviceResult.isOk()) {
        var applepayTokenPostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response ApplePay Token', applepayTokenPostedPayload);

        if (applepayTokenPostedPayload
            && applepayTokenPostedPayload.type === checkoutcomConstants.sourceType.applepay
            && applepayTokenPostedPayload.token) {
            // success
            return {
                success: true,
                applepayToken: applepayTokenPostedPayload
            };
        }

        // set last payment status as declined
        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.declined);

        return {
            error: true,
            message: StringUtils.format('ApplePay Token call is not succeeded, orderNo: {0}', order.orderNo)
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        checkoutcomConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:ApplePay Token call, status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Continue Successfully Authorized CreditCard payment.
 * Checks the fraud
 * Places the order
 * Sends the confirmation email
 * @param {Object} req - The SFCC request object
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted payload from checkout.com
 * @return {Object} returns an error or success object
 */
function continueAuthorizedCreditCard(req, order, paymentInstrument, paymentProcessor, postedPayload) { // eslint-disable-line no-unused-vars
    processAuthorizedResponse(order, paymentInstrument, paymentProcessor, postedPayload);
    return COHelpers.processPlaceOrder(req, order);
}

/**
 * Gets the payment details with given checkoutcom session id or payment id
 * @param {string} checkoutcomId - The payment or payment session identifier
 * @return {Object} the payment details object
 */
function getPaymentDetailsByCheckoutcomId(checkoutcomId) {
    var action = checkoutcomConstants.action.paymentDetail;

    checkoutcomServiceHelpers.initService(
        checkoutcomServiceHelpers.checkoutcomService,
        null,
        null,
        null,
        action,
        null,
        [checkoutcomId]
    );

    var serviceResult = checkoutcomServiceHelpers.checkoutcomService.call();

    if (serviceResult.isOk()) {
        return {
            success: true,
            paymentDetail: serviceResult.object
        };
    }

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, checkoutcomId: {1}, status: {2}, errorCode: {3}, errorMessage: {4}, extraErrorMessage: {5}',
            'Payment Detail',
            checkoutcomId,
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Captures the payment.
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to capture
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted payload from checkout.com
 * @return {Object} returns an error or success object
 */
function capturePayment(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var action = checkoutcomConstants.action.capture;

    var payload = checkoutcomServiceHelpers.initService(
        checkoutcomServiceHelpers.checkoutcomService,
        order,
        null,
        paymentInstrument,
        action,
        null,
        [paymentInstrument.paymentTransaction.getTransactionID()]
    );

    // add request payload as order note
    addNoteToOrder(order, 'Request CAPTURE', payload);
    var serviceResult = checkoutcomServiceHelpers.checkoutcomService.call(payload);

    if (serviceResult.isOk()) {
        var capturePostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response CAPTURE', capturePostedPayload);

        /*
        // Capture accepted
        // success
        // set order as PAID
        // INFO: setting order as paid is being done with webhooks
        // ACTIVATED TEMPORARILY! Requires more investigation.
        */
        Transaction.wrap(function () {
            hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
            });
        });

        // set last payment status as captured
        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.captured);

        return {
            success: true
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        checkoutcomConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'CAPTURE',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Checks the payment status of the given order
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to check
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function checkPaymentStatus(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var checkoutcomId = paymentInstrument.paymentTransaction.getTransactionID();

    // add request payload as order note
    addNoteToOrder(order, 'Request CHECK_STATUS', { checkoutcomId: checkoutcomId });
    var paymentDetailsResult = getPaymentDetailsByCheckoutcomId(checkoutcomId);

    if (paymentDetailsResult.success) {
        var checkStatusPostedPayload = paymentDetailsResult.paymentDetail;

        // add response payload as order note
        addNoteToOrder(order, 'Response CHECK_STATUS', checkStatusPostedPayload);

        var pspStatus;
        switch (checkStatusPostedPayload.status) {
            case checkoutcomConstants.serviceStatus.authorized:
                pspStatus = checkoutcomConstants.pspPaymentStatus.authorized;
                break;
            case checkoutcomConstants.serviceStatus.captured:
            case checkoutcomConstants.serviceStatus.cardVerified:
                pspStatus = checkoutcomConstants.pspPaymentStatus.captured;
                break;
            case checkoutcomConstants.serviceStatus.refunded:
                pspStatus = checkoutcomConstants.pspPaymentStatus.refunded;
                break;
            case checkoutcomConstants.serviceStatus.voided:
                pspStatus = checkoutcomConstants.pspPaymentStatus.voided;
                break;
            default:
                pspStatus = 'Unknown';
                break;
        }

        return {
            success: true,
            status: pspStatus
        };
    }

    var message = paymentDetailsResult.message
        ? paymentDetailsResult.message
        : StringUtils.format('Error occured on getting payment details, orderNo: {0}, checkoutcomId: {1}',
            order.orderNo,
            checkoutcomId);

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        checkoutcomConstants.pspPaymentStatus.error,
        message
    );

    return {
        error: true,
        message: message
    };
}

/**
 * Cancels the payment.
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to cancel
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function cancelPayment(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var action = checkoutcomConstants.action.void;

    var payload = checkoutcomServiceHelpers.initService(
        checkoutcomServiceHelpers.checkoutcomService,
        order,
        null,
        paymentInstrument,
        action,
        null,
        [paymentInstrument.paymentTransaction.getTransactionID()]
    );

    // add request payload as order note
    addNoteToOrder(order, 'Request VOID', payload);
    var serviceResult = checkoutcomServiceHelpers.checkoutcomService.call(payload);

    if (serviceResult.isOk()) {
        var voidPostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response VOID', voidPostedPayload);

        // success
        // set order as cancelled
        /*
        Transaction.wrap(function () {
            hooksHelper('app.order.status.cancelOrder', 'cancelOrder', [order, null, 'checkout.com VOID'], function () {
                OrderMgr.cancelOrder(order);
            });
        });
        */

        // set last payment status as voided
        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.voided);

        return {
            success: true
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        checkoutcomConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'VOID',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Refunds the payment.
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to refund
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function refundPayment(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var action = checkoutcomConstants.action.refund;

    var payload = checkoutcomServiceHelpers.initService(
        checkoutcomServiceHelpers.checkoutcomService,
        order,
        null,
        paymentInstrument,
        action,
        null,
        [paymentInstrument.paymentTransaction.getTransactionID()]
    );

    // add request payload as order note
    addNoteToOrder(order, 'Request REFUND', payload);
    var serviceResult = checkoutcomServiceHelpers.checkoutcomService.call(payload);

    if (serviceResult.isOk()) {
        var refundPostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response REFUND', refundPostedPayload);

        // set last payment status as refunded
        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.refunded);

        return {
            success: true
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        checkoutcomConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'REFUND',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Adds the feedback custom object to the site
 * @param {string} orderNo - The order number
 * @param {string} status - The status of the payment
 * @param {string} payload - The stringified json payload object
 */
function addFeedback(orderNo, status, payload) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var UUIDUtils = require('dw/util/UUIDUtils');

    Transaction.wrap(function () {
        var feedback = CustomObjectMgr.createCustomObject('PSPFeedback', UUIDUtils.createUUID());
        feedback.custom.orderNo = orderNo;
        feedback.custom.status = status;
        feedback.custom.payload = payload;
    });
}

/**
 * Process order feedback payment status against order payment status
 * If different then arrange order payment status
 * @param {Object} req - The SFCC request object
 * @param {dw.order.Order} order - The order to be checked for feedback status
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be checked for feedback status
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {dw.object.CustomObject} feedback - The feedback custom object
 * @return {Object} returns an error or success object
 */
function feedbackPayment(req, order, paymentInstrument, paymentProcessor, feedback) {
    var transactionStatus;
    var processPlaceOrderResult;
    switch (feedback.custom.status) {
        case checkoutcomConstants.webhooksType.paymentApproved:
        case checkoutcomConstants.webhooksType.paymentCaptured:
            switch (order.status.value) {
                case Order.ORDER_STATUS_NEW:
                case Order.ORDER_STATUS_OPEN:
                case Order.ORDER_STATUS_COMPLETED:
                    transactionStatus = new Status(Status.OK);
                    // if the order payment status is not paid and feedback status is paymentCaptured
                    if (order.paymentStatus.value !== Order.PAYMENT_STATUS_PAID
                        && feedback.custom.status === checkoutcomConstants.webhooksType.paymentCaptured) {
                        addNoteToOrder(order, 'Feedback Authorization/Purchase Success, Order payment status: NotPaid', feedback.custom.payload);
                        Transaction.wrap(function () {
                            hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                                order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                            });
                        });

                        // set last payment status as captured
                        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.captured);
                    }
                    break;
                case Order.ORDER_STATUS_CREATED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Success, Order status: Created', feedback.custom.payload);
                    processPlaceOrderResult = COHelpers.processPlaceOrder(req, order);
                    if (processPlaceOrderResult.error) {
                        transactionStatus = new Status(Status.ERROR);
                    } else {
                        transactionStatus = new Status(Status.OK);

                        // service cloud set
                        require('dw/system/HookMgr').callHook('app.order.created', 'created', order);

                        // if the status is paymentCaptured
                        if (feedback.custom.status === checkoutcomConstants.webhooksType.paymentCaptured) {
                            Transaction.wrap(function () {
                                hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                                });
                            });

                            // set last payment status as captured
                            setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.captured);
                        } else { // if paymentApproved
                            // set last payment status as authorized
                            setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.authorized);
                        }
                    }

                    break;
                case Order.ORDER_STATUS_CANCELLED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Success, Order status: Cancelled', feedback.custom.payload);
                    transactionStatus = Transaction.wrap(function () {
                        return hooksHelper('app.order.status.undoCancelOrder', 'undoCancelOrder', [order, 'checkout.com feedback'], function () {
                            return OrderMgr.undoCancelOrder(order);
                        });
                    });

                    if (!transactionStatus.error) {
                        // if the status is paymentCaptured
                        if (feedback.custom.status === checkoutcomConstants.webhooksType.paymentCaptured) {
                            Transaction.wrap(function () {
                                hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                                });
                            });

                            // set last payment status as captured
                            setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.captured);
                        } else { // if paymentApproved
                            // set last payment status as authorized
                            setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.authorized);
                        }
                    }

                    break;
                case Order.ORDER_STATUS_FAILED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Success, Order status: Failed', feedback.custom.payload);
                    transactionStatus = Transaction.wrap(function () {
                        return hooksHelper('app.order.status.undoFailOrder', 'undoFailOrder', [order, 'checkout.com feedback'], function () {
                            return OrderMgr.undoFailOrder(order);
                        });
                    });

                    if (!transactionStatus.error) {
                        processPlaceOrderResult = COHelpers.processPlaceOrder(req, order);

                        if (processPlaceOrderResult.error) {
                            transactionStatus = new Status(Status.ERROR);
                        } else {
                            transactionStatus = new Status(Status.OK);

                            // service cloud set
                            require('dw/system/HookMgr').callHook('app.order.created', 'created', order);

                            // if the status is paymentCaptured
                            if (feedback.custom.status === checkoutcomConstants.webhooksType.paymentCaptured) {
                                Transaction.wrap(function () {
                                    hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                                        order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                                    });
                                });

                                // set last payment status as captured
                                setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.captured);
                            } else { // if paymentApproved
                                // set last payment status as authorized
                                setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.authorized);
                            }
                        }
                    }

                    break;
                default: break;
            }
            break;
        case checkoutcomConstants.webhooksType.paymentDeclined:
        case checkoutcomConstants.webhooksType.paymentCaptureDeclined:
            switch (order.status.value) {
                case Order.ORDER_STATUS_CREATED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Failed, Order status: Created', feedback.custom.payload);
                    transactionStatus = failOrder(order, 'checkout.com feedback');
                    if (!transactionStatus.error) {
                        // set last payment status as declined
                        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.declined);
                    }

                    break;
                case Order.ORDER_STATUS_NEW:
                case Order.ORDER_STATUS_OPEN:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Failed, Order status: New/Open', feedback.custom.payload);
                    transactionStatus = Transaction.wrap(function () {
                        return hooksHelper('app.order.status.cancelOrder', 'cancelOrder', [order, null, 'checkout.com feedback'], function () {
                            return OrderMgr.cancelOrder(order);
                        });
                    });

                    if (!transactionStatus.error) {
                        // set last payment status as declined
                        setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.declined);
                    }

                    break;
                default: break;
            }
            break;
        default: break;
    }

    return {
        error: transactionStatus ? transactionStatus.error : false,
        message: transactionStatus ? transactionStatus.message : ''
    };
}

/**
 * Checks if the saved card locale and target locale have the same service,
 * Different PSP merchants can be used for different country/locale,
 * In order to use saved card functionality, the payment services for savedCard and target locales should be same
 * @param {string} savedCardLocale - the locale for saved card
 * @param {string} targetLocale - the target locale to be checked for saved card is applicable or not
 * @returns {boolean} true if the saved card is applicable for the target locale, false otherwise
 */
function isSavedCardApplicableForLocale(savedCardLocale, targetLocale) {
    return checkoutcomServiceHelpers.getServiceCredentialId(null, savedCardLocale) === checkoutcomServiceHelpers.getServiceCredentialId(null, targetLocale);
}

module.exports = {
    handleCreditCard: handleCreditCard,
    handleKnet: handleKnet,
    authorizeCreditCard: authorizeCreditCard,
    authorizeKnet: authorizeKnet,
    tokenizeApplePay: tokenizeApplePay,
    continueAuthorizedCreditCard: continueAuthorizedCreditCard,
    addNoteToOrder: addNoteToOrder,
    getLogablePayload: getLogablePayload,
    failOrder: failOrder,
    capturePayment: capturePayment,
    preparePaymentInformation: preparePaymentInformation,
    getPaymentDetailsByCheckoutcomId: getPaymentDetailsByCheckoutcomId,
    setLastPSPPaymentStatus: setLastPSPPaymentStatus,
    checkPaymentStatus: checkPaymentStatus,
    cancelPayment: cancelPayment,
    refundPayment: refundPayment,
    addFeedback: addFeedback,
    feedbackPayment: feedbackPayment,
    isSavedCardApplicableForLocale: isSavedCardApplicableForLocale
};
