'use strict';

var server = require('server');

var logger = require('dw/system/Logger').getLogger('checkoutcom.credit');
var Resource = require('dw/web/Resource');

var checkoutcomHelpers = require('*/cartridge/scripts/helpers/checkoutcomHelpers');
var checkoutcomConstants = require('*/cartridge/scripts/util/checkoutcomConstants');
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

/**
 * Generates the handle process error
 * @return {Object} error object
 */
function handleError() {
    return { fieldErrors: [], serverErrors: [Resource.msg('error.card.information.error', 'creditCard', null)], error: true };
}

/**
 * Verifies that entered credit card information is a valid card. If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function Handle(basket, paymentInformation) {
    // check the provided parameters
    if (!basket || !paymentInformation) {
        return handleError();
    }

    try {
        if (paymentInformation.paymentMethod === paymentMethodHelpers.getKnetPaymentMethodId()) {
            return checkoutcomHelpers.handleKnet(basket, paymentInformation);
        }

        return checkoutcomHelpers.handleCreditCard(basket, paymentInformation);
    } catch (e) {
        logger.error('An error occured while handling basket payment, basket: {0}, message {1}, stack: {2}',
            basket.UUID,
            e.message,
            e.stack);
    }

    return handleError();
}

/**
 * Generates the authorize process error
 * @return {Object} error object
 */
function authorizeError() {
    return { fieldErrors: {}, serverErrors: [Resource.msg('error.technical', 'checkout', null)], error: true };
}

/**
 * Authorizes a payment using a credit card.
 * Customizations for checkout.com processor and
 * checkout.com logic to authorize credit card payment.
 * @param {dw.order.Order} order - The order object
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @return {Object} returns an error object
 */
function Authorize(order, orderNumber, paymentInstrument, paymentProcessor) {
    var Transaction = require('dw/system/Transaction');

    // check the provided parameters
    if (!order || !orderNumber || !paymentInstrument || !paymentProcessor) {
        logger.error('An error occured while authorizing order payment, these params are empty: order: {0}, orderNo: {1}, paymentInstrument: {2}, paymentProcessor: {3}',
            !!order, orderNumber, !!paymentInstrument, !!paymentProcessor);
        return authorizeError();
    }

    var creditCardForm = server.forms.getForm('billing').creditCardFields;
    if (!creditCardForm) {
        logger.error('An error occured while authorizing order payment, these are empty: creditCardForm: {0}',
            !!creditCardForm);
        return authorizeError();
    }

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });

        var paymentInformation = checkoutcomHelpers.preparePaymentInformation(creditCardForm, paymentInstrument);

        if (paymentInformation.paymentMethod === paymentMethodHelpers.getKnetPaymentMethodId()) {
            return checkoutcomHelpers.authorizeKnet(order, paymentInstrument, paymentProcessor, paymentInformation);
        }

        return checkoutcomHelpers.authorizeCreditCard(order, paymentInstrument, paymentProcessor, paymentInformation);
    } catch (e) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.error, e.message);

        logger.error('An error occured while authorizing order payment, orderNo: {0}, message {1}, stack: {2}',
            orderNumber,
            e.message,
            e.stack);
    }

    return authorizeError();
}

/**
 * Captures the given order's payment instrument
 * @param {dw.order.Order} order - The order to be captured
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to be captured
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function Capture(order, paymentInstrument, paymentProcessor) {
    try {
        var pspStatus = checkoutcomHelpers.checkPaymentStatus(order, paymentInstrument, paymentProcessor);
        if (pspStatus.success) {
            switch (pspStatus.status) {
                case checkoutcomConstants.pspPaymentStatus.authorized:
                    return checkoutcomHelpers.capturePayment(order, paymentInstrument, paymentProcessor);
                case checkoutcomConstants.pspPaymentStatus.captured:
                    var Transaction = require('dw/system/Transaction');
                    var Order = require('dw/order/Order');
                    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

                    // Capture accepted
                    // success
                    // set order as PAID
                    Transaction.wrap(function () {
                        hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                            order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                            if (order.getExportStatus().value !== Order.EXPORT_STATUS_READY) {
                                order.setExportStatus(Order.EXPORT_STATUS_READY);
                            }
                        });
                    });

                    // set last payment status as captured
                    checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.captured);

                    return {
                        success: true
                    };
                default: break;
            }
        }

        return pspStatus;
    } catch (e) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while capturing order payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Checks the payment status of the given order's payment instrument
 * @param {dw.order.Order} order - The order to be checked
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to be checked
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function CheckStatus(order, paymentInstrument, paymentProcessor) {
    try {
        return checkoutcomHelpers.checkPaymentStatus(order, paymentInstrument, paymentProcessor);
    } catch (e) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomConstants.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while checking the order payment status, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Cancel/Void the given order's payment instrument
 * @param {dw.order.Order} order - The order to be cancelled
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to be cancelled
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function Cancel(order, paymentInstrument, paymentProcessor) {
    try {
        return checkoutcomHelpers.cancelPayment(order, paymentInstrument, paymentProcessor);
    } catch (e) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomHelpers.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while cancelling order payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Refund the given order's payment instrument
 * @param {dw.order.Order} order - The order to be refunded
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to be refunded
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function Refund(order, paymentInstrument, paymentProcessor) {
    try {
        return checkoutcomHelpers.refundPayment(order, paymentInstrument, paymentProcessor);
    } catch (e) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomHelpers.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while refunding order payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Gets the Payment Processor's custom value for save card disabled or not
 * @returns {boolean} true if saved card functionality is disabled
 */
function IsSaveCardDisabled() {
    var Site = require('dw/system/Site');

    return Site.getCurrent().getCustomPreferenceValue('checkoutcomDisableSaveCard');
}

/**
 * Checks if the saved card locale and target locale have the same service,
 * Different PSP merchants can be used for different country/locale,
 * In order to use saved card functionality, the payment services for savedCard and target locales should be same
 * @param {string} savedCardLocale - the locale for saved card
 * @param {string} targetLocale - the target locale to be checked for saved card is applicable or not
 * @returns {boolean} true if the saved card is applicable for the target locale, false otherwise
 */
function IsSavedCardApplicableForLocale(savedCardLocale, targetLocale) {
    return checkoutcomHelpers.isSavedCardApplicableForLocale(savedCardLocale, targetLocale);
}

/**
 * Process order feedback payment status against order payment status
 * If different then arrange order payment status
 * @param {Object} req - The SFCC request object
 * @param {dw.order.Order} order - The order to be checked for feedback status
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be checked for feedback status
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {dw.object.CustomObject} feedback - The feedback custom object
 * @return {Object} returns an error or success object
 */
function Feedback(req, order, paymentInstrument, paymentProcessor, feedback) {
    try {
        return checkoutcomHelpers.feedbackPayment(req, order, paymentInstrument, paymentProcessor, feedback);
    } catch (e) {
        var StringUtils = require('dw/util/StringUtils');
        var message = StringUtils.format(
            'An error occured while processing PSP feedback, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );
        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Inactivate token on PSP side
 * @param {dw.order.Order} order - The refence order for the payment token
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be processed for inactivate psp token
 */
function DeleteToken(order, paymentInstrument) { // eslint-disable-line no-unused-vars

}

/**
 * Gets the Payment Processor's custom value for apple pay is enabled or not
 * also checks the visible/hide areas
 * @returns {Object} Object that has showOnPDP and showOnCart if apple pay is enabled, null otherwise
 */
function ApplePay() {
    var Site = require('dw/system/Site');
    var collections = require('*/cartridge/scripts/util/collections');

    var checkoutcomApplePayEnabled = Site.getCurrent().getCustomPreferenceValue('checkoutcomApplePayEnabled') || false;

    // if it is not enabled then no need to check the locales
    // just return null
    if (!checkoutcomApplePayEnabled) {
        return null;
    }

    var ArrayList = require('dw/util/ArrayList');
    var checkoutcomApplePayEnabledLocales = new ArrayList(Site.getCurrent().getCustomPreferenceValue('checkoutcomApplePayEnabledLocales'));

    // check if the current locale is enabled for the apple pay
    checkoutcomApplePayEnabled = collections.some(checkoutcomApplePayEnabledLocales, function (checkoutcomApplePayEnabledLocale) {
        return checkoutcomApplePayEnabledLocale === request.locale; // eslint-disable-line no-undef
    });

    // if it is not enabled for the current locale
    // just return null
    if (!checkoutcomApplePayEnabled) {
        return null;
    }

    return {
        showOnPDP: (Site.getCurrent().getCustomPreferenceValue('checkoutcomApplePayInjectOnPDP') || false),
        showOnCart: (Site.getCurrent().getCustomPreferenceValue('checkoutcomApplePayInjectOnCart') || false)
    };
}

/**
 * Authorizes a payment using a credit card.
 * Customizations for checkout.com processor and
 * checkout.com logic to authorize apple pay payment.
 * @param {dw.order.Order} order - The order object
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} applePayData - The applePay JSON object
 * @return {Object} returns an error object
 */
function AuthorizeApplePay(order, orderNumber, paymentInstrument, paymentProcessor, applePayData) {
    var Transaction = require('dw/system/Transaction');

    // check the provided parameters
    if (!order || !paymentInstrument || !paymentProcessor || !applePayData) {
        logger.error('An error occured while authorizing order apple pay payment, these params are empty: order: {0}, paymentInstrument: {1}, paymentProcessor: {2}, applePayData: {3}',
            !!order, !!paymentInstrument, !!paymentProcessor, !!applePayData);
        return authorizeError();
    }

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(order.orderNo);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });

        var tokenizeApplePayResult = checkoutcomHelpers.tokenizeApplePay(order, paymentInstrument, paymentProcessor, applePayData);
        if (!tokenizeApplePayResult || tokenizeApplePayResult.error) {
            return authorizeError();
        }

        return checkoutcomHelpers.authorizeCreditCard(order,
            paymentInstrument,
            paymentProcessor,
            tokenizeApplePayResult.applepayToken,
            checkoutcomConstants.action.token,
            checkoutcomConstants.sourceType.token);
    } catch (e) {
        // set last payment status as error
        checkoutcomHelpers.setLastPSPPaymentStatus(order, checkoutcomHelpers.pspPaymentStatus.error, e.message);

        logger.error('An error occured while authorizing order apple pay payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack);
    }

    return authorizeError();
}

exports.Handle = Handle;
exports.Authorize = Authorize;
exports.Capture = Capture;
exports.IsSaveCardDisabled = IsSaveCardDisabled;
exports.IsSavedCardApplicableForLocale = IsSavedCardApplicableForLocale;
exports.CheckStatus = CheckStatus;
exports.Cancel = Cancel;
exports.Refund = Refund;
exports.Feedback = Feedback;
exports.DeleteToken = DeleteToken;
exports.ApplePay = ApplePay;
exports.AuthorizeApplePay = AuthorizeApplePay;
