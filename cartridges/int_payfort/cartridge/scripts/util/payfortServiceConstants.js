'use strict';

var CurrentSite = require('dw/system/Site').getCurrent();

/**
 * Define payFort service related constants.
 */
module.exports = {
    tokenizationURL: CurrentSite.getCustomPreferenceValue('payfortMode').value === 'production' ? 'https://checkout.PayFort.com/FortAPI/paymentPage' : 'https://sbcheckout.PayFort.com/FortAPI/paymentPage',
    apiURL: CurrentSite.getCustomPreferenceValue('payfortMode').value === 'production' ? 'https://paymentservices.payfort.com/FortAPI/paymentApi' : 'https://sbpaymentservices.PayFort.com/FortAPI/paymentApi',
    serviceCredentialType: CurrentSite.getCustomPreferenceValue('payfortServiceCredentialType').value,
    serviceCredentialId: CurrentSite.getCustomPreferenceValue('payfortServiceCredentialId'),
    applePayServiceCredentialType: CurrentSite.getCustomPreferenceValue('payfortApplePayServiceCredentialType').value,
    applePayServiceCredentialId: CurrentSite.getCustomPreferenceValue('payfortApplePayServiceCredentialId')
};
