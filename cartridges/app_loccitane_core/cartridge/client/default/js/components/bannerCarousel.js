'use strict';

var nextArrow = require('../templates/slickArrowNext');
var carousel = require('brand_core/components/carousel');

module.exports = function (selector) {
    $(selector || '.js-banner-carousel').each(function () {
        $(this).removeClass('d-none');
        carousel($(this), {
            dots: false,
            nextArrow: nextArrow,
            prevArrow: false,
            autoplay: true,
            autoplaySpeed: 5000
        });
    });
};
