'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');

/**
 * Render logic for storefront.carousel layout.
 * @param {dw.experience.ComponentScriptContext} context The component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();

    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    model.header = content.header || null;
    model.direction = content.direction === 'row' ? 'flex-row' : 'flex-column';


    return new Template('experience/components/commerce_layouts/container').render(model).text;
};
