'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;

    var model = new HashMap();
    model.imageURL = content.image.file.URL;
    model.imageAlt = content.imageAlt ? content.imageAlt : '';
    model.title = content.title;
    model.description = content.description;
    model.link = content.link;
    model.URL = content.URL;
    model.productConfig = content.productConfig;
    model.id = context.component.ID;

    return new Template('experience/components/commerce_assets/navbarItem').render(model).text;
};
