'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for the storefront.editorialRichText component
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;
    model.containerTitle = content.containerTitle || null;
    model.contentTitle = content.contentTitle || null;
    model.description = content.description;
    model.imageAlt = content.imageAlt;
    model.imageTitle = content.imageTitle;
    model.desktopImage = ImageTransformation.getScaledImage(content.desktopImage);
    model.mobileImage = ImageTransformation.getScaledImage(content.mobileImage);
    model.buttonText = content.buttonText;
    model.link = content.link;
    model.product = content.product;
    return new Template('experience/components/commerce_assets/heroPetite').render(model).text;
};
