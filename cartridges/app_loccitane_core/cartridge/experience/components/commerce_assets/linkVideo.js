'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');
var visibilityMapper = require('*/cartridge/experience/utilities/VisibilityMapper.js');


/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    var thumbnail = content.thumbnail && ImageTransformation.getScaledImage(content.thumbnail);

    model.bgColor = content.bgColor;
    model.duration = content.duration;
    model.link = content.link;
    model.text = content.text;
    model.textColor = content.textColor;
    model.visibility = visibilityMapper(content.visibility);
    model.thumbnailUrl = (thumbnail && thumbnail.src && thumbnail.src.desktop) || null;

    return new Template('experience/components/commerce_assets/linkVideo').render(model).text;
};
