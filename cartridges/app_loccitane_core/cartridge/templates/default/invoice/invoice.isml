<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <!--[if gt IE 9]><!-->
    <isinclude sf-toolkit="off" template="/common/scripts" />
    <!--<![endif]-->
    <isinclude template="/common/htmlHead" />
    <link rel="stylesheet" href="${URLUtils.staticURL('/css/invoice.css')}" />
    <link rel="stylesheet" href="${URLUtils.staticURL('/css/global.css')}" />
    <isactivedatahead />
</head>

<body class="text-direction">
    <div class="header">
        <div class="header-top-border"></div>
        <div class="logo">
            <a href="${URLUtils.url('Home-Show')}"
            title="${Resource.msgf('global.homepage.tooltip', 'common', null,
            Resource.msg('global.storename', 'common', null),
            Resource.msg('global.home', 'common', null))}">
                <img src="${URLUtils.staticURL('/images/logo.svg')}"
                    alt="${Resource.msg('global.storename', 'common', null)}" />
            </a>
        </div>
    </div>
    <div class="container mb-0 invoice-body">
        <div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-12">
                            <p class="invoice-header">
                                ${Resource.msg('invoice.header', 'invoice', null)}
                            </p>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6 invoice-address">
                                    <p class="sub-header">
                                        ${Resource.msg('invoice.address', 'invoice', null)}
                                    </p>
                                    <isif condition="${pdict.order.billing.billingAddress.address.firstName !== null && pdict.order.billing.billingAddress.address.lastName !== null}">
                                        <p>
                                            ${pdict.order.billing.billingAddress.address.firstName}
                                            ${pdict.order.billing.billingAddress.address.lastName}
                                        </p>
                                    </isif>

                                    <isif condition="${pdict.order.billing.billingAddress.address.address1 !== null}">
                                        <p>${pdict.order.billing.billingAddress.address.address1}</p>
                                    </isif>

                                    <isif condition="${pdict.order.billing.billingAddress.address.address2 !== null}">
                                        <p>${pdict.order.billing.billingAddress.address.address2}</p>
                                    </isif>

                                    <isif condition="${pdict.order.billing.billingAddress.address.city !== null}">
                                        <p>
                                            ${pdict.order.billing.billingAddress.address.city}
                                        </p>
                                    </isif>
                                    <isif condition="${pdict.order.billing.billingAddress.address.phone !== null}">
                                        <p dir="ltr">
                                            ${pdict.order.billing.billingAddress.address.phone}
                                        </p>
                                    </isif>
                                </div>

                                <isloop items="${pdict.order.shipping}" var="shippingModel" status="shippingLoop">
                                    <div class="col-6 delivery-address">
                                        <p class="sub-header">
                                            ${Resource.msg('invoice.delivery', 'invoice', null)}
                                        </p>

                                        <isif
                                            condition="${shippingModel.shippingAddress.firstName !== null && shippingModel.shippingAddress.lastName !== null}">
                                            <p>
                                                ${shippingModel.shippingAddress.firstName}
                                                ${shippingModel.shippingAddress.lastName}
                                            </p>
                                        </isif>

                                        <isif condition="${shippingModel.shippingAddress.address1 !== null}">
                                            <p>${shippingModel.shippingAddress.address1}</p>
                                        </isif>

                                        <isif condition="${shippingModel.shippingAddress.address2 !== null}">
                                            <p>${shippingModel.shippingAddress.address2}</p>
                                        </isif>

                                        <isif condition="${shippingModel.shippingAddress.city !== null}">
                                            <p>
                                                ${shippingModel.shippingAddress.city}
                                            </p>
                                        </isif>

                                        <p dir="ltr">
                                            ${shippingModel.shippingAddress.phone}
                                        </p>
                                    </div>
                                </isloop>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-sm-4 tax-right">
                    <p class="e-commerce">
                        ${Resource.msg('invoice.eCommerce', 'invoice', null)}
                    </p>
                    <p class="pb-2 pt-1">
                        ${Resource.msg('invoice.url', 'invoice', null)}
                    </p>
                    <p dir="ltr">
                        ${Resource.msg('invoice.phone', 'invoice', null)}
                    </p>
                    <p>
                        ${Resource.msg('invoice.email', 'invoice', null)}
                    </p>
                    <p class="light">
                        ${Resource.msg('invoice.working', 'invoice', null)}
                    </p>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col">
                    <p class="customer">
                        ${Resource.msg('invoice.customerNumber', 'invoice', null)} ${pdict.customerNo}
                    </p>
                    <p class="customer">
                        ${Resource.msg('invoice.orderNumber', 'invoice', null)} ${pdict.order.orderNumber}
                    </p>
                    <p class="customer">
                        ${Resource.msg('invoice.date', 'invoice', null)}
                        <isprint value="${pdict.order.creationDate}" style="DATE_LONG" />
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col table">
                    <table>
                        <thead>
                            <tr>
                                <th class="first-col">
                                    ${Resource.msg('invoice.item', 'invoice', null)}
                                </th>
                                <th class="min-col">
                                    ${Resource.msg('invoice.qty', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.unitPrice', 'invoice', null)}
                                    <br />
                                    ${Resource.msg('invoice.excl.VAT', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.total', 'invoice', null)}
                                    <br />
                                    ${Resource.msg('invoice.excl.VAT', 'invoice', null)}
                                </th>
                                <th class="min-col">
                                    ${Resource.msg('invoice.vatRate', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.vatAmt', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.total', 'invoice', null)}
                                    <br />
                                    ${Resource.msg('invoice.incl.VAT', 'invoice', null)}
                                </th>
                            </tr>
                        </thead>
                        <isloop items="${pdict.order.shipping}" var="invoiceProduct" status="shippingLoop">
                            <tbody>
                                <isloop items="${invoiceProduct.productLineItems.items}" var="lineItem">
                                    <tr>
                                        <td class="first-col">${lineItem.productName}</td>
                                        <td class="min-col">
                                            <isprint value="${lineItem.quantity}" formatter="##" />
                                        </td>
                                        <td>${lineItem.invoicePrice.unitPriceExclVat}</td>
                                        <td>${lineItem.invoicePrice.totalPriceExclVat}</td>
                                        <td class="min-col">
                                            <isprint value="${pdict.order.totals.taxRate}" formatter="##" />%
                                        </td>
                                        <td>${lineItem.invoicePrice.totalVatAmount}</td>
                                        <td>${lineItem.invoicePrice.totalPriceInclVat}</td>
                                    </tr>
                                </isloop>
                                <tr>
                                    <td class="first-col">${Resource.msg('invoice.shipping', 'invoice', null)}</td>
                                    <td class="min-col"></td>
                                    <td></td>
                                    <td>${pdict.totalShippingCostExclVat ? pdict.totalShippingCostExclVat : ''}</td>
                                    <td class="min-col">
                                        <isprint
                                            value="${pdict.totalShippingCostExclVat ? pdict.order.totals.taxRate : ''}"
                                            formatter="##" />%
                                    </td>
                                    <td>${pdict.totalShippingCostTax ? pdict.totalShippingCostTax : ''}</td>
                                    <td>${pdict.order.totals.totalShippingCost}</td>
                                </tr>
                                <isif condition="${pdict.order.totals.serviceFee}">
                                    <tr>
                                        <td class="first-col">${Resource.msg('invoice.cod.fee', 'invoice', null)}</td>
                                        <td class="min-col"></td>
                                        <td></td>
                                        <td>${pdict.serviceFeeForInvoice.withoutTax}</td>
                                        <td class="min-col">
                                            <isprint value="${pdict.order.totals.taxRate}" formatter="##" />%
                                        </td>
                                        <td>${pdict.serviceFeeForInvoice.taxAmount}</td>
                                        <td>${pdict.order.totals.serviceFee.formatted}</td>
                                    </tr>
                                </isif>
                            </tbody>
                        </isloop>
                    </table>

                    <table class="mt-4">
                        <thead>
                            <tr>
                                <th class="first-col">
                                    ${Resource.msg('invoice.summary', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.vatRate', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.subtotal', 'invoice', null)}
                                    <br />
                                    ${Resource.msg('invoice.excl.VAT', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.total', 'invoice', null)}
                                    <br />
                                    ${Resource.msg('invoice.excl.VAT', 'invoice', null)}
                                </th>
                                <th>
                                    ${Resource.msg('invoice.total.amount', 'invoice', null)}
                                    <br />
                                    ${Resource.msg('invoice.incl.VAT', 'invoice', null)}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="first-col"></td>
                                <td>
                                    <isprint value="${pdict.order.totals.taxRate}" formatter="##" />%
                                </td>
                                <td>${pdict.subtotalExclTax}</td>
                                <td>${pdict.totalExclTax}</td>
                                <td>${pdict.order.priceTotal}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <p class="table-footer">
                        ${Resource.msg('invoice.referance', 'invoice', null)} ${pdict.refNo}
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-12 footer-policy">
                    <p class="sub-header">${Resource.msg('invoice.exchange', 'invoice', null)}</p>
                    <p class="footer-msg">
                        ${Resource.msg('invoice.msg', 'invoice', null)}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="footer-logo">
            <img src="${URLUtils.staticURL('/images/logo.svg')}" alt="lOccitane" />
        </div>
        <p class="pb-2 footer-info">
            ${Resource.msg('invoice.info', 'invoice', null)}
        </p>
        <p class="footer-info">
            ${Resource.msg('invoice.legalName', 'invoice', null)}
        </p>
        <p class="pb-2 footer-info">
            ${Resource.msg('invoice.po.box', 'invoice', null)}
        </p>
        <div class="header-top-border"></div>
    </div>
</body>

</html>
