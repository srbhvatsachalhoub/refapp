<iscomment>

Template Notes:

- This template is intended to be referenced from an <isinclude> tag within an <isloop> in another
  source template.  The <isloop> is expected to have a "status" attribute, named "attributeStatus".
- ${attributeStatus.last} is checked to see whether a particular attribute row should include the
  Quantity drop-down menu

</iscomment>

<isif condition="${attr.id === 'color' && product.colorFamilies}">
    <isinclude template="product/components/colorFamilies" />
</isif>

<div class="attribute attribute-${attr.id} attribute-list-${attr.id} js-attribute-container" data-attribute-id="${attr.id}">
    <isif condition="${attr.swatchable}">
        <isif condition="${!(isBundle && product.productType === 'variant' && !attr.selectedValue)}">
            <div class="attribute-item d-flex flex-wrap">
                <isloop items="${attr.values}" var="attrValue">
                    <button type="button" class="attribute-item-link js-attribute-item-link btn m-1 p-0"
                        data-color-family='${attrValue.colorFamily ? attrValue.colorFamily[0].value : "none"}'
                        data-url="${attrValue.url || 'javscript:void(0)'}"
                        <isif condition="${product.gtmEnhancedEcommerce && product.gtmEnhancedEcommerce.productImpression}">
                        data-gtm-enhancedecommerce-onclick="${JSON.stringify(product.gtmEnhancedEcommerce.productImpression)}"
                        </isif>
                        title="${attrValue.displayValue}"
                        ${ product.productType === "variant" && isBundle ? "disabled" : "" }>
                        <span class="d-flex align-items-center">
                            <span class="${attr.id}-value swatch-circle swatch-value ${attrValue.selected ? 'selected' : ''} ${attrValue.selectable ? 'selectable' : 'unselectable'}"
                                data-attr-value="${attrValue.value}"
                                title="${attrValue.displayValue}"
                                style="background-image: ${attrValue.images &&
                                    attrValue.images.swatch &&
                                    attrValue.images.swatch.length > 0 ? 'url("' + attrValue.images.swatch[0].url + '")' : 'none'}"
                            ></span>
                            <isif condition="${attr.values.length == 1}">
                                <span class="ml-3">${attrValue.displayValue}</span>
                            </isif>
                        </span>
                    </button>
                </isloop>
            </div>
        </isif>
    </isif>
</div>

<div class="attribute attribute-${attr.id} attribute-dropdown-${attr.id} js-attribute-container">
    <isif condition="${attr.swatchable}">
        <isif condition="${attr.values.length > 1}">
            <iscomment>Select color button</iscomment>
            <isif condition="${!(isBundle && product.productType === 'variant' && !attr.selectedValue)}">
                <isif condition="${attr.selectedValue}">
                    <span class="btn btn-link btn-block btn-select custom-select mt-3 d-flex align-items-center ${attr.values.length > 1 ? '' : 'single-select-option'}"
                        data-toggle-class-open="parent">
                        <span data-attr-value="${attr.selectedValue.value}"
                            class="${attr.id}-value swatch-circle swatch-value js-selected-swatch"
                            style="background-image: ${attr.selectedValue.images &&
                                attr.selectedValue.images.swatch &&
                                attr.selectedValue.images.swatch.length > 0 ? 'url("' + attr.selectedValue.images.swatch[0].url + '")' : 'none'}"
                        ></span>
                        <span class="js-selected-swatch-name">${attr.selectedValue.displayValue}</span>
                    </span>
                <iselse/>
                    <span class="btn btn-link btn-block btn-select custom-select mt-3 d-flex align-items-center"
                        data-toggle-class-open="parent">
                        <span class="swatch-circle swatch-value js-selected-swatch d-none"></span>
                        <span class="js-selected-swatch-name">
                            ${Resource.msgf('button.select.color', 'product', null, attr.displayName)}
                        </span>
                    </span>
                </isif>
            </isif>

            <iscomment>Select color dropdown</iscomment>
            <div class="swatches">
                <iscomment>Other color options</iscomment>
                <isloop items="${attr.values}" var="attrValue">
                    <isif condition="${!(isBundle && product.productType === 'variant')}">
                        <button type="button" class="btn btn-block p-0 my-2"
                            data-url="${attrValue.url || 'javscript:void(0)'}"
                            title="${attrValue.displayValue}"
                            ${ product.productType === "variant" && isBundle ? "disabled" : "" }>
                            <span class="d-flex align-items-center">
                                <span class="${attr.id}-value swatch-circle swatch-value ${attrValue.selectable ? 'selectable' : 'unselectable'}"
                                    data-attr-value="${attrValue.value}"
                                    style="background-image: ${attrValue.images &&
                                        attrValue.images.swatch &&
                                        attrValue.images.swatch.length > 0 ? 'url("' + attrValue.images.swatch[0].url + '")' : 'none'}"
                                ></span>
                                <span>${attrValue.displayValue}</span>
                            </span>
                        </button>
                    </isif>
                </isloop>
            </div>
        </isif>

    <iselse/>
        <iscomment>Attribute Values Drop Down Menu</iscomment>
        <select id="${attr.id}" class="d-none form-control select-${attr.id} ${attr.values.length > 1 ? '' : 'single-select-option'}" ${ (product.productType === "variant" && isBundle) || attr.disabled ? "disabled" : "" }>
            <isif condition="${!attr.selectedValue}">
                <option value="${attr.resetUrl}" class="js-attr-default-option">
                    ${Resource.msgf('button.select', 'product', null, attr.displayName)}
                </option>
            </isif>
            <isloop items="${attr.values}" var="attrValue">
                <option value="${attrValue.url}"
                        data-attr-value="${attrValue.value ? attrValue.value : ''}"
                        data-subtext="${attrValue.price ? attrValue.price : ''}"
                        ${!attrValue.selectable ? 'disabled' : ''}
                        ${attrValue.selected ? 'selected' : ''}
                >
                    ${attrValue.displayValue}
                </option>
            </isloop>
        </select>

        <div class="attribute-container js-attribute-carousel"
            data-carousel-slides-to-scroll="1"
            data-initial-item-count="3"
            data-carousel-slides-to-show="[3,3,4,4,4]">
            <isloop items="${attr.values}" var="attrValue">
                <button type="button"
                    data-attr-id="${attr.id}"
                    data-attr-value="${attrValue.value ? attrValue.value : ''}"
                    class="btn btn-select-attribute js-select-attribute ${attrValue.selected ? 'selected' : ''} ${!attrValue.selectable ? 'disabled' : ''}">
                    ${attrValue.displayValue}
                    <isif condition="${attrValue.price}">
                        <span class="d-block attribute-price">${attrValue.price}</span>
                    </isif>
                </button>
            </isloop>
        </div>
    </isif>
</div>
