<isinclude template="/components/modules" sf-toolkit="off" />
<div>
    <div class="js-reviews-response-data" data-more-exists="${pdict.moreExists}"></div>
    <isloop items="${pdict.reviews}" var="review">
        <div class="col-12 col-xl-6 col-4 js-review" data-review-id="${review.id}">
            <div class="review-item">
                <div class="review-date">
                    <isprint value="${review.date}" formatter="dd/MM/YYYY" />
                </div>
                <div class="reviewer-name">${review.reviewerName}</div>
                <div class="reviewer-country">${review.country}</div>
                <div class="review-rating-stars">
                    <isratingstar
                        ratingstar_show_count="false"
                        ratingstar_value="${review.rating}"
                        ratingstar_filled_class="icon-star"
                        ratingstar_half_filled_class="icon-star-fill-half"
                        ratingstar_empty_class="icon-star-empty" />
                </div>
                <isif condition="${review.title}">
                    <div class="review-title">${review.title}</div>
                <iselse/>
                    <div class="review-title">-</div>
                </isif>
                <div class="review-content">
                    <isset name="charLimitForMessage" value="150" scope="page" />
                    <isprint value="${review.message.substring(0, charLimitForMessage)}" encoding="htmlcontent" />
                    <isif condition="${review.message.length > charLimitForMessage}">
                        <a href="${'#'}" class="text-underline font-weight-bold js-review-more-${review.id}"
                            data-toggle-class-hidden=".js-review-more-${review.id}">
                            ${Resource.msg('button.see.more', 'ratingsReviews', null)}
                        </a>
                    </isif>
                    <span class="js-review-more-${review.id} hidden">
                        <isprint value="${review.message.substring(charLimitForMessage)}" encoding="htmlcontent" />
                    </span>
                </div>
                <isif condition="${review.recommended}">
                    <button type="button" class="btn btn-link btn-link-with-icon">
                        <i class="icon-check"></i>
                        <span>${Resource.msg('label.recommended', 'ratingsReviews', null)}</span>
                    </button>
                </isif>
                <div class="review-feedback">${Resource.msg('label.vote.question', 'ratingsReviews', null)}</div>
                <div class="review-feedback-buttons">
                    <button type="button" class="btn btn-link-black js-rating-vote"
                        data-review-id="${review.id}" data-vote-url="${URLUtils.url('RatingsReviews-VoteUp')}">
                        ${Resource.msgf('button.vote.yes.x', 'ratingsReviews', null, review.positiveRelevancy)}
                    </button>
                    <button type="button" class="btn btn-link-black js-rating-vote"
                        data-review-id="${review.id}" data-vote-url="${URLUtils.url('RatingsReviews-VoteDown')}">
                        ${Resource.msgf('button.vote.no.x', 'ratingsReviews', null, review.negativeRelevancy)}
                    </button>
                </div>
            </div>
        </div>
    </isloop>
</div>
