<isinclude template="/components/modules" sf-toolkit="off" />

<div class="product-tile js-product-tile ${product.available ? 'is-instock' : 'is-outofstock'}"
    <isif condition="${product.gtmEnhancedEcommerce && product.gtmEnhancedEcommerce.productImpression}">
        data-gtm-enhancedecommerce-impression="${JSON.stringify(product.gtmEnhancedEcommerce.productImpression)}"
    </isif>>
    <!-- dwMarker="product" dwContentID="${product.uuid}" -->

    <isset name="isEngravingEnabled" value="${product.customization && product.customization.isEngravingEnabled}" scope="page" />

    <div class="engravable-badge ${isEngravingEnabled ? '' : 'invisible'}"><isif condition="${isEngravingEnabled}">${Resource.msg('label.engravable', 'product', null)}</isif></div>

    <iscustombadge custombadge_product="${product}" />

    <isinclude template="product/components/productTileImage" />

    <div class="tile-body">
        <div class="badges-container">
            <isif condition="${pdict.display.showBadges !== false}">
                <isset name="badges" value="${product.badges}" scope="page" />
                <isinclude template="product/components/productTileBadges" />
            </isif>
        </div>

        <div class="swatches-container">
            <isif condition="${pdict.display.swatches !== false}">
                <isinclude template="product/components/productTileVariations" />
            </isif>
        </div>

        <div class="name-container">
            <isinclude template="product/components/productTileName" />

            <isif condition="${pdict.display.showDescription !== false}">
                <isinclude template="product/components/productTileDescription" />
            </isif>
        </div>

        <div class="ratings-price-container">
            <isif condition="${product.ratingSummary}">
                <div class="ratings-container">
                    <isif condition="${pdict.display.ratings !== false}">
                        <isratingstar
                            ratingstar_show_count="false"
                            ratingstar_show_summary="false"
                            ratingstar_count="${product.ratingSummary.ratingCount}"
                            ratingstar_value="${product.ratingSummary.averageRating}"
                            ratingstar_filled_class="icon-star"
                            ratingstar_half_filled_class="icon-star-fill-half"
                            ratingstar_empty_class="icon-star icon-star-empty" />
                    </isif>
                    <span class="separator hidden-md-down"></span>
                </div>
            </isif>

            <div class="price-container">
                <isset name="price" value="${product.price}" scope="page" />
                <isif condition="${product.productType === 'set'}">
                    <isinclude template="product/components/pricing/setPrice" />
                <iselse/>
                    <isinclude template="product/components/pricing/main" />
                </isif>
            </div>
        </div>

     <iscomment>    <isif condition="${pdict.display.showShopNowButton !== false}">
            <isinclude template="product/components/productTileButtonShopNow" />
        </isif> </iscomment>

        <iscomment> <isif condition="${pdict.display.showAddToBagButton}">
            <isinclude template="product/components/productTileButtonAddToBag" />
        </isif> </iscomment>

        <isif condition="${pdict.display.isKitBuilderStarted !== false}">
            <isinclude template="kitBuilder/productTileButtonSelect" />
            <isinclude template="kitBuilder/productTileSelectModal" />
        </isif>
    </div>
    <!-- END_dwmarker -->
</div>
