'use strict';

var currentSite = require('dw/system/Site').getCurrent();

module.exports = currentSite && currentSite.ID !== 'Sites-Site' && currentSite.getCustomPreferenceValue('countries')
    ? JSON.parse(currentSite.getCustomPreferenceValue('countries'))
    : [{
        id: 'en_AE',
        currencyCode: 'AED',
        siteId: 'YSL_AE',
        default: true
    }, {
        id: 'ar_AE',
        currencyCode: 'AED',
        siteId: 'YSL_AE'
    }, {
        id: 'ar_SA',
        currencyCode: 'SAR',
        siteId: 'YSL_SA',
        default: true
    }, {
        id: 'en_SA',
        currencyCode: 'SAR',
        siteId: 'YSL_SA'
    }];
