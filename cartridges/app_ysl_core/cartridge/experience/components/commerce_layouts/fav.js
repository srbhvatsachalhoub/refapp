'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');
var visibilityMapper = require('*/cartridge/experience/utilities/VisibilityMapper.js');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();

    model.bgColor = content.bgColor;
    model.preheader = content.preheader || null;
    model.header = content.header || null;
    model.visibility = visibilityMapper(content.visibility);
    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    model.navItemCount = model.regions.navItems.region.size;
    model.regions.navItems.setClassName('tab-content');
    for (var i = 0; i < model.navItemCount; i++) {
        if (i === 0) {
            model.regions.navItems.setComponentClassName('tab-pane fade active show', { position: i });
        } else {
            model.regions.navItems.setComponentClassName('tab-pane fade', { position: i });
        }
        model.regions.navItems.setComponentAttribute('role', 'tab-panel', { position: i });
        model.regions.navItems.setComponentAttribute('id', 'tab-' + model.regions.navItems.region.visibleComponents[i].ID, { position: i });
        model.regions.navItems.setComponentAttribute('aria-labelledby', 'tab-link-' + model.regions.navItems.region.visibleComponents[i].ID, { position: i });
    }
    model.navItemList = model.regions.navItems.region.visibleComponents;
    model.titles = content.tabHeader.titles;

    return new Template('experience/components/commerce_layouts/fav').render(model).text;
};
