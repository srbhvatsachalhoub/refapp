'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;

    var model = new HashMap();

    // Content
    model.image = ImageTransformation.getScaledImage(content.image);
    model.imageMobile = content.imageMobile ? ImageTransformation.getScaledImage(content.imageMobile) : null;
    model.alt = content.alt || null;
    model.preheader = content.preheader || null;
    model.header = content.header || null;
    model.description = content.description || null;
    model.link = content.link;
    model.buttonName = content.buttonName || null;

    // Styles
    model.position = content.position;
    model.textAlignment = content.textAlignment;
    model.bgColor = content.bgColor;
    model.preheaderTextColor = content.preheaderTextColor;
    model.headerTextColor = content.headerTextColor;
    model.descriptionTextColor = content.descriptionTextColor;
    model.buttonType = content.buttonType;

    return new Template('experience/components/commerce_assets/sliderBanner').render(model).text;
};
