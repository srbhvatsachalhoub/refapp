'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('brand_core/productTile'));
    processInclude(require('brand_core/product/wishlist'));
    processInclude(require('./cart/cart'));
});
