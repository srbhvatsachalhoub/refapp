'use strict';
var base = require('brand_core/checkout/summary');
/**
 * updates the order product shipping summary for an order model
 * @param {Object} order - rendered orderProductSummary html
 */
function updateBonusProductSummary(order) {
    $('.product-summary-block').find('.product-line-item').each(function () {
        if ($(this).data('sample-line-item')) {
            $(this).remove();
        }
    });
    order.shipping.forEach(function (shipping) {
        shipping.productLineItems.items.forEach(function (lineItem) {
            var tmpl = $('#sample-product-template').clone();
            if (!lineItem.isBonusProductLineItem) {
                return;
            }
            if (lineItem.images && lineItem.images.small && lineItem.images.small.length > 0) {
                $('.item-image', tmpl).append('<img class="" src="' + lineItem.images.small[0].url + '" ' +
                    'alt="' + lineItem.images.small[0].alt + '" ' +
                'title="' + lineItem.images.small[0].title + '" />');
            }
            $('.item-name', tmpl).text(lineItem.productName);

            var $itemQty = $('.item-quantity-value', tmpl);
            $itemQty.text($itemQty.data('text').replace('{0}', lineItem.quantity));
            if (lineItem.variationAttributes) {
                for (var i = 0; i < lineItem.variationAttributes.length; i++) {
                    var attribute = lineItem.variationAttributes[i];
                    if (attribute.displayValue) {
                        $('.line-item-attributes-container', tmpl)
                            .append('<span class="line-item-attributes">' + attribute.displayName + ':' + attribute.displayValue + '</span>');
                    }
                }
            }
            if (lineItem.options) {
                for (var k = 0; k < lineItem.options.length; k++) {
                    var option = lineItem.options[k];
                    $('.item-options', tmpl).append('<div class="line-item-option">' + option.displayName + '</div>');
                }
            }
            $('.product-summary-block').append(tmpl.html());
        });
    });
}

base.updateBonusProductSummary = updateBonusProductSummary;

module.exports = base;
