'use strict';

module.exports = function () {
    var $doc = $(document);
    $doc
        .on('click', '.js-tile-attribute-link', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $productTile = $this.closest('.js-product-tile-container');

            // Update selected item
            $this.closest('.js-attribute-carousel')
                .find('.js-tile-attribute-link .js-tile-attribute').removeClass('selected');
            $this.find('.js-tile-attribute').addClass('selected');

            var href = $this.attr('href');
            var qs = href.split('?')[1];
            var url = window.RA_URL['Product-Variation'] + '?' + qs;

            $productTile.spinner().start();
            $.get(url, function (res) {
                // Update product id
                $productTile.attr('data-pid', res.product.id);
                $productTile.find('.js-pt-add-to-cart')
                    .attr('data-pid', res.product.id)
                    .removeClass('disabled')
                    .addClass(res.product.available ? '' : 'disabled');


                // Update image
                if (res.product.images && res.product.images.large && res.product.images.large[0]) {
                    $productTile.find('.js-tile-image').attr('src', res.product.images.large[0].url);
                }

                // Update short description
                $productTile.find('.js-tile-short-description').html(res.product.shortDescription);

                // Update url
                $productTile.find('a:not(.js-tile-attribute-link)').attr('href', res.product.selectedProductUrl);

                // Update price
                var $rangePrice = $productTile.find('.js-price-range');
                if ($rangePrice.length) {
                    $rangePrice.removeClass('range').html(res.product.price.html);
                }

                // Update availability
                $productTile.find('.js-product-tile')
                    .removeClass('is-outofstock')
                    .removeClass('is-instock')
                    .addClass(res.product.available ? 'is-instock' : 'is-outofstock');

                $doc.trigger('tile:update', res);
                $productTile.spinner().stop();
            });
        });
};
