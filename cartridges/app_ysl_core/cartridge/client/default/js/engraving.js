'use strict';


$(document)
    .on('click', '.js-toggle-engraving-settings', function () {
        var $this = $(this);
        var $engravingText = $('.js-engraving-text');
        if ($engravingText.val()) return;
        $this.toggleClass('open');
        $('.js-engraving-settings').slideToggle(function () {
            $engravingText.focus();
        });
    })
    .on('input', '.js-engraving-text', function () {
        if (this.value) {
            $('.js-toggle-engraving-settings .js-icon').addClass('d-none');
            $('.js-shipping-info-regular').addClass('d-none');
            $('.js-shipping-info-customizable').removeClass('d-none');
            this.value = this.value.replace(/[^a-zA-Z0-9\s.\-_!@:.]+/, '');
        } else {
            $('.js-toggle-engraving-settings .js-icon').removeClass('d-none');
            $('.js-shipping-info-regular').removeClass('d-none');
            $('.js-shipping-info-customizable').addClass('d-none');
        }
    })
    .ready(function () {
        $('body')
            .on('updateAddToCartFormData', function (context, form) {
                var $engravingText = $('.js-engraving-text');
                form.customizationSettings = JSON.stringify({ // eslint-disable-line no-param-reassign
                    personalizationRequested: $('.js-personalization-checkbox').prop('checked'),
                    engravingRequested: !!$engravingText.val(),
                    engravingText: $engravingText.val()
                });
            })
            .on('product:afterAttributeSelect', function () {
                $('.js-engraving-text').trigger('input');
            });
    });
