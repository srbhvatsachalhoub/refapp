'use strict';
var processInclude = require('base/util');
var notificationHelper = require('brand_core/helpers/notificationHelper')(
    '.error-messaging'
);
var basePref = require('brand_core/accountPreferences');
/**
 * before previous messages show notification
 */
basePref.showNotification = function () {
    $('body').on('account:preferences:save', function (e, data) {
        // Remove previous messages
        // eslint-disable-next-line no-undef
        var p1 = new Promise(
            function (resolve) {
                resolve($('body').find('.alert').remove());
            });

        p1.then(
            function () {
                if (data && data.success && data.message) {
                    notificationHelper.success(data.message);

                    setTimeout(function () {
                        $('body').find('.alert').remove();
                    }, 5000);
                }
            })
            .catch(
                function () {
                    if (data && data.success && data.message) {
                        notificationHelper.success(data.message);

                        setTimeout(function () {
                            $('body').find('.alert').remove();
                        }, 5000);
                    }
                });
    });
};

module.exports = basePref;
processInclude(module.exports);
