'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    var hrefs = $('.column-2 a');
    if (hrefs) {
        hrefs.each(function () {
            var $this = $(this);
            if (window.location.href.indexOf($this[0].href) !== -1) {
                $this.addClass('current');
            }
        });
    }
});

processInclude(module.exports);
