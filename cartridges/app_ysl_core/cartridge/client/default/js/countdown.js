'use strict';

var processInclude = require('base/util');

$('.header-logo img').css('height', 0);

$(document).ready(function () {
    $('.storepage>div.container').addClass('w-mobile-100');
    // date format is new Date('Jan 25, 2020 15:37:25').getTime();
    var $countdownBox = $('.js-countdown-box');
    if (!$countdownBox.length) { return; }
    var countDownDate = new Date($countdownBox.attr('data-onlinefrom')).getTime();
    // Update the count down every 1 second
    var x = setInterval(function () {
        var now = new Date().getTime();
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
        // If the count down is over, show PDP
        if (distance < 0) {
            clearInterval(x);
            location.reload();
        }
        // Time calculations for days, hours, minutes and seconds and Output the result in an elements
        $('.js-countdown-day').text(Math.floor(distance / (1000 * 60 * 60 * 24)));
        $('.js-countdown-hour').text(Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)));
        $('.js-countdown-min').text(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)));
        $('.js-countdown-sec').text(Math.floor((distance % (1000 * 60)) / 1000));
    }, 1000);
});

processInclude(module.exports);
