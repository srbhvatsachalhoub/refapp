/* globals google */
'use strict';

var geolib = require('geolib');
var MarkerClusterer = require('@google/markerclusterer');

var storeLocator = {
    $mapCanvas: $('.map-canvas'),
    allStores: null,
    stores: null,
    map: null,
    markerClusterer: null,
    markers: [],
    locateButton: null,
    markerClustererOptions: null,
    $form: $('.js-storelocator'),
    $countrySelect: $('.js-storelocator-country'),
    $pacInput: $('.js-storelocator-q'),
    markerImage: null,
    countriesGmapOptions: null,
    currentCountryCode: null,
    $results: $('.js-results'),
    $noResults: $('.js-no-results'),
    infowindow: null,
    $filters: $('.js-storelocator-filters'),
    placesSearchBox: null,
    userPosition: null
};

var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

storeLocator.init = function () {
    if (storeLocator.$mapCanvas.data('has-google-api')) {
        storeLocator.settings();
        storeLocator.maps();
    } else {
        $('.store-locator-no-apiKey').show();
    }

    if (!storeLocator.$results.data('has-results')) {
        storeLocator.$noResults.show();
    }
};

storeLocator.settings = function () {
    storeLocator.markerClustererOptions = {
        imagePath: storeLocator.$mapCanvas.data('gmap-cluster-marker-image-path')
    };
    storeLocator.markerImage = storeLocator.$mapCanvas.data('gmap-marker-image');
    storeLocator.countriesGmapOptions = storeLocator.$mapCanvas.data('countries-gmap-options') || {};
    storeLocator.currentCountryCode = storeLocator.$countrySelect.find('option:selected').val();
    storeLocator.$countrySelect.on('change', function () {
        var selectedCountryCode = $(this).find('option:selected').val();
        if (selectedCountryCode === storeLocator.currentCountryCode) {
            return;
        }
        storeLocator.currentCountryCode = selectedCountryCode;
        storeLocator.loadStores(-1, -1, -1);
    });
    storeLocator.infowindow = new google.maps.InfoWindow();

    // disable form submit. which makes visitor to loose selected location.
    storeLocator.$form.on('submit', function (e) { e.preventDefault(); return false; });

    storeLocator.$filters.find('input[type="checkbox"]').change(function () {
        storeLocator.filterStores();
    });

    $(document).on('click', '.js-get-directions', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var startAddress = $('#id_dwfrm_storeLocator_startAddress')[0].value;
        if (startAddress) {
            url = url + '&saddr=' + startAddress;
            navigator.geolocation.getCurrentPosition(function () {
                url = url + '&saddr=' + startAddress.replace(' ', '+');
                window.open(url, '_blank');
            },
            function () {
                window.open(url, '_blank');
            });
        } else {
            navigator.geolocation.getCurrentPosition(function (position) {
                url = url + '&saddr=' + position.coords.latitude + ',' + position.coords.longitude;
                window.open(url, '_blank');
            },
            function () {
                window.open(url, '_blank');
            });
        }
    });

    $(document).on('click', '.js-use-my-position', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');

        navigator.geolocation.getCurrentPosition(function (position) {
            url = url + '&saddr=' + position.coords.latitude + ',' + position.coords.longitude;
            window.open(url, '_blank');
        },
        function () {
            window.open(url, '_blank');
        });
    });

    $(document).on('click', '.js-store-name', function (e) {
        e.preventDefault();
        storeLocator.linkClickFunction($(this).data('index'));
    });
};

storeLocator.searchbox = function () {
    if (!storeLocator.$pacInput.length) {
        return;
    }

    storeLocator.placesSearchBox = new google.maps.places.SearchBox(storeLocator.$pacInput[0]);

    // [START region_getplaces]
    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(storeLocator.placesSearchBox, 'places_changed', function () {
        var places = storeLocator.placesSearchBox.getPlaces();

        var place = places[0];
        if (place == null) {
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            storeLocator.map.fitBounds(place.geometry.viewport);
            if (storeLocator.map.getZoom() < 11) {
                storeLocator.map.setZoom(11);
            }
        } else {
            storeLocator.map.setCenter(place.geometry.location);
            storeLocator.map.setZoom(12);
        }
    });
    // [END region_getplaces]
};

/**
 * Uses google maps api to render a map
 */
storeLocator.maps = function () {
    var mapOptions;
    mapOptions = {
        maxZoom: 15,
        gestureHandling: 'cooperative'
    };

    storeLocator.map = new google.maps.Map(storeLocator.$mapCanvas[0], mapOptions);
    storeLocator.searchbox();

    var initialFilterStore = true;
    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(storeLocator.map, 'bounds_changed', function () {
        if (storeLocator.placesSearchBox) {
            var bounds = storeLocator.map.getBounds();
            storeLocator.placesSearchBox.setBounds(bounds);
        }

        // initial filtering stores should be done after first bounds_chnaged event
        if (initialFilterStore) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var mapBounds = storeLocator.map.getBounds();
                if (!mapBounds) {
                    return;
                }

                storeLocator.userPosition = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                if (mapBounds.contains(storeLocator.userPosition)) {
                    storeLocator.map.setCenter(storeLocator.userPosition);
                    storeLocator.map.setZoom(12);
                } else {
                    storeLocator.userPosition = null;
                }
            });

            initialFilterStore = false;
            storeLocator.filterStores();
        }
    });

    google.maps.event.addListener(storeLocator.map, 'idle', function () {
        storeLocator.updateList();
        storeLocator.markerClusterer.resetViewport();
    });


    var locations = storeLocator.$mapCanvas.attr('data-locations');
    storeLocator.allStores = JSON.parse(locations);
    storeLocator.stores = storeLocator.allStores;
};

storeLocator.filterStores = function () {
    storeLocator.$pacInput.val('');
    if (storeLocator.allStores == null || storeLocator.allStores.length === 0) {
        storeLocator.stores = storeLocator.allStores;
        storeLocator.showMarkers();
        return;
    }
    var checkeds = storeLocator.$filters.find('input[type="checkbox"]:checked');

    if (checkeds.length <= 0) {
        storeLocator.stores = storeLocator.allStores;
        storeLocator.showMarkers();
        return;
    }

    var newStores = [];
    for (var i = 0; i < storeLocator.allStores.length; i++) {
        var store = storeLocator.allStores[i];

        var cont = true;
        for (var j = 0; j < checkeds.length; j++) {
            if (!store[checkeds[j].value]) {
                cont = false;
                break;
            }
        }
        if (!cont) continue;
        newStores.push(store);
    }
    storeLocator.stores = newStores;
    storeLocator.showMarkers();
};


storeLocator.showMarkers = function () {
    storeLocator.markers = [];

    if (storeLocator.markerClusterer) {
        storeLocator.markerClusterer.clearMarkers();
    }

    var bounds = new google.maps.LatLngBounds();
    if (storeLocator.stores != null) {
        for (var i = 0; i < storeLocator.stores.length; i++) {
            if (storeLocator.stores[i].latitude !== '' && storeLocator.stores[i].longitude !== '' && storeLocator.stores[i].latitude !== null && storeLocator.stores[i].longitude !== null) {
                var latLng = new google.maps.LatLng(storeLocator.stores[i].latitude,
                    storeLocator.stores[i].longitude);

                storeLocator.pushMarker(latLng, i, bounds);
            }
        }
    }

    storeLocator.markerClusterer = new MarkerClusterer(storeLocator.map, storeLocator.markers, storeLocator.markerClustererOptions); // eslint-disable-line no-undef
    var styles = storeLocator.markerClusterer.getStyles();
    styles.forEach(function (style) {
        // eslint-disable-next-line no-param-reassign
        style.textColor = 'white';
    });
    storeLocator.markerClusterer.setStyles(styles);

    // Fit the all the store marks in the center of a minimum bounds when any store has been found.
    if (!bounds.isEmpty() && storeLocator.$mapCanvas && storeLocator.$mapCanvas.length !== 0) {
        storeLocator.map.fitBounds(bounds);
    }
};

storeLocator.pushMarker = function (latLng, i, bounds) {
    var label = labels[i % labels.length];
    var marker = new google.maps.Marker({
        position: latLng,
        icon: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + label + '|000|FFF'
    });

    var fn = storeLocator.markerClickFunction(storeLocator.stores[i], marker);
    google.maps.event.addListener(marker, 'click', fn);
    storeLocator.markers.push(marker);

    // Create a minimum bound based on a set of storeLocations
    bounds.extend(marker.position);
};

storeLocator.linkClickFunction = function (index) {
    var store = storeLocator.stores[index];
    var marker = storeLocator.markers[index];
    if (store == null || marker == null) {
        return;
    }

    var position = marker.getPosition();
    storeLocator.map.setCenter(position);
    storeLocator.map.setZoom(15);

    storeLocator.infowindow.setOptions({
        content: store.infoWindowHtml
    });
    storeLocator.infowindow.open(storeLocator.map, marker);
};

storeLocator.markerClickFunction = function (store, marker) {
    return function (e) {
        if (!store.infoWindowHtml) {
            return;
        }

        e.cancelBubble = true; // eslint-disable-line no-param-reassign
        e.returnValue = false; // eslint-disable-line no-param-reassign
        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }

        storeLocator.infowindow.setOptions({
            content: store.infoWindowHtml
        });
        storeLocator.infowindow.open(storeLocator.map, marker);
    };
};

storeLocator.updateList = function () {
    storeLocator.$results.empty();

    var bounds = storeLocator.map.getBounds();
    if (!bounds) {
        return;
    }

    var mapCenter = storeLocator.map.getCenter();
    var sourcePosition = storeLocator.userPosition ? storeLocator.userPosition : { lat: mapCenter.lat(), lng: mapCenter.lng() };

    var visibleStores = [];
    var i;
    for (i = 0; i < storeLocator.markers.length; i++) {
        var markerPosition = storeLocator.markers[i].getPosition();
        if (bounds.contains(markerPosition)) {
            var distance = geolib.getDistance(
                { latitude: markerPosition.lat(), longitude: markerPosition.lng() },
                { latitude: sourcePosition.lat, longitude: sourcePosition.lng },
                1
            );

            visibleStores.push({
                distance: distance,
                store: storeLocator.stores[i],
                index: i
            });
        }
    }

    visibleStores.sort(function (a, b) {
        return a.distance >= b.distance ? 1 : -1;
    });

    var $mapCanvas = $('.map-canvas');

    for (i = 0; i < visibleStores.length; i++) {
        var store = visibleStores[i].store;
        var $storeListHtml = $(store.infoListHtml);

        var iconUrl = $mapCanvas.data('gmap-marker-image');
        var label = labels[visibleStores[i].index % labels.length];
        iconUrl = iconUrl.replace('{0}', label);
        var icon = $('<img>');
        icon.attr('src', iconUrl);
        $storeListHtml.find('.js-loopstate-count').html('').append(icon);
        $storeListHtml.find('.js-store-name').data('index', visibleStores[i].index);
        storeLocator.$results.append($storeListHtml);
    }

    if (visibleStores.length > 0) {
        storeLocator.$noResults.hide();
    } else {
        storeLocator.$noResults.show();
    }

    $('.js-store-count').html(visibleStores.length);

    var placeQuery = $('.js-storelocator-q').val();
    if (placeQuery) {
        $('.js-store-whereabout span').html(placeQuery);
        $('.js-store-whereabout').removeClass('d-none');
    } else {
        $('.js-store-whereabout').addClass('d-none');
    }
};

storeLocator.loadStores = function (lat, long, radius) {
    var mapCenter = storeLocator.map.getCenter();
    var mapBounds = storeLocator.map.getBounds();
    var topRight = mapBounds.getNorthEast(); // coordinates of the top right corner of the map
    var bottomLeft = mapBounds.getSouthWest(); // coordinates of the bottom left corner of the map

    var distanceInMeter = geolib.getDistance(
        { latitude: topRight.lat(), longitude: topRight.lng() },
        { latitude: bottomLeft.lat(), longitude: bottomLeft.lng() },
        1
    );

    $.spinner().start();
    $.ajax({
        url: storeLocator.$form.attr('action'),
        type: storeLocator.$form.attr('method'),
        dataType: 'json',
        data: {
            lat: lat || mapCenter.lat(),
            long: long || mapCenter.lng(),
            radius: radius || (distanceInMeter / 1000),
            country: storeLocator.$form.find('.js-storelocator-country option:selected').val()
        },
        success: function (data) {
            var locations = JSON.parse(data.locations);
            storeLocator.allStores = locations;
            storeLocator.stores = locations;

            storeLocator.$mapCanvas.attr('data-locations', data.locations);

            var hasResults = data.stores.length > 0;

            storeLocator.$results.empty()
                .data('has-results', hasResults)
                .data('search-key', data.searchKey);

            if (data.storesResultsHtml) {
                storeLocator.$results.append(data.storesResultsHtml);
            }

            storeLocator.filterStores();

            $.spinner().stop();
        }
    });
};

module.exports = {
    init: function () {
        storeLocator.init();
    },
    detectLocation: function () {
        // clicking on detect location.
        $('.js-use-my-current-location').on('click', function () {
            $.spinner().start();
            var geolocatingNotAllowed = function () {
                var $errorMessage = $('.js-geolocation-error');
                $errorMessage.removeClass('d-none');
                setTimeout(function () {
                    $errorMessage.addClass('d-none');
                }, 5000);
            };

            if (!navigator.geolocation || !storeLocator.map) {
                $.spinner().stop();
                geolocatingNotAllowed();
                return;
            }

            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                storeLocator.map.setCenter(pos);
                storeLocator.map.setZoom(12);
            }, function () {
                geolocatingNotAllowed();
            });

            $.spinner().stop();
        });
    }
};

$(document).on('click', '.js-print-map', function () {
    window.print();
});
