'use strict';
/* global session */

var CurrentSite = require('dw/system/Site').current;

/**
 * Gets order's coupon code excluding shipping promotiond
 * @param {Object} order - the order object
 * @returns {string} the coupon code
 */
function getCouponCode(order) {
    var collections = require('*/cartridge/scripts/util/collections');
    var PROMOTION_CLASS_SHIPPING = require('dw/campaign/Promotion').PROMOTION_CLASS_SHIPPING;

    var couponCode = '';
    collections.forEach(order.priceAdjustments, function (priceAdjustment) {
        if ('promotion' in priceAdjustment && priceAdjustment.promotion !== null && priceAdjustment.promotion.promotionClass !== PROMOTION_CLASS_SHIPPING && 'couponLineItem' in priceAdjustment && priceAdjustment.couponLineItem !== null) {
            couponCode = priceAdjustment.couponLineItem.couponCode;
        }
    });

    return couponCode;
}


/**
 * Gets the site current currency code
 * @param {Object} object - the product view model
 * @returns {Object} the current currency code
 */
function getCurrencyCode(object) {
    var currencyConversionRate = CurrentSite.getCustomPreferenceValue('revenueCurrencyConversionRate') || 1.00;
    if (Number(currencyConversionRate) !== 1.00) {
        return 'USD';
    }
    var currencyCode = CurrentSite.getDefaultCurrency();
    if (object && object.price && object.price.sales && object.price.sales.currency) {
        currencyCode = object.price.sales.currency;
    } else if (session && session.currency && session.currency.currencyCode) {
        currencyCode = session.currency.currencyCode;
    }
    return currencyCode;
}

/**
 * adjusts the revenue value with the currency conversion preference. used for unsupported currencies.
 * @param {number} price value to be adjusted by the currency conversion
 * @returns {number} adjusted price
 */
function currencyAdjusted(price) {
    var currencyConversionRate = CurrentSite.getCustomPreferenceValue('revenueCurrencyConversionRate') || 1.00;
    return (price * currencyConversionRate).toFixed(2);
}

/**
 * Generates datalayer product object
 * product: mandatory
 * position: optional
 * quantity: optional
 * @param {Object} product - the product view model
 * @param {number} position - the position
 * @param {number} quantity - the quantity
 * @returns {Object} the datalayer product object
 */
function getProductObject(product, position, quantity) {
    if (!product) {
        return {};
    }

    var productObj = {
        id: product.id,
        name: product.productName,
        brand: 'YSL',
        category: product.category ? product.category.displayName : null
    };

    quantity = quantity || product.quantity; // eslint-disable-line no-param-reassign

    // set price
    if (product.priceTotal && quantity) {
        productObj.price = currencyAdjusted(product.priceTotal.decimalPrice / quantity);
    } else if (product.price && product.price.sales && product.price.sales.value) {
        productObj.price = currencyAdjusted(product.price.sales.value);
    }

    // set position
    if (position || position === 0) {
        productObj.position = position;
    }

    if (product.ratingSummary && product.ratingSummary.averageRating) {
        productObj.dimension38 = product.ratingSummary.averageRating;
    }

    // set quantity
    if (quantity) {
        productObj.quantity = quantity;
    }
    var listValue = [];
    // set variationAttribute
    if (product.variationAttributes) {
        product.variationAttributes.forEach(function (variationAttribute) {
            if (variationAttribute.selectedValue) {
                if (variationAttribute.id && variationAttribute.id.indexOf('size') > -1) {
                    productObj.dimension35 = variationAttribute.selectedValue.displayValue;
                } else if (variationAttribute.id && variationAttribute.id.indexOf('color') > -1) {
                    productObj.dimension36 = variationAttribute.selectedValue.displayValue;
                } else {
                    productObj[variationAttribute.id] = variationAttribute.selectedValue.displayValue;
                }
                listValue.push(variationAttribute.selectedValue.displayValue);
            }
        });
    }
    productObj.variant = listValue.join('/');
    productObj.list = listValue.join('/');
    return productObj;
}

/**
 * Attach enhanced ecommerce product detail
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 */
function productDetail(req, res) { // eslint-disable-line no-unused-vars
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    var product = res.viewData.product;
    if (!product) {
        return;
    }

    res.setViewData({
        pageDataLayerEnhancedEcommerce: [{
            event: 'uaevent',
            eventCategory: 'Ecommerce',
            eventAction: 'Product Detail',
            numberReviews: 'ratingSummary' in product && product.ratingSummary.ratingCount ? product.ratingSummary.ratingCount : 0,
            ecommerce: {
                detail: {
                    products: [getProductObject(product, 0)]
                },
                promoView: {
                    promotions: product.promotions
                }
            }
        }]
    });
}

/**
 * Attach enhanced ecommerce checkoutOption
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {string} currentStage - the current stage
 */
function checkoutOption(req, res, currentStage) {
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    var order = res.viewData.order;
    if (!order) {
        return;
    }

    var step;
    var option;
    switch (res.viewData.currentStage || currentStage) {
        case 'shipping':
            step = 5;

            if (order.shipping && order.shipping.length > 0 && order.shipping[0].selectedShippingMethod) {
                option = order.shipping[0].selectedShippingMethod.ID;
            }
            break;
        case 'payment':
            step = 6;

            if (order.billing && order.billing.payment && order.billing.payment.selectedPaymentInstruments && order.billing.payment.selectedPaymentInstruments.length) {
                option = order.billing.payment.selectedPaymentInstruments[0].paymentMethod;
            }
            break;
        default:
            return;
    }

    if (!option) {
        return;
    }

    var pageDataLayerEnhancedEcommerce = res.viewData.pageDataLayerEnhancedEcommerce || [];
    pageDataLayerEnhancedEcommerce.push({
        event: 'checkoutOption',
        ecommerce: {
            checkout_option: {
                actionField: { step: step, option: option }
            }
        }
    });
    res.setViewData({
        pageDataLayerEnhancedEcommerce: pageDataLayerEnhancedEcommerce
    });
}

/**
 * Attach enhanced ecommerce checkout
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 * @param {string} currentStage - the current stage
 * @param {Object} action - the parent action
 */
function checkout(req, res, currentStage) { // eslint-disable-line no-unused-vars
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    var order = res.viewData.order;
    var items;
    if (res.viewData.items) {
        items = res.viewData.items;
    } else if (order && order.items && order.items.items) {
        items = order.items.items;
    }

    if (!items) {
        return;
    }
    var reqPath = req.path;
    var virtualEvent = {
        event: 'updatevirtualpath',
        virtualPageURL: reqPath,
        virtualPageTitle: ''
    };
    var step = 1;
    switch (res.viewData.currentStage || currentStage) {
        case 'identification':
            step = 3;
            virtualEvent.virtualPageTitle = 'checkout identification - step 2';
            break;
        case 'sample':
            step = 4;
            virtualEvent.virtualPageTitle = 'checkout samples - step 4';
            break;
        case 'shipping':
            step = 5;
            virtualEvent.virtualPageTitle = 'checkout shipping - step 5';
            break;
        case 'payment':
            step = 6;
            virtualEvent.virtualPageTitle = 'checkout payment - step 6';
            break;
        default:
            step = 1;
            virtualEvent.virtualPageTitle = 'checkout cart - step 1';
            break;
    }

    var pageDataLayerEnhancedEcommerce = res.viewData.pageDataLayerEnhancedEcommerce || [];
    pageDataLayerEnhancedEcommerce.push({
        event: 'checkout',
        eventCategory: 'Ecommerce',
        eventAction: 'checkout',
        ecommerce: {
            checkout: {
                actionField: {
                    step: step
                },
                products: items.map(function (pli, index) {
                    return getProductObject(pli, index);
                })
            }
        }
    });
    pageDataLayerEnhancedEcommerce.push(virtualEvent);
    res.setViewData({
        pageDataLayerEnhancedEcommerce: pageDataLayerEnhancedEcommerce
    });
}

/**
 * Attach enhanced ecommerce purchase
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 */
function purchase(req, res) { // eslint-disable-line no-unused-vars
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    var orderModel = res.viewData.order;
    if (!orderModel) {
        return;
    }

    var OrderMgr = require('dw/order/OrderMgr');
    var order = OrderMgr.getOrder(orderModel.orderNumber);
    if (!order || order.custom.isPurchaseGTagPushed) {
        return;
    }

    var Transaction = require('dw/system/Transaction');

    Transaction.wrap(function () {
        // set purchase event pushed as true
        // in order to prevent push the event again
        order.custom.isPurchaseGTagPushed = true;
    });

    var pageDataLayerEnhancedEcommerce = res.viewData.pageDataLayerEnhancedEcommerce || [];
    pageDataLayerEnhancedEcommerce.push({
        event: 'checkout',
        eventCategory: 'Ecommerce',
        eventAction: 'Purchase',
        ecommerce: {
            purchase: {
                actionField: {
                    id: order.orderNo,
                    revenue: currencyAdjusted(order.getAdjustedMerchandizeTotalPrice(true).value),
                    tax: currencyAdjusted(order.adjustedMerchandizeTotalTax.value),
                    shipping: currencyAdjusted(order.adjustedShippingTotalPrice.value),
                    coupon: getCouponCode(order)
                },
                products: orderModel.items.items.map(function (pli, index) {
                    return getProductObject(pli, index);
                })
            }
        }
    }, {
        event: 'updatevirtualpath',
        virtualPageURL: req.path,
        virtualPageTitle: 'checkout confirmation - step 8'
    });

    res.setViewData({
        pageDataLayerEnhancedEcommerce: pageDataLayerEnhancedEcommerce
    });
}

/**
 * Temporary will be reversed
 * @param {Object} req - Current Request from Chain
 * @param {Object} res - Current Response from Chain
 */
function forcePurchase(req, res) { // eslint-disable-line no-unused-vars
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    var orderModel = res.viewData.order;
    if (!orderModel) {
        return;
    }

    var OrderMgr = require('dw/order/OrderMgr');
    var order = OrderMgr.getOrder(orderModel.orderNumber);
    if (!order) {
        return;
    }
    var pageDataLayerEnhancedEcommerce = res.viewData.pageDataLayerEnhancedEcommerce || [];
    pageDataLayerEnhancedEcommerce.push({
        event: 'transaction',
        ecommerce: {
            purchase: {
                actionField: {
                    id: order.orderNo,
                    revenue: currencyAdjusted(order.getAdjustedMerchandizeTotalPrice(true).value),
                    tax: currencyAdjusted(order.adjustedMerchandizeTotalTax.value),
                    shipping: currencyAdjusted(order.adjustedShippingTotalPrice.value)
                },
                products: orderModel.items.items.map(function (pli, index) {
                    return getProductObject(pli, index);
                })
            }
        }
    });

    res.setViewData({
        pageDataLayerEnhancedEcommerce: pageDataLayerEnhancedEcommerce
    });
}

module.exports = {
    getCurrencyCode: getCurrencyCode,
    getProductObject: getProductObject,
    productDetail: productDetail,
    checkout: checkout,
    checkoutOption: checkoutOption,
    purchase: purchase,
    forcePurchase: forcePurchase
};
