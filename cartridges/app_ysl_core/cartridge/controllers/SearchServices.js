'use strict';

var server = require('server');

var cache = require('*/cartridge/scripts/middleware/cache');

server.get('GetSuggestions', cache.applyDefaultCache, function (req, res, next) {
    var SuggestModel = require('dw/suggest/SuggestModel');
    var ContentSuggestions = require('*/cartridge/models/search/suggestions/content');
    var ProductSuggestions = require('*/cartridge/models/search/suggestions/product');
    var contentSuggestions;
    var productSuggestions;
    var searchTerms = req.querystring.q;
    var suggestions;

    var minChars = 3;
    var maxProductSuggestions = 4;
    var maxContentSuggestions = 2;

    if (searchTerms && searchTerms.length >= minChars) {
        suggestions = new SuggestModel();
        suggestions.setSearchPhrase(searchTerms);
        suggestions.setMaxSuggestions(maxProductSuggestions);
        contentSuggestions = new ContentSuggestions(suggestions, maxContentSuggestions);
        productSuggestions = new ProductSuggestions(suggestions, maxProductSuggestions);

        if (productSuggestions.available || contentSuggestions.available) {
            res.render('search/suggestions', {
                suggestions: {
                    product: productSuggestions,
                    content: contentSuggestions
                }
            });
        } else {
            res.json({});
        }
    } else {
        // Return an empty object that can be checked on the client.  By default, rendered
        // templates automatically get a diagnostic string injected into it, making it difficult
        // to check for a null or empty response on the client.
        res.json({});
    }

    next();
});

module.exports = server.exports();
