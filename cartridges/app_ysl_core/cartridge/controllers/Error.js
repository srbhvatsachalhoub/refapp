'use strict';

var server = require('server');
server.extend(module.superModule);

var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Start', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('ErrorCode', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

module.exports = server.exports();
