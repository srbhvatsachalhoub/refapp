'use strict';
var server = require('server');
var RRHelpers = require('*/cartridge/scripts/helpers/ratingsReviewsHelpers');

server.extend(module.superModule);

server.get('WriteReviewButton', function (req, res, next) {
    var productId = req.querystring.pid;
    var currentUserProfile = req.currentCustomer.profile;
    var canReview = RRHelpers.canCustomerReviewProduct(currentUserProfile, productId);

    res.render('/product/components/ratingsReviews/writeReviewButton', {
        allowWriteReview: canReview.allowWriteReview
    });
    next();
});

module.exports = server.exports();
