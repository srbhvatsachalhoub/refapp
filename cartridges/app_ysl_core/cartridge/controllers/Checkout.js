'use strict';
var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

var server = require('server');

server.extend(module.superModule);

/**
 * Appends guestemail form to Checkout Login
 */
server.append('Login', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var CartModel = require('*/cartridge/models/cart');

    var currentBasket = BasketMgr.getCurrentBasket();
    var URLUtils = require('dw/web/URLUtils');
    if (!currentBasket) {
        res.setStatusCode(301);
        res.redirect(URLUtils.url('Cart-Show'));
        return next();
    }
    var TotalsModel = require('*/cartridge/models/totals');
    var totalsModel = new TotalsModel(currentBasket);

    if (req.currentCustomer.profile) {
        return next();
    }
    var guestEmailForm = server.forms.getForm('guestemail');
    guestEmailForm.clear();

    if (currentBasket && currentBasket.getCustomerEmail()) {
        guestEmailForm.email.value = currentBasket.getCustomerEmail();
    }
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.checkout(req, res);
    if (currentBasket.bonusDiscountLineItems && currentBasket.bonusDiscountLineItems.length) {
        res.setViewData({
            hasSample: true,
            currentStage: 'identification'
        });
    }

    var basketModel = new CartModel(currentBasket);

    res.setViewData({
        totals: totalsModel,
        guestEmailForm: guestEmailForm,
        actionUrl: URLUtils.url('Account-Login', 'rurl', 2),
        basketModel: basketModel
    });

    return next();
});

server.append(
    'Begin', function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var currentBasket = BasketMgr.getCurrentBasket();

        var viewData = res.getViewData();

        var CartModel = require('*/cartridge/models/cart');
        var cartModel = new CartModel(currentBasket);
        if (cartModel.bonusDiscountItems && cartModel.bonusDiscountItems.length > 0) {
            var selectedBonusItem = 0;
            cartModel.bonusDiscountItems[0].bonusProducts.forEach(function (bonusProduct) {
                if (bonusProduct.selected) {
                    selectedBonusItem++;
                }
            });
            var currentStage = req.querystring.stage ? req.querystring.stage : 'sample';

            viewData.bonusDiscountItems = cartModel.bonusDiscountItems;
            viewData.selectedBonusDiscountItems = selectedBonusItem;
            viewData.currentStage = currentStage;
        }

        res.setViewData(viewData);
        next();
    }
);

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Begin', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.checkout(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.checkoutOption(req, res);
    next();
});

module.exports = server.exports();
