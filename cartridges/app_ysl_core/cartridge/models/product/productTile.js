'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');
var gtmEnhancedEcommerceAddToCart = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerceAddToCart');
var gtmEnhancedEcommerceProductClick = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerceProductClick');
var gtmEnhancedEcommerceProductImpression = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerceProductImpression');

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
module.exports = function productTile(product, apiProduct, productType, options) {
    decorators.base(product, apiProduct, productType);
    decorators.description(product, apiProduct);
    decorators.sellable(product, apiProduct);
    decorators.availability(
        product,
        options.quantity,
        apiProduct.minOrderQuantity.value,
        inventoryHelpers.getProductAvailabilityModel(apiProduct)
    );
    decorators.quantity(product, apiProduct, 1);
    decorators.variationAttributes(product, options.variationModel, {
        attributes: '*',
        endPoint: 'Variation'
    });
    decorators.price(product, apiProduct, options.promotions, false, options.optionModel);
    decorators.images(product, apiProduct, { types: ['medium'], quantity: 'single' });
    decorators.badges(product, apiProduct, options.promotions);
    decorators.ratingSummary(product, apiProduct);
    decorators.selectedVariationAttributes(product, apiProduct, options.variationModel);
    decorators.customBadge(product, apiProduct);
    decorators.customization(product, apiProduct);
    decorators.category(product, apiProduct, productType);
    gtmEnhancedEcommerceAddToCart(product, apiProduct);
    gtmEnhancedEcommerceProductClick(product, apiProduct);
    gtmEnhancedEcommerceProductImpression(product, apiProduct);
    return product;
};
