'use strict';

var GeneralUtils = require('~/cartridge/scripts/utils/generalUtils');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var fileUtils = require('~/cartridge/scripts/io/libFileUtils').FileUtils;
var XMLIndentingStreamWriter = require('dw/io/XMLIndentingStreamWriter');
var StringUtils = require('dw/util/StringUtils');
var config = GeneralUtils.config;

/**
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 */
function writeAkeneoProductHeader(xswHandle) {
    var catalogID = config.sfccMasterCatalogID;
    // XML definition & first node
    xswHandle.writeStartDocument('UTF-8', '1.0');
    xswHandle.writeStartElement('catalog');
    xswHandle.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/catalog/2006-10-31');
    xswHandle.writeAttribute('catalog-id', catalogID);

    xswHandle.writeStartElement('header');

    xswHandle.writeStartElement('image-settings');
    GeneralUtils.writeElement(xswHandle, 'internal-location', '', 'base-path', '/');
    xswHandle.writeStartElement('view-types');
    GeneralUtils.writeElement(xswHandle, 'view-type', 'hi-res');
    GeneralUtils.writeElement(xswHandle, 'view-type', 'swatch');
    // close xml view-types
    xswHandle.writeEndElement();
    // close xml image-settings
    xswHandle.writeEndElement();
    // close xml header
    xswHandle.writeEndElement();
}

/**
 * @desc Calls Akeneo API to get the Products list
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 * @param {Array} imageCodeList - list of image codes
 * @param {Array} assetCodeList - list of asset codes
 * @returns {void}
 */
function productImageXML(xswHandle) {
    var response;
    var akeneoProduct;
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    var filesList = customCacheWebdav.listFilesInCache(GeneralUtils.config.cacheDirectory.modelProducts.baseLocation);
    var akeneoProductsUrl = GeneralUtils.config.APIURL.endpoints.ProductsUrl;
    var akeneoAttrServiceCall = require('~/cartridge/scripts/akeneoAttributes/akeneoAttrServiceCall');
    var akeneoImageAttrs = akeneoAttrServiceCall.getImageAttrs();
    var writeImagesXML = require('~/cartridge/scripts/akeneoMediaFiles/writeImagesXML');
    var productPagination = require('~/cartridge/scripts/akeneoProducts/productPagination');
    var debugConfig = GeneralUtils.config.debug;
    var pageCounter = 0;
    // write master Products
    for (var index = 0; index < filesList.length; index++) {
        var file = filesList[index];
        var fileName = StringUtils.format(GeneralUtils.config.cacheDirectory.modelProducts.endPoint, file.substring(0, file.lastIndexOf('.')));
        var masterProduct = customCacheWebdav.getCache(fileName);
        if (masterProduct && !masterProduct.parent) {
            writeImagesXML.createImageXML(xswHandle, masterProduct, true, akeneoImageAttrs.imageCodeList, akeneoImageAttrs.assetCodesList);
        }
    }

    try {
        do {
            var paginationURL = (typeof (response) !== 'undefined' && response.serviceNextURL) ? response.serviceNextURL : null;
            response = 	productPagination.getProductsList(akeneoProductsUrl, paginationURL);

            if (response.productsList && response.productsList.getLength() > 0) {
                var iter = response.productsList.iterator();
                while (iter.hasNext()) {
                    akeneoProduct = iter.next();
                    if (akeneoProduct.parent === null) {
                        writeImagesXML.createImageXML(xswHandle, akeneoProduct, false, akeneoImageAttrs.imageCodeList, akeneoImageAttrs.assetCodesList);
                    }
                }
            }

            if (debugConfig.breakCodeOnLimit && ++pageCounter >= debugConfig.pageLimit) {
                break;
            }
        } while (response.serviceNextURL !== '');
    } catch (e) {
        throw new Error('Error occured due to ' + e.stack + ' with Error: ' + e.message);
    }
}

/**
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 */
function createProductFooterXML(xswHandle) {
    // close xml root catalog
    xswHandle.writeEndElement();
    xswHandle.writeEndDocument();
    xswHandle.flush();
}

/**
 * @desc Creates Image XML Header
 * @returns {Object} - XML and File writer Object
 */
function createMediaXml() {
    var catalogID = config.sfccMasterCatalogID;
    var AKENEO_CATALOG_FLUX_DIR = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'akeneo' + File.SEPARATOR + 'catalog' + File.SEPARATOR;
    var AKENEO_CATALOG_FILE_PATH = 'images-akeneo-' + catalogID + '-' + new Date().getTime() + '.xml';
    var file = new File(AKENEO_CATALOG_FLUX_DIR + AKENEO_CATALOG_FILE_PATH);
    fileUtils.createFileAndFolders(file);

    var fwHandle = new FileWriter(file);
    var xswHandle = new XMLIndentingStreamWriter(fwHandle);

    try {
        writeAkeneoProductHeader(xswHandle);
        productImageXML(xswHandle);
        createProductFooterXML(xswHandle);
    } catch (e) {
        throw new Error('ERROR : While writing XML Catalog file : ' + e.stack + ' with Error: ' + e.message);
    }

    return {
        fwHandle: fwHandle,
        xswHandle: xswHandle
    };
}

/* Exported functions */
module.exports = {
    createMediaXml: createMediaXml
};
