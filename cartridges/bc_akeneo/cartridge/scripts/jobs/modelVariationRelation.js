'use strict';

var GeneralUtils = require('~/cartridge/scripts/utils/generalUtils');
var StringUtils = require('dw/util/StringUtils');

/**
 * @desc gets model products from custom cache
 * @param {Object} productWriter - XML writer
 */
function getMasterObject(productWriter) {
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    var filesList = customCacheWebdav.listFilesInCache(GeneralUtils.config.cacheDirectory.modelProducts.baseLocation);
    var akeneoVariationProducts = require('~/cartridge/scripts/akeneoModelProducts/akeneoVariationProducts');

    for (var index = 0; index < filesList.length; index++) {
        var file = filesList[index];
        var fileName = StringUtils.format(GeneralUtils.config.cacheDirectory.modelProducts.endPoint, file.substring(0, file.lastIndexOf('.')));
        var masterProduct = customCacheWebdav.getCache(fileName);

        if (masterProduct && !masterProduct.parent) {
            akeneoVariationProducts.variationProducts(productWriter.xswHandle, masterProduct);
        }
    }
}

/**
 * @desc Writes Model variation relation XML
 */
function createMasterVarCatalog() {
    var akeneoCreateProductsXML = require('~/cartridge/scripts/akeneoCatalog/akeneoCreateProductsXML');
    var productWriter = akeneoCreateProductsXML.createCatalogHeaderXML(6, 'model-variation-products');

    getMasterObject(productWriter);

    akeneoCreateProductsXML.createCatalogFooterXML(productWriter);
}

/* Exported functions */
module.exports = {
    createMasterVarCatalog: createMasterVarCatalog
};
