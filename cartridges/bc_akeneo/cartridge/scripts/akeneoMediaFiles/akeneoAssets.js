'use strict';

/* eslint no-underscore-dangle: 0 */

var Site = require('dw/system/Site');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var Catalog = require('dw/catalog/Catalog');
var File = require('dw/io/File');
var FileUtils = require('~/cartridge/scripts/io/libFileUtils').FileUtils;

var GeneralUtils = require('~/cartridge/scripts/utils/generalUtils');

var akeneoAssets = {};

/**
 * @desc before importing assets into the catalog, clear the directory and import new files
 * @param {Object} CustomPreferences - custom preferences
 * @param {Object} AssetFamily - asset family object
 */
function clearAssetDirectory(CustomPreferences, AssetFamily) {
    var assetFamilyCode = AssetFamily.code.replace(/_|\.|-/g, '').toLowerCase();
    var AKENEO_CATALOG_FLUX_DIR = File.CATALOGS + File.SEPARATOR + CustomPreferences.akeneoProductsCatalogID + File.SEPARATOR + 'default' + File.SEPARATOR + assetFamilyCode + File.SEPARATOR;

    var AkeneoFluxPath = new File(AKENEO_CATALOG_FLUX_DIR);

    // clean directory
    FileUtils.deleteDirectory(AkeneoFluxPath);
}

/**
 * @desc After retrieving the asset file , import the asset into the catalog
 * @param {dw.io.File} fileObject - the file object
 * @param {string} assetUniqueDir - unique path for asset
 * @param {dw.svc.Service} akeneoService - akeneo general Service
 */
function downloadAssets(fileObject, assetUniqueDir, akeneoService) {
    var assetFile = new File(assetUniqueDir + fileObject.code);

    // getting the full folder path for creating it before import
    var folderToCreate = assetUniqueDir + fileObject.code;

    folderToCreate = folderToCreate.split('/');
    folderToCreate.pop();
    folderToCreate = folderToCreate.join('/');

    FileUtils.createFileAndFolders(assetFile);

    // remove file if already exist. We do this because, when the Service set OutFile, it does not remove or overwrite the file.
    if (assetFile.exists()) {
        assetFile.remove();
    }
    // process download asset file from direct link
    akeneoService.setURL(fileObject._links.download.href);

    akeneoService.call({
        outputFile: true,
        fileToOutput: assetFile
    });
}

/**
 * @desc Loop through each asset file in the list and retrieve the variation asset file or reference asset file based on the scope then import it into the catalog
 * @param {Array} AkeneoAssetsList - list of akeneo assets
 * @param {dw.catalog.Catalog} AkeneoCatalog - akeneo catalog
 * @param {dw.svc.Service} AkeneoService - akeneo general service
 * @param {string} AssetFamilyID - akeneo asset family id
 */
function handleAssetsResponse(AkeneoAssetsList, AkeneoCatalog, AkeneoService, AssetFamilyID) {
    var AKENEO_CATALOG_ASSETS_DIR = File.CATALOGS + File.SEPARATOR + AkeneoCatalog.getID() + File.SEPARATOR + 'default' + File.SEPARATOR + 'product';
    if (AssetFamilyID === 'shadethumbnail') {
        AKENEO_CATALOG_ASSETS_DIR = File.CATALOGS + File.SEPARATOR + AkeneoCatalog.getID() + File.SEPARATOR + 'default' + File.SEPARATOR + 'shadethumbnail';
    }
    // Loop on all asset file retrieved from Akeneo API
    var assetsIterator = AkeneoAssetsList.iterator();

    while (assetsIterator.hasNext()) {
        var AkeneoAssetFile = assetsIterator.next();
        try {
            var AKENEO_CATALOG_ASSET_UNIQUE_DIR = AKENEO_CATALOG_ASSETS_DIR + File.SEPARATOR + AkeneoAssetFile.code + File.SEPARATOR;
            if (AkeneoAssetFile.values && AkeneoAssetFile.values.media) {
                for (var i = 0; i < AkeneoAssetFile.values.media.length; i++) {
                    var fileObject = AkeneoAssetFile.values.media[i];
                    fileObject.code = fileObject.data.split(/_(.+)/)[1];
                    downloadAssets(fileObject, AKENEO_CATALOG_ASSET_UNIQUE_DIR, AkeneoService);
                }
            }
        } catch (e) {
            throw new Error('ERROR : While downloading Asset File : ' + AkeneoAssetFile.code + ' with error : ' + e.stack + ' and message : ' + e.message);
        }
    }
}

/**
 * @desc Process akeneo api call for getting all Media files required for products. The call will be processed by SearviceHandler.ds
 *  We ask for ProductsCatalogID because, products can be in an other catalog
 * @param {string} akeneoAssetFamiliesUrl - akeneo Asset Url
 * @param {string} akeneoAssetUrl - akeneo Asset Url
 * @param {Object} getAssetFamily - akeneo Asset Family
 * @param {boolean} isDeltaUpdate - delta parameter
 */
akeneoAssets.generateAssetFiles = function (akeneoAssetFamiliesUrl, akeneoAssetUrl, getAssetFamily, isDeltaUpdate) {
    var CustomPreferences = Site.current.preferences.custom;
    // first of all, we clean directory. If necessary, in other jobs, do a step with this function
    if (!isDeltaUpdate) {
        clearAssetDirectory(CustomPreferences, getAssetFamily);
    }

    if (!CustomPreferences.akeneoServiceGeneralUrl || !CustomPreferences.akeneoProductsCatalogID) {
        throw new Error('ERROR : Site Preference are missing : akeneoServiceGeneralUrl or akeneoProductsCatalogID');
    }

    // Catalog ID must be provide by Site Preference.
    if (CatalogMgr.getCatalog(CustomPreferences.akeneoProductsCatalogID) instanceof Catalog === false) {
        throw new Error('ERROR : No catalog retrieved with ID : ' + CustomPreferences.akeneoProductsCatalogID);
    }

    var AkeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');

    // define service used for call
    var akeneoGetServiceFile = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
    var AkeneoService = akeneoGetServiceFile.getGeneralService();
    var AkeneoAssetDownloadService = akeneoGetServiceFile.getGeneralService();

    AkeneoService.setURL(CustomPreferences.akeneoServiceGeneralUrl);
    AkeneoAssetDownloadService.setURL(CustomPreferences.akeneoServiceGeneralUrl);
    var assetsPerPage;
    var assetsUrl = '';
    var paginationLimit = '?limit=' + GeneralUtils.config.APIURL.parameter.pagination;

    if (isDeltaUpdate) {
        var lastImportedTime = GeneralUtils.getLastImportedTime();
        if (lastImportedTime.length) {
            lastImportedTime = lastImportedTime.replace(' ', 'T') + 'Z';
            var queryObj = {
                updated: [{
                    operator: '>',
                    value: lastImportedTime
                }
                ]
            };
            var StringUtils = require('dw/util/StringUtils');
            assetsUrl = StringUtils.format('{0}?search={1}', akeneoAssetFamiliesUrl + File.SEPARATOR + getAssetFamily.code + File.SEPARATOR + akeneoAssetUrl, encodeURI(JSON.stringify(queryObj)));
        }
    }
    // calling Akeneo asset Files flux
    do {
        // assetsPerPage = AkeneoServicesHandler.serviceRequestCatalogAkeneo(AkeneoService, akeneoAssetUrl + paginationLimit);
        if (typeof assetsUrl !== undefined) {
            if (assetsUrl.length === 0) {
                assetsUrl = akeneoAssetFamiliesUrl + File.SEPARATOR + getAssetFamily.code + File.SEPARATOR + akeneoAssetUrl + paginationLimit;
            }
        }
        assetsPerPage = AkeneoServicesHandler.serviceRequestCatalogAkeneo(AkeneoService, assetsUrl);
        if (assetsPerPage && assetsPerPage.getLength() > 0) {
            handleAssetsResponse(assetsPerPage, CatalogMgr.getCatalog(CustomPreferences.akeneoProductsCatalogID), AkeneoAssetDownloadService, getAssetFamily.code);
        }
    } while (AkeneoServicesHandler.nextUrl !== '');
};

module.exports = akeneoAssets;
