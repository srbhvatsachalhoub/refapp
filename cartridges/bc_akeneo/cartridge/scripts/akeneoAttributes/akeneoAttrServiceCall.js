'use strict';

var ArrayList = require('dw/util/ArrayList');
var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = generalUtils.config;

var akeneoAttrServiceCall = {};

akeneoAttrServiceCall.getAkeneoItems = function () {
    var AkeneoItemsList = new ArrayList();
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    var itemsPerPage;
    var attributesList = customCacheWebdav.getCache(config.cacheDirectory.attributes.attributesList);

    if (attributesList != null) {
        AkeneoItemsList.add(attributesList);
        return AkeneoItemsList;
    }

    var AkeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
    // define service used for call
    var akeneoService = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
    var AkeneoService = akeneoService.getGeneralService();

    // setting the default akeneo hostname
    AkeneoService.setURL(config.serviceGeneralUrl + config.APIURL.endpoints.AttributesUrl + '?limit=' + config.APIURL.parameter.pagination);

    AkeneoServicesHandler.nextUrl = '';

    try {
        do {
            itemsPerPage = AkeneoServicesHandler.serviceRequestAttributesAkeneo(AkeneoService);
            AkeneoItemsList.addAll(itemsPerPage);
        } while (AkeneoServicesHandler.nextUrl !== '');
    } catch (e) {
        throw new Error('ERROR : While calling service to get Attributes List : ' + e.stack + ' with Error: ' + e.message);
    }
    customCacheWebdav = customCacheWebdav.setCache(config.cacheDirectory.attributes.attributesList, AkeneoItemsList.toArray());

    return AkeneoItemsList;
};

akeneoAttrServiceCall.getImageAttrs = function () {
    var imageCodesList = [];
    var assetCodesList = [];
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');

    var imageCodes = customCacheWebdav.getCache(config.cacheDirectory.attributes.imageCodesList);
    var assetCodes = customCacheWebdav.getCache(config.cacheDirectory.attributes.assetCodesList);

    if (imageCodes && assetCodes) {
        return {
            imageCodesList: imageCodes,
            assetCodesList: assetCodes
        };
    }
    var AkeneoAttributesList = this.getAkeneoItems();
    var attributesIterator = AkeneoAttributesList.iterator();
    while (attributesIterator.hasNext()) {
        var attr = attributesIterator.next();

        if (attr.type === 'pim_catalog_asset_collection') {
            assetCodesList.push(attr.code);
        } else if (attr.type === 'pim_catalog_image') {
            imageCodesList.push(attr.code);
        }
    }

    customCacheWebdav.setCache(config.cacheDirectory.attributes.imageCodesList, imageCodesList);
    customCacheWebdav.setCache(config.cacheDirectory.attributes.assetCodesList, assetCodesList);

    return {
        imageCodesList: imageCodesList,
        assetCodesList: assetCodesList
    };
};

module.exports = akeneoAttrServiceCall;
