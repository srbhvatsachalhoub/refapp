'use strict';

var StringUtils = require('dw/util/StringUtils');
var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = generalUtils.config;
var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');

var getFamilyVariants = {};

getFamilyVariants.variantsByfamily = function (masterFamilyCode, variationFamilyCode) {
    if (!(masterFamilyCode && variationFamilyCode)) {
        return undefined;
    }
    var response;

    var serviceURL = StringUtils.format(config.APIURL.endpoints.getFamilyVariant, variationFamilyCode, masterFamilyCode);
    var cacheFileName = StringUtils.format(config.cacheDirectory.familyVariants.endPoint, variationFamilyCode, masterFamilyCode);
    var akeneoFamilyVariantsObject = customCacheWebdav.getCache(cacheFileName);

    if (akeneoFamilyVariantsObject) {
        return akeneoFamilyVariantsObject;
    }

    var AkeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
    // define service used for call
    var initAkeneoServices = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
    var AkeneoService = initAkeneoServices.getGeneralService();

    // setting the default akeneo hostname
    AkeneoService.setURL(config.serviceGeneralUrl);

    AkeneoServicesHandler.nextUrl = '';

    try {
        response = AkeneoServicesHandler.serviceRequestFamilyAkeneo(AkeneoService, serviceURL);
        customCacheWebdav.setCache(cacheFileName, response);
    } catch (e) {
        throw new Error('ERROR : While calling service to get family variants  : ' + e.stack + ' with Error: ' + e.message);
    }

    return response;
};

module.exports = getFamilyVariants;
