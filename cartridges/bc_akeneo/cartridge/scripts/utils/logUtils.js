'use strict';

// Global Variables
var logUtils = {};

/**
 * Creates custom log file for the cartridge
 * @param {string} category - the category to log in
 * @returns {dw.system.Logger} - logger object
 */
logUtils.getLogger = function (category) {
    var Logger = require('dw/system/Logger');
    var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
    var defaultLogFilePrefix = generalUtils.config.customLogFileName;

    if (category) {
        return Logger.getLogger(defaultLogFilePrefix, category);
    }
    return Logger.getLogger(defaultLogFilePrefix);
};

module.exports = logUtils;
