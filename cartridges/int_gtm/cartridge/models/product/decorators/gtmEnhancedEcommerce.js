'use strict';

module.exports = function (object) {
    var CurrentSite = require('dw/system/Site').current;
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId') || object.gtmEnhancedEcommerce) {
        return;
    }

    Object.defineProperty(object, 'gtmEnhancedEcommerce', {
        enumerable: true,
        value: {}
    });
};
