'use strict';

var base = module.superModule;

var gtmEnhancedEcommerceAddToCart = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerceAddToCart');
var gtmEnhancedEcommerceProductClick = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerceProductClick');
var gtmEnhancedEcommerceProductImpression = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerceProductImpression');

/**
 * Decorate product with gtm enhanced ecommerce event object
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {Object} options - Options passed in from the factory
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function productTile(product, apiProduct, productType, options) {
    base.call(this, product, apiProduct, productType, options);
    gtmEnhancedEcommerceAddToCart(product, apiProduct);
    gtmEnhancedEcommerceProductClick(product, apiProduct);
    gtmEnhancedEcommerceProductImpression(product, apiProduct);
    return product;
};
