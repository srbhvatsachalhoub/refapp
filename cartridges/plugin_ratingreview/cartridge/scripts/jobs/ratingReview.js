/* global request */
var StringUtils = require('dw/util/StringUtils');
var ratings = [1, 2, 3, 4, 5];

/**
 * Rating Validity Check
 * @param {int} rating rating value
 * @returns {boolean} rating validity
 */
function checkRatingValidity(rating) {
    if (ratings.indexOf(rating) !== -1) {
        return true;
    }
    return false;
}

/**
 * Generates a key for product Id and language
 * @param {string} productId Id of the product
 * @param {string} language language
 * @returns {string} concataneted string for product Id and language
 */
function getProductIdAndLanguageKey(productId, language) {
    return StringUtils.format('{0}_{1}', productId, language);
}

/**
 * Returns Products Reviews key productId
 * @returns {Object[]} object array with products reviews.
 */
function getProductsReviews() {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var reviewsItr = CustomObjectMgr.queryCustomObjects('RatingReview', 'custom.approved = true', null);
    var productsReviews = {};
    while (reviewsItr.hasNext()) {
        var reviewObj = reviewsItr.next();
        var reviewLocale = reviewObj.custom.locale;
        var language = reviewLocale.split('_')[0];
        var productIdAndLanguageKey = getProductIdAndLanguageKey(reviewObj.custom.productId, language);
        if (productIdAndLanguageKey in productsReviews === false) {
            productsReviews[productIdAndLanguageKey] = [];
        }
        productsReviews[productIdAndLanguageKey].push({
            rating: reviewObj.custom.rating,
            recommended: reviewObj.custom.recommended
        });
    }
    return productsReviews;
}

/**
 * Rating Count Calculation
 * @param {Object[]} reviews reviews array for a specific product
 * @returns {int} rating count.
 */
function calculateRatingCount(reviews) {
    return reviews ? reviews.length : 0;
}

/**
 * Rating Distrubution Calculation
 * @param {Object[]} reviews reviews array for a specific product
 * @returns {string} rating Distrubution string.
 */
function calculateRatingDistribution(reviews) {
    var ratingDistribution = ratings.reduce(function (map, rating) {
        map[rating] = 0; // eslint-disable-line no-param-reassign
        return map;
    }, {});
    if (reviews) {
        for (var i = 0; i < reviews.length; i++) {
            if (checkRatingValidity(reviews[i].rating)) {
                ratingDistribution[reviews[i].rating]++;
            }
        }
    }
    return JSON.stringify(ratingDistribution);
}

/**
 * Recommended Percentage Calculation
 * @param {Object[]} reviews reviews array for a specific product
 * @returns {int} recommended percentage.
 */
function calculateRecommendedPercentage(reviews) {
    var recommendedCount = 0;
    var recommendedPercentage;
    if (reviews) {
        for (var i = 0; i < reviews.length; i++) {
            if (reviews[i].recommended) {
                recommendedCount++;
            }
        }
        recommendedPercentage = (recommendedCount / reviews.length) * 100;
    } else {
        recommendedPercentage = 0;
    }
    return recommendedPercentage;
}

/**
 * Average Rating Calculation
 * @param {Object[]} reviews reviews array for a specific product
 * @returns {double} average rating.
 */
function calculateAverageRating(reviews) {
    var totalRating = 0;
    var totalCount = 0;
    var averageRating = 0;
    if (reviews) {
        for (var i = 0; i < reviews.length; i++) {
            if (checkRatingValidity(reviews[i].rating)) {
                totalRating += reviews[i].rating;
                totalCount++;
            }
        }
        if (totalCount !== 0) {
            averageRating = totalRating / totalCount;
        }
    } else {
        averageRating = 0;
    }
    return parseFloat(averageRating.toFixed(1));
}

/**
 * Product Review Stat Calculation calculates review statistics and assigns it to product
 * @param {dw.catalog.Product} product product for assigning review stats
 * @param {Object[]} productsReviews array with products reviews
 * @param {Object} groupedLocales grouped locales
 * @param {dw.system.Request} req request object
 */
function calculateReviewStats(product, productsReviews, groupedLocales, req) {
    var Transaction = require('dw/system/Transaction');

    Object.keys(groupedLocales).forEach(function (language) {
        var productIdLanguageKey = getProductIdAndLanguageKey(product.ID, language);
        var reviews = productsReviews[productIdLanguageKey];
        if (!reviews) {
            return;
        }
        var ratingCount = calculateRatingCount(reviews);
        var ratingDistribution = calculateRatingDistribution(reviews);
        var recommendedPercentage = calculateRecommendedPercentage(reviews);
        var averageRating = calculateAverageRating(reviews);

        groupedLocales[language].forEach(function (locale) {
            req.setLocale(locale);
            if (product.custom.ratingCount !== ratingCount) {
                Transaction.wrap(function () {
                    product.custom.ratingCount = ratingCount; // eslint-disable-line no-param-reassign
                    product.custom.ratingDistribution = ratingDistribution; // eslint-disable-line no-param-reassign
                    product.custom.averageRating = averageRating; // eslint-disable-line no-param-reassign
                    product.custom.recommendedPercentage = recommendedPercentage; // eslint-disable-line no-param-reassign
                });
            }
        });
    });
}

/**
 * Puts allows locales for given site into a map object by languages
 * @returns {Object} groupped locales
 */
function getGroupedLocalesByLanguage() {
    var Site = require('dw/system/Site');
    var currentSite = Site.getCurrent();
    var allowedLocales = currentSite.allowedLocales;
    var groupedLocales = {};
    var collections = require('*/cartridge/scripts/util/collections');
    collections.forEach(allowedLocales, function (locale) {
        var language = locale.split('_')[0];
        if (language in groupedLocales === false) {
            groupedLocales[language] = [];
        }
        groupedLocales[language].push(locale);
    });
    return groupedLocales;
}

/**
 * Entry function for the job: traverses every product and sets review stats
 */
function execute() {
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var productsReviews = getProductsReviews();
    var groupedLocales = getGroupedLocalesByLanguage();
    var psm = new ProductSearchModel();
    psm.setCategoryID('root');
    psm.orderableProductsOnly = false;
    psm.recursiveCategorySearch = true;
    psm.search();
    var products = psm.getProductSearchHits();
    var distinctProducts = [];

    while (products.hasNext()) {
        var productHit = products.next();

        var product = productHit.product;
        if (!product) {
            continue;
        }
        if (distinctProducts.indexOf(product.ID) === -1) {
            distinctProducts.push(product.ID);
            calculateReviewStats(product, productsReviews, groupedLocales, request);
        }

        var representedProducts = productHit.representedProducts.iterator();
        while (representedProducts.hasNext()) {
            product = representedProducts.next();
            if (distinctProducts.indexOf(product.ID) === -1) {
                distinctProducts.push(product.ID);
                calculateReviewStats(product, productsReviews, groupedLocales, request);
            }
        }
    }
}
module.exports = {
    execute: execute
};
