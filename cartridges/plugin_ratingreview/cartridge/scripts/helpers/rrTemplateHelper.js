
var Site = require('dw/system/Site');

/**
 * Checks if the feature switch for Ratings and Reviews is enabled
 * @returns {boolean} true if enabled or empty, false otherwise
 */
function ratingsReviewsEnabled() {
    return Site.current.getCustomPreferenceValue('rrRatingsAndReviews') !== false;
}
/**
 * Checks if the feature switch for mandatory of Ratings and Reviews's attribute which "bought product before" is enabled
 * @returns {boolean} false if enabled or empty, true otherwise
 */
function customerBoughtProduct() {
    return Site.current.getCustomPreferenceValue('rrProductBought') !== false;
}
/**
 * Checks if the feature switch for mandatory of Ratings and Reviews's attribute which "review already submitted" is enabled
 * @returns {boolean} false if enabled or empty, true otherwise
 */
function reviewAlreadySubmitted() {
    return Site.current.getCustomPreferenceValue('rrReviewAlreadySubmitted') !== false;
}

module.exports = {
    ratingsReviewsEnabled: ratingsReviewsEnabled,
    customerBoughtProduct: customerBoughtProduct,
    reviewAlreadySubmitted: reviewAlreadySubmitted
};
