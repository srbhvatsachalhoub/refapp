/**
 * Builds order transaction details
 * @param {Object} orderModel order export model
 * @param {dw.order.Order} lineItemCtnr lineItemCtnr of Order
 */
module.exports = function (orderModel, lineItemCtnr) {
    var UUIDUtils = require('dw/util/UUIDUtils');
    var StringUtils = require('dw/util/StringUtils');
    var Site = require('dw/system/Site');

    var customerLocaleId = lineItemCtnr.customerLocaleID;

    var Locale = require('dw/util/Locale');

    var custLocale = Locale.getLocale(customerLocaleId);
    var fulfillmentStoreJson = Site.current.getCustomPreferenceValue('speedBusFullfillmentId') ? JSON.parse(Site.current.getCustomPreferenceValue('speedBusFullfillmentId')) : {};
    var locationJson = Site.current.getCustomPreferenceValue('speedBusStoreId') ? JSON.parse(Site.current.getCustomPreferenceValue('speedBusStoreId')) : {};

    var speedBusFullfillmentId = custLocale.getCountry() in fulfillmentStoreJson ? fulfillmentStoreJson[custLocale.getCountry()] : null;
    var location = custLocale.getCountry() in locationJson ? locationJson[custLocale.getCountry()] : null;

    var transactionNumber = UUIDUtils.createUUID().substr(0, 10);
    Object.defineProperty(orderModel, 'Base_Site_Currency', {
        enumerable: true,
        value: lineItemCtnr.getCurrencyCode()
    });
    var transactionDate = new Date();
    Object.defineProperty(orderModel, 'Transaction_Number', {
        enumerable: true,
        value: transactionNumber
    });
    Object.defineProperty(orderModel, 'Transaction_Time', {
        enumerable: true,
        value: transactionDate.toISOString()
    });
    Object.defineProperty(orderModel, 'Location', {
        enumerable: true,
        value: location || -1
    });
    Object.defineProperty(orderModel, 'fulfillmentStore', {
        enumerable: true,
        value: speedBusFullfillmentId || -1
    });
    Object.defineProperty(orderModel, 'Business_Date', {
        enumerable: true,
        value: StringUtils.formatCalendar(Site.getCurrent().getCalendar(), 'yyyyMMdd')
    });
};
