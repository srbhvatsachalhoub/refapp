'use strict';
var Status = require('dw/system/Status');
var logger = require('dw/system/Logger').getLogger('int_speedbus');
var File = require('dw/io/File');
var StringUtils = require('dw/util/StringUtils');
var HashMap = require('dw/util/HashMap');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

var CSVHEADERS = {
    0: 'Location',
    1: 'Item',
    2: 'Barcode',
    3: 'VPN',
    4: 'STOCK_ON_HAND',
    5: 'Timestamp'
};
/**
 * Reads CSV file and extracts allocations of inventory records of Products.
 * @param {string} csvSeparator SEPARATOR of CSV File default is comma
 * @param {int} globalThreshold Global Threshold value
 * @param {dw.io.File} file file to be read
 * @param {dw.util.HashMap} records records of inventory productid, allocation pair
 */
function processFiles(csvSeparator, globalThreshold, file, records) {
    var FileReader = require('dw/io/FileReader');
    var CSVStreamReader = require('dw/io/CSVStreamReader');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var fileReader = new FileReader(file);
    var csvReader = new CSVStreamReader(fileReader, csvSeparator);
    var expectedLengthOfLines = Object.keys(CSVHEADERS).length;
    var line = csvReader.readNext(); // headers of csv file
    var hasSiteMultipleInventory = inventoryHelpers.hasSiteMultipleInventory();
    if (line.length !== expectedLengthOfLines) {
        logger.error('\n\nCSV header length does not match with expected length.\n File name: {0}, expected length: {1}, actual length: {2}', file.getName(), expectedLengthOfLines, line.length);
    }
    while (line !== null) {
        line = csvReader.readNext(); // move to next line
        if (line === null) {
            break;
        }
        if (line.length !== expectedLengthOfLines) {
            logger.error('\n\nCSV line length does not match with expected length. \n File name: {0}, expected length: {1}, actual length: {2}', file.getName(), expectedLengthOfLines, line.length);
            continue;
        }
        var productId = line[3]; // VPN (SKU) = Product ID
        var allocation = parseInt(line[4], 10) || 0; // STOCK_ON_HAND = allocation
        if (!productId) {
            logger.info('\n\nVPN (SKU) value is not presented in file {0}, skipping line', file.getName());
            continue;
        }

        if (allocation < 0) {
            allocation = 0; // set as 0 if it is less than 0
        }

        var product = ProductMgr.getProduct(productId);
        if (!product) {
            logger.info('\n\nThere is no matcing product with {0} in file {1}, record will be added without having product definition', productId, file.getName());
        } else {
            // check product threshold value definition first, if defined use it. Otherwise check global threshold value if presented use it. Worst case is no threshold value
            var thresholdValue = ('safetyThreshold' in product.custom && product.custom.safetyThreshold) ? product.custom.safetyThreshold : globalThreshold;

            // safety stock logic CHL-74
            if (thresholdValue && allocation < thresholdValue) {
                allocation = 0; // if safetyThreshold amount is bigger than allocation, set allocation of product to 0.
            }

            // check if the current site has multiple inventory list
            // check if the product is SAMPLE product
            // if so then increment allocation by one
            // because: these products are being used by Bonus promotion(s) and should be added to cart by default
            // then cart.calculate will check the availability from the relevant inventory list
            // sample product ATS=10 means, it is not available
            if (hasSiteMultipleInventory && ('isSample' in product.custom) && product.custom.isSample) {
                allocation += 10;
            }
        }
        records.put(productId, allocation);
    }
    csvReader.close();
    fileReader.close();
}

/**
 * Prepares SFCC Inventory File for Import
 * @param {string} directory directory to generate new XML file
 * @param {string} inventoryListId inventory list id
 * @param {dw.util.HashMap} records  allocations
 */
function generateXmlFile(directory, inventoryListId, records) {
    var Site = require('dw/system/Site');
    var FileWriter = require('dw/io/FileWriter');
    var XMLStreamWriter = require('dw/io/XMLStreamWriter');
    var namespace = 'http://www.demandware.com/xml/impex/inventory/2007-05-31';
    var fileName = StringUtils.format('{0}-inventory-{1}.xml', Site.current.ID, new Date().getTime().toString());
    var inventoryFile = new File(
        // File.IMPEX + File.SEPARATOR + directory + File.SEPARATOR + fileName
        StringUtils.format('{0}{1}{2}{1}{3}', File.IMPEX, File.SEPARATOR, directory, fileName)
    );

    if (inventoryFile.exists()) {
        inventoryFile.remove();
    }

    var inventoryFileWriter = new FileWriter(inventoryFile);
    var xsw = new XMLStreamWriter(inventoryFileWriter);
    xsw.writeStartDocument('UTF-8', '1.0');
    xsw.writeStartElement('inventory');
    xsw.writeNamespace('xmlns', namespace);
    /* eslint-disable */
        xsw.writeStartElement('inventory-list');
            // inventory header
            xsw.writeStartElement('header');
                xsw.writeAttribute('list-id', inventoryListId); // inventory list id
                xsw.writeStartElement('default-instock');
                    xsw.writeCharacters('false');
                xsw.writeEndElement();
            xsw.writeEndElement();

            // records
            xsw.writeStartElement('records');
                // iterate records
                var recordItr = records.entrySet().iterator();
                while (recordItr.hasNext()) {
                    var inventoryRecord = recordItr.next();
                    xsw.writeStartElement('record');
                        xsw.writeAttribute('product-id', inventoryRecord.key);
                        xsw.writeStartElement('allocation');
                            xsw.writeCharacters(inventoryRecord.value);
                        xsw.writeEndElement();
                        xsw.writeStartElement('perpetual');
                            xsw.writeCharacters('false');
                        xsw.writeEndElement();
                    xsw.writeEndElement();
                }
            xsw.writeEndElement();
        xsw.writeEndElement();
    xsw.writeEndElement();
    xsw.writeEndDocument();
    xsw.close();
    inventoryFileWriter.close();
}


/**
 * Generates SFCC Standart Inventory File with given arguments
 * @param {dw.util.HashMap} args job arguments
 * @returns {dw.system.Status} status of process
 */
function execute(args) {
    var Site = require('dw/system/Site');
    var inventoryListId = args.inventoryListId;

    var processFilePattern = args.processFilePattern || '^.*\.csv$';
    var globalThreshold = args.threshold || null;
    var inventoryDirectory = args.inventoryDirectory || StringUtils.format('src/speedbus/{0}/inventory', Site.current.ID);
    var regExpPattern = new RegExp(processFilePattern);
    var csvSeparator = args.separator || ',';
    var error = new Status(Status.ERROR);
    var ok = new Status(Status.OK);

    if (!inventoryListId) {
        logger.error('\n\ninventoryListId must be defined');
        return error;
    }

    var directoryToProcess = new File(File.IMPEX + File.SEPARATOR + inventoryDirectory);

    if (!directoryToProcess.exists()) {
        logger.info('\n\n{0} is not exists, created directories', inventoryDirectory);
        directoryToProcess.mkdirs();
        return ok;
    }

    if (!directoryToProcess.isDirectory()) {
        logger.error('\n\nExpected {0} to be a directory, but it is not!', inventoryDirectory);
        return error;
    }

    var csvFiles = directoryToProcess.listFiles(function (file) {
        return file && file.getName() && !!file.getName().match(regExpPattern);
    });

    if (!csvFiles || !csvFiles.length) {
        logger.info('\n\nNo files found with matching {0} regexp in {1}', processFilePattern, inventoryDirectory);
        return ok;
    }

    if (csvFiles.length > 1) {
        csvFiles.sort(function (lhs, rhs){
            if(lhs.lastModified() < rhs.lastModified()){
                return -1;
            }
            return 1;
        });
    }

    /*
    * we can have multiple csv files at the same time, so when we have multiple records for the same product, we need to use minimum amount of inventory.
    */
    var records = new HashMap();
    var collections = require('*/cartridge/scripts/util/collections');
    collections.forEach(csvFiles, function (file) {
        processFiles(csvSeparator, globalThreshold, file, records);
    });

    // when all files were processed, check if we have records and generate XML file
    if (records.isEmpty()) {
        logger.info('\n\nThere are no inventory records to process, ending process');
        return ok;
    }
    // Final step to generate Inventory XML file based on records
    try {
        generateXmlFile(inventoryDirectory, inventoryListId, records);
    } catch (e) {
        logger.error('\n\nSomething went wrong while generating XML file, ending process as ERROR, exception message : {0}', e.message);
        return error;
    }

    return ok;
}

module.exports = {
    execute: execute
};
