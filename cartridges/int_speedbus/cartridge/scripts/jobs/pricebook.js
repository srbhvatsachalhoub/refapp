'use strict';
var Status = require('dw/system/Status');
var logger = require('dw/system/Logger').getLogger('int_speedbus');
var File = require('dw/io/File');
var StringUtils = require('dw/util/StringUtils');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');

/**
 {
  "currencyCode": "AED",
  "listPriceDefault": "",
  "salePriceBookId": ""
  "prices": [
    {
      "vpn": "5415274",
      "srp": "379",
      "rsp": "350"
    }
  ]
}
 */

/**
 * Prepares SFCC Price Book File for Import
 * @param {string} directory directory to generate new XML file
 * @param {string} priceBookId price book id
 * @param {dw.util.HashMap} records  prices
 * @param {string} currencyCode currencyCode for price book
 */
function generateXmlFile(directory, priceBookId, records, currencyCode) {
    var FileWriter = require('dw/io/FileWriter');
    var XMLStreamWriter = require('dw/io/XMLStreamWriter');
    var namespace = 'http://www.demandware.com/xml/impex/pricebook/2006-10-31';
    var fileName = StringUtils.format('{0}-{1}.xml', priceBookId, new Date().getTime().toString());
    var priceBookFile = new File(
        // directory + fileName
        StringUtils.format('{0}{1}', directory, fileName)
    );

    if (priceBookFile.exists()) {
        priceBookFile.remove();
    }

    var priceBookFileWriter = new FileWriter(priceBookFile);
    var xsw = new XMLStreamWriter(priceBookFileWriter);
    xsw.writeStartDocument('UTF-8', '1.0');
    xsw.writeStartElement('pricebooks');
    xsw.writeNamespace('xmlns', namespace);
    /* eslint-disable */
        xsw.writeStartElement('pricebook');
            // price book header
            xsw.writeStartElement('header');
                xsw.writeAttribute('pricebook-id', priceBookId); // pricebook id id
                xsw.writeStartElement('currency');
                    xsw.writeCharacters(currencyCode.toUpperCase());
                xsw.writeEndElement();
            xsw.writeEndElement();

            // records
            xsw.writeStartElement('price-tables');
                // iterate records
                var recordItr = records.entrySet().iterator();
                while (recordItr.hasNext()) {
                    var priceRecord = recordItr.next();
                    xsw.writeStartElement('price-table');
                        xsw.writeAttribute('product-id', priceRecord.key);
                        xsw.writeStartElement('amount');
                            xsw.writeAttribute('quantity', 1);
                            xsw.writeCharacters(priceRecord.value);
                        xsw.writeEndElement();
                    xsw.writeEndElement();
                }
            xsw.writeEndElement();
        xsw.writeEndElement();
    xsw.writeEndElement();
    xsw.writeEndDocument();
    xsw.close();
    priceBookFileWriter.close();
}

/**
 * Reads JSON file and extracts currency based list & sale prices of products.
 * @param {string} directory directory to generate new XML file
 * @param {dw.object.CustomObject} customObject object to be read
 */
function processObjects(directory, customObject) {
    var priceFileContent = JSON.parse(customObject.custom.value);
    if (!('listPriceBookId' in customObject.custom) || !('salePriceBookId' in customObject.custom)) {
        throw Error(StringUtils.format('Process is halted, please define list & sale price book ids from Custom Prefs > Storefront Configs: {0}', customObject.custom.key));
    }
    if (!('prices' in priceFileContent) || !priceFileContent.prices.length) {
        Transaction.wrap(function () {
            customObject.custom.isProcessed = true;
        });
        return;
    }
    var HashMap = require('dw/util/HashMap');
    var listPrices = new HashMap();
    var salePrices = new HashMap();
    priceFileContent.prices.forEach(function (priceObj) {
        if ((!('vpn' in priceObj) || !priceObj.vpn) && (!('SKU_ID' in priceObj) || !priceObj.SKU_ID)) {
            return;
        }
        var productId = priceObj.vpn || priceObj.SKU_ID;
        if ('rsp' in priceObj && priceObj.rsp) { // rsp -> list price
            listPrices.put(productId, priceObj.rsp);
        }
        if ('srp' in priceObj && priceObj.srp) {
            salePrices.put(productId, priceObj.srp);
        }
    });

    if (listPrices.length) {
        generateXmlFile(directory, customObject.custom.listPriceBookId, listPrices, priceFileContent.currencyCode);
    }
    if (salePrices.length) {
        generateXmlFile(directory, customObject.custom.salePriceBookId, salePrices, priceFileContent.currencyCode);
    }
    Transaction.wrap(function () {
        customObject.custom.isProcessed = true;
    });
}


/**
 * Generates SFCC Standart Price Book File with given arguments
 * @param {dw.util.HashMap} args job arguments
 * @returns {dw.system.Status} status of process
 */
function execute(args) {
    var error = new Status(Status.ERROR);
    var ok = new Status(Status.OK);
    var dirPath = StringUtils.format('{0}{1}src{1}speedbus{1}pricebook{1}', File.IMPEX, File.SEPARATOR);
    var directoryToProcess = new File(dirPath);

    if (!directoryToProcess.exists()) {
        logger.info('\n\n{0} is not exists, created directories', dirPath);
        directoryToProcess.mkdirs();
        return ok;
    }

    if (!directoryToProcess.isDirectory()) {
        logger.error('\n\nExpected {0} to be a directory, but it is not!', dirPath);
        return error;
    }

    var priceObjects = CustomObjectMgr.queryCustomObjects('SpeedBusPrice', 'NOT(custom.isProcessed = true)', 'creationDate asc');
   
    while(priceObjects.hasNext()) {
        try {
            /*
            * we can have multiple json files at the same time, so when we have multiple price books last one always wins.
            */
            var priceObj = priceObjects.next();
            processObjects(dirPath, priceObj);
        } catch (e) {
            logger.error('\n\nSomething went wrong while generating XML file from {0}, ending process as ERROR, exception message : {0}', priceObj.custom.key, e.message);
            continue;
        }
    }
    return ok;
}

module.exports = {
    execute: execute
};
