'use strict';
/* global request */
var logger = require('dw/system/Logger').getLogger('speedbus');

/**
 * Pulls primary payment method from payment instrument
 * @param {dw.order.Order} lineItemCtnr Order
 * @returns {string} payment Method
 */
function getOrderPrimaryPaymentMethod(lineItemCtnr) {
    var collections = require('*/cartridge/scripts/util/collections');
    var paymentMethod = null;
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

    collections.forEach(lineItemCtnr.getPaymentInstruments(), function (paymentInstrument) {
        switch (paymentInstrument.paymentMethod) {
            case PaymentInstrument.METHOD_CREDIT_CARD:
                paymentMethod = 'CREDIT_CARD';
                break;
            case PaymentInstrument.METHOD_DW_APPLE_PAY:
                paymentMethod = 'DW_APPLE_PAY';
                break;
            case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
                paymentMethod = 'GIFT_CERTIFICATE';
                break;
            case paymentMethodHelpers.getCODPaymentMethodId():
                paymentMethod = 'COD';
                break;
            case paymentMethodHelpers.getPrePaidPaymentMethodId():
                paymentMethod = 'PREPAID';
                break;
            case paymentMethodHelpers.getKnetPaymentMethodId():
                paymentMethod = 'KNET';
                break;
            default:
                paymentMethod = null;
                break;
        }
        if (paymentMethod) {
            return;
        }
    });
    return paymentMethod;
}
/**
 * calls hook based on lineItemCtnr
 * @param {dw.order.Order} lineItemCtnr Order
 * @returns {dw.service.ServiceResult} service result
 */
function callHook(lineItemCtnr) {
    var HookMgr = require('dw/system/HookMgr');
    var Order = require('dw/order/Order');
    var hookId = null;
    var paymentMethod = getOrderPrimaryPaymentMethod(lineItemCtnr);

    // early quit if payment method is not found!
    if (!paymentMethod) {
        return {
            success: false,
            message: 'Payment method is not found for the order'
        };
    }
    if (paymentMethod === 'COD') {
        if (lineItemCtnr.getConfirmationStatus().value === Order.CONFIRMATION_STATUS_NOTCONFIRMED &&
            lineItemCtnr.getStatus().value !== Order.ORDER_STATUS_CANCELLED) {
            // LAYINT for COD Orders
            // Order is placed but not confirmed && not cancelled
            hookId = 'speedbus.order.export';
        } else if (lineItemCtnr.getConfirmationStatus().value === Order.CONFIRMATION_STATUS_CONFIRMED &&
                    lineItemCtnr.getShippingStatus().value !== Order.SHIPPING_STATUS_SHIPPED &&
                    lineItemCtnr.getStatus().value !== Order.ORDER_STATUS_CANCELLED) {
            /* LAYCMP for COD Orders
            * Order is confirmed, not shipped & not cancelled to proceed with shipment
            */
            hookId = 'speedbus.order.complete';
        } else if (lineItemCtnr.getConfirmationStatus().value === Order.CONFIRMATION_STATUS_NOTCONFIRMED &&
                    lineItemCtnr.getStatus().value === Order.ORDER_STATUS_CANCELLED) {
            // LAYDEL for COD Orders
            // not confirmed, cancelled COD orders will be sent with LAYDEL
            hookId = 'speedbus.order.cancel';
        }
    } else if (lineItemCtnr.getConfirmationStatus().value === Order.CONFIRMATION_STATUS_CONFIRMED &&
                lineItemCtnr.getShippingStatus().value !== Order.SHIPPING_STATUS_SHIPPED &&
                lineItemCtnr.getStatus().value !== Order.ORDER_STATUS_CANCELLED) {
        hookId = 'speedbus.order.complete';
    }

    if (lineItemCtnr.getShippingStatus().value === Order.SHIPPING_STATUS_SHIPPED &&
        lineItemCtnr.getPaymentStatus().value === Order.PAYMENT_STATUS_PAID &&
        lineItemCtnr.getStatus().value !== Order.ORDER_STATUS_CANCELLED &&
        lineItemCtnr.getStatus().value !== Order.ORDER_STATUS_COMPLETED) {
        hookId = 'speedbus.order.sale';
    } else if (lineItemCtnr.getConfirmationStatus().value === Order.CONFIRMATION_STATUS_CONFIRMED &&
                lineItemCtnr.getShippingStatus().value === Order.SHIPPING_STATUS_SHIPPED &&
                lineItemCtnr.getStatus().value === Order.ORDER_STATUS_CANCELLED) { // already confirmed (exported before) and cancelled orders will be sent to SpeedBus as RETURN
        hookId = 'speedbus.order.return';
    }

    if (hookId) {
        return HookMgr.callHook(hookId, 'execute', lineItemCtnr);
    }

    return {
        success: false,
        message: 'Hook is not found for the order'
    };
}

/**
 * Export Orders to SpeedBus
 * @returns {dw.system.Status} Status
 */
function execute() {
    var OrderMgr = require('dw/order/OrderMgr');
    var HookMgr = require('dw/system/HookMgr');
    var Status = require('dw/system/Status');
    var Order = require('dw/order/Order');
    var Transaction = require('dw/system/Transaction');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var allowedLocales = require('dw/system/Site').getCurrent().allowedLocales.toArray();

    // Latin numeric system is not working for ar locales, although it is set properly in BM. Temporary fix to set english locale in job.
    var reqLocale;
    for (var i = 0; i < allowedLocales.length; i++) {
        if (allowedLocales[i].indexOf('en') !== -1) {
            reqLocale = allowedLocales[i];
            break;
        }
    }
    request.setLocale(reqLocale);

    if (!HookMgr.hasHook('speedbus.order.export')) {
        logger.error('speedbus.order.export hook is not defined');
        return new Status(Status.ERROR);
    }
    var orders = OrderMgr.searchOrders('(status = {0} OR status = {1} OR status = {2}) AND exportStatus = {3}',
        null,
        Order.ORDER_STATUS_NEW,
        Order.ORDER_STATUS_OPEN,
        Order.ORDER_STATUS_CANCELLED,
        Order.EXPORT_STATUS_READY);
    if (!orders.hasNext()) {
        logger.info('No orders are found to export in this iteration');
        return new Status(Status.OK);
    }
    while (orders.hasNext()) {
        var order = orders.next();
        try {
            var serviceStatus = callHook(order);
            if (!serviceStatus.success) {
                logger.error('While exporting order {0}, an error occured {1}', order.orderNo, serviceStatus.message);
                if (order.getExportStatus().value === Order.EXPORT_STATUS_READY) {
                    Transaction.wrap(function () { // eslint-disable-line
                        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_EXPORTED], function () {
                            // if there is no hook for this order
                            order.setExportStatus(Order.EXPORT_STATUS_EXPORTED);
                        });
                    });
                }
            }
        } catch (e) {
            logger.error('While exporting order {0}, an error occured {1}', order.orderNo, e.message);
            continue;
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
