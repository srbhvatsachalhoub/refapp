/**
 * Handles OCAPI hooks for order patch calls
 */
var Status = require('dw/system/Status');
var speedbusConstants = require('*/cartridge/scripts/util/speedbusConstants');
var statusUpdates = require('*/cartridge/scripts/order/statusUpdates');
/**
 * the beforePATCH hook - called before patch order
 */

/* eslint-disable camelcase */
exports.beforePATCH = function (order, orderInput) {
    // check the parameteres
    if (!order || !orderInput) {
        return new Status(Status.ERROR);
    }

    // check if the requested body has the shipping_status value
    if ((speedbusConstants.shipping.keys.shippingStatus in orderInput)
        && orderInput[speedbusConstants.shipping.keys.shippingStatus]) {
        statusUpdates.updateShippingStatus(order, orderInput);
    }

    // check if the requested body has the shipments value
    if ((speedbusConstants.shipment.keys.shipments in orderInput)
        && orderInput[speedbusConstants.shipment.keys.shipments]
        && orderInput[speedbusConstants.shipment.keys.shipments].length) {
        statusUpdates.updateShipments(order, orderInput[speedbusConstants.shipment.keys.shipments]);
    }

    // check if the requested body has the additional_order_shipping_status value
    if ((speedbusConstants.shipping.keys.additionalShippingStatus in orderInput)
        && orderInput[speedbusConstants.shipping.keys.additionalShippingStatus]) {
        statusUpdates.updateAdditionalShippingStatus(order, orderInput);
    }

    // check if the requested body has the order status value
    if (((speedbusConstants.order.keys.status in orderInput) && orderInput[speedbusConstants.order.keys.status])
        || ((speedbusConstants.order.keys.externalOrderStatus in orderInput) && orderInput[speedbusConstants.order.keys.externalOrderStatus])) {
        statusUpdates.updateOrderStatus(order, orderInput);
    }

    return new Status(Status.OK);
};
