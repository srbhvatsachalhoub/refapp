'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var StringUtils = require('dw/util/StringUtils');
var baseServiceUrl;
var serviceDef = {
    createRequest: function (svc, args) { // eslint-disable-line consistent-return
        baseServiceUrl = baseServiceUrl || svc.getURL();
        if ('url' in args && args.url) {
            svc.setURL(args.url);
        } else if ('path' in args && args.path) {
            var svcUrl = StringUtils.format('{0}{1}', baseServiceUrl, args.path);
            svc.setURL(svcUrl);
        }
        var requestMethod = 'GET';
        if (('method' in args) && args.method) {
            requestMethod = args.method;
        }
        svc.setRequestMethod(requestMethod);

        if ('headers' in args && args.headers) {
            Object.keys(args.headers).forEach(function (prop) {
                svc.addHeader(prop, args.headers[prop]);
            });
        }
        if (requestMethod !== 'GET' && (!('headers' in args) || !args.headers || !('Content-Type' in args.headers) || !args.headers['Content-Type'])) {
            svc.addHeader('Content-Type', 'application/json');
        }
        var body = '';
        if ('body' in args) {
            if (typeof args.body === 'object') {
                body = JSON.stringify(args.body);
            } else {
                body = args.body;
            }
        }

        return body;
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    },
    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: JSON.stringify({})
        };
    }
};


module.exports = {
    OCAPI: LocalServiceRegistry.createService('sfcc.internal.api', serviceDef)
};

