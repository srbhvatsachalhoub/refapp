'use strict';
/* global request */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var serviceImpl = LocalServiceRegistry.createService('speedbus.newsletter', {
    createRequest: function (svc, payload) { // eslint-disable-line consistent-return
        var logger = require('dw/system/Logger').getLogger('speedbus');
        var currentSite = require('dw/system/Site').getCurrent();
        var speedBusApiKey = currentSite.getCustomPreferenceValue('speedBusApiKey') || svc.getConfiguration().getCredential().custom.speedBusApiKey;
        svc.addHeader('x-api-key', speedBusApiKey);
        svc.addHeader('Content-Type', 'application/json');
        var speedBusStoreIdJson = currentSite.getCustomPreferenceValue('speedBusStoreId');
        var speedBusStoreIds;
        try {
            speedBusStoreIds = speedBusStoreIdJson ? JSON.parse(speedBusStoreIdJson) : {};
        } catch (e) {
            logger.error('While parsing speedBusStoreId configuration, an unexpected error raised {0}', e.message);
            // let it fail
        }
        var storeId;
        var country;
        if (!!payload && !!payload.request && 'country' in payload.request && !!payload.request.country) {
            country = payload.request.country;
        } else if (request.locale) {
            var Locale = require('dw/util/Locale');
            var loc = Locale.getLocale(request.locale);
            country = loc.getCountry();
        }

        if (country && country in speedBusStoreIds && !!speedBusStoreIds[country]) {
            storeId = speedBusStoreIds[country];
        }

        if (!storeId) {
            logger.warn('SpeedBus store ID is not defined for Country {0}, please define it in SpeedBus custom preferences, will sent null in this request', country);
        }

        svc.addHeader('store-id', storeId);

        if (payload) {
            return JSON.stringify(payload);
        }
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    },
    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: JSON.stringify({
                message: 'customer is already subscribed'
            })
        };
    }
});


module.exports = serviceImpl;

