'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var serviceImpl = LocalServiceRegistry.createService('speedbus.export', {
    createRequest: function (svc, payload) { // eslint-disable-line consistent-return
        var currentSite = require('dw/system/Site').getCurrent();
        var speedBusApiKey = currentSite.getCustomPreferenceValue('speedBusApiKey') || svc.getConfiguration().getCredential().custom.speedBusApiKey;
        svc.addHeader('x-api-key', speedBusApiKey);
        svc.addHeader('Content-Type', 'application/json');
        svc.setRequestMethod(payload.method);
        if (payload) {
            return JSON.stringify(payload.body);
        }
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    },
    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: JSON.stringify({
                Response: {
                    status: 'S'
                }
            })
        };
    }
});


module.exports = serviceImpl;

