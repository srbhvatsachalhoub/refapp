'use strict';
var StringUtils = require('dw/util/StringUtils');
var currentSite = require('dw/system/Site').getCurrent();
var sfccInternalService = require('*/cartridge/scripts/services/sfccInternalService');
var API_VERSION = currentSite.getCustomPreferenceValue('speedBusOcapiVersion') || 'v19_10';
/**
 * Gets user information.
 * @param {string} token bearer token
 * @returns {Object} user object or null
 */
function getOCAPIUser(token) {
    if (!token) {
        return null;
    }
    var requestPayload = {
        url: StringUtils.format('https://{0}/s/-/dw/data/{1}/users/this', currentSite.getHttpsHostName(), API_VERSION),
        headers: {
            Authorization: token
        }
    };
    var serviceResponse = sfccInternalService.OCAPI.call(requestPayload);
    if (serviceResponse.isOk()) {
        return serviceResponse.object;
    }
    return null;
}
/**
 * Returns true or false based on OCAPI user/this call.
 * @param {string} token bearer token
 * @returns {boolean} validity of given token
 */
function isOCAPITokenValid(token) {
    return getOCAPIUser(token) !== null;
}

module.exports = {
    isOCAPITokenValid: isOCAPITokenValid
};
