'use strict';

var Transaction = require('dw/system/Transaction');
var Order = require('dw/order/Order');
var HookMgr = require('dw/system/HookMgr');
var StringUtils = require('dw/util/StringUtils');
var speedbusConstants = require('*/cartridge/scripts/util/speedbusConstants');
var logger = require('dw/system/Logger').getLogger('speedbus.orderstatushelpers');
var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
var Site = require('dw/system/Site');
var Resource = require('dw/web/Resource');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

/**
 * Sends shipping e-mail
 * @param {dw.order.Order} order order to send shipping confirmation e-mail
 */
function sendShippingEmail(order) {
    if (emailHelpers.isMCOrderEmailsDisabled()) {
        return;
    }
    var orderObject = { Order: order };
    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msgf('subject.order.shipped.email', 'order', null, order.orderNo),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.orderShipped
    };
    emailHelpers.sendEmail(emailObj, 'order/shipmentEmail', orderObject);

    var invoiceEmailRecipients = Array.prototype.slice.call(Site.current.getCustomPreferenceValue('invoiceEmailRecipients'));
    if (invoiceEmailRecipients && invoiceEmailRecipients.length) {
        invoiceEmailRecipients.forEach(function (invoiceRecipient) {
            emailObj.to = invoiceRecipient;
            emailHelpers.sendEmail(emailObj, 'order/shipmentEmail', orderObject);
        });
    }
}

/**
 * Sends order cancelled e-mail
 * @param {dw.order.Order} order order to send cancellation e-mail
 */
function sendCancelledEmail(order) {
    if (emailHelpers.isMCOrderEmailsDisabled()) {
        return;
    }
    var orderObject = { Order: order };
    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msgf('subject.order.cancelled.email', 'order', null, order.orderNo),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.orderCancelled
    };

    emailHelpers.sendEmail(emailObj, 'order/cancelledEmail', orderObject);
}

/**
 * Sends a confirmation to the current user
 * @param {dw.order.Order} order - The current user's order
 * @param {string} locale - the current request's locale id
 * @returns {void}
 */
function sendConfirmationEmail(order) {
    if (emailHelpers.isMCOrderEmailsDisabled()) {
        return;
    }
    var orderObject = { Order: order };

    var emailObj = {
        to: order.customerEmail,
        subject: Resource.msg('subject.order.confirmation.email', 'order', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.orderConfirmation
    };

    emailHelpers.sendEmail(emailObj, 'checkout/confirmation/confirmationEmail', orderObject);
}

/**
 * Sets Order's shipping status as SHIPPED
 * @param {dw.order.Order} order - The current order
 * @returns {boolean} true if success, false otherwise
 */
function setShippingStatusAsShipped(order) {
    if (order.shippingStatus.value === Order.SHIPPING_STATUS_SHIPPED) {
        // order shipping status was set to shipped already
        logger.warn('Order shipping status was set to shipped already, orderNo: {0}', order.orderNo);
        return false;
    }
    Transaction.wrap(function () {
        hooksHelper('app.order.status.setShippingStatus', 'setShippingStatus', [order, Order.SHIPPING_STATUS_SHIPPED], function () {
            order.setShippingStatus(Order.SHIPPING_STATUS_SHIPPED);
        });
    });

    return true;
}

/**
 * Sets Order's shipping status as NOT_SHIPPED
 * @param {dw.order.Order} order - The current order
 * @returns {boolean} true if success, false otherwise
 */
function setShippingStatusAsNotShipped(order) {
    if (order.shippingStatus.value === Order.SHIPPING_STATUS_NOTSHIPPED) {
        // order shipping status is not shipped already
        logger.warn('Order shipping status is not shipped already, orderNo: {0}', order.orderNo);
        return false;
    }

    Transaction.wrap(function () {
        hooksHelper('app.order.status.setShippingStatus', 'setShippingStatus', [order, Order.SHIPPING_STATUS_NOTSHIPPED], function () {
            order.setShippingStatus(Order.SHIPPING_STATUS_NOTSHIPPED);
        });
    });

    return true;
}

/**
 * Sets Order's export status as READY
 * @param {dw.order.Order} order - The current order
 * @returns {boolean} true if success, false otherwise
 */
function setExportStatusAsReady(order) {
    if (order.exportStatus.value === Order.EXPORT_STATUS_READY) {
        // order export status was set to ready already
        logger.warn('Order export status was set to ready already, orderNo: {0}', order.orderNo);
        return false;
    }

    Transaction.wrap(function () {
        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_READY], function () {
            order.setExportStatus(Order.EXPORT_STATUS_READY);
        });
    });

    return true;
}

/**
 * Sets Order's additional shipping status
 * @param {dw.order.Order} order - The current order
 * @param {string} additionalShippingStatus - Additional shipping status that speedbus provided
 * @returns {boolean} true if success, false otherwise
 */
function setAdditionalShippingStatus(order, additionalShippingStatus) {
    if (order.custom.additionalOrderShipmentStatus.value === additionalShippingStatus) {
        // order additional shipping status is set already
        logger.warn('Order additional shipping status is {0} already, orderNo: {1}', additionalShippingStatus, order.orderNo);
        return false;
    }

    Transaction.wrap(function () {
        hooksHelper('app.order.status.setAdditionalShippingStatus', 'setAdditionalShippingStatus', [order, additionalShippingStatus], function () {
            order.custom.additionalOrderShipmentStatus = additionalShippingStatus; // eslint-disable-line no-param-reassign
        });
    });

    return true;
}

/**
 * Sets Marketing Cloud status of the order
 * @param {dw.order.Order} order - The current order
 * @param {string} mcStatus - Marketing Cloud Status (shipped, confirmed, cancelled, null)
 */
function setMarketingCloudStatus(order, mcStatus) {
    if (order.custom.smcStatus === mcStatus) {
        return;
    }

    Transaction.wrap(function () {
        order.custom.smcStatus = mcStatus; // eslint-disable-line no-param-reassign
    });
}

/**
 * Credit Card Capture
 * Triggers the capture request to relevant PSP
 * if the order has CreditCard payment
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to capture
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @returns {boolean} true if success, false otherwise
 */
function creditCardCapture(order, paymentInstrument, paymentProcessor) {
    var captureResult;
    var hookId = StringUtils.format('app.payment.processor.{0}', paymentProcessor.ID.toLowerCase());
    if (HookMgr.hasHook(hookId)) {
        captureResult = HookMgr.callHook(
            hookId,
            'Capture',
            order,
            paymentInstrument,
            paymentProcessor
        );

        if (captureResult.error) {
            logger.error(captureResult.message);
            return false;
        }

        return true;
    }

    logger.error('Order: {0} cant be captured, processor: {1} doesnt have hook: {2}', order.orderNo, paymentProcessor.ID, hookId);
    return false;
}

/**
 * Credit Card Checks Payment last status
 * Checks the status of the payment
 * Triggers the CHECK_STATUS request to relevant PSP
 * if the order has CreditCard payment
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be checked
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @returns {string} the status of the payment, Authorized or Captured, if service returned none then set status from order payment status
 */
function creditCardGetPaymentStatus(order, paymentInstrument, paymentProcessor) {
    var checkStatusResult = {
        error: true
    };
    var hookId = StringUtils.format('app.payment.processor.{0}', paymentProcessor.ID.toLowerCase());
    if (HookMgr.hasHook(hookId)) {
        checkStatusResult = HookMgr.callHook(
            hookId,
            'CheckStatus',
            order,
            paymentInstrument,
            paymentProcessor
        );

        if (checkStatusResult.success) {
            return checkStatusResult.status;
        }
    }

    return order.paymentStatus.value === Order.PAYMENT_STATUS_PAID
        ? speedbusConstants.pspPaymentStatus.captured
        : speedbusConstants.pspPaymentStatus.authorized;
}

/**
 * Credit Card Void or Refund based on current payment status on PSP
 * Checks the status of the payment
 * Triggers the void/refund request to relevant PSP
 * if the order has CreditCard payment
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to void/refund
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @returns {boolean} true if success, false otherwise
 */
function creditCardVoidOrRefund(order, paymentInstrument, paymentProcessor) {
    var hookId = StringUtils.format('app.payment.processor.{0}', paymentProcessor.ID.toLowerCase());
    if (!HookMgr.hasHook(hookId)) {
        logger.error('Order: {0} cant be void/refund, processor: {1} doesnt have hook: {2}', order.orderNo, paymentProcessor.ID, hookId);
        return false;
    }

    var pspStatus = creditCardGetPaymentStatus(order, paymentInstrument, paymentProcessor);
    switch (pspStatus) {
        case speedbusConstants.pspPaymentStatus.authorized:
            var voidResult = HookMgr.callHook(
                hookId,
                'Cancel',
                order,
                paymentInstrument,
                paymentProcessor
            );

            if (voidResult.error) {
                logger.error(voidResult.message);
                return false;
            }
            return !!voidResult.success;
        case speedbusConstants.pspPaymentStatus.captured:
            var refundResult = HookMgr.callHook(
                hookId,
                'Refund',
                order,
                paymentInstrument,
                paymentProcessor
            );

            if (refundResult.error) {
                logger.error(refundResult.message);
                return false;
            }

            return !!refundResult.success;
        default: break;
    }

    return false;
}

/**
 * Sets Order's payment status as PAID
 * Request to PSP to make authorized payment as captured
 * @param {dw.order.Order} order - The current order
 * @param {string} status - Shipping or additional shipping status that speedbus provided
 * @returns {boolean} true if success, false otherwise
 */
function setPaymentStatusAsPaid(order, status) {
    if (order.paymentStatus.value === Order.PAYMENT_STATUS_PAID) {
        // order payment status was set to paid already
        return false;
    }

    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var PaymentMgr = require('dw/order/PaymentMgr');

    var paymentInstruments = order.getPaymentInstruments();
    if (paymentInstruments.empty) {
        logger.error('Order: {0} has no payment payment instruments', order.orderNo);
        return false;
    }

    var paymentInstrumentsIterator = paymentInstruments.iterator();
    var creditCardPayment;
    var giftCertificatePayment;
    var codPayment;
    while (paymentInstrumentsIterator.hasNext()) {
        var paymentInstrument = paymentInstrumentsIterator.next();
        var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).paymentProcessor;
        if (!paymentProcessor) {
            logger.error('Order: {0} paymentInstrument has no payment payment processor', order.orderNo);
            continue;
        }

        switch (paymentInstrument.paymentMethod) {
            case PaymentInstrument.METHOD_CREDIT_CARD:
            case PaymentInstrument.METHOD_DW_APPLE_PAY:
            case 'KNET':
                creditCardCapture(order, paymentInstrument, paymentProcessor);
                creditCardPayment = true;
                break;
            case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
                giftCertificatePayment = true;
                break;
            case 'COD':
                codPayment = true;
                break;
            default:
                logger.error('Order: {0} payment method is not implemented', order.orderNo);
                break;
        }
    }

    // check if only paid with gift certificate
    // OR
    // paid with COD and additionalShippingStatus is shipped
    // then set order as PAID then
    if ((giftCertificatePayment && !creditCardPayment && !codPayment)
        || (codPayment && status === speedbusConstants.shipping.values.shipped)) {
        Transaction.wrap(function () {
            hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
            });
        });
    }

    return true;
}

/**
 * Sets Order's status
 * Possible values: cancel, refund
 * Request to PSP to make payment as cancel/void or refund
 * @param {dw.order.Order} order - The current order
 * @param {string} orderStatus - Order status that speedbus provided
 * @param {string} cancelDescription - Order Cancel Description (optional)
 * @returns {boolean} true if success, false otherwise
 */
function setOrderStatus(order, orderStatus, cancelDescription) {
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var OrderMgr = require('dw/order/OrderMgr');

    var paymentInstruments = order.getPaymentInstruments();
    if (paymentInstruments.empty) {
        logger.error('Order: {0} has no payment payment instruments', order.orderNo);
        return false;
    }

    var paymentInstrumentsIterator = paymentInstruments.iterator();
    var creditCardPayment;
    var creditCardSucceeded;
    while (paymentInstrumentsIterator.hasNext()) {
        var paymentInstrument = paymentInstrumentsIterator.next();
        var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod).paymentProcessor;
        if (!paymentProcessor) {
            logger.error('Order: {0} paymentInstrument has no payment payment processor', order.orderNo);
            continue;
        }

        switch (paymentInstrument.paymentMethod) {
            case PaymentInstrument.METHOD_CREDIT_CARD:
                if (orderStatus === speedbusConstants.order.values.cancelled
                    || orderStatus === speedbusConstants.order.values.refunded) {
                    creditCardSucceeded = creditCardVoidOrRefund(order, paymentInstrument, paymentProcessor);
                }
                creditCardPayment = true;
                break;
            case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
                break;
            case 'COD':
                break;
            default:
                logger.error('Order: {0} payment method is not implemented', order.orderNo);
                break;
        }
    }

    if ((orderStatus === speedbusConstants.order.values.cancelled || orderStatus === speedbusConstants.order.values.refunded)
        && order.status.value !== Order.ORDER_STATUS_CANCELLED) {
        // cancel orders if status was not cancelled before
        // and if it is creditcard payment and void/refund succeeded
        // other payment methods will be cancelled as well
        if (!creditCardPayment || creditCardSucceeded) {
            Transaction.wrap(function () {
                hooksHelper('app.order.status.cancelOrder', 'cancelOrder', [order, null, cancelDescription], function () {
                    if (cancelDescription) {
                        order.setCancelDescription(cancelDescription);
                    }
                    OrderMgr.cancelOrder(order);
                });
            });
        }
    }

    return true;
}

module.exports = {
    setShippingStatusAsShipped: setShippingStatusAsShipped,
    setShippingStatusAsNotShipped: setShippingStatusAsNotShipped,
    setAdditionalShippingStatus: setAdditionalShippingStatus,
    setPaymentStatusAsPaid: setPaymentStatusAsPaid,
    setOrderStatus: setOrderStatus,
    setExportStatusAsReady: setExportStatusAsReady,
    sendShippingEmail: sendShippingEmail,
    sendCancelledEmail: sendCancelledEmail,
    sendConfirmationEmail: sendConfirmationEmail,
    setMarketingCloudStatus: setMarketingCloudStatus
};
