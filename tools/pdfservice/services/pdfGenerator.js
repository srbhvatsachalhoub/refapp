const PathHelper = require('path');
const Fs = require('fs-extra');
const PdfMerger = require('./pdfMerger');

const app_data = 'app_data';

/**
 * Creates pdf files from given objects and put's them in directories as requested
 *
 * @param {Object} request contains array of invoice request objects and a flag to merged pdfs
 *
 * @returns {string} path of working directory
 */
exports.generateAllPdfs = async (request) => {
    console.log('request: ');
    console.log(request);

    const browser = await startBrowser();
    const page = await browser.newPage();

    const workingDirectoryPath = await createWorkingDirectory();

    // TODO: improve this loop to work in parallel to improve performance
    for (const invoiceRequest of request.invoiceRequests) {
        await generatePdfFromInvoiceRequest(page, invoiceRequest, workingDirectoryPath);
    }

    await browser.close();

    if (request.mergedPdfs) {
        await mergePdfsForSameInvoice(workingDirectoryPath);
    }

    return workingDirectoryPath;
}

const startBrowser = () => {
    const puppeteer = require('puppeteer');
    return puppeteer.launch({
        headless: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
}

const createWorkingDirectory = async () => {
    console.log('creating working directory');
    const randomFolderNameGenerator = require('../services/randomFolderNameGenerator');
    const workingDirectoryName = randomFolderNameGenerator.getRandomFolderName(app_data);
    const workingDirectoryPath = PathHelper.join(app_data, workingDirectoryName);

    // create working directory
    if (!await Fs.exists(app_data)) {
        await Fs.mkdir(app_data);
    }

    await Fs.mkdir(workingDirectoryPath);
    console.log('workingDirectory created: ' + workingDirectoryPath);
    return workingDirectoryPath;
}

const generatePdfFromInvoiceRequest = async (browserPage, invoiceRequest, workingDirectoryPath) => {
    console.group('pdf invoice request');
    if (!invoiceRequest.url || invoiceRequest.url.length <= 0 ||
        !invoiceRequest.fileName || invoiceRequest.fileName.length <= 0 ||
        !invoiceRequest.directoryName || invoiceRequest.directoryName.length <= 0) {
        console.log('invoiceRequest has insufficient information');
        return;
    }
    try {
        console.log('navigating to url: ' + invoiceRequest.url);

        await browserPage.authenticate(require('../auth/cred.json').storefront);

        var response = await browserPage.goto(invoiceRequest.url);

        if (response.status() != 200) {
            console.log('navigation could not be completed, statusCode: ' + response.status());
            return;
        }
        var orderFolderPath = PathHelper.join(workingDirectoryPath, invoiceRequest.directoryName);
        console.log('navigation completed, orderFolderPath: ' + orderFolderPath);

        var orderDirExists = await Fs.exists(orderFolderPath);
        if (!orderDirExists) {
            await Fs.mkdir(orderFolderPath);
            console.log('orderFolderPath did not exist. Created.');
        }

        const pdfPath = PathHelper.join(orderFolderPath, invoiceRequest.fileName);
        console.log('pdf will be created at: ' + pdfPath);
        await browserPage.pdf({ path: pdfPath, format: 'A4' });
        console.log('pdf created');
    } catch (err) {
        console.log(err);
    }
    console.groupEnd();
}

const mergePdfsForSameInvoice = async (workingDirectoryPath) => {
    for (const directoryName of await Fs.readdir(workingDirectoryPath)) {
        var pdfPathList = [];
        for (const fileName of await Fs.readdir(PathHelper.join(workingDirectoryPath, directoryName))) {
            pdfPathList.push(PathHelper.join(workingDirectoryPath, directoryName, fileName));
        }
        await PdfMerger.combinePDFsAsync(pdfPathList, PathHelper.join(workingDirectoryPath, directoryName, directoryName + '.pdf'));
    }
}